package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.Script;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoAPI.CargoItemType;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.CharacterCreationData;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.rulecmd.DS_NGCAddFactionOptions.FactionChoice;
import com.fs.starfarer.api.impl.campaign.rulecmd.DS_NGCAddShipOptions.ShipTypeChoice;
import com.fs.starfarer.api.impl.campaign.tutorial.TutorialMissionEvent;
import com.fs.starfarer.api.util.Misc.Token;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * SSP_AddStartingScript
 */
public class DS_AddStartingScript extends BaseCommandPlugin {

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params,
                           Map<String, MemoryAPI> memoryMap) {
        if (dialog == null) {
            return false;
        }

        CharacterCreationData data = (CharacterCreationData) memoryMap.get(MemKeys.LOCAL).get("$characterData");
        final MemoryAPI memory = memoryMap.get(MemKeys.LOCAL);
        data.addScript(new StartScript(memory));
        return true;
    }

    private static class MoveScript implements EveryFrameScript {

        private boolean done = false;
        private final SectorEntityToken entity;
        private final CampaignFleetAPI fleet;

        MoveScript(SectorEntityToken entity, CampaignFleetAPI fleet) {
            this.entity = entity;
            this.fleet = fleet;
        }

        @Override
        public void advance(float amount) {
            fleet.setLocation(entity.getLocation().x, entity.getLocation().y);
            done = true;
        }

        @Override
        public boolean isDone() {
            return done;
        }

        @Override
        public boolean runWhilePaused() {
            return true;
        }
    }

    private static class StartScript implements Script {

        final MemoryAPI memory;

        StartScript(MemoryAPI memory) {
            this.memory = memory;
        }

        @Override
        public void run() {
            CampaignFleetAPI fleet = Global.getSector().getPlayerFleet();
            // add crew, supplies, and fuel
            int crew = 0;
            int supplies = 0;
            for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
                crew += Math.ceil(member.getMinCrew() + (member.getMaxCrew() - member.getMinCrew()) * 0.5f);
                supplies += member.getDeploymentCostSupplies() * 4f;
            }

            CargoAPI cargo = fleet.getCargo();
            cargo.addCrew(crew);
            cargo.addSupplies(Math.min(cargo.getMaxCapacity() * 0.5f, supplies));
            cargo.addFuel(cargo.getMaxFuel() * 0.5f);

            fleet.getFleetData().ensureHasFlagship();

            if (memory.getString("$ngcShipType").contentEquals(ShipTypeChoice.TRADER.name())) {
                float space = cargo.getSpaceLeft() * .9f;
                float premium = Math.min(50, space);
                addRandom(cargo, premium, 10f,
                          Commodities.LUXURY_GOODS,
                          Commodities.RARE_ORE,
                          Commodities.HEAVY_MACHINERY,
                          Commodities.RARE_METALS
                );
                addRandom(cargo, space - premium, 10f,
                          Commodities.DOMESTIC_GOODS,
                          Commodities.FOOD,
                          Commodities.ORGANICS,
                          Commodities.VOLATILES,
                          Commodities.METALS
                );
            }
            if (memory.getString("$ngcShipType").contentEquals(ShipTypeChoice.SMUGGLER.name())) {
                float spacePremium = Math.min(50, cargo.getSpaceLeft() * .9f * 0.5f);
                float space = cargo.getSpaceLeft() * .9f - spacePremium;
                addRandom(cargo, spacePremium, 10f,
                          Commodities.HAND_WEAPONS,
                          Commodities.ORGANS,
                          Commodities.DRUGS
                );
                addRandom(cargo, space, 10f,
                          Commodities.DOMESTIC_GOODS,
                          Commodities.FOOD,
                          Commodities.METALS,
                          Commodities.ORGANICS,
                          Commodities.VOLATILES
                );
            }
            if (memory.getString("$ngcShipType").contentEquals(ShipTypeChoice.SCAVENGER.name())) {
                float space = Math.min(100, cargo.getSpaceLeft() * .9f * 0.5f);
                addRandom(cargo, space, 10f,
                          Commodities.HEAVY_MACHINERY,
                          Commodities.VOLATILES
                );
                cargo.addSupplies((int) (cargo.getSpaceLeft()));
                cargo.addCrew((int) (cargo.getFreeCrewSpace() * 0.5f));
                cargo.addFuel(cargo.getFreeFuelSpace());
            }

            FactionChoice faction = FactionChoice.getFactionChoice(memory.getString("$ngcFaction"));
            int officers;
            switch (memory.getString("$ngcSize")) {
                default:
                    officers = 0;
                    break;
                case "medium":
                    officers = 1;
                    break;
                case "large":
                    officers = 2;
                    break;
            }
            for (; officers > 0; officers--) {
                boolean found = false;
                for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
                    if (!member.isFlagship() && member.getCaptain() != null && member.getCaptain().isDefault()) {
                        PersonAPI officer = OfficerManagerEvent.createOfficer(Global.getSector().getFaction(
                                  faction.factionId), 1, true);
                        officer.setFaction(Global.getSector().getPlayerFaction().getId());
                        member.setCaptain(officer);
                        fleet.getFleetData().addOfficer(officer);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    PersonAPI officer =
                              OfficerManagerEvent.createOfficer(Global.getSector().getPlayerFaction(), 1, true);
                    fleet.getFleetData().addOfficer(officer);
                }
            }

            @SuppressWarnings("unchecked")
            Map<String, Float> relAdjustMap = (Map<String, Float>) memory.get("$ngcRepAdjust");
            for (Entry<String, Float> entry : relAdjustMap.entrySet()) {
                String otherId = entry.getKey();

                FactionChoice otherFaction = FactionChoice.getFactionChoice(otherId);
                if (otherFaction.req.isLoaded()) {
                    float relAdjust = entry.getValue();
                    if (relAdjust > 0f) {
                        Global.getSector().getFaction(otherId).getRelToPlayer().adjustRelationship(relAdjust,
                                                                                                   RepLevel.FRIENDLY);
                    } else {
                        Global.getSector().getFaction(otherId).getRelToPlayer().adjustRelationship(relAdjust,
                                                                                                   RepLevel.HOSTILE);
                    }
                }
            }

            for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
                float max = member.getRepairTracker().getMaxCR();
                member.getRepairTracker().setCR(max);
            }
            fleet.getFleetData().setSyncNeeded();

            TutorialMissionEvent.endGalatiaPortionOfMission();

            LocationAPI location;
            if (faction.starSystemName.contentEquals("hyperspace")) {
                location = Global.getSector().getHyperspace();
            } else {
                location = Global.getSector().getStarSystem(faction.starSystemName);
            }

            SectorEntityToken entityToSpawnAt = null;
            int biggestMarket = 0;
            for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
                if (!market.getFactionId().contentEquals(faction.factionId)) {
                    continue;
                }
                if (location != null && market.getContainingLocation() != location) {
                    continue;
                }

                if (market.getSize() > biggestMarket) {
                    entityToSpawnAt = market.getPrimaryEntity();
                    biggestMarket = market.getSize();
                }
            }
            if (entityToSpawnAt == null && location != null) {
                for (PlanetAPI planet : location.getPlanets()) {
                    if (planet.isStar()) {
                        continue;
                    }
                    entityToSpawnAt = planet;
                    break;
                }
            }
            if (entityToSpawnAt == null) {
                location = Global.getSector().getStarSystem("Corvus");
                if (location != null) {
                    entityToSpawnAt = Global.getSector().getEconomy().getMarket("jangala").getPrimaryEntity();
                } else {
                    // Fine, fuck you!
                    entityToSpawnAt = Global.getSector().getEconomy().getMarketsCopy().get(0).getPrimaryEntity();
                }
            }

            fleet.addScript(new MoveScript(entityToSpawnAt, fleet));
        }

        private void addRandom(CargoAPI cargo, float space, float incr, String... commodities) {
            if (space <= 0) {
                return;
            }

            WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
            for (String id : commodities) {
                picker.add(id);
            }

            float soFar = 0;
            while (soFar < space) {
                String id = picker.pick();
                float quantity = Math.min(space - soFar, incr);
                if (quantity <= 0) {
                    break;
                }
                cargo.addItems(CargoItemType.RESOURCES, id, quantity);
                soFar += quantity;
            }
        }

    }
}
