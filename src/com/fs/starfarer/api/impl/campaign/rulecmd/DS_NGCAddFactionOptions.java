package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.rulecmd.DS_NGCAddShipOptions.ShipTypeChoice;
import com.fs.starfarer.api.util.Misc.Token;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Util.RequiredFaction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * SSP_NGCAddFactionOptions
 */
public class DS_NGCAddFactionOptions extends BaseCommandPlugin {

    static final List<FactionChoice> civilianFactionChoices = new ArrayList<>(20);
    static final List<FactionChoice> corporateFactionChoices = new ArrayList<>(20);
    static final List<FactionChoice> mercenaryFactionChoices = new ArrayList<>(20);
    static final List<FactionChoice> militaryFactionChoices = new ArrayList<>(20);
    static final List<FactionChoice> outlawFactionChoices = new ArrayList<>(20);
    static final List<FactionChoice> outsiderFactionChoices = new ArrayList<>(20);

    public static void generateChoiceLists() {
        militaryFactionChoices.clear();
        civilianFactionChoices.clear();
        corporateFactionChoices.clear();
        mercenaryFactionChoices.clear();
        outlawFactionChoices.clear();
        outsiderFactionChoices.clear();

        militaryFactionChoices.add(FactionChoice.HEGEMONY);
        corporateFactionChoices.add(FactionChoice.TRITACHYON);
        civilianFactionChoices.add(FactionChoice.LUDDIC_CHURCH);
        militaryFactionChoices.add(FactionChoice.DIKTAT);
        civilianFactionChoices.add(FactionChoice.INDEPENDENT);
        outlawFactionChoices.add(FactionChoice.PIRATES);
        outlawFactionChoices.add(FactionChoice.LUDDIC_PATH);
        mercenaryFactionChoices.add(FactionChoice.PERSEAN);
        if (DSModPlugin.blackrockExists) {
            corporateFactionChoices.add(FactionChoice.BLACKROCK);
        }
        if (DSModPlugin.citadelExists) {
            mercenaryFactionChoices.add(FactionChoice.CITADEL);
        }
        if (DSModPlugin.diableExists) {
            corporateFactionChoices.add(FactionChoice.DIABLE);
        }
        if (DSModPlugin.exigencyExists) {
            outsiderFactionChoices.add(FactionChoice.EXIGENCY);
            outlawFactionChoices.add(FactionChoice.AHRIMAN);
        }
        if (DSModPlugin.imperiumExists) {
            militaryFactionChoices.add(FactionChoice.IMPERIUM);
        }
        if (DSModPlugin.junkPiratesExists) {
            outlawFactionChoices.add(FactionChoice.JUNK_PIRATES);
            militaryFactionChoices.add(FactionChoice.PACK);
            civilianFactionChoices.add(FactionChoice.ASP);
        }
        if (DSModPlugin.mayorateExists) {
            mercenaryFactionChoices.add(FactionChoice.MAYORATE);
        }
        if (DSModPlugin.oraExists) {
            civilianFactionChoices.add(FactionChoice.ORA);
        }
        if (DSModPlugin.scyExists) {
            militaryFactionChoices.add(FactionChoice.SCY);
        }
        if (DSModPlugin.shadowyardsExists) {
            civilianFactionChoices.add(FactionChoice.SHADOWYARDS);
        }
        if (DSModPlugin.templarsExists) {
            outsiderFactionChoices.add(FactionChoice.TEMPLARS);
        }
        if (DSModPlugin.tiandongExists) {
            mercenaryFactionChoices.add(FactionChoice.TIANDONG);
        }
    }

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params,
                           Map<String, MemoryAPI> memoryMap) {
        generateChoiceLists();

        String param = params.get(0).getString(memoryMap);
        switch (param) {
            default:
                return false;
            case "group": {
                dialog.getOptionPanel().clearOptions();
                dialog.getOptionPanel().addOption("An honest civilian", "ngcGroupCivilian");
                dialog.getOptionPanel().addOption("A commissioned officer", "ngcGroupMilitary");
                dialog.getOptionPanel().addOption("An employee of a large corporation", "ngcGroupCorporate");
                dialog.getOptionPanel().addOption("A stalwart mercenary", "ngcGroupMercenary");
                dialog.getOptionPanel().addOption("An outlaw, living on the edge", "ngcGroupOutlaw");
                dialog.getOptionPanel().addOption("...none of your business, outsider", "ngcGroupOutsider");
                dialog.getOptionPanel().addOption("Back", "ngcGroupBack");
                if (civilianFactionChoices.isEmpty()) {
                    dialog.getOptionPanel().setEnabled("ngcGroupCivilian", false);
                }
                if (militaryFactionChoices.isEmpty()) {
                    dialog.getOptionPanel().setEnabled("ngcGroupMilitary", false);
                }
                if (corporateFactionChoices.isEmpty()) {
                    dialog.getOptionPanel().setEnabled("ngcGroupCorporate", false);
                }
                if (mercenaryFactionChoices.isEmpty()) {
                    dialog.getOptionPanel().setEnabled("ngcGroupMercenary", false);
                }
                if (outlawFactionChoices.isEmpty()) {
                    dialog.getOptionPanel().setEnabled("ngcGroupOutlaw", false);
                }
                if (outsiderFactionChoices.isEmpty()) {
                    dialog.getOptionPanel().setEnabled("ngcGroupOutsider", false);
                }
                break;
            }
            case "faction": {
                dialog.getOptionPanel().clearOptions();
                List<FactionChoice> factionGroup;

                String group = params.get(1).getString(memoryMap);
                switch (group) {
                    case "civilian":
                        dialog.getTextPanel().addParagraph("More specifically, you have found work...");
                        factionGroup = civilianFactionChoices;
                        break;
                    case "military":
                        dialog.getTextPanel().addParagraph("In particular, you joined...");
                        factionGroup = militaryFactionChoices;
                        break;
                    case "corporate":
                        dialog.getTextPanel().addParagraph("You were hired by...");
                        factionGroup = corporateFactionChoices;
                        break;
                    case "mercenary":
                        dialog.getTextPanel().addParagraph("Generally, you favored...");
                        factionGroup = mercenaryFactionChoices;
                        break;
                    case "outlaw":
                        dialog.getTextPanel().addParagraph("You fell in deep with...");
                        factionGroup = outlawFactionChoices;
                        break;
                    case "outsider":
                        dialog.getTextPanel().addParagraph("You have been living amongst...");
                        factionGroup = outsiderFactionChoices;
                        break;
                    default:
                        return false;
                }
                if (factionGroup != null) {
                    for (FactionChoice factionChoice : factionGroup) {
                        dialog.getOptionPanel().addOption(factionChoice.flavorString, "ngcFaction" +
                                                          factionChoice.factionId);
                    }
                }
                dialog.getOptionPanel().addOption("Back", "ngcFactionBack");
                break;
            }
        }

        return true;
    }

    static enum FactionChoice {

        HEGEMONY(Factions.HEGEMONY, RequiredFaction.NONE, "the Hegemony",
                 "The Hegemony, strong and stable",
                 "Aztlan" /* 8 */, null,
                 Arrays.asList(
                         new RelMapEntry(Factions.HEGEMONY, 0.25f),
                         new RelMapEntry("syndicate_asp", 0.05f),
                         new RelMapEntry(Factions.TRITACHYON, -0.15f),
                         new RelMapEntry(Factions.PERSEAN, -0.15f),
                         new RelMapEntry("blackrock_driveyards", -0.15f),
                         new RelMapEntry("diableavionics", -0.15f),
                         new RelMapEntry("interstellarimperium", -0.25f),
                         new RelMapEntry("junk_pirates", -0.15f),
                         new RelMapEntry("shadow_industry", -0.15f)
                 ),
                 Arrays.asList(ShipTypeChoice.PATROL, ShipTypeChoice.CARRIER, ShipTypeChoice.TRADER)),
        TRITACHYON(Factions.TRITACHYON, RequiredFaction.NONE, "the Tri-Tachyon Corporation",
                   "The Tri-Tachyon Corporation, ruthless and powerful",
                   "Hybrasil" /* 6 */, null,
                   Arrays.asList(
                           new RelMapEntry(Factions.TRITACHYON, 0.25f),
                           new RelMapEntry(Factions.HEGEMONY, -0.15f),
                           new RelMapEntry(Factions.LUDDIC_CHURCH, -0.15f),
                           new RelMapEntry("blackrock_driveyards", -0.15f),
                           new RelMapEntry("diableavionics", -0.2f),
                           new RelMapEntry("junk_pirates", -0.15f)
                   ),
                   Arrays.asList(ShipTypeChoice.PATROL, ShipTypeChoice.CARRIER, ShipTypeChoice.PRIVATEER,
                                 ShipTypeChoice.SMUGGLER)),
        LUDDIC_CHURCH(Factions.LUDDIC_CHURCH, RequiredFaction.NONE, "the Church of Galactic Redemption",
                      "Ferrying passengers and food for the Church of Galactic Redemption",
                      "Eos Exodus" /* 7 */ /* "Canaan" 7 */, null,
                      Arrays.asList(
                              new RelMapEntry(Factions.LUDDIC_CHURCH, 0.25f),
                              new RelMapEntry(Factions.LUDDIC_PATH, 0.25f),
                              new RelMapEntry(Factions.TRITACHYON, -0.15f),
                              new RelMapEntry("blackrock_driveyards", -0.15f),
                              new RelMapEntry("junk_pirates", -0.15f),
                              new RelMapEntry("templars", -0.25f)
                      ),
                      Arrays.asList(ShipTypeChoice.TRADER, ShipTypeChoice.BOUNTY_HUNTER, ShipTypeChoice.PATROL)),
        DIKTAT(Factions.DIKTAT, RequiredFaction.NONE, "the Sindrian Diktat",
               "The Sindrian Diktat, centralized and independent",
               "Askonia" /* 7 */, null,
               Arrays.asList(
                       new RelMapEntry(Factions.DIKTAT, 0.25f),
                       new RelMapEntry(Factions.PERSEAN, 0.15f),
                       new RelMapEntry("syndicate_asp", 0.05f),
                       new RelMapEntry("junk_pirates", -0.15f),
                       new RelMapEntry("shadow_industry", -0.15f)
               ),
               Arrays.asList(ShipTypeChoice.PRIVATEER, ShipTypeChoice.PATROL, ShipTypeChoice.BOUNTY_HUNTER)),
        INDEPENDENT(Factions.INDEPENDENT, RequiredFaction.NONE, "the independents",
                    "Making your way as an independent trader",
                    "Magec" /* 5 */ /* "Eos Exodus" 5 */ /* "Arcadia" 5 */ /* "Thule" 5 */ /* "Westernesse" 5 */ /* "Zagan" 5 */,
                    null,
                    Arrays.asList(
                            new RelMapEntry(Factions.INDEPENDENT, 0.25f),
                            new RelMapEntry("syndicate_asp", 0.05f),
                            new RelMapEntry("interstellarimperium", 0.1f),
                            new RelMapEntry("tiandong", 0.15f)
                    ),
                    Arrays.asList(ShipTypeChoice.TRADER, ShipTypeChoice.SMUGGLER, ShipTypeChoice.BOUNTY_HUNTER,
                                  ShipTypeChoice.PRIVATEER, ShipTypeChoice.SCAVENGER)),
        PERSEAN(Factions.PERSEAN, RequiredFaction.NONE, "the Persean League",
                "Taking mercenary jobs for the Persean League",
                "Thule" /* 7 */ /* "Zagan" 7 */, null,
                Arrays.asList(
                        new RelMapEntry(Factions.PERSEAN, 0.25f),
                        new RelMapEntry(Factions.DIKTAT, 0.15f),
                        new RelMapEntry(Factions.HEGEMONY, -0.15f)
                ),
                Arrays.asList(ShipTypeChoice.BOUNTY_HUNTER, ShipTypeChoice.PATROL, ShipTypeChoice.CARRIER,
                              ShipTypeChoice.PRIVATEER, ShipTypeChoice.SCAVENGER)),
        PIRATES(Factions.PIRATES, RequiredFaction.NONE, "the pirates",
                "Underworld elements and corsairs who call themselves Pirates",
                "Yma" /* 5 */ /* "Askonia" 5 */, null,
                Arrays.asList( // one-third negative impact
                        new RelMapEntry(Factions.PIRATES, 0.5f),
                        new RelMapEntry("junk_pirates", 0.05f),
                        new RelMapEntry(Factions.INDEPENDENT, -0.05f),
                        new RelMapEntry("syndicate_asp", -0.05f),
                        new RelMapEntry("tiandong", -0.05f)
                ),
                Arrays.asList(ShipTypeChoice.RAIDER, ShipTypeChoice.PRIVATEER, ShipTypeChoice.SMUGGLER,
                              ShipTypeChoice.SCAVENGER)),
        LUDDIC_PATH(Factions.LUDDIC_PATH, RequiredFaction.NONE, "the Luddic Path",
                    "Anti-tech extremists who call themselves the Luddic Path",
                    "Kumari Kandam" /* 5 */, null,
                    Arrays.asList( // one-third negative impact
                            new RelMapEntry(Factions.LUDDIC_PATH, 0.5f),
                            new RelMapEntry("blackrock_driveyards", -0.1f),
                            new RelMapEntry("diableavionics", -0.1f),
                            new RelMapEntry("templars", -0.1f),
                            new RelMapEntry("shadow_industry", -0.1f)
                    ),
                    Arrays.asList(ShipTypeChoice.RAIDER, ShipTypeChoice.PRIVATEER)),
        BLACKROCK("blackrock_driveyards", RequiredFaction.BLACKROCK, "the Blackrock Drive Yards",
                  "The mysterious, prestigious Blackrock Drive Yards",
                  "Rama", "brdy_",
                  Arrays.asList(
                          new RelMapEntry("blackrock_driveyards", 0.25f),
                          new RelMapEntry(Factions.HEGEMONY, -0.15f),
                          new RelMapEntry(Factions.LUDDIC_CHURCH, -0.15f),
                          new RelMapEntry(Factions.TRITACHYON, -0.15f),
                          new RelMapEntry("SCY", -0.15f)
                  ),
                  Arrays.asList(ShipTypeChoice.PRIVATEER, ShipTypeChoice.PATROL, ShipTypeChoice.TRADER,
                                ShipTypeChoice.SCAVENGER)),
        CITADEL("citadeldefenders", RequiredFaction.CITADEL, "the Citadel Conglomerate",
                "Escort jobs for the Citadel Conglomerate",
                "Citadel", null,
                Arrays.asList(
                        new RelMapEntry("citadeldefenders", 0.25f),
                        new RelMapEntry("tiandong", -0.15f)
                ),
                Arrays.asList(ShipTypeChoice.BOUNTY_HUNTER, ShipTypeChoice.PATROL, ShipTypeChoice.TRADER,
                              ShipTypeChoice.SCAVENGER)),
        DIABLE("diableavionics", RequiredFaction.DIABLE, "the Diable Avionics Corporation",
               "The Diable Avionics Corporation, self-proclaimed successor to the Domain",
               "Outer Terminus", "diableavionics_",
               Arrays.asList(
                       new RelMapEntry("diableavionics", 0.25f),
                       new RelMapEntry("tiandong", 0.05f),
                       new RelMapEntry(Factions.HEGEMONY, -0.15f),
                       new RelMapEntry(Factions.TRITACHYON, -0.15f),
                       new RelMapEntry("SCY", -0.15f),
                       new RelMapEntry("shadow_industry", -0.15f)
               ),
               Arrays.asList(ShipTypeChoice.RAIDER, ShipTypeChoice.CARRIER, ShipTypeChoice.SMUGGLER)),
        EXIGENCY("exigency", RequiredFaction.EXIGENCY, "the Exigency Corporation",
                 "The Exigency Corporation, advanced and mysterious",
                 "Tasserus", "exigency_",
                 Arrays.asList(
                         new RelMapEntry("exigency", 0.3f),
                         new RelMapEntry("exipirated", -0.25f),
                         new RelMapEntry(Factions.HEGEMONY, -0.15f),
                         new RelMapEntry(Factions.TRITACHYON, -0.15f),
                         new RelMapEntry("SCY", -0.15f),
                         new RelMapEntry("shadow_industry", -0.15f)
                 ),
                 Arrays.asList(ShipTypeChoice.PATROL, ShipTypeChoice.CARRIER)),
        AHRIMAN("exipirated", RequiredFaction.EXIGENCY, "the Ahriman Association",
                "An insular group of neo-Exigency criminals called the Ahriman Association",
                "hyperspace", "exipirated_",
                Arrays.asList( // one-third negative impact
                        new RelMapEntry("exipirated", 0.5f),
                        new RelMapEntry("exigency", -0.75f),
                        new RelMapEntry(Factions.PIRATES, 0.05f),
                        new RelMapEntry("junk_pirates", 0.05f),
                        new RelMapEntry(Factions.INDEPENDENT, -0.05f),
                        new RelMapEntry("syndicate_asp", -0.05f),
                        new RelMapEntry("tiandong", -0.05f)
                ),
                Arrays.asList(ShipTypeChoice.RAIDER, ShipTypeChoice.PRIVATEER, ShipTypeChoice.PATROL,
                              ShipTypeChoice.SCAVENGER)),
        IMPERIUM("interstellarimperium", RequiredFaction.IMPERIUM, "the Interstellar Imperium",
                 "The Interstellar Imperium, antiquated and industrious",
                 "Thracia", "ii_",
                 Arrays.asList(
                         new RelMapEntry("interstellarimperium", 0.25f),
                         new RelMapEntry(Factions.INDEPENDENT, 0.1f),
                         new RelMapEntry("tiandong", 0.1f),
                         new RelMapEntry(Factions.HEGEMONY, -0.15f),
                         new RelMapEntry("junk_pirates", -0.15f)
                 ),
                 Arrays.asList(ShipTypeChoice.PATROL, ShipTypeChoice.TRADER, ShipTypeChoice.CARRIER,
                               ShipTypeChoice.SCAVENGER)),
        JUNK_PIRATES("junk_pirates", RequiredFaction.JUNK_PIRATES, "the Junk Pirates",
                     "An organized faction of raiders and tinkerers called Junk Pirates",
                     "Breh'Inni", "junk_pirates_",
                     Arrays.asList( // one-third negative impact
                             new RelMapEntry("junk_pirates", 0.25f),
                             new RelMapEntry(Factions.PIRATES, 0.05f),
                             new RelMapEntry("pack", 0.05f),
                             new RelMapEntry(Factions.HEGEMONY, -0.05f),
                             new RelMapEntry(Factions.LUDDIC_CHURCH, -0.05f),
                             new RelMapEntry(Factions.TRITACHYON, -0.05f),
                             new RelMapEntry("syndicate_asp", -0.05f)
                     ),
                     Arrays.asList(ShipTypeChoice.RAIDER, ShipTypeChoice.PRIVATEER, ShipTypeChoice.SMUGGLER,
                                   ShipTypeChoice.SCAVENGER)),
        PACK("pack", RequiredFaction.JUNK_PIRATES, "the Post Anarchist Canis Kollective",
             "The Post Anarchist Canis Kollective, isolated and pragmatic",
             "Canis", "pack_",
             Arrays.asList(
                     new RelMapEntry("pack", 0.25f),
                     new RelMapEntry(Factions.INDEPENDENT, 0.05f),
                     new RelMapEntry("junk_pirates", 0.05f),
                     new RelMapEntry("tiandong", 0.05f)
             ),
             Arrays.asList(ShipTypeChoice.RAIDER, ShipTypeChoice.PATROL, ShipTypeChoice.PRIVATEER)),
        ASP("syndicate_asp", RequiredFaction.JUNK_PIRATES, "the Associated Starline Parcels Syndicate",
            "Undertaking missions for the Associated Starline Parcels Syndicate",
            "Ursulo", "syndicate_asp_",
            Arrays.asList(
                    new RelMapEntry("syndicate_asp", 0.25f),
                    new RelMapEntry(Factions.HEGEMONY, 0.05f),
                    new RelMapEntry(Factions.INDEPENDENT, 0.05f),
                    new RelMapEntry("SCY", 0.1f),
                    new RelMapEntry("shadow_industry", 0.1f),
                    new RelMapEntry("junk_pirates", -0.15f)
            ),
            Arrays.asList(ShipTypeChoice.TRADER, ShipTypeChoice.PRIVATEER, ShipTypeChoice.BOUNTY_HUNTER)),
        MAYORATE("mayorate", RequiredFaction.MAYORATE, "the Mayorate",
                 "Privateering for the Mayorate",
                 "Rasht", "ilk_",
                 Arrays.asList(
                         new RelMapEntry("mayorate", 0.25f),
                         new RelMapEntry(Factions.PIRATES, 0.1f),
                         new RelMapEntry(Factions.HEGEMONY, -0.15f),
                         new RelMapEntry(Factions.LUDDIC_CHURCH, -0.15f)
                 ),
                 Arrays.asList(ShipTypeChoice.PRIVATEER, ShipTypeChoice.SMUGGLER, ShipTypeChoice.RAIDER,
                               ShipTypeChoice.PATROL)),
        SCY("SCY", RequiredFaction.SCY, "the Scy Nation",
            "The Scy Nation, xenophobic and adaptable",
            "Acheron", "SCY_",
            Arrays.asList(
                    new RelMapEntry("SCY", 0.25f),
                    new RelMapEntry("syndicate_asp", 0.1f),
                    new RelMapEntry("shadow_industry", 0.1f),
                    new RelMapEntry("blackrock_driveyards", -0.15f),
                    new RelMapEntry("diableavionics", -0.15f),
                    new RelMapEntry("junk_pirates", -0.15f)
            ),
            Arrays.asList(ShipTypeChoice.PATROL, ShipTypeChoice.PRIVATEER, ShipTypeChoice.RAIDER, ShipTypeChoice.CARRIER)),
        SHADOWYARDS("shadow_industry", RequiredFaction.SHADOWYARDS, "the Shadowyards Reconstruction Authority",
                    "The Shadowyards Reconstruction Authority, progressive and bureaucratic",
                    "Gigas", "ms_",
                    Arrays.asList(
                            new RelMapEntry("shadow_industry", 0.25f),
                            new RelMapEntry("syndicate_asp", 0.1f),
                            new RelMapEntry("SCY", 0.1f),
                            new RelMapEntry(Factions.HEGEMONY, -0.15f),
                            new RelMapEntry(Factions.DIKTAT, -0.15f),
                            new RelMapEntry("diableavionics", -0.15f),
                            new RelMapEntry("junk_pirates", -0.15f)
                    ),
                    Arrays.asList(ShipTypeChoice.PATROL, ShipTypeChoice.CARRIER, ShipTypeChoice.TRADER,
                                  ShipTypeChoice.SCAVENGER)),
        TEMPLARS("templars", RequiredFaction.TEMPLARS, "the Knights Templar",
                 "The Knights Templar, terrifying and misguided",
                 "Antioch", "tem_",
                 Arrays.asList(
                         new RelMapEntry("templars", 0.51f),
                         new RelMapEntry(Factions.HEGEMONY, -0.2f),
                         new RelMapEntry(Factions.DIKTAT, -0.2f),
                         new RelMapEntry(Factions.INDEPENDENT, -0.2f),
                         new RelMapEntry(Factions.LUDDIC_CHURCH, -0.35f),
                         new RelMapEntry(Factions.LUDDIC_PATH, -0.35f),
                         new RelMapEntry(Factions.TRITACHYON, -0.2f),
                         new RelMapEntry("blackrock_driveyards", -0.2f),
                         new RelMapEntry("citadeldefenders", -0.2f),
                         new RelMapEntry("diableavionics", -0.2f),
                         new RelMapEntry("exigency", -0.2f),
                         new RelMapEntry("exipirated", -0.2f),
                         new RelMapEntry("interstellarimperium", -0.2f),
                         new RelMapEntry("junk_pirates", -0.2f),
                         new RelMapEntry("pack", -0.2f),
                         new RelMapEntry("syndicate_asp", -0.2f),
                         new RelMapEntry("mayorate", -0.2f),
                         new RelMapEntry("SCY", -0.2f),
                         new RelMapEntry("shadow_industry", -0.2f),
                         new RelMapEntry("tiandong", -0.2f)
                 ),
                 Arrays.asList(ShipTypeChoice.BOUNTY_HUNTER, ShipTypeChoice.PATROL)),
        TIANDONG("tiandong", RequiredFaction.TIANDONG, "Tiandong Heavy Industries",
                 "Enforcement gigs for Tiandong Heavy Industries",
                 "Shaanxi", "tiandong_",
                 Arrays.asList(
                         new RelMapEntry("tiandong", 0.25f),
                         new RelMapEntry("interstellarimperium", 0.1f),
                         new RelMapEntry(Factions.INDEPENDENT, 0.15f),
                         new RelMapEntry("junk_pirates", -0.15f)
                 ),
                 Arrays.asList(ShipTypeChoice.BOUNTY_HUNTER, ShipTypeChoice.PRIVATEER, ShipTypeChoice.PATROL,
                               ShipTypeChoice.SCAVENGER)),
        ORA("ORA", RequiredFaction.ORA, "Outer Rim Alliance",
            "Volunteering as an auxiliary for the Alliance Navy",
            "Godunov", "ora_",
            Arrays.asList(
                    new RelMapEntry("ORA", 0.2f),
                    new RelMapEntry(Factions.LUDDIC_CHURCH, 0.15f),
                    new RelMapEntry(Factions.TRITACHYON, 0.1f),
                    new RelMapEntry("blackrock_driveyards", 0.1f),
                    new RelMapEntry("diableavionics", -0.1f),
                    new RelMapEntry("interstellarimperium", -0.1f),
                    new RelMapEntry(Factions.DIKTAT, -0.1f),
                    new RelMapEntry(Factions.HEGEMONY, -0.15f),
                    new RelMapEntry("mayorate", -0.15f),
                    new RelMapEntry("exigency", -0.15f)
            ),
            Arrays.asList(ShipTypeChoice.TRADER, ShipTypeChoice.SMUGGLER, ShipTypeChoice.PATROL,
                          ShipTypeChoice.SCAVENGER));

        final String factionId;
        final RequiredFaction req;
        final String nameWithArticle;
        final String flavorString;
        final String starSystemName;
        final String factionPrefix;
        final Map<String, Float> relAdjustMap;
        final List<ShipTypeChoice> shipTypeChoices;

        private FactionChoice(String factionId, RequiredFaction req, String nameWithArticle, String flavorString,
                              String starSystemName, String factionPrefix,
                              List<RelMapEntry> relAdjustList, List<ShipTypeChoice> shipTypeChoices) {
            this.factionId = factionId;
            this.req = req;
            this.nameWithArticle = nameWithArticle;
            this.flavorString = flavorString;
            this.starSystemName = starSystemName;
            this.factionPrefix = factionPrefix;
            relAdjustMap = new LinkedHashMap<>(relAdjustList.size());
            for (RelMapEntry entry : relAdjustList) {
                relAdjustMap.put(entry.faction, entry.relAdjust);
            }
            this.shipTypeChoices = shipTypeChoices;
        }

        static FactionChoice getFactionChoice(String factionId) {
            for (FactionChoice choice : FactionChoice.values()) {
                if (choice.factionId.contentEquals(factionId)) {
                    return choice;
                }
            }
            return null;
        }

        private static final class RelMapEntry {

            final String faction;
            final float relAdjust;

            RelMapEntry(String faction, float relAdjust) {
                this.faction = faction;
                this.relAdjust = relAdjust;
            }
        }
    }
}
