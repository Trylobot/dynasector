package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.Misc.Token;
import java.util.List;
import java.util.Map;

/**
 * SSP_NGCSetGroup
 */
public class DS_NGCSetGroup extends BaseCommandPlugin {

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params,
                           Map<String, MemoryAPI> memoryMap) {
        String param = params.get(0).getString(memoryMap);
        String group = Misc.lcFirst(param.substring("ngcGroup".length()));
        memoryMap.get(MemKeys.LOCAL).set("$ngcGroup", group, 7);

        return true;
    }
}
