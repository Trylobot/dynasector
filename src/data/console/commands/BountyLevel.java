package data.console.commands;

import com.fs.starfarer.api.Global;
import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;

public class BountyLevel implements BaseCommand {

    @Override
    public CommandResult runCommand(String args, CommandContext context) {
        if (context != CommandContext.CAMPAIGN_MAP) {
            Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
            return CommandResult.WRONG_CONTEXT;
        }

        if (args.isEmpty()) {
            return CommandResult.BAD_SYNTAX;
        }

        float amount;

        try {
            amount = Float.parseFloat(args);
        } catch (NumberFormatException ex) {
            Console.showMessage("Error: level must be a number!");
            return CommandResult.BAD_SYNTAX;
        }

        if (amount < 0f || amount > 20f) {
            Console.showMessage("Error: level must be between 0 and 20!");
            return CommandResult.BAD_SYNTAX;
        }

        Global.getSector().getPersistentData().put("ds_personBountyLevel", amount);
        Console.showMessage("Set named bounty level to " + amount + ".");
        return CommandResult.SUCCESS;
    }
}
