package data.missions;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.PlanetSpecAPI;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.missions.DS_FleetGenerator.GeneratorFleetTypes;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs.Archetype;
import data.scripts.util.DS_Defs.FleetStyle;
import data.scripts.util.DS_Util;
import data.scripts.variants.DS_FleetRandomizer;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class DS_BaseRandomBattle implements MissionDefinitionPlugin {

    private static boolean first = true;

    protected static final WeightedRandomPicker<String> FACTIONS = new WeightedRandomPicker<>();
    protected static final String[] OBJECTIVE_TYPES = {
        "sensor_array", "sensor_array", "nav_buoy", "nav_buoy", "comm_relay"
    };
    protected static final List<String> PLANETS = new ArrayList<>(35);
    protected static final List<Color> STAR_COLORS = new ArrayList<>(13);

    protected static final Random rand = new Random();

    public static void inflateFleet(List<FleetMemberAPI> fleet, FleetSide side, MissionDefinitionAPI api) {
        boolean flagship = true;
        for (FleetMemberAPI fleetMember : fleet) {
            FleetMemberAPI member;
            if (fleetMember.isFighterWing()) {
                member = api.addToFleet(side, fleetMember.getSpecId(), FleetMemberType.FIGHTER_WING, false);
            } else {
                member = api.addToFleet(side, fleetMember.getVariant().getHullVariantId(), FleetMemberType.SHIP,
                                        flagship);
                member.setVariant(fleetMember.getVariant(), false, true);
                for (String slot : member.getVariant().getStationModules().keySet()) {
                    if (fleetMember.getModuleVariant(slot) != null) {
                        member.setModuleVariant(slot, fleetMember.getModuleVariant(slot));
                    }
                }

                flagship = false;
            }

            member.getRepairTracker().setCR(0.7f);
        }
    }

    protected static FleetData generateFleet(int maxPts, float qualityFactor, float opBonus, FleetSide side,
                                             String faction, String fleetType, MissionDefinitionAPI api) {
        GeneratorFleetTypes type;
        Map<Archetype, Float> archetypeWeights;
        switch (fleetType) {
            case "raiders":
                type = GeneratorFleetTypes.RAIDERS;
                archetypeWeights = DS_Util.getArchetypeWeights(FleetStyle.RAIDER, faction);
                break;
            case "patrol fleet":
                type = GeneratorFleetTypes.PATROL;
                archetypeWeights = DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction);
                break;
            case "hunter-killers":
                type = GeneratorFleetTypes.HUNTERS;
                archetypeWeights = DS_Util.getArchetypeWeights(FleetStyle.RAIDER, faction);
                break;
            case "war fleet":
                type = GeneratorFleetTypes.WAR;
                archetypeWeights = DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction);
                break;
            case "defense fleet":
                type = GeneratorFleetTypes.DEFENSE;
                archetypeWeights = DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction);
                break;
            case "convoy":
                type = GeneratorFleetTypes.CONVOY;
                archetypeWeights = DS_Util.getArchetypeWeights(FleetStyle.CIVILIAN, faction);
                break;
            case "blockade-runners":
                type = GeneratorFleetTypes.BLOCKADE;
                archetypeWeights = DS_Util.getArchetypeWeights(FleetStyle.STANDARD, faction);
                break;
            case "invasion fleet":
                type = GeneratorFleetTypes.INVASION;
                archetypeWeights = DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction);
                break;
            default:
                return null;
        }

        float realMax = maxPts;
        switch (faction) {
            case "exipirated":
                break;
            case "blackrock_driveyards":
                realMax *= 0.9;
                break;
            case "citadeldefenders":
                break;
            case "exigency":
                break;
            case Factions.HEGEMONY:
                break;
            case "interstellarimperium":
                break;
            case Factions.INDEPENDENT:
                break;
            case Factions.KOL:
                break;
            case "mayorate":
                break;
            case Factions.LUDDIC_CHURCH:
                realMax *= 1.1;
                break;
            case Factions.LUDDIC_PATH:
                realMax *= 1.1;
                break;
            case Factions.PIRATES:
                realMax *= 1.1;
                break;
            case Factions.DIKTAT:
                break;
            case Factions.LIONS_GUARD:
                realMax *= 0.9;
                break;
            case "shadow_industry":
                realMax *= 0.9;
                break;
            case "templars":
                realMax *= 0.85;
                break;
            case Factions.TRITACHYON:
                realMax *= 0.9;
                break;
            case Factions.PERSEAN:
                break;
            case Factions.DERELICT:
                realMax *= 1.5;
                break;
            case Factions.REMNANTS:
                realMax *= 0.9;
                break;
            case "junk_pirates":
                break;
            case "pack":
                break;
            case "syndicate_asp":
                realMax *= 1.1;
                break;
            case "SCY":
                break;
            case "tiandong":
                break;
            case "diableavionics":
                realMax *= 0.95;
                break;
            case "cabal":
                realMax *= 0.85;
                break;
            case "ORA":
                realMax *= 0.9;
                break;
            default:
        }

        return type.generate(api, side, faction, qualityFactor, opBonus, (int) realMax, archetypeWeights);
    }

    static FleetData finishFleet(List<FleetMemberAPI> fleet, FleetSide side, String faction, MissionDefinitionAPI api) {
        Collections.sort(fleet, DS_FleetRandomizer.PRIORITY);

        inflateFleet(fleet, side, api);

        return new FleetData(fleet);
    }

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        if (first) {
            first = false;
            init();
        }
    }

    private void init() {
        for (PlanetSpecAPI spec : Global.getSettings().getAllPlanetSpecs()) {
            PLANETS.add(spec.getPlanetType());
            if (spec.isStar()) {
                STAR_COLORS.add(spec.getCoronaColor());
            }
        }
        FACTIONS.add(Factions.PIRATES, 1f);
        FACTIONS.add(Factions.HEGEMONY, 1f);
        FACTIONS.add(Factions.DIKTAT, 0.5f);
        FACTIONS.add(Factions.LIONS_GUARD, 0.25f);
        FACTIONS.add(Factions.INDEPENDENT, 1f);
        FACTIONS.add(Factions.SCAVENGERS, 0.25f);
        FACTIONS.add(Factions.TRITACHYON, 1f);
        FACTIONS.add(Factions.PERSEAN, 1f);
        FACTIONS.add(Factions.LUDDIC_PATH, 0.5f);
        FACTIONS.add(Factions.LUDDIC_CHURCH, 1f);
        FACTIONS.add(Factions.DERELICT, 0.25f);
        FACTIONS.add(Factions.REMNANTS, 0.5f);
        if (DSModPlugin.hasUnderworld) {
            FACTIONS.add("cabal", 0.25f);
        }
        if (DSModPlugin.imperiumExists) {
            FACTIONS.add("interstellarimperium", 1f);
        }
        if (DSModPlugin.citadelExists) {
            FACTIONS.add("citadeldefenders", 1f);
        }
        if (DSModPlugin.blackrockExists) {
            FACTIONS.add("blackrock_driveyards", 1f);
        }
        if (DSModPlugin.exigencyExists) {
            FACTIONS.add("exipirated", 0.5f);
            FACTIONS.add("exigency", 0.75f);
        }
        if (DSModPlugin.templarsExists) {
            FACTIONS.add("templars", 0.25f);
        }
        if (DSModPlugin.shadowyardsExists) {
            FACTIONS.add("shadow_industry", 1f);
        }
        if (DSModPlugin.mayorateExists) {
            FACTIONS.add("mayorate", 1f);
        }
        if (DSModPlugin.junkPiratesExists) {
            FACTIONS.add("junk_pirates", 1f);
            FACTIONS.add("pack", 0.75f);
            FACTIONS.add("syndicate_asp", 0.5f);
        }
        if (DSModPlugin.scyExists) {
            FACTIONS.add("SCY", 1f);
        }
        if (DSModPlugin.tiandongExists) {
            FACTIONS.add("tiandong", 0.75f);
        }
        if (DSModPlugin.diableExists) {
            FACTIONS.add("diableavionics", 0.75f);
        }
        if (DSModPlugin.oraExists) {
            FACTIONS.add("ORA", 0.75f);
        }
    }

    public static class FleetData {

        public final List<FleetMemberAPI> fleet;

        FleetData(List<FleetMemberAPI> fleet) {
            this.fleet = fleet;
        }
    };

    protected static class Plugin extends BaseEveryFrameCombatPlugin {

        private final float range;

        @SuppressWarnings(value = "PublicConstructorInNonPublicClass")
        public Plugin(float range) {
            this.range = range;
        }

        @Override
        public void advance(float amount, List<InputEventAPI> events) {
        }

        @Override
        public void init(CombatEngineAPI engine) {
            engine.getContext().setStandoffRange(range);
            engine.getContext().setInitialEscapeRange(range);
            engine.getContext().setFlankDeploymentDistance(range * 2f);
        }
    }
}
