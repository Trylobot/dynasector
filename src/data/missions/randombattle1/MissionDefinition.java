package data.missions.randombattle1;

import com.fs.starfarer.api.combat.BattleCreationContext;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.impl.combat.EscapeRevealPlugin;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.missions.DS_BaseRandomBattle;
import data.scripts.util.DS_Util;
import org.lazywizard.lazylib.MathUtils;

public class MissionDefinition extends DS_BaseRandomBattle {

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        super.defineMission(api);

        String playerFaction = FACTIONS.pick();
        String enemyFaction = FACTIONS.pick();

        FleetSide escape;
        double r = Math.random();
        if (r < 0.8) {
            escape = null;
            api.addBriefingItem("Defeat all enemy forces");
        } else if (r < 0.9) {
            escape = FleetSide.PLAYER;
            api.addBriefingItem("Escape from the enemy forces");
        } else {
            escape = FleetSide.ENEMY;
            api.addBriefingItem("Prevent the enemy forces from escaping");
        }

        int size = 5 + (int) ((float) Math.random() * 55);
        float width, height;
        if (escape != null) {
            width = (12000f + 10000f * (size / 40f)) * ((float) Math.random() * 0.6f + 0.4f);
            height = (12000f + 10000f * (size / 40f));
        } else {
            width = (12000f + 10000f * (size / 40f)) * ((float) Math.random() * 0.4f + 0.6f);
            height = (12000f + 10000f * (size / 40f)) * ((float) Math.random() * 0.4f + 0.6f);
        }
        api.initMap(-width / 2f, width / 2f, -height / 2f, height / 2f);

        if (escape == FleetSide.PLAYER) {
            api.initFleet(FleetSide.PLAYER, DS_Util.getFactionPrefix(playerFaction), FleetGoal.ESCAPE, false, size / 8);
        } else {
            api.initFleet(FleetSide.PLAYER, DS_Util.getFactionPrefix(playerFaction), FleetGoal.ATTACK, false, size / 8);
        }
        if (escape == FleetSide.ENEMY) {
            api.initFleet(FleetSide.ENEMY, DS_Util.getFactionPrefix(enemyFaction), FleetGoal.ESCAPE, true, size / 8);
        } else {
            api.initFleet(FleetSide.ENEMY, DS_Util.getFactionPrefix(enemyFaction), FleetGoal.ATTACK, true, size / 8);
        }

        int objectiveCount = (int) Math.floor(size * ((float) Math.random() * 0.75f + 0.5f) / 8f);

        String battleSize;
        if (size >= 50) {
            battleSize = "Armageddon";
        } else if (size >= 35) {
            battleSize = "War";
        } else if (size >= 20) {
            battleSize = "Assault";
        } else if (size >= 10) {
            battleSize = "Raid";
        } else {
            battleSize = "Skirmish";
        }
        if (objectiveCount == 0) {
            api.addBriefingItem("Battle size: " + battleSize);
        } else if (objectiveCount == 1) {
            api.addBriefingItem("Battle size: " + battleSize + "  -  " + objectiveCount + " objective");
        } else {
            api.addBriefingItem("Battle size: " + battleSize + "  -  " + objectiveCount + " objectives");
        }

        int playerSize = (int) (size * (escape == FleetSide.PLAYER ? 0.5f : 1f));
        int enemySize = (int) (size * (escape == FleetSide.ENEMY ? 0.5f : 1f) * 1.05f + 5f);

        WeightedRandomPicker<String> fleetTypes = new WeightedRandomPicker<>();
        if (escape == null) {
            if (size < 20) {
                fleetTypes.add("raiders", 1f);
            }
            if (size < 30) {
                fleetTypes.add("patrol fleet", 1f);
            }
            if (size < 30 && size >= 15) {
                fleetTypes.add("hunter-killers", 1f);
            }
            if (size >= 25) {
                fleetTypes.add("defense fleet", 2f);
            }
            if (size >= 30) {
                fleetTypes.add("war fleet", 2f);
            }
        } else {
            if (size < 30) {
                fleetTypes.add("convoy", 2f);
            }
            if (size >= 15 && size < 45) {
                fleetTypes.add("blockade-runners", 1f);
            }
            if (size >= 30) {
                fleetTypes.add("invasion fleet", 1f);
            }
        }

        String playerFleet = fleetTypes.pick();
        String enemyFleet = fleetTypes.pick();

        if (escape == FleetSide.PLAYER) {
            WeightedRandomPicker<String> enemyFleetTypes = new WeightedRandomPicker<>();
            if (playerFleet.contentEquals("convoy")) {
                if (size < 20) {
                    enemyFleetTypes.add("raiders", 1f);
                }
                if (size < 30) {
                    enemyFleetTypes.add("patrol fleet", 1f);
                }
                if (size < 30 && size >= 15) {
                    enemyFleetTypes.add("hunter-killers", 1f);
                }
                if (size >= 25) {
                    enemyFleetTypes.add("war fleet", 2f);
                }
            }
            if (playerFleet.contentEquals("blockade-runners")) {
                if (size < 30) {
                    enemyFleetTypes.add("patrol fleet", 1f);
                }
                if (size >= 25) {
                    enemyFleetTypes.add("defense fleet", 2f);
                }
            }
            if (playerFleet.contentEquals("invasion fleet")) {
                enemyFleetTypes.add("defense fleet", 2f);
            }
            enemyFleet = enemyFleetTypes.pick();
        } else if (escape == FleetSide.ENEMY) {
            WeightedRandomPicker<String> playerFleetTypes = new WeightedRandomPicker<>();
            if (enemyFleet.contentEquals("convoy")) {
                if (size < 20) {
                    playerFleetTypes.add("raiders", 1f);
                }
                if (size < 30) {
                    playerFleetTypes.add("patrol fleet", 1f);
                }
                if (size < 30 && size >= 15) {
                    playerFleetTypes.add("hunter-killers", 1f);
                }
                if (size >= 25) {
                    playerFleetTypes.add("war fleet", 2f);
                }
            }
            if (enemyFleet.contentEquals("blockade-runners")) {
                if (size < 30) {
                    playerFleetTypes.add("patrol fleet", 1f);
                }
                if (size >= 25) {
                    playerFleetTypes.add("defense fleet", 2f);
                }
            }
            if (enemyFleet.contentEquals("invasion fleet")) {
                playerFleetTypes.add("defense fleet", 2f);
            }
            playerFleet = playerFleetTypes.pick();
        }

        float playerQualityFactor = MathUtils.getRandomNumberInRange(0, 10) / 10f;
        float enemyQualityFactor = MathUtils.getRandomNumberInRange(0, 10) / 10f;

        api.setFleetTagline(FleetSide.PLAYER, DS_Util.getFactionName(playerFaction) + " " + playerFleet +
                            " (" + playerSize + " points)" +
                            " (QF " + String.format("%.1f", playerQualityFactor) + ")");
        api.setFleetTagline(FleetSide.ENEMY, DS_Util.getFactionName(enemyFaction) + " " + enemyFleet +
                            " (" + enemySize + " points)" +
                            " (QF " + String.format("%.1f", enemyQualityFactor) + ")");

        generateFleet(playerSize, playerQualityFactor, 0f, FleetSide.PLAYER, playerFaction, playerFleet, api);
        generateFleet(enemySize, enemyQualityFactor, 0f, FleetSide.ENEMY, enemyFaction, enemyFleet, api);

        float minX = -width / 2;
        float minY = -height / 2;

        int nebulaCount = (int) ((1f + (float) Math.random() * 3f) * (float) Math.sqrt(size * 3f));
        float nebulaSize = (float) Math.random();

        if (nebulaSize * nebulaCount >= 32.5) {
            api.addBriefingItem("Alert: Deep nebula");
        } else if (nebulaSize * nebulaCount >= 25) {
            api.addBriefingItem("Alert: Thick nebula");
        } else if (nebulaSize * nebulaCount >= 17.5) {
            api.addBriefingItem("Alert: Moderate nebula");
        } else if (nebulaSize * nebulaCount >= 10) {
            api.addBriefingItem("Alert: Sparse nebula");
        }

        for (int i = 0; i < nebulaCount; i++) {
            float x = (float) Math.random() * width - width / 2;
            float y = (float) Math.random() * height - height / 2;
            float radius = (1f + (float) Math.random() * 4f) * nebulaSize * size;
            api.addNebula(x, y, radius);
        }

        while (objectiveCount > 0) {
            String type = OBJECTIVE_TYPES[rand.nextInt(OBJECTIVE_TYPES.length)];
            int configuration;
            r = Math.random();
            if (r < 0.5) {
                configuration = 0;
            } else if (r < 0.75) {
                configuration = 1;
            } else {
                configuration = 2;
            }

            if (objectiveCount == 1) {
                r = Math.random();
                if (r < 0.75) {
                    api.addObjective(0f, 0f, type);
                } else if (r < 0.875) {
                    float x = (width * 0.075f + width * 0.3f * (float) Math.random()) * (Math.random() > 0.5 ? 1f : -1f);
                    api.addObjective(x, 0f, type);
                } else {
                    float y = (height * 0.075f + height * 0.3f * (float) Math.random()) * (Math.random() > 0.5 ? 1f :
                                                                                           -1f);
                    api.addObjective(0f, y, type);
                }

                objectiveCount -= 1;
            } else {
                float x1, x2, y1, y2;
                r = Math.random();
                if ((r < 0.75 && configuration == 0) || (r < 0.5 && configuration == 1) || (r < 0.25 && configuration ==
                                                                                            2)) {
                    float theta = (float) (Math.random() * Math.PI);
                    double radius = Math.min(width, height);
                    radius = radius * 0.1 + radius * 0.3 * Math.random();
                    x1 = (float) (Math.cos(theta) * radius);
                    y1 = (float) -(Math.sin(theta) * radius);
                    x2 = -x1;
                    y2 = -y1;
                } else if ((r < 0.875 && configuration == 0) || (r < 0.75 && configuration == 1) || (r < 0.625 &&
                                                                                                     configuration == 2)) {
                    x1 = (width * 0.075f + width * 0.3f * (float) Math.random()) * (Math.random() > 0.5 ? 1f : -1f);
                    x2 = x1;
                    y1 = (height * 0.075f + height * 0.3f * (float) Math.random()) * (Math.random() > 0.5 ? 1f : -1f);
                    y2 = -y1;
                } else {
                    x1 = (width * 0.075f + width * 0.3f * (float) Math.random()) * (Math.random() > 0.5 ? 1f : -1f);
                    x2 = -x1;
                    y1 = (height * 0.075f + height * 0.3f * (float) Math.random()) * (Math.random() > 0.5 ? 1f : -1f);
                    y2 = y1;
                }

                r = Math.random();
                if (r < 0.75) {
                    api.addObjective(x1, y1, type);
                    api.addObjective(x2, y2, type);
                } else {
                    api.addObjective(x1, y1, type);
                    type = OBJECTIVE_TYPES[rand.nextInt(OBJECTIVE_TYPES.length)];
                    api.addObjective(x2, y2, type);
                }

                objectiveCount -= 2;
            }
        }

        int asteroidSpeed = (int) (Math.pow(Math.random(), 3.0) * 200f);
        int asteroidCount = size + (int) (size * 4 * Math.pow(Math.random(), 3.0));
        int asteroidFieldCount = (int) (size * (float) Math.pow(Math.random(), 2.0) / 6.5f);
        int ringAsteroidCount = (int) (asteroidCount * 4 * Math.random());
        int ringAsteroidFieldCount = (int) (size * (float) Math.pow(Math.random(), 2.0) / 10f);

        if ((float) Math.sqrt(asteroidSpeed) *
                (asteroidCount * asteroidFieldCount + ringAsteroidCount * (ringAsteroidFieldCount / 4)) / (size * size) >=
                6f) {
            api.addBriefingItem("Alert: Asteroid storm");
        } else if ((float) Math.sqrt(asteroidSpeed) *
                (asteroidCount * asteroidFieldCount + ringAsteroidCount * (ringAsteroidFieldCount / 4)) / (size * size) >=
                3f) {
            api.addBriefingItem("Alert: Thick asteroids");
        } else if ((float) Math.sqrt(asteroidSpeed) *
                (asteroidCount * asteroidFieldCount + ringAsteroidCount * (ringAsteroidFieldCount / 4)) / (size * size) >=
                1.5f) {
            api.addBriefingItem("Alert: Moderate asteroids");
        } else if ((float) Math.sqrt(asteroidSpeed) *
                (asteroidCount * asteroidFieldCount + ringAsteroidCount * (ringAsteroidFieldCount / 4)) / (size * size) >=
                0.75f) {
            api.addBriefingItem("Alert: Sparse asteroids");
        }

        for (int i = 0; i < asteroidFieldCount; i++) {
            api.addAsteroidField(minX + width * 0.5f,
                                 minY + height * 0.5f,
                                 (float) Math.random() * 360f,
                                 Math.min(width, height) * ((float) (Math.random() * Math.random()) * 0.8f + 0.2f),
                                 asteroidSpeed * 0.75f,
                                 asteroidSpeed,
                                 asteroidCount);
        }
        for (int i = 0; i < ringAsteroidFieldCount; i++) {
            api.addRingAsteroids(minX + width * 0.5f,
                                 minY + height * 0.5f,
                                 (float) Math.random() * 360f,
                                 Math.min(width, height) * ((float) (Math.random() * Math.random()) * 0.8f + 0.2f),
                                 asteroidSpeed * 1.5f,
                                 asteroidSpeed * 2f,
                                 ringAsteroidCount);
        }

        String planet = PLANETS.get((int) (Math.random() * PLANETS.size()));
        float radius = 25f + (float) Math.random() * (float) Math.random() * 500f;

        api.addPlanet(0, 0, radius, planet, 0f, true);
        if (planet.contentEquals("wormholeUnder")) {
            api.addPlanet(0, 0, radius, "wormholeA", 0f, true);
            api.addPlanet(0, 0, radius, "wormholeB", 0f, true);
            api.addPlanet(0, 0, radius, "wormholeC", 0f, true);
        }

        if (escape != null) {
            BattleCreationContext context = new BattleCreationContext(null, null, null, null);
            if (playerFleet.contentEquals("convoy") || enemyFleet.contentEquals("convoy")) {
                context.setInitialEscapeRange(height / 5f);
                api.addPlugin(new EscapeRevealPlugin(context));
                api.addPlugin(new Plugin(height / 3.5f));
            } else {
                context.setInitialEscapeRange(height / 8f);
                api.addPlugin(new EscapeRevealPlugin(context));
                api.addPlugin(new Plugin(height / 6f));
            }
        }
    }
}
