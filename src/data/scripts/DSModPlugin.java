package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignEventListener;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventManagerAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventPlugin;
import com.fs.starfarer.api.impl.campaign.CoreScript;
import com.fs.starfarer.api.impl.campaign.fleets.BountyPirateFleetManager;
import com.fs.starfarer.api.impl.campaign.fleets.LuddicPathFleetManager;
import com.fs.starfarer.api.impl.campaign.fleets.MercFleetManager;
import com.fs.starfarer.api.impl.campaign.fleets.PirateFleetManager;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.loading.FleetCompositionDoctrineAPI;
import com.thoughtworks.xstream.XStream;
import data.scripts.campaign.DS_CoreScript;
import data.scripts.campaign.DS_Profiler;
import data.scripts.campaign.events.DS_FactionHostilityEvent;
import data.scripts.campaign.events.DS_PersonBountyEvent;
import data.scripts.campaign.events.DS_PersonBountyEvent.BountyType;
import data.scripts.campaign.fleets.DS_BountyPirateFleetManager;
import data.scripts.campaign.fleets.DS_FleetInjector;
import data.scripts.campaign.fleets.DS_LuddicPathFleetManager;
import data.scripts.campaign.fleets.DS_MercFleetManager;
import data.scripts.campaign.fleets.DS_PatrolFleetManager;
import data.scripts.campaign.fleets.DS_PirateFleetManager;
import data.scripts.campaign.submarkets.DS_BlackMarketPlugin;
import data.scripts.campaign.submarkets.DS_MilitarySubmarketPlugin;
import data.scripts.campaign.submarkets.DS_OpenMarketPlugin;
import data.scripts.util.DS_Util;
import data.scripts.variants.DS_Database;
import data.scripts.variants.DS_WeaponGrouper;
import exerelin.campaign.SectorManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Level;
import org.json.JSONException;
import org.json.JSONObject;
import org.lwjgl.opengl.Display;

public class DSModPlugin extends BaseModPlugin {

    public static boolean Module_MarketIntegration = true;
    public static boolean Module_ProceduralVariants = false;
    public static boolean Module_RareShips = false;
    public static boolean Module_ScaledVariants = true;

    public static boolean SHOW_DEBUG_INFO = false;

    public static boolean blackrockExists = false;
    public static boolean checkMemory = false;
    public static boolean citadelExists = false;
    public static boolean diableExists = false;
    public static boolean exigencyExists = false;
    public static boolean hasSWP = false;
    public static boolean hasUnderworld = false;
    public static boolean imperiumExists = false;
    public static boolean isExerelin = false;
    public static boolean junkPiratesExists = false;
    public static boolean mayorateExists = false;
    public static boolean oraExists = false;
    public static boolean scyExists = false;
    public static boolean shadowyardsExists = false;
    public static boolean templarsExists = false;
    public static boolean tiandongExists = false;

    private static final Comparator<MarketAPI> MARKET_SIZE =
                                               new Comparator<MarketAPI>() {
                                                   @Override
                                                   public int compare(MarketAPI market1, MarketAPI market2) {
                                                       int cmp = Integer.compare(market1.getSize(), market2.getSize());
                                                       if (cmp == 0) {
                                                           return market1.getId().compareTo(market2.getId());
                                                       }
                                                       return cmp;
                                                   }
                                               };

    private static final String SETTINGS_FILE = "DYNASECTOR_OPTIONS.ini";

    public static void refresh() {
        try {
            Display.processMessages();
        } catch (Throwable t) {
        }
    }

    public static void removeScriptAndListener(Class<?> oldClass, Class<?> newClass) {
        Global.getSector().removeScriptsOfClass(oldClass);
        CampaignEventListener listener = null;
        for (CampaignEventListener l : Global.getSector().getAllListeners()) {
            if (oldClass.isInstance(l) && !newClass.isInstance(l)) {
                listener = l;
                break;
            }
        }
        if (listener != null) {
            Global.getSector().removeListener(listener);
        }
    }

    public static void syncDSScripts() {
        if (!Global.getSector().hasScript(DS_FleetInjector.class)) {
            Global.getSector().addScript(new DS_FleetInjector());
        }
        if (!Global.getSector().hasScript(DS_Profiler.class)) {
            Global.getSector().addScript(new DS_Profiler());
        }
        removeScriptAndListener(MercFleetManager.class, DS_MercFleetManager.class);
        if (!Global.getSector().hasScript(DS_MercFleetManager.class)) {
            Global.getSector().addScript(new DS_MercFleetManager());
        }
        removeScriptAndListener(LuddicPathFleetManager.class, DS_LuddicPathFleetManager.class);
        if (!Global.getSector().hasScript(DS_LuddicPathFleetManager.class)) {
            Global.getSector().addScript(new DS_LuddicPathFleetManager());
        }
        removeScriptAndListener(PirateFleetManager.class, DS_PirateFleetManager.class);
        if (!Global.getSector().hasScript(DS_PirateFleetManager.class)) {
            Global.getSector().addScript(new DS_PirateFleetManager());
        }
        removeScriptAndListener(BountyPirateFleetManager.class, DS_BountyPirateFleetManager.class);
        if (!Global.getSector().hasScript(DS_BountyPirateFleetManager.class)) {
            Global.getSector().addScript(new DS_BountyPirateFleetManager());
        }
        removeScriptAndListener(CoreScript.class, DS_CoreScript.class);
        if (!Global.getSector().hasScript(DS_CoreScript.class)) {
            Global.getSector().addScript(new DS_CoreScript());
        }
    }

    public static void syncDSScriptsExerelin() {
        if (!Global.getSector().hasScript(DS_FleetInjector.class)) {
            Global.getSector().addScript(new DS_FleetInjector());
        }
        if (!Global.getSector().hasScript(DS_Profiler.class)) {
            Global.getSector().addScript(new DS_Profiler());
        }
        removeScriptAndListener(MercFleetManager.class, DS_MercFleetManager.class);
        if (!Global.getSector().hasScript(DS_MercFleetManager.class)) {
            Global.getSector().addScript(new DS_MercFleetManager());
        }
        removeScriptAndListener(LuddicPathFleetManager.class, DS_LuddicPathFleetManager.class);
        if (!Global.getSector().hasScript(DS_LuddicPathFleetManager.class)) {
            Global.getSector().addScript(new DS_LuddicPathFleetManager());
        }
        removeScriptAndListener(PirateFleetManager.class, DS_PirateFleetManager.class);
        if (!Global.getSector().hasScript(DS_PirateFleetManager.class)) {
            Global.getSector().addScript(new DS_PirateFleetManager());
        }
        removeScriptAndListener(BountyPirateFleetManager.class, DS_BountyPirateFleetManager.class);
        if (!Global.getSector().hasScript(DS_BountyPirateFleetManager.class)) {
            Global.getSector().addScript(new DS_BountyPirateFleetManager());
        }
        removeScriptAndListener(CoreScript.class, DS_CoreScript.class);
        if (!Global.getSector().hasScript(DS_CoreScript.class)) {
            Global.getSector().addScript(new DS_CoreScript());
        }
    }

    private static void loadSettings() throws IOException, JSONException {
        JSONObject settings = Global.getSettings().loadJSON(SETTINGS_FILE);

        Module_MarketIntegration = settings.getBoolean("marketIntegration");
        Module_ProceduralVariants = settings.getBoolean("proceduralVariants");
        Module_RareShips = settings.getBoolean("rareShips");
        Module_ScaledVariants = settings.getBoolean("scaledVariants");

        checkMemory = settings.getBoolean("checkMemory");
    }

    @Override
    public void configureXStream(XStream x) {
        x.alias("DS_FltInj", DS_FleetInjector.class);
        x.alias("DS_BlkMP", DS_BlackMarketPlugin.class);
        x.alias("DS_MilSP", DS_MilitarySubmarketPlugin.class);
        x.alias("DS_OpenMP", DS_OpenMarketPlugin.class);
        x.alias("DS_FltPro", DS_Profiler.class);
        x.alias("DS_PBtyE", DS_PersonBountyEvent.class);
        x.alias("DS_FHE", DS_FactionHostilityEvent.class);
        x.alias("DS_BtyT", BountyType.class);
        x.alias("DS_BtyPFM", DS_BountyPirateFleetManager.class);
        x.alias("DS_LPathFM", DS_LuddicPathFleetManager.class);
        x.alias("DS_MercFM", DS_MercFleetManager.class);
        x.alias("DS_PirFM", DS_PirateFleetManager.class);
        x.alias("DS_PatrolFM", DS_PatrolFleetManager.class);
        x.alias("DS_CS", DS_CoreScript.class);
        DS_ModPluginAlt.configureModdedXStream(x);
    }

    @Override
    public void onApplicationLoad() throws Exception {
        boolean hasLazyLib = Global.getSettings().getModManager().isModEnabled("lw_lazylib");
        if (!hasLazyLib) {
            throw new RuntimeException("DynaSector requires LazyLib!");
        }

        isExerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        hasSWP = Global.getSettings().getModManager().isModEnabled("swp");
        hasUnderworld = Global.getSettings().getModManager().isModEnabled("underworld");

        try {
            loadSettings();
        } catch (IOException | JSONException e) {
            Global.getLogger(DSModPlugin.class).log(Level.ERROR, "Settings loading failed! " + e.getMessage());
        }

        if (Global.getSettings().getModManager().isModEnabled("dr_ssp")) {
            String[] verString = Global.getSettings().getModManager().getModSpec("dr_ssp").getVersion().split("\\.");
            int major = Integer.parseInt(verString[0]);
            int minor = Integer.parseInt(verString[1]);
            if (major <= 3 && minor < 8) {
                throw new RuntimeException("Dynasector is not compatible with Starsector+");
            }
        }

        DS_Database.init();
        DS_WeaponGrouper.init();

        imperiumExists = Global.getSettings().getModManager().isModEnabled("Imperium");
        templarsExists = Global.getSettings().getModManager().isModEnabled("Templars");
        blackrockExists = Global.getSettings().getModManager().isModEnabled("blackrock_driveyards");
        exigencyExists = Global.getSettings().getModManager().isModEnabled("exigency");
        citadelExists = Global.getSettings().getModManager().isModEnabled("Citadel");
        shadowyardsExists = Global.getSettings().getModManager().isModEnabled("shadow_ships");
        mayorateExists = Global.getSettings().getModManager().isModEnabled("mayorate");
        junkPiratesExists = Global.getSettings().getModManager().isModEnabled("junk_pirates_release");
        scyExists = Global.getSettings().getModManager().isModEnabled("SCY");
        oraExists = Global.getSettings().getModManager().isModEnabled("ORA");
        tiandongExists = Global.getSettings().getModManager().isModEnabled("THI");
        diableExists = Global.getSettings().getModManager().isModEnabled("diableavionics");

        FleetCompositionDoctrineAPI kol = Global.getSector().getFaction(Factions.KOL).getCompositionDoctrine();
        kol.setSmall(8.5f);
        kol.setFast(4.5f);
        kol.setMedium(11f);
        kol.setLarge(4.5f);
        kol.setCapital(1f);
        kol.setEscortSmallFraction(1f);
        kol.setEscortMediumFraction(1f);
        kol.setCombatFreighterProbability(0.375f);
        kol.setMinPointsForCombatCapital(22f);
        kol.setMinPointsForLargeCarrier(22f);
        kol.setSmallCarrierProbability(0.25f);
        kol.setMediumCarrierProbability(0.25f);
        kol.setLargeCarrierProbability(0.25f);
        kol.setOfficersPerPoint(0.35f);
        kol.setOfficerLevelPerPoint(0.35f);
        kol.setOfficerLevelBase(6f);
        kol.setOfficerLevelVariance(0.3f);
        kol.setCommanderSkillsPerLevel(0.25f);
        kol.setCommanderSkillChance(0.5f);
        kol.setCommanderSkillLevelPerLevel(0.225f);
        kol.getCommanderSkills().put("officer_management", 10f);
        kol.getCommanderSkills().put("coordinated_maneuvers", 5f);
        kol.getCommanderSkills().put("fighter_doctrine", 10f);
        kol.getCommanderSkills().put("electronic_warfare", 5f);
    }

    @Override
    public void onGameLoad(boolean newGame) {
        if (!newGame) {
            Boolean inUse = (Boolean) Global.getSector().getPersistentData().get("DS_in_use");
            if (inUse == null || inUse == false) {
                throw new RuntimeException("You cannot add DynaSector to an existing campaign!");
            }
        }

        if (isExerelin) {
            syncDSScriptsExerelin();
        } else {
            syncDSScripts();
        }

        DS_Database.checkForUnusedFighters(Global.getSettings());
        DS_Database.checkForUnusedHulls(Global.getSettings());
        DS_Database.checkForUnusedWeapons(Global.getSettings());
    }

    @Override
    public void onNewGame() {
        Global.getSector().getPersistentData().put("DS_in_use", true);

        if (isExerelin) {
            syncDSScriptsExerelin();
        } else {
            syncDSScripts();
        }

        long seed = Global.getSector().getSeedString().hashCode();
        DS_FleetInjector.getInjector().setMasterSeed(seed);
    }

    @Override
    public void onNewGameAfterEconomyLoad() {
        MarketAPI market;
        if (isExerelin && !SectorManager.getCorvusMode()) {
            return;
        }

        /*
         * VANILLA SYSTEMS
         */

        /* Arcadia */
        market = Global.getSector().getEconomy().getMarket("arcadia_station");
        DS_Util.setMarketInfluence(market, Factions.INDEPENDENT);
        market = Global.getSector().getEconomy().getMarket("agreus");
        DS_Util.setMarketInfluence(market, Factions.HEGEMONY);

        /* Askonia */
        market = Global.getSector().getEconomy().getMarket("cruor");
        DS_Util.setMarketInfluence(market, Factions.PIRATES);
        market = Global.getSector().getEconomy().getMarket("nortia");
        DS_Util.setMarketInfluence(market, Factions.DIKTAT);
        market = Global.getSector().getEconomy().getMarket("umbra");
        DS_Util.setMarketInfluence(market, Factions.DIKTAT);
        market = Global.getSector().getEconomy().getMarket("outpost_tiger"); // SRA
        DS_Util.setMarketInfluence(market, Factions.DIKTAT);

        /* Eos Exodus */
        market = Global.getSector().getEconomy().getMarket("baetis");
        DS_Util.setMarketInfluence(market, Factions.LUDDIC_CHURCH);
        market = Global.getSector().getEconomy().getMarket("hesperus");
        DS_Util.setMarketInfluence(market, Factions.KOL);

        /* Hybrasil */
        market = Global.getSector().getEconomy().getMarket("cethlenn");
        DS_Util.setMarketInfluence(market, Factions.TRITACHYON);
        market = Global.getSector().getEconomy().getMarket("donn");
        DS_Util.setMarketInfluence(market, Factions.TRITACHYON);

        /* Isirah */
        market = Global.getSector().getEconomy().getMarket("station_kapteyn");
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);

        /* Kumari Kandam */
        market = Global.getSector().getEconomy().getMarket("olinadu");
        DS_Util.setMarketInfluence(market, Factions.LUDDIC_PATH);
        market = Global.getSector().getEconomy().getMarket("kanni");
        DS_Util.setMarketInfluence(market, Factions.LUDDIC_PATH);

        /* Magec */
        market = Global.getSector().getEconomy().getMarket("tibicena");
        DS_Util.setMarketInfluence(market, Factions.INDEPENDENT);

        /* Mayasura */
        market = Global.getSector().getEconomy().getMarket("mairaath_abandoned_station2");
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);
        market = Global.getSector().getEconomy().getMarket("port_tse");
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);

        /* Samarra */
        market = Global.getSector().getEconomy().getMarket("orthrus");
        DS_Util.setMarketInfluence(market, Factions.HEGEMONY);

        /* Thule */
        market = Global.getSector().getEconomy().getMarket("eldfell");
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);
        market = Global.getSector().getEconomy().getMarket("thule_pirate_station");
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);

        /* Westernesse */
        market = Global.getSector().getEconomy().getMarket("ailmar");
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);

        /* Yma */
        market = Global.getSector().getEconomy().getMarket("qaras");
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);
        market = Global.getSector().getEconomy().getMarket("salamanca");
        DS_Util.setMarketInfluence(market, Factions.PIRATES);
        market = Global.getSector().getEconomy().getMarket("udana_stations"); // SRA
        DS_Util.setMarketInfluence(market, Factions.PIRATES);

        /* Zagan */
        market = Global.getSector().getEconomy().getMarket("ilm");
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);

        /*
         * SHADOWYARDS SYSTEMS
         */

        /* Anar */
        market = Global.getSector().getEconomy().getMarket("melancholia"); // SRA
        DS_Util.setMarketInfluence(market, "shadow_industry");
        market = Global.getSector().getEconomy().getMarket("berins_stash"); // SRA
        DS_Util.setMarketInfluence(market, "shadow_industry");

        /* Gigas */
        market = Global.getSector().getEconomy().getMarket("auris_grip"); // SRA
        DS_Util.setMarketInfluence(market, "shadow_industry");
        market = Global.getSector().getEconomy().getMarket("lance_base"); // SRA
        DS_Util.setMarketInfluence(market, "shadow_industry");

        /* Yajna */
        market = Global.getSector().getEconomy().getMarket("pillager"); // SRA
        DS_Util.setMarketInfluence(market, "shadow_industry", Factions.PERSEAN);
        market = Global.getSector().getEconomy().getMarket("jnana"); // SRA
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);
        market = Global.getSector().getEconomy().getMarket("hotri"); // SRA
        DS_Util.setMarketInfluence(market, "shadow_industry", Factions.PERSEAN);
        market = Global.getSector().getEconomy().getMarket("mantra"); // SRA
        DS_Util.setMarketInfluence(market, "shadow_industry");
        market = Global.getSector().getEconomy().getMarket("karma"); // SRA
        DS_Util.setMarketInfluence(market, Factions.PERSEAN);
        market = Global.getSector().getEconomy().getMarket("maks_hole"); // SRA
        DS_Util.setMarketInfluence(market, "shadow_industry", Factions.PERSEAN);

        /*
         * SCY SYSTEMS
         */

        /* Acheron */
        market = Global.getSector().getEconomy().getMarket("SCY_prismFreeport"); // SCY
        DS_Util.setMarketInfluence(market, "SCY");
        market = Global.getSector().getEconomy().getMarket("SCY_piratePort"); // SCY
        DS_Util.setMarketInfluence(market, "SCY", Factions.INDEPENDENT);

        /*
         * ORA SYSTEMS
         */

        /* Valis */
        market = Global.getSector().getEconomy().getMarket("ora_tschai"); // ORA
        DS_Util.setMarketInfluence(market, "ORA");

        /*
         * BLACKROCK SYSTEMS
         */

        /* Gneiss */
        market = Global.getSector().getEconomy().getMarket("lydia_market"); // BRDY
        DS_Util.setMarketInfluence(market, "blackrock_driveyards");
        market = Global.getSector().getEconomy().getMarket("preclusion_market"); // BRDY
        DS_Util.setMarketInfluence(market, "blackrock_driveyards");

        /* Rama */
        market = Global.getSector().getEconomy().getMarket("bharata_market"); // BRDY
        DS_Util.setMarketInfluence(market, Factions.HEGEMONY);
        market = Global.getSector().getEconomy().getMarket("staloplanet_market"); // BRDY
        DS_Util.setMarketInfluence(market, "blackrock_driveyards");
        market = Global.getSector().getEconomy().getMarket("thalm_post_market"); // BRDY
        DS_Util.setMarketInfluence(market, "blackrock_driveyards");
        market = Global.getSector().getEconomy().getMarket("limboplanet_market"); // BRDY
        DS_Util.setMarketInfluence(market, "blackrock_driveyards", Factions.HEGEMONY);
    }

    @Override
    public void onNewGameAfterTimePass() {
        Map<String, Float> commodityDemand = new HashMap<>(50);
        Map<String, Float> commodityNonConsumingDemand = new HashMap<>(50);
        Map<String, Float> commoditySupply = new HashMap<>(50);
        Map<String, Float> commodityGreed = new HashMap<>(50);
        Map<String, Float> commodityStockpile = new HashMap<>(50);
        Map<String, Float> commodityAmount = new HashMap<>(50);
        Map<String, Float> commodityPrice = new HashMap<>(50);

        List<MarketAPI> sortedMarkets = new ArrayList<>(Global.getSector().getEconomy().getMarketsCopy());
        Collections.sort(sortedMarkets, MARKET_SIZE);
        for (MarketAPI market : sortedMarkets) {
            Global.getLogger(DSModPlugin.class).info(
                    market.getName() + " (" + market.getContainingLocation().getName() + ")" + " - " +
                    market.getFaction().getDisplayName() + " - " + "Size " + market.getSize());
            if (market.hasSubmarket(Submarkets.GENERIC_MILITARY)) {
                Global.getLogger(DSModPlugin.class).info("---- Has Military Market");
            }
            if (market.hasCondition(Conditions.DECIVILIZED)) {
                Global.getLogger(DSModPlugin.class).info("---- Is Decivilized");
            }
            if (market.hasCondition(Conditions.ORBITAL_STATION)) {
                Global.getLogger(DSModPlugin.class).info("---- Has Orbital Station");
            }
            if (market.hasCondition(Conditions.SPACEPORT)) {
                Global.getLogger(DSModPlugin.class).info("---- Has Spaceport");
            }
            if (market.hasCondition(Conditions.HEADQUARTERS)) {
                Global.getLogger(DSModPlugin.class).info("---- Is Headquarters");
            }
            if (market.hasCondition(Conditions.REGIONAL_CAPITAL)) {
                Global.getLogger(DSModPlugin.class).info("---- Is Regional Capital");
            }
            for (CommodityOnMarketAPI commodity : market.getAllCommodities()) {
                String id = commodity.getId();

                Float demand = commodityDemand.get(id);
                if (demand == null) {
                    demand = 0f;
                }
                demand += commodity.getDemand().getDemandValue();
                commodityDemand.put(id, demand);

                Float nonConsumingDemand = commodityNonConsumingDemand.get(id);
                if (nonConsumingDemand == null) {
                    nonConsumingDemand = 0f;
                }
                nonConsumingDemand += commodity.getDemand().getNonConsumingDemandValue();
                commodityNonConsumingDemand.put(id, nonConsumingDemand);

                Float supply = commoditySupply.get(id);
                if (supply == null) {
                    supply = 0f;
                }
                supply += commodity.getSupplyValue();
                commoditySupply.put(id, supply);

                Float greed = commodityGreed.get(id);
                if (greed == null) {
                    greed = 0f;
                }
                greed += commodity.getGreedValue();
                commodityGreed.put(id, greed);

                Float stockpile = commodityStockpile.get(id);
                if (stockpile == null) {
                    stockpile = 0f;
                }
                stockpile += commodity.getStockpile();
                commodityStockpile.put(id, stockpile);

                Float amount = commodityAmount.get(id);
                if (amount == null) {
                    amount = 0f;
                }
                amount += commodity.getAboveDemandStockpile();
                commodityAmount.put(id, amount);

                Float price = commodityPrice.get(id);
                if (price == null) {
                    price = 0f;
                }
                price += market.getSupplyPrice(id, commodity.getAboveDemandStockpile(), true);
                commodityPrice.put(id, price);
            }
        }

        Global.getLogger(DSModPlugin.class).info("-= Sector Economy Report =-");
        Global.getLogger(DSModPlugin.class).info("    - Demand Data -");
        for (Map.Entry<String, Float> entry : commodityDemand.entrySet()) {
            String id = entry.getKey();
            float amount = entry.getValue();
            float average = amount / sortedMarkets.size();
            Global.getLogger(DSModPlugin.class).info("        ~ " +
                    Global.getSector().getEconomy().getCommoditySpec(id).getName() + ": " +
                    amount + ", " + average + " average");
        }
        Global.getLogger(DSModPlugin.class).info("    - Non-Consuming Demand Data -");
        for (Map.Entry<String, Float> entry : commodityNonConsumingDemand.entrySet()) {
            String id = entry.getKey();
            float amount = entry.getValue();
            float average = amount / sortedMarkets.size();
            Global.getLogger(DSModPlugin.class).info("        ~ " +
                    Global.getSector().getEconomy().getCommoditySpec(id).getName() + ": " +
                    amount + ", " + average + " average");
        }
        Global.getLogger(DSModPlugin.class).info("    - Supply Data -");
        for (Map.Entry<String, Float> entry : commoditySupply.entrySet()) {
            String id = entry.getKey();
            float amount = entry.getValue();
            float average = amount / sortedMarkets.size();
            Global.getLogger(DSModPlugin.class).info("        ~ " +
                    Global.getSector().getEconomy().getCommoditySpec(id).getName() + ": " +
                    amount + ", " + average + " average");
        }
        Global.getLogger(DSModPlugin.class).info("    - Greed Data -");
        for (Map.Entry<String, Float> entry : commodityGreed.entrySet()) {
            String id = entry.getKey();
            float amount = entry.getValue();
            float average = amount / sortedMarkets.size();
            Global.getLogger(DSModPlugin.class).info("        ~ " +
                    Global.getSector().getEconomy().getCommoditySpec(id).getName() + ": " +
                    amount + ", " + average + " average");
        }
        Global.getLogger(DSModPlugin.class).info("    - Stockpile Data -");
        for (Map.Entry<String, Float> entry : commodityStockpile.entrySet()) {
            String id = entry.getKey();
            float amount = entry.getValue();
            float average = amount / sortedMarkets.size();
            Global.getLogger(DSModPlugin.class).info("        ~ " +
                    Global.getSector().getEconomy().getCommoditySpec(id).getName() + ": " +
                    amount + ", " + average + " average");
        }
        Global.getLogger(DSModPlugin.class).info("    - Amount Data -");
        for (Map.Entry<String, Float> entry : commodityAmount.entrySet()) {
            String id = entry.getKey();
            float amount = entry.getValue();
            float average = amount / sortedMarkets.size();
            Global.getLogger(DSModPlugin.class).info("        ~ " +
                    Global.getSector().getEconomy().getCommoditySpec(id).getName() + ": " +
                    amount + ", " + average + " average");
        }
        Global.getLogger(DSModPlugin.class).info("    - Price Data -");
        for (Map.Entry<String, Float> entry : commodityPrice.entrySet()) {
            String id = entry.getKey();
            float average = entry.getValue() / commodityAmount.get(id);
            Global.getLogger(DSModPlugin.class).info("        ~ " +
                    Global.getSector().getEconomy().getCommoditySpec(id).getName() + ": " +
                    average + " average");
        }

        CampaignEventManagerAPI eventManager = Global.getSector().getEventManager();
        for (CampaignEventPlugin e : eventManager.getOngoingEvents()) {
            if (e instanceof DS_PersonBountyEvent) {
                DS_PersonBountyEvent pbe = (DS_PersonBountyEvent) e;
                pbe.setElapsedDays(pbe.getElapsedDays() * (float) Math.random() * 0.25f);

            }
        }

        Global.getSector().setPaused(true);
    }
}
