package data.scripts.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CampaignTerrainAPI;
import com.fs.starfarer.api.campaign.CustomCampaignEntityAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.CoreScript;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin.DerelictShipData;
import com.fs.starfarer.api.impl.campaign.TOffAlarm;
import com.fs.starfarer.api.impl.campaign.events.BaseEventPlugin.MarketFilter;
import com.fs.starfarer.api.impl.campaign.ids.Drops;
import com.fs.starfarer.api.impl.campaign.ids.Entities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.procgen.SalvageEntityGenDataSpec.DropData;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.PerShipData;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.ShipCondition;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.ShipRecoverySpecialData;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import com.fs.starfarer.api.impl.campaign.terrain.DebrisFieldTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.DebrisFieldTerrainPlugin.DebrisFieldParams;
import com.fs.starfarer.api.impl.campaign.terrain.DebrisFieldTerrainPlugin.DebrisFieldSource;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;
import com.fs.starfarer.api.loading.HullModSpecAPI;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.DS_Defs;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.util.vector.Vector2f;

public class DS_CoreScript extends CoreScript {

    public static void generateOrAddToDebrisFieldFromBattleDS(CampaignFleetAPI primaryWinner, BattleAPI battle) {
        if (primaryWinner == null) {
            return;
        }
        LocationAPI location = primaryWinner.getContainingLocation();
        if (location == null) {
            return;
        }

        //if (location.isHyperspace()) return;
        boolean allowDebris = !location.isHyperspace();

        boolean playerInvolved = battle.isPlayerInvolved();

        DropData misc = new DropData();
        int miscChances = 0;

        List<DropData> cargoList = new ArrayList<>(20);

        float battleDebrisChance = 1f;
        float battleDebrisScale = 1f;
        float battleDerelictsChance = 1f;
        float battleDerelictsScale = 1f;

        for (CampaignFleetAPI fleet : battle.getSnapshotSideOne()) {
            battleDebrisChance *= fleet.getStats().getDynamic().getValue(DS_Defs.STAT_BATTLE_DEBRIS_CHANCE, 1f);
            battleDebrisScale *= fleet.getStats().getDynamic().getValue(DS_Defs.STAT_BATTLE_DEBRIS_SCALE, 1f);
            battleDerelictsChance *= fleet.getStats().getDynamic().getValue(DS_Defs.STAT_BATTLE_DERELICTS_CHANCE, 1f);
            battleDerelictsScale *= fleet.getStats().getDynamic().getValue(DS_Defs.STAT_BATTLE_DERELICTS_SCALE, 1f);
        }
        for (CampaignFleetAPI fleet : battle.getSnapshotSideTwo()) {
            battleDebrisChance *= fleet.getStats().getDynamic().getValue(DS_Defs.STAT_BATTLE_DEBRIS_CHANCE, 1f);
            battleDebrisScale *= fleet.getStats().getDynamic().getValue(DS_Defs.STAT_BATTLE_DEBRIS_SCALE, 1f);
            battleDerelictsChance *= fleet.getStats().getDynamic().getValue(DS_Defs.STAT_BATTLE_DERELICTS_CHANCE, 1f);
            battleDerelictsScale *= fleet.getStats().getDynamic().getValue(DS_Defs.STAT_BATTLE_DERELICTS_SCALE, 1f);
        }

        WeightedRandomPicker<FleetMemberAPI> recoverySpecialChoices = new WeightedRandomPicker<>();

        Vector2f com = new Vector2f();
        float count = 0f;
        float fpDestroyed = 0;
        for (CampaignFleetAPI fleet : battle.getSnapshotSideOne()) {
            count++;
            com.x += fleet.getLocation().x;
            com.y += fleet.getLocation().y;

            float debrisChance = fleet.getStats().getDynamic().getValue(DS_Defs.STAT_FLEET_DEBRIS_CHANCE, 1f);
            debrisChance *= battleDebrisChance;
            float debrisScale = fleet.getStats().getDynamic().getValue(DS_Defs.STAT_FLEET_DEBRIS_SCALE, 1f);
            debrisScale *= battleDebrisScale;
            boolean makeDebris = Math.random() <= debrisChance;

            float derelictsChance = fleet.getStats().getDynamic().getValue(DS_Defs.STAT_FLEET_DERELICTS_CHANCE, 1f);
            derelictsChance *= battleDerelictsChance;
            float derelictsScale = fleet.getStats().getDynamic().getValue(DS_Defs.STAT_FLEET_DERELICTS_SCALE, 1f);
            derelictsScale *= battleDerelictsScale;
            boolean makeDerelicts = Math.random() <= derelictsChance;

            float fpForThisFleet = 0;
            for (FleetMemberAPI loss : Misc.getSnapshotMembersLost(fleet)) {
                if (!loss.getVariant().hasTag(Tags.SHIP_RECOVERABLE)) { // was not recoverable by player
                    if (makeDerelicts && Math.random() <= derelictsScale &&
                            !loss.getHullSpec().getHints().contains(ShipTypeHints.UNBOARDABLE)) {
                        recoverySpecialChoices.add(loss);
                    }
                } else {
                    loss.getVariant().removeTag(Tags.SHIP_RECOVERABLE);
                }
                if (makeDebris) {
                    fpDestroyed += loss.getFleetPointCost() * debrisScale;
                    fpForThisFleet += loss.getFleetPointCost() * debrisScale;
                    if (Math.random() <= debrisScale &&
                            allowDebris && !fleet.isPlayerFleet()) {
                        addMiscToDropData(misc, loss, true, true, true);
                        miscChances++;
                    }
                }
            }

            if (makeDebris && allowDebris && !fleet.isPlayerFleet()) {
                DropData cargo = new DropData();
                float cargoValue = fpForThisFleet * Global.getSettings().getFloat("salvageValuePerFP");
                cargoValue *= Global.getSettings().getFloat("salvageDebrisFieldFraction");
                if (cargoValue >= 1) {
                    for (String cid : getCargoCommodities(fleet.getCargo())) {
                        cargo.addCommodity(cid, 1f);
                    }
                    cargo.value = (int) cargoValue;
                    cargo.chances = 1;
                    cargoList.add(cargo);
                }
            }
        }
        for (CampaignFleetAPI fleet : battle.getSnapshotSideTwo()) {
            count++;
            com.x += fleet.getLocation().x;
            com.y += fleet.getLocation().y;

            float debrisChance = fleet.getStats().getDynamic().getValue(DS_Defs.STAT_FLEET_DEBRIS_CHANCE, 1f);
            debrisChance *= battleDebrisChance;
            float debrisScale = fleet.getStats().getDynamic().getValue(DS_Defs.STAT_FLEET_DEBRIS_SCALE, 1f);
            debrisScale *= battleDebrisScale;
            boolean makeDebris = Math.random() <= debrisChance;

            float derelictsChance = fleet.getStats().getDynamic().getValue(DS_Defs.STAT_FLEET_DERELICTS_CHANCE, 1f);
            derelictsChance *= battleDerelictsChance;
            float derelictsScale = fleet.getStats().getDynamic().getValue(DS_Defs.STAT_FLEET_DERELICTS_SCALE, 1f);
            derelictsScale *= battleDerelictsScale;
            boolean makeDerelicts = Math.random() <= derelictsChance;

            float fpForThisFleet = 0;
            for (FleetMemberAPI loss : Misc.getSnapshotMembersLost(fleet)) {
                if (!loss.getVariant().hasTag(Tags.SHIP_RECOVERABLE)) { // was not recoverable by player
                    if (makeDerelicts && Math.random() <= derelictsScale &&
                            !loss.getHullSpec().getHints().contains(ShipTypeHints.UNBOARDABLE)) {
                        recoverySpecialChoices.add(loss);
                    }
                } else {
                    loss.getVariant().removeTag(Tags.SHIP_RECOVERABLE);
                }
                if (makeDebris) {
                    fpDestroyed += loss.getFleetPointCost() * debrisScale;
                    fpForThisFleet += loss.getFleetPointCost() * debrisScale;
                    if (Math.random() <= debrisScale &&
                            allowDebris && !fleet.isPlayerFleet()) {
                        addMiscToDropData(misc, loss, true, true, true);
                        miscChances++;
                    }
                }
            }
            if (makeDebris && allowDebris && !fleet.isPlayerFleet()) {
                DropData cargo = new DropData();
                float cargoValue = fpForThisFleet * Global.getSettings().getFloat("salvageValuePerFP");
                cargoValue *= Global.getSettings().getFloat("salvageDebrisFieldFraction");
                if (cargoValue >= 1) {
                    for (String cid : getCargoCommodities(fleet.getCargo())) {
                        cargo.addCommodity(cid, 1f);
                    }
                    cargo.value = (int) cargoValue;
                    cargo.chances = 1;
                    cargoList.add(cargo);
                }
            }
        }
        //Global.getSector().getPlayerFleet().getLocation()
        if (count <= 0) {
            return;
        }

        com.scale(1f / count);

        // spawn some derelict ships, maybe. do this here, regardless of whether the value is enough to
        // warrant a debris field
        float numShips = recoverySpecialChoices.getItems().size();
        float chanceDerelict = 1f - 10f / (numShips + 10f);
        //chanceNothing = 0f;
        //Vector2f com = battle.computeCenterOfMass();

        int max = 3;
        if (playerInvolved) {
            max = 2;
            chanceDerelict *= 0.25f;
        }
        for (int i = 0; i < max && !recoverySpecialChoices.isEmpty(); i++) {
            boolean spawnShip = Math.random() < chanceDerelict;
            if (spawnShip) {
                FleetMemberAPI member = recoverySpecialChoices.pickAndRemove();
                DerelictShipData params = new DerelictShipData(new PerShipData(member.getVariant().getHullVariantId(),
                                                                               DerelictShipEntityPlugin.pickBadCondition(
                                                                                       null)), false);
                params.durationDays = DerelictShipEntityPlugin.getBaseDuration(member.getHullSpec().getHullSize());
                CustomCampaignEntityAPI entity = (CustomCampaignEntityAPI) BaseThemeGenerator.addSalvageEntity(
                                        primaryWinner.getContainingLocation(),
                                        Entities.WRECK, Factions.NEUTRAL, params);
                entity.addTag(Tags.EXPIRES);
                SalvageSpecialAssigner.assignSpecialForBattleWreck(entity);

//				entity.getLocation().x = com.x + (50f - (float) Math.random() * 100f);
//				entity.getLocation().y = com.y + (50f - (float) Math.random() * 100f);
                float angle = (float) Math.random() * 360f;
                float speed = 10f + 10f * (float) Math.random();
                Vector2f vel = Misc.getUnitVectorAtDegreeAngle(angle);
                vel.scale(speed);
                entity.getVelocity().set(vel);

                entity.getLocation().x = com.x + vel.x * 3f;
                entity.getLocation().y = com.y + vel.y * 3f;
            }
        }

        float salvageValue = fpDestroyed * Global.getSettings().getFloat("salvageValuePerFP");
        if (Misc.isEasy()) {
            salvageValue *= Global.getSettings().getFloat("easySalvageMult");
        }

        salvageValue *= Global.getSettings().getFloat("salvageDebrisFieldFraction");
        float salvageXP = salvageValue * 0.1f;

        float minForField = Global.getSettings().getFloat("minSalvageValueForDebrisField");
        //if (playerInvolved) minForField *= 6f;
        if (playerInvolved) {
            minForField = 2500f + (float) Math.random() * 1000f;
        }

        if (salvageValue < minForField || !allowDebris) {
            return;
        }

        CampaignTerrainAPI debris = null;
        for (CampaignTerrainAPI curr : primaryWinner.getContainingLocation().getTerrainCopy()) {
            if (curr.getPlugin() instanceof DebrisFieldTerrainPlugin) {
                DebrisFieldTerrainPlugin plugin = (DebrisFieldTerrainPlugin) curr.getPlugin();
                if (plugin.params.source == DebrisFieldSource.BATTLE &&
                        plugin.params.density >= 1f &&
                        plugin.containsPoint(com, 100f)) {
                    debris = curr;
                    break;
                }
            }
        }

        if (debris == null) {
            DebrisFieldParams params = new DebrisFieldParams(
                              200f, // field radius - should not go above 1000 for performance reasons
                              1f, // density, visual - affects number of debris pieces
                              1f, // duration in days
                              1f); // days the field will keep generating glowing pieces
            params.source = DebrisFieldSource.BATTLE;
            params.baseSalvageXP = (long) salvageXP; // base XP for scavenging in field

            debris = (CampaignTerrainAPI) Misc.addDebrisField(location, params, null);

            // makes the debris field always visible on map/sensors and not give any xp or notification on being discovered
            //debris.setSensorProfile(null);
            //debris.setDiscoverable(null);
            // makes it discoverable and give 200 xp on being found
            // sets the range at which it can be detected (as a sensor contact) to 2000 units
            //debris.setDiscoverable(true);
            //debris.setDiscoveryXP(200f);
            //debris.setSensorProfile(1f);
            //debris.getDetectedRangeMod().modifyFlat("gen", 2000);
            debris.setDiscoverable(null);
            debris.setDiscoveryXP(null);
            //debris.setSensorProfile(1f);
            //debris.getDetectedRangeMod().modifyFlat("gen", 1000);

            debris.getLocation().set(com);

            debris.getDropValue().clear();
            debris.getDropRandom().clear();
        }

        DebrisFieldTerrainPlugin plugin = (DebrisFieldTerrainPlugin) debris.getPlugin();
        DropData basicDrop = null;
        for (DropData data : debris.getDropValue()) {
            if (Drops.BASIC.equals(data.group)) {
                basicDrop = data;
                break;
            }
        }

        // since we're only adding to fields with density 1 (i.e. ones the player hasn't salvaged)
        // no need to worry about how density would affect the salvage value of what we're adding
        if (basicDrop == null) {
            basicDrop = new DropData();
            basicDrop.group = Drops.BASIC;
            debris.addDropValue(basicDrop);
        }
        basicDrop.value += salvageValue;

        if (misc.getCustom() != null) {
            misc.chances = miscChances;

            float total = misc.getCustom().getTotal();
            if (total > 0) {
                misc.addNothing(total);
            }
            debris.addDropRandom(misc);
            //misc.getCustom().print("MISC DROP");
        }

        for (DropData cargo : cargoList) {
            debris.addDropRandom(cargo);
        }

        //if (!battle.isPlayerInvolved()) {
        ShipRecoverySpecialData data = ShipRecoverySpecial.getSpecialData(debris, null, true, false);
        if (data != null && data.ships.size() < 3) {
            float items = recoverySpecialChoices.getTotal();
            float total = items + 25f;
            for (int i = 0; i < 3; i++) {
                if ((float) Math.random() * total < items) {
                    FleetMemberAPI pick = recoverySpecialChoices.pick();
                    if (pick != null) {
                        data.addShip(pick.getVariant().getHullVariantId(), ShipCondition.WRECKED);
                    }
                }
            }
        }
        //}

//		basicDrop = new DropData();
//		basicDrop.group = "misc_test";
//		basicDrop.value = 100000;
//		existing.addDropValue(basicDrop);
        // resize and adjust duration here
        float radius = 100f + (float) Math.min(900, Math.sqrt(basicDrop.value));
        float durationExtra = (float) Math.sqrt(salvageValue) * 0.1f;

        float minDays = DebrisFieldTerrainPlugin.DISSIPATE_DAYS + 1f;
        if (durationExtra < minDays) {
            durationExtra = minDays;
        }

        float time = durationExtra + plugin.params.lastsDays;
        if (time > 30f) {
            time = 30f;
        }

        plugin.params.lastsDays = time;
        plugin.params.glowsDays = time;

        plugin.params.bandWidthInEngine = radius;
        plugin.params.middleRadius = plugin.params.bandWidthInEngine / 2f;

        float range = DebrisFieldTerrainPlugin.computeDetectionRange(plugin.params.bandWidthInEngine);
        debris.getDetectedRangeMod().modifyFlat("gen", range);
    }

    private static void addMiscToDropData(
            DropData data, FleetMemberAPI member, boolean weapons, boolean mods,
            boolean fighters) {
        ShipVariantAPI variant = member.getVariant();

        if (weapons) {
            float p = Global.getSettings().getFloat("salvageWeaponProb");
            for (String slotId : variant.getNonBuiltInWeaponSlots()) {
                String weaponId = variant.getWeaponId(slotId);
                data.addWeapon(weaponId, 1f * p);
            }
        }

        if (mods) {
            float p = Global.getSettings().getFloat("salvageHullmodProb");
            for (String id : member.getVariant().getHullMods()) {
                HullModSpecAPI spec = Global.getSettings().getHullModSpec(id);
                if (spec.isHidden() || spec.isHiddenEverywhere()) {
                    continue;
                }
                if (spec.hasTag(Tags.HULLMOD_NO_DROP)) {
                    continue;
                }
                data.addHullMod(id, 1f * p);
            }
        }

        if (fighters) {
            float p = Global.getSettings().getFloat("salvageWingProb");

            for (String id : member.getVariant().getFittedWings()) {
                FighterWingSpecAPI spec = Global.getSettings().getFighterWingSpec(id);
                if (spec.hasTag(Tags.WING_NO_DROP)) {
                    continue;
                }
                data.addFighterChip(id, 1f * p);
            }
        }

        data.valueMult = Global.getSettings().getFloat("salvageDebrisFieldFraction");
    }

    private final SharedData sharedData;

    public DS_CoreScript() {
        super();
        sharedData = SharedData.getData();
    }

    @Override
    public void reportBattleOccurred(CampaignFleetAPI primaryWinner, BattleAPI battle) {
        generateOrAddToDebrisFieldFromBattleDS(primaryWinner, battle);

        if (!battle.isPlayerInvolved()) {
            return;
        }

        CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
        if (!playerFleet.isValidPlayerFleet()) {
            float fp = 0;
            float crew = 0;
            for (FleetMemberAPI member : Misc.getSnapshotMembersLost(playerFleet)) {
                fp += member.getFleetPointCost();
                crew = member.getMinCrew();
            }
            sharedData.setPlayerPreLosingBattleFP(fp);
            sharedData.setPlayerPreLosingBattleCrew(crew);
            sharedData.setPlayerLosingBattleTimestamp(Global.getSector().getClock().getTimestamp());
        }

        for (final CampaignFleetAPI otherFleet : battle.getNonPlayerSideSnapshot()) {
            MemoryAPI memory = otherFleet.getMemoryWithoutUpdate();
            //if (!playerFleet.isTransponderOn()) {
            //if (!memory.getBoolean(MemFlags.MEMORY_KEY_LOW_REP_IMPACT)) {
            Misc.setFlagWithReason(memory, MemFlags.MEMORY_KEY_MAKE_HOSTILE_WHILE_TOFF, "battle", true, 7f +
                                   (float) Math.random() * 7f);
            //}
            //}
            otherFleet.addScript(new TOffAlarm(otherFleet));

            float fpLost = Misc.getSnapshotFPLost(otherFleet);

            List<MarketAPI> markets = Misc.findNearbyLocalMarkets(otherFleet,
                                                                  Global.getSettings().getFloat("sensorRangeMax") + 500f,
                                                                  new MarketFilter() {
                                                                      @Override
                                                                      public boolean acceptMarket(MarketAPI market) {
                                                                          return market.getFaction().isAtWorst(
                                                                                  otherFleet.getFaction(),
                                                                                  RepLevel.COOPERATIVE);
                                                                      }
                                                                  });

            for (MarketAPI market : markets) {
                MemoryAPI mem = market.getMemoryWithoutUpdate();
                float expire = fpLost;
                if (mem.contains(MemFlags.MEMORY_KEY_PLAYER_HOSTILE_ACTIVITY_NEAR_MARKET)) {
                    expire += mem.getExpire(MemFlags.MEMORY_KEY_PLAYER_HOSTILE_ACTIVITY_NEAR_MARKET);
                }
                if (expire > 180) {
                    expire = 180;
                }
                if (expire > 0) {
                    mem.set(MemFlags.MEMORY_KEY_PLAYER_HOSTILE_ACTIVITY_NEAR_MARKET, true, expire);
                }
            }
        }
    }
}
