package data.scripts.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent.SkillPickPreference;
import com.fs.starfarer.api.loading.FleetCompositionDoctrineAPI;
import com.fs.starfarer.api.plugins.OfficerLevelupPlugin;
import data.scripts.util.DS_Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static data.scripts.variants.DS_FleetRandomizer.PRIORITY;

public class DS_FleetFactory {

    public static CampaignFleetAPI enhancedCreateFleet(FactionAPI faction, int fleetSize, FleetFactoryDelegate delegate) {
        FleetCompositionDoctrineAPI doctrine = faction.getCompositionDoctrine();
        float preSmall = doctrine.getSmall();
        float preFast = doctrine.getFast();
        float preMedium = doctrine.getMedium();
        float preLarge = doctrine.getLarge();
        float preCapital = doctrine.getCapital();
        float preSmallCarrierProbability = doctrine.getSmallCarrierProbability();
        float preMediumCarrierProbability = doctrine.getMediumCarrierProbability();
        float preLargeCarrierProbability = doctrine.getLargeCarrierProbability();

        if (fleetSize > 25 && fleetSize <= 50) {
            doctrine.setSmall(preSmall * 0.5f);
            doctrine.setFast(preFast * 0.5f);
            doctrine.setMedium(preMedium);
            doctrine.setLarge(preLarge * 1.25f);
            doctrine.setCapital(preCapital * 1.5f);
            doctrine.setSmallCarrierProbability(preSmallCarrierProbability * 0.8f);
            doctrine.setMediumCarrierProbability(preMediumCarrierProbability * 0.9f);
            doctrine.setLargeCarrierProbability(preLargeCarrierProbability);
        } else if (fleetSize > 50 && fleetSize <= 100) {
            doctrine.setSmall(preSmall * 0.25f);
            doctrine.setFast(preFast * 0.25f);
            doctrine.setMedium(preMedium * 0.75f);
            doctrine.setLarge(preLarge);
            doctrine.setCapital(preCapital * 1.25f);
            doctrine.setSmallCarrierProbability(preSmallCarrierProbability * 0.5f);
            doctrine.setMediumCarrierProbability(preMediumCarrierProbability * 0.65f);
            doctrine.setLargeCarrierProbability(preLargeCarrierProbability * 0.8f);
        } else if (fleetSize > 100) {
            doctrine.setSmall(preSmall * 0.125f);
            doctrine.setFast(preFast * 0.125f);
            doctrine.setMedium(preMedium * 0.375f);
            doctrine.setLarge(preLarge * 0.75f);
            doctrine.setCapital(preCapital);
            doctrine.setSmallCarrierProbability(preSmallCarrierProbability * 0.2f);
            doctrine.setMediumCarrierProbability(preMediumCarrierProbability * 0.4f);
            doctrine.setLargeCarrierProbability(preLargeCarrierProbability * 0.6f);
        }

        CampaignFleetAPI fleet = delegate.createFleet();

        doctrine.setSmall(preSmall);
        doctrine.setFast(preFast);
        doctrine.setMedium(preMedium);
        doctrine.setLarge(preLarge);
        doctrine.setCapital(preCapital);
        doctrine.setSmallCarrierProbability(preSmallCarrierProbability);
        doctrine.setMediumCarrierProbability(preMediumCarrierProbability);
        doctrine.setLargeCarrierProbability(preLargeCarrierProbability);

        return fleet;
    }

    public static void finishFleetNonIntrusive(CampaignFleetAPI fleet, String faction, boolean changePrefix, Random r) {
        List<FleetMemberAPI> members = new ArrayList<>(fleet.getFleetData().getNumMembers());
        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            members.add(member);
        }
        for (FleetMemberAPI member : members) {
            fleet.getFleetData().removeFleetMember(member);
        }
        Collections.sort(members, PRIORITY);
        for (FleetMemberAPI member : members) {
            fleet.getFleetData().addFleetMember(member);
        }

        FactionAPI currF = fleet.getFaction();
        FactionAPI f = Global.getSector().getFaction(faction);
        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            if (member.getShipName() != null) {
                String[] words = member.getShipName().split(" ");
                if (words.length > 1) {
                    String lastWord = words[words.length - 1];
                    if (DS_Util.isValidRoman(lastWord)) {
                        String name = words[0];
                        if (name.contentEquals(fleet.getFaction().getEntityNamePrefix())) {
                            name = "";
                        }
                        for (int i = 1; i < words.length - 1; i++) {
                            if (name.isEmpty()) {
                                name += words[i];
                            } else {
                                name += " " + words[i];
                            }
                        }
                        name += DS_Util.getNumerals(fleet, r);
                        member.setShipName(name);
                    }
                }

                if (changePrefix) {
                    if (f != currF && f.getEntityNamePrefix() != null) {
                        String currPrefix = currF.getEntityNamePrefix();
                        String prefix = f.getEntityNamePrefix();
                        if (!currPrefix.isEmpty() && member.getShipName().startsWith(currPrefix)) {
                            if (prefix.isEmpty()) {
                                member.setShipName(member.getShipName().replaceFirst(currPrefix + " ", prefix));
                            } else {
                                member.setShipName(member.getShipName().replaceFirst(currPrefix, prefix));
                            }
                        } else if (currPrefix.isEmpty()) {
                            member.setShipName(prefix + " " + member.getShipName());
                        }
                    }
                }
            }

            member.getRepairTracker().setCR(member.getRepairTracker().getMaxCR());
        }
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static void levelOfficer(PersonAPI person, FleetDataAPI fleetData, int toLevel, boolean alwaysPickHigherSkill,
                                    SkillPickPreference pref, Random random) {
        if (random == null) {
            random = new Random();
        }

        OfficerLevelupPlugin plugin = (OfficerLevelupPlugin) Global.getSettings().getPlugin("officerLevelUp");
        if (toLevel > plugin.getMaxLevel(person)) {
            toLevel = plugin.getMaxLevel(person);
        }
        person.getStats().setSkipRefresh(true);

        OfficerDataAPI officerData = fleetData.getOfficerData(person);
        if (officerData == null) {
            officerData = Global.getFactory().createOfficerData(person);
        }

        long xp = plugin.getXPForLevel(toLevel);
        officerData.addXP(xp - person.getStats().getXP());
        while (officerData.canLevelUp()) {
            String skillId =
                   OfficerManagerEvent.pickSkill(officerData.getPerson(), officerData.getSkillPicks(),
                                                 alwaysPickHigherSkill, pref, random);
            if (skillId != null) {
                officerData.levelUp(skillId);
            }
        }

        person.getStats().setSkipRefresh(false);
        person.getStats().refreshCharacterStatsEffects();
    }

    public interface FleetFactoryDelegate {

        public CampaignFleetAPI createFleet();
    }
}
