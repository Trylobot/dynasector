package data.scripts.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Util;
import java.awt.Color;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;

public class DS_Profiler implements EveryFrameScript {

    public static Logger log = Global.getLogger(DS_Profiler.class);

    private static final float CALC_INTERVAL = 1f;
    private static final double DECAY = 0.9;
    private static final float DISPLAY_INTERVAL = 14f;
    private static final float HEAP_INTERVAL = 1f;
    private static final float MEMORY_INTERVAL = 10f * 60f;
    private static final double WARN_THRESHOLD = 0.35;

    private final IntervalUtil calcInterval = new IntervalUtil(CALC_INTERVAL, CALC_INTERVAL);
    private final IntervalUtil displayInterval = new IntervalUtil(DISPLAY_INTERVAL, DISPLAY_INTERVAL);
    private final Map<String, Double> fleetMapAverage = new LinkedHashMap<>(Global.getSector().getAllFactions().size() +
                                      1);
    private final IntervalUtil heapInterval = new IntervalUtil(HEAP_INTERVAL, HEAP_INTERVAL);
    private double heapMaxSizeAverage = 0f;
    private double heapUsageAverage = 0f;
    private final IntervalUtil memoryInterval = new IntervalUtil(MEMORY_INTERVAL, MEMORY_INTERVAL);
    private boolean warnedLast = true;

    @Override
    public void advance(float amount) {
        if (Global.getSector().isInFastAdvance() || Global.getSector().isInNewGameAdvance()) {
            return;
        }

        float days = Global.getSector().getClock().convertToDays(amount);
        calcInterval.advance(days);
        if (calcInterval.intervalElapsed()) {
            for (Entry<String, Double> entry : fleetMapAverage.entrySet()) {
                entry.setValue(entry.getValue() * DECAY);
            }
            for (LocationAPI location : Global.getSector().getAllLocations()) {
                for (CampaignFleetAPI fleet : location.getFleets()) {
                    String faction = fleet.getFaction().getId();
                    Double average = fleetMapAverage.get(faction);
                    if (average == null) {
                        average = 0.0;
                    }
                    average += 1.0 * (1.0 - DECAY);
                    Double averageAll = fleetMapAverage.get("all");
                    if (averageAll == null) {
                        averageAll = 0.0;
                    }
                    averageAll += 1.0 * (1.0 - DECAY);
                    fleetMapAverage.put(faction, average);
                    fleetMapAverage.put("all", averageAll);
                }
            }
        }

        heapInterval.advance(amount);
        if (heapInterval.intervalElapsed()) {
            heapMaxSizeAverage *= DECAY;
            heapUsageAverage *= DECAY;
            long heapSize = Runtime.getRuntime().totalMemory();
            long heapMaxSize = Runtime.getRuntime().maxMemory();
            long heapFreeSize = Runtime.getRuntime().freeMemory();
            double heapMaxSizeMB = heapMaxSize / 1048576.0;
            double heapUsageMB = (heapSize - heapFreeSize) / 1048576.0;
            if (heapMaxSizeAverage == 0f) {
                heapMaxSizeAverage = heapMaxSizeMB;
            } else {
                heapMaxSizeAverage += heapMaxSizeMB * (1.0 - DECAY);
            }
            if (heapUsageAverage == 0f) {
                heapUsageAverage = heapUsageMB;
            } else {
                heapUsageAverage += heapUsageMB * (1.0 - DECAY);
            }
        }

        displayInterval.advance(days);
        if (displayInterval.intervalElapsed()) {
            DecimalFormat format = new DecimalFormat("#0.00");

            log.info("-= Fleet Count Averages =-");
            for (Entry<String, Double> entry : fleetMapAverage.entrySet()) {
                String factionId = entry.getKey();
                FactionAPI faction = Global.getSector().getFaction(factionId);
                String factionName;
                if (faction == null) {
                    factionName = factionId;
                } else {
                    factionName = faction.getDisplayName();
                }
                log.info("    ~ " + factionName + ": " + format.format(entry.getValue()));
            }

            log.info("-= Ship Type Counts =-");
            Map<String, Integer> shipMap = new LinkedHashMap<>(500);
            for (LocationAPI location : Global.getSector().getAllLocations()) {
                for (CampaignFleetAPI fleet : location.getFleets()) {
                    for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
                        String id = DS_Util.getNonDHullId(member.getHullSpec());
                        Integer total = shipMap.get(id);
                        if (total == null) {
                            total = 1;
                        } else {
                            total++;
                        }
                        shipMap.put(id, total);
                    }
                }
            }
            List<ShipCountToken> shipList = new ArrayList<>(shipMap.size());
            for (Entry<String, Integer> entry : shipMap.entrySet()) {
                shipList.add(new ShipCountToken(entry.getKey(), entry.getValue()));
            }
            Collections.sort(shipList, new ShipCountComparator());
            for (ShipCountToken ship : shipList) {
                log.info("    ~ " + ship.id + ": " + ship.count);
            }
        }

        if (DSModPlugin.checkMemory) {
            memoryInterval.advance(amount * (warnedLast ? 10f : 1f));
            if (memoryInterval.intervalElapsed()) {
                DecimalFormat format = new DecimalFormat("#0.00");
                log.info("JVM Heap Average: " + format.format(heapUsageAverage) + " / " + format.format(
                        heapMaxSizeAverage) + " MiB");
                if (heapMaxSizeAverage > 1.0) {
                    System.gc(); // Here we go...
                    long heapSize = Runtime.getRuntime().totalMemory();
                    long heapMaxSize = Runtime.getRuntime().maxMemory();
                    long heapFreeSize = Runtime.getRuntime().freeMemory();
                    double heapMaxSizeMB = heapMaxSize / 1048576.0;
                    double heapUsageMB = (heapSize - heapFreeSize) / 1048576.0;

                    RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
                    List<String> arguments = runtimeMxBean.getInputArguments();
                    long chosenS = 0L;
                    long chosenX = 0L;
                    for (String arg : arguments) {
                        if (arg.startsWith("-Xms")) {
                            if (arg.endsWith("g")) {
                                chosenS = Long.parseLong(arg.replace("-Xms", "").replaceAll("[gG]", "")) * 1073741824L;
                            } else if (arg.endsWith("m")) {
                                chosenS = Long.parseLong(arg.replace("-Xms", "").replaceAll("[mM]", "")) * 1048576L;
                            } else if (arg.endsWith("k")) {
                                chosenS = Long.parseLong(arg.replace("-Xms", "").replaceAll("[kK]", "")) * 1024L;
                            } else {
                                chosenS = Long.parseLong(arg.replace("-Xms", ""));
                            }
                        } else if (arg.startsWith("-Xmx")) {
                            if (arg.endsWith("g")) {
                                chosenX = Long.parseLong(arg.replace("-Xmx", "").replaceAll("[gG]", "")) * 1073741824L;
                            } else if (arg.endsWith("m")) {
                                chosenX = Long.parseLong(arg.replace("-Xmx", "").replaceAll("[mM]", "")) * 1048576L;
                            } else if (arg.endsWith("k")) {
                                chosenX = Long.parseLong(arg.replace("-Xmx", "").replaceAll("[kK]", "")) * 1024L;
                            } else {
                                chosenX = Long.parseLong(arg.replace("-Xmx", ""));
                            }
                        }
                    }

                    if ((heapUsageMB / heapMaxSizeMB) > WARN_THRESHOLD) {
                        warnedLast = true;
                        Global.getSector().getCampaignUI().addMessage(
                                "WARNING: JVM heap is " + Math.round(100.0 * heapUsageAverage / heapMaxSizeAverage) +
                                "% full! (" + Math.round(heapUsageMB - heapMaxSizeMB * WARN_THRESHOLD) +
                                " MiB over safe limit). Increase memory allocation to avoid save corruption.", Color.RED,
                                "" + Math.round(100.0 * heapUsageAverage / heapMaxSizeAverage), "" + Math.round(
                                        heapUsageMB - heapMaxSizeMB * WARN_THRESHOLD),
                                Color.YELLOW, Color.YELLOW);
                    } else if (chosenS != chosenX) {
                        warnedLast = true;
                        Global.getSector().getCampaignUI().addMessage(
                                "WARNING: JVM '-Xms' and '-Xmx' values are different! Minimum and maximum heap should " +
                                "be the same!", Color.RED, "-Xms", "-Xmx", Color.YELLOW, Color.YELLOW);
                    } else {
                        warnedLast = false;
                    }
                }
            }
        }
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    private static final class ShipCountComparator implements Comparator<ShipCountToken> {

        @Override
        public int compare(ShipCountToken s1, ShipCountToken s2) {
            // Reverse order!
            return -Integer.compare(s1.count, s2.count);
        }
    }

    private static final class ShipCountToken {

        final int count;
        final String id;

        ShipCountToken(String id, int count) {
            this.id = id;
            this.count = count;
        }
    }
}
