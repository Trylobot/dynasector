package data.scripts.campaign.events;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseOnMessageDeliveryScript;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.comm.CommMessageAPI;
import com.fs.starfarer.api.campaign.comm.MessagePriority;
import com.fs.starfarer.api.campaign.events.CampaignEventTarget;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.CustomRepImpact;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActionEnvelope;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActions;
import com.fs.starfarer.api.impl.campaign.events.BaseEventPlugin;
import com.fs.starfarer.api.impl.campaign.events.FactionHostilityEvent.FactionHostilityPairKey;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.Misc.Token;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class DS_FactionHostilityEvent extends BaseEventPlugin {

    public static final float HOSTILITY_PENALTY = 0.2f;

    public static Logger log = Global.getLogger(DS_FactionHostilityEvent.class);

    protected float duration = 0f;
    protected float elapsedDays = 0f;
    protected boolean ended = false;
    protected boolean hostilityStarted = false;
    protected FactionAPI one;
    protected float prevRel = 0f;
    protected float prevRelOne = 0f;
    protected float prevRelTwo = 0f;
    protected FactionHostilityPairKey target = null;
    protected FactionAPI two;

    @Override
    public void advance(float amount) {
        if (!isEventStarted()) {
            return;
        }
        if (isDone()) {
            return;
        }

        float days = Global.getSector().getClock().convertToDays(amount);
        elapsedDays += days;

        if (!hostilityStarted && elapsedDays > 10f) {
            startHostilities();
            hostilityStarted = true;
        }

        if (elapsedDays >= duration) {
            endEvent();
        }
    }

    @Override
    public boolean callEvent(String ruleId, InteractionDialogAPI dialog, List<Token> params,
                             Map<String, MemoryAPI> memoryMap) {
        String action = params.get(0).getString(memoryMap);
        CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
        CargoAPI cargo = playerFleet.getCargo();

        if (action.equals("TODO")) {
        }
        return true;
    }

    @Override
    public String getEventName() {
        String postfix = " hostilities";
        if (isDone()) {
            postfix = " hostilities - over";
        }
        //return "Hostilities: " + Misc.ucFirst(one.getDisplayName()) + " / " + Misc.ucFirst(two.getDisplayName()) + postfix;
        return Misc.ucFirst(one.getDisplayName()) + " / " + Misc.ucFirst(two.getDisplayName()) + postfix;
    }

    @Override
    @SuppressWarnings("CollectionsToArray")
    public String[] getHighlights(String stageId) {
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        List<String> result = new ArrayList<>(0);
        //addTokensToList(result, "$duration");
        return result.toArray(new String[0]);
    }

    @Override
    public Map<String, String> getTokenReplacements() {
        Map<String, String> map = super.getTokenReplacements();

        addFactionNameTokens(map, "one", one);
        addFactionNameTokens(map, "two", two);

        FactionAPI commFac = Misc.getCommissionFaction();
        if (commFac != null && (commFac == one || commFac == two)) {
            map.put("$sender", commFac.getDisplayName());
            addFactionNameTokens(map, "commission", commFac);
            addFactionNameTokens(map, "other", commFac == one ? two : one);
        }
        return map;
    }

    @Override
    public void init(String type, CampaignEventTarget eventTarget) {
        super.init(type, eventTarget, false);
    }

    @Override
    public boolean isDone() {
        return ended;
    }

    @Override
    public void startEvent() {
        super.startEvent(true);

        if (!(eventTarget.getCustom() instanceof FactionHostilityPairKey)) {
            endEvent();
            return;
        }
        target = (FactionHostilityPairKey) eventTarget.getCustom();
        one = target.getOne();
        two = target.getTwo();

        duration = 365f * (0.5f + 0.5f * (float) Math.random());

        log.info(String.format("Starting hostility event: %s -> %s", one.getDisplayName(), two.getDisplayName()));

        FactionAPI commFac = Misc.getCommissionFaction();
        if (commFac != null && (commFac == one || commFac == two)) {
            Global.getSector().reportEventStage(this, "warning", Global.getSector().getPlayerFleet(),
                                                MessagePriority.ENSURE_DELIVERY, null);
        }
    }

    protected void endEvent() {
        if (ended) {
            return;
        }

        ended = true;

        one.setRelationship(two.getId(), prevRel);

        Global.getSector().reportEventStage(this, "end", Global.getSector().getPlayerFleet(),
                                            MessagePriority.ENSURE_DELIVERY, new BaseOnMessageDeliveryScript() {
                    @Override
                    public void beforeDelivery(CommMessageAPI message) {
                        FactionAPI commFac = Misc.getCommissionFaction();
                        if (commFac == null) {
                            return;
                        }
                        if (commFac != one && commFac != two) {
                            return;
                        }

                        FactionAPI other = one;
                        float prevRel = prevRelOne;
                        if (other == commFac) {
                            other = two;
                            prevRel = prevRelTwo;
                        }

                        float currRel = other.getRelationship(Factions.PLAYER);
                        CustomRepImpact impact = new CustomRepImpact();
                        impact.delta = (prevRel - currRel - HOSTILITY_PENALTY);
                        if (impact.delta < 0) {
                            impact.delta = 0;
                        }
                        Global.getSector().adjustPlayerReputation(
                                new RepActionEnvelope(RepActions.CUSTOM, impact, message, true),
                                other.getId());
                    }
                });
    }

    protected void startHostilities() {
        log.info(String.format("Making factions hostile: %s -> %s", one.getDisplayName(), two.getDisplayName()));
        prevRel = one.getRelationship(two.getId());
        one.setRelationship(two.getId(), RepLevel.HOSTILE);

        prevRelOne = one.getRelationship(Factions.PLAYER);
        prevRelTwo = two.getRelationship(Factions.PLAYER);

        final FactionAPI commFac = Misc.getCommissionFaction();
        if (commFac != null && (commFac == one || commFac == two)) {
            Global.getSector().reportEventStage(this, "start", Global.getSector().getPlayerFleet(),
                                                MessagePriority.ENSURE_DELIVERY, new BaseOnMessageDeliveryScript() {
                        @Override
                        public void beforeDelivery(CommMessageAPI message) {
                            Global.getSector().adjustPlayerReputation(
                                    new RepActionEnvelope(RepActions.MAKE_HOSTILE_AT_BEST, null, message, true),
                                    (commFac == one ? two : one).getId());
                        }
                    });
        } else {
            Global.getSector().reportEventStage(this, "start", Global.getSector().getPlayerFleet(),
                                                MessagePriority.ENSURE_DELIVERY, null);
        }
    }
}
