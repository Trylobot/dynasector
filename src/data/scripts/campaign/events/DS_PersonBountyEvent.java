package data.scripts.campaign.events;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseOnMessageDeliveryScript;
import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.comm.CommMessageAPI;
import com.fs.starfarer.api.campaign.comm.MessagePriority;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventManagerAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventPlugin;
import com.fs.starfarer.api.campaign.events.CampaignEventTarget;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.CustomRepImpact;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActionEnvelope;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActions;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepRewards;
import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.impl.campaign.events.BaseEventPlugin;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent.SkillPickPreference;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactory;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParams;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.impl.campaign.ids.Skills;
import com.fs.starfarer.api.impl.campaign.ids.Strings;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.BreadcrumbSpecial;
import com.fs.starfarer.api.impl.campaign.shared.PersonBountyEventData;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.campaign.DS_FleetFactory;
import data.scripts.campaign.DS_FleetFactory.FleetFactoryDelegate;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Defs.Archetype;
import data.scripts.util.DS_Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.MathUtils;

public class DS_PersonBountyEvent extends BaseEventPlugin {

    public static Logger log = Global.getLogger(DS_PersonBountyEvent.class);

    private static MarketAPI findNearestHostileMarket(SectorEntityToken token, float maxDist,
                                                      FactionAPI faction) {
        List<MarketAPI> localMarkets = Misc.getMarketsInLocation(token.getContainingLocation());
        float distToLocalMarket = Float.MAX_VALUE;
        MarketAPI closest = null;
        for (MarketAPI market : localMarkets) {
            if (market.isPlanetConditionMarketOnly()) {
                continue;
            }
            if (market.getPrimaryEntity() == null) {
                continue;
            }
            if (market.getPrimaryEntity().getContainingLocation() != token.getContainingLocation()) {
                continue;
            }

            if (!faction.isHostileTo(market.getFaction())) {
                continue;
            }

            float currDist = Misc.getDistance(market.getPrimaryEntity().getLocation(), token.getLocation());
            if (currDist > maxDist) {
                continue;
            }
            if (currDist < distToLocalMarket) {
                distToLocalMarket = currDist;
                closest = market;
            }
        }
        return closest;
    }

    private float bountyCredits = 0;
    private FactionAPI bountyFaction = null;
    private BountyType bountyType;
    private PersonBountyEventData data;
    private final float duration = 60f;
    private float elapsedDays = 0f;
    private boolean ended = false;
    private CampaignFleetAPI fleet;
    private SectorEntityToken hideoutLocation = null;
    private MessagePriority messagePriority;
    private float payment = 0;
    private PersonAPI person;
    private float qualityFactor = 0;
    private String targetDesc;

    float thisLevel = 0f;

    @Override
    public void advance(float amount) {
        if (eventTarget != null) {
            setTarget(eventTarget);
        }

        if (!isEventStarted()) {
            return;
        }
        if (fleet != null && fleet.isAlive()) {
            if (bountyType == BountyType.ASSASSINATION) {
                if (fleet.isInCurrentLocation() && fleet.getFaction() != bountyFaction) {
                    fleet.setFaction(bountyFaction.getId(), true);
                } else if (!fleet.isInCurrentLocation() && !fleet.getFaction().getId().contentEquals(Factions.NEUTRAL)) {
                    fleet.setFaction(Factions.NEUTRAL, true);
                }
            } else if (bountyType == BountyType.MERCENARY) {
                if (fleet.isInCurrentLocation() && !fleet.getFaction().getId().contentEquals(Factions.INDEPENDENT)) {
                    fleet.setFaction(Factions.INDEPENDENT, true);
                } else if (!fleet.isInCurrentLocation() && !fleet.getFaction().getId().contentEquals(Factions.NEUTRAL)) {
                    fleet.setFaction(Factions.NEUTRAL, true);
                }
            } else if (bountyType == BountyType.PATHER) {
                if (fleet.isInCurrentLocation() && !fleet.getFaction().getId().contentEquals(Factions.LUDDIC_PATH)) {
                    fleet.setFaction(Factions.LUDDIC_PATH, true);
                } else if (!fleet.isInCurrentLocation() && !fleet.getFaction().getId().contentEquals(Factions.NEUTRAL)) {
                    fleet.setFaction(Factions.NEUTRAL, true);
                }
            } else if (bountyType == BountyType.CABAL) {
                if (fleet.isInCurrentLocation() && !fleet.getFaction().getId().contentEquals("cabal")) {
                    fleet.setFaction("cabal", true);
                } else if (!fleet.isInCurrentLocation() && !fleet.getFaction().getId().contentEquals(Factions.NEUTRAL)) {
                    fleet.setFaction(Factions.NEUTRAL, true);
                }
            } else {
                if (fleet.isInCurrentLocation() && !fleet.getFaction().getId().contentEquals(Factions.PIRATES)) {
                    fleet.setFaction(Factions.PIRATES, true);
                } else if (!fleet.isInCurrentLocation() && !fleet.getFaction().getId().contentEquals(Factions.NEUTRAL)) {
                    fleet.setFaction(Factions.NEUTRAL, true);
                }
            }
        }
        if (isDone()) {
            return;
        }

        float days = Global.getSector().getClock().convertToDays(amount);
        elapsedDays += days;

        boolean allowed = true;
        if (bountyType == BountyType.ASSASSINATION) {
            if (!faction.isHostileTo(bountyFaction)) {
                allowed = false;
            }
        }

        if (((elapsedDays >= duration) || !allowed) && !isDone()) {
            boolean canEnd = (fleet == null) || !fleet.isInCurrentLocation() || !allowed;
            if (canEnd) {
                log.info(String.format("Ending bounty on %s by %s", person.getName().getFullName(), market.getName()));
                if (fleet != null) {
                    if (bountyType == BountyType.ASSASSINATION) {
                        fleet.setFaction(bountyFaction.getId(), true);
                    } else if (bountyType == BountyType.MERCENARY) {
                        fleet.setFaction(Factions.INDEPENDENT, true);
                    } else if (bountyType == BountyType.PATHER) {
                        fleet.setFaction(Factions.LUDDIC_PATH, true);
                    } else if (bountyType == BountyType.CABAL) {
                        fleet.setFaction("cabal", true);
                    } else {
                        fleet.setFaction(Factions.PIRATES, true);
                    }
                }
                if (bountyType == BountyType.ASSASSINATION) {
                    Global.getSector().reportEventStage(this, "assassination_expire_end", messagePriority);
                } else if (bountyType == BountyType.MERCENARY) {
                    Global.getSector().reportEventStage(this, "mercenary_expire_end", messagePriority);
                } else {
                    Global.getSector().reportEventStage(this, "expire_end", messagePriority);
                }
                endEvent();
                return;
            }
        }

        if (fleet.getFlagship() == null || fleet.getFlagship().getCaptain() != person) {
            if (bountyType == BountyType.ASSASSINATION) {
                fleet.setFaction(bountyFaction.getId(), true);
            } else if (bountyType == BountyType.MERCENARY) {
                fleet.setFaction(Factions.INDEPENDENT, true);
            } else if (bountyType == BountyType.PATHER) {
                fleet.setFaction(Factions.LUDDIC_PATH, true);
            } else if (bountyType == BountyType.CABAL) {
                fleet.setFaction("cabal", true);
            } else {
                fleet.setFaction(Factions.PIRATES, true);
            }
            if (bountyType == BountyType.ASSASSINATION) {
                Global.getSector().reportEventStage(this, "assassination_taken_end", market.getPrimaryEntity(),
                                                    messagePriority);
            } else if (bountyType == BountyType.MERCENARY) {
                Global.getSector().reportEventStage(this, "mercenary_taken_end", market.getPrimaryEntity(),
                                                    messagePriority);
            } else {
                Global.getSector().reportEventStage(this, "taken_end", market.getPrimaryEntity(), messagePriority);
            }
            endEvent();
        }
    }

    @Override
    public String getCurrentImage() {
        return person.getPortraitSprite();
    }

    @Override
    public String getCurrentMessageIcon() {
        return person.getPortraitSprite();
    }

    public float getElapsedDays() {
        return elapsedDays;
    }

    public void setElapsedDays(float elapsedDays) {
        this.elapsedDays = elapsedDays;
    }

    @Override
    public CampaignEventCategory getEventCategory() {
        return CampaignEventCategory.BOUNTY;
    }

    @Override
    public String getEventIcon() {
        return person.getPortraitSprite();
    }

    @Override
    public String getEventName() {
        int daysLeft = (int) (duration - elapsedDays);
        String days;
        if (daysLeft > 0) {
            days = ", " + daysLeft + "d";
        } else {
            days = ", <1d";
        }

        if (bountyType == BountyType.ASSASSINATION) {
            if (isDone()) {
                return "Kill: " + person.getName().getFullName() + " - over";
            }
            return "Kill: " + person.getName().getFullName() + " - " + Misc.getWithDGS(bountyCredits) + Strings.C + days;
        } else if (bountyType == BountyType.MERCENARY) {
            if (isDone()) {
                return "Hit List: " + person.getName().getFullName() + " - over";
            }
            return "Hit List: " + person.getName().getFullName() + " - " + Misc.getWithDGS(bountyCredits) + Strings.C +
                    days;
        } else {
            if (isDone()) {
                return "" + person.getName().getFullName() + " - over";
            }
            return "" + person.getName().getFullName() + " - " + Misc.getWithDGS(bountyCredits) + Strings.C + days;
        }
    }

    @Override
    public String[] getHighlights(String stageId) {
        List<String> result = new ArrayList<>(2);
        addTokensToList(result, "$bountyCredits");

        if (stageId != null && stageId.startsWith("start_")) {
            addTokensToList(result, "$daysLeft");
        }

        return result.toArray(new String[result.size()]);
    }

    public int getLevel() {
        return Math.round(thisLevel);
    }

    @Override
    public Map<String, String> getTokenReplacements() {
        Map<String, String> map = super.getTokenReplacements();
        // this is ok because the token replacement happens right as the message is sent, not when it's received
        // so the lastBounty is the correct value for the message
        map.put("$bountyCredits", Misc.getWithDGS(payment));
        map.put("$sender", faction.getDisplayName());

        map.put("$daysLeft", "" + (int) +Math.max(1, duration - elapsedDays));
        map.put("$targetName", person.getName().getFullName());
        map.put("$targetRank", person.getRank());
        if (targetDesc != null) {
            map.put("$targetDesc", targetDesc);
        }

        if (bountyFaction != null) {
            map.put("$targetFaction", bountyFaction.getDisplayName());
            map.put("$theTargetFaction", bountyFaction.getDisplayNameWithArticle());
            map.put("$targetFactionLong", bountyFaction.getDisplayNameLong());
            map.put("$theTargetFactionLong", bountyFaction.getDisplayNameLongWithArticle());
        }

        if (hideoutLocation != null) {
            if (hideoutLocation.getContainingLocation() instanceof StarSystemAPI) {
                map.put("$hideoutSystem", ((StarSystemAPI) hideoutLocation.getContainingLocation()).getBaseName());
            } else {
                map.put("$hideoutSystem", hideoutLocation.getContainingLocation().getName());
            }

            SectorEntityToken fake = hideoutLocation.getContainingLocation().createToken(0, 0);
            fake.setOrbit(Global.getFactory().createCircularOrbit(hideoutLocation, 0, 1000, 100));

            map.put("$hideout", hideoutLocation.getName());
            String loc = BreadcrumbSpecial.getLocatedString(fake);
            loc = loc.replaceAll("orbiting", "hiding out near");
            loc = loc.replaceAll("located in", "hiding out in");
            map.put("$hideoutCrumb", loc);
        }

        return map;
    }

    @Override
    public void init(String type, CampaignEventTarget eventTarget) {
        super.init(type, eventTarget, false);
        messagePriority = MessagePriority.SECTOR;
    }

    @Override
    public boolean isDone() {
        return ended;
    }

    @Override
    public void reportBattleOccurred(CampaignFleetAPI primaryWinner, BattleAPI battle) {
        if (!isEventStarted()) {
            return;
        }
        if (isDone()) {
            return;
        }

        if (battle.isInvolved(fleet) && !battle.isPlayerInvolved()) {
            if (fleet.getFlagship() == null || fleet.getFlagship().getCaptain() != person) {
                fleet.setCommander(fleet.getFaction().createRandomPerson());
                if (bountyType == BountyType.ASSASSINATION) {
                    fleet.setFaction(bountyFaction.getId(), true);
                } else if (bountyType == BountyType.MERCENARY) {
                    fleet.setFaction(Factions.INDEPENDENT, true);
                } else if (bountyType == BountyType.PATHER) {
                    fleet.setFaction(Factions.LUDDIC_PATH, true);
                } else if (bountyType == BountyType.CABAL) {
                    fleet.setFaction("cabal", true);
                } else {
                    fleet.setFaction(Factions.PIRATES, true);
                }
                if (bountyType == BountyType.ASSASSINATION) {
                    Global.getSector().reportEventStage(this, "assassination_taken_end", market.getPrimaryEntity(),
                                                        messagePriority);
                } else if (bountyType == BountyType.MERCENARY) {
                    Global.getSector().reportEventStage(this, "mercenary_taken_end", market.getPrimaryEntity(),
                                                        messagePriority);
                } else {
                    Global.getSector().reportEventStage(this, "taken_end", market.getPrimaryEntity(), messagePriority);
                }
                endEvent();
                return;
            }
        }

        CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
        if (!battle.isPlayerInvolved() || !battle.isInvolved(fleet) || battle.onPlayerSide(fleet)) {
            return;
        }

        // didn't destroy the original flagship
        if (fleet.getFlagship() != null && fleet.getFlagship().getCaptain() == person) {
            return;
        }

        RepLevel repLevel = playerFleet.getFaction().getRelationshipLevel(market.getFaction());
        payment = (int) (bountyCredits * battle.getPlayerInvolvementFraction());
        if (payment <= 0) {
            return;
        }

        if (repLevel.isAtWorst(RepLevel.SUSPICIOUS)) {
            log.info(String.format("Paying bounty of %f from faction [%s]", payment, faction.getDisplayName()));
            if (bountyType == BountyType.ASSASSINATION) {
                String reportId = "assassination_payment_end";
                if (battle.getPlayerInvolvementFraction() < 1) {
                    reportId = "assassination_payment_share_end";
                }
                Global.getSector().reportEventStage(this, reportId, market.getPrimaryEntity(),
                                                    MessagePriority.ENSURE_DELIVERY,
                                                    new BaseOnMessageDeliveryScript() {
                                                        @Override
                                                        public void beforeDelivery(CommMessageAPI message) {
                                                            CampaignFleetAPI playerFleet =
                                                                             Global.getSector().getPlayerFleet();
                                                            playerFleet.getCargo().getCredits().add(payment);
                                                            CustomRepImpact repImpact = new CustomRepImpact();
                                                            repImpact.delta = RepRewards.VERY_HIGH;
                                                            repImpact.limit = RepLevel.COOPERATIVE;
                                                            repImpact.requireAtWorst = RepLevel.NEUTRAL;
                                                            Global.getSector().adjustPlayerReputation(
                                                                    new RepActionEnvelope(RepActions.CUSTOM, repImpact,
                                                                                          message, true),
                                                                    faction.getId());
                                                        }
                                                    });
            } else if (bountyType == BountyType.MERCENARY) {
                String reportId = "mercenary_payment_end";
                if (battle.getPlayerInvolvementFraction() < 1) {
                    reportId = "mercenary_payment_share_end";
                }
                Global.getSector().reportEventStage(this, reportId, market.getPrimaryEntity(),
                                                    MessagePriority.ENSURE_DELIVERY,
                                                    new BaseOnMessageDeliveryScript() {
                                                        @Override
                                                        public void beforeDelivery(CommMessageAPI message) {
                                                            CampaignFleetAPI playerFleet =
                                                                             Global.getSector().getPlayerFleet();
                                                            playerFleet.getCargo().getCredits().add(payment);
                                                            Global.getSector().adjustPlayerReputation(
                                                                    new RepActionEnvelope(
                                                                            RepActions.PERSON_BOUNTY_REWARD,
                                                                            null, message, true),
                                                                    faction.getId());
                                                        }
                                                    });
            } else {
                String reportId = "bounty_payment_end";
                if (battle.getPlayerInvolvementFraction() < 1) {
                    reportId = "bounty_payment_share_end";
                }
                Global.getSector().reportEventStage(this, reportId, market.getPrimaryEntity(),
                                                    MessagePriority.ENSURE_DELIVERY,
                                                    new BaseOnMessageDeliveryScript() {
                                                        @Override
                                                        public void beforeDelivery(CommMessageAPI message) {
                                                            CampaignFleetAPI playerFleet =
                                                                             Global.getSector().getPlayerFleet();
                                                            playerFleet.getCargo().getCredits().add(payment);
                                                            Global.getSector().adjustPlayerReputation(
                                                                    new RepActionEnvelope(
                                                                            RepActions.PERSON_BOUNTY_REWARD,
                                                                            null, message, true),
                                                                    faction.getId());
                                                        }
                                                    });
            }
        } else if (repLevel.isAtWorst(RepLevel.HOSTILE) && bountyType != BountyType.ASSASSINATION) {
            log.info(String.format("Not paying bounty, but improving rep with faction [%s]", faction.getDisplayName()));
            Global.getSector().reportEventStage(this, "bounty_no_payment_end", market.getPrimaryEntity(),
                                                MessagePriority.ENSURE_DELIVERY,
                                                new BaseOnMessageDeliveryScript() {
                                                    @Override
                                                    public void beforeDelivery(CommMessageAPI message) {
                                                        Global.getSector().adjustPlayerReputation(new RepActionEnvelope(
                                                                        RepActions.PERSON_BOUNTY_REWARD, null,
                                                                        message, true), faction.getId());
                                                    }
                                                });
        } else {
            log.info(String.format("Not paying bounty or improving rep with faction [%s]", faction.getDisplayName()));
            Global.getSector().reportEventStage(this, "bounty_no_rep_end", market.getPrimaryEntity(),
                                                MessagePriority.ENSURE_DELIVERY);
        }

        data.reportSuccess();
        if (!Global.getSector().getPersistentData().containsKey("ds_personBountyLevel")) {
            Global.getSector().getPersistentData().put("ds_personBountyLevel", 0f);
        }
        float level = (float) Global.getSector().getPersistentData().get("ds_personBountyLevel");
        level += 0.35f;
        level = Math.min(20f, level);
        Global.getSector().getPersistentData().put("ds_personBountyLevel", level);
        endEvent();
    }

    @Override
    public void reportFleetDespawned(CampaignFleetAPI fleet, FleetDespawnReason reason, Object param) {
        if (isDone()) {
            return;
        }

        if (this.fleet == fleet) {
            fleet.setCommander(fleet.getFaction().createRandomPerson());
            if (bountyType == BountyType.ASSASSINATION) {
                fleet.setFaction(bountyFaction.getId(), true);
            } else if (bountyType == BountyType.MERCENARY) {
                fleet.setFaction(Factions.INDEPENDENT, true);
            } else if (bountyType == BountyType.PATHER) {
                fleet.setFaction(Factions.LUDDIC_PATH, true);
            } else if (bountyType == BountyType.CABAL) {
                fleet.setFaction("cabal", true);
            } else {
                fleet.setFaction(Factions.PIRATES, true);
            }
            if (bountyType == BountyType.ASSASSINATION) {
                Global.getSector().reportEventStage(this, "assassination_other_end", market.getPrimaryEntity(),
                                                    messagePriority);
            } else if (bountyType == BountyType.MERCENARY) {
                Global.getSector().reportEventStage(this, "mercenary_other_end", market.getPrimaryEntity(),
                                                    messagePriority);
            } else {
                Global.getSector().reportEventStage(this, "other_end", market.getPrimaryEntity(), messagePriority);
            }
            endEvent();
        }
    }

    @Override
    public void startEvent() {
        if (eventTarget != null) {
            setTarget(eventTarget);
        }

        super.startEvent(true);

        data = SharedData.getData().getPersonBountyEventData();
        data.reportStarted();
        if (!Global.getSector().getPersistentData().containsKey("ds_personBountyLevel")) {
            Global.getSector().getPersistentData().put("ds_personBountyLevel", 0f);
        }

        PersonAPI player = Global.getSector().getPlayerPerson();
        if (player != null && Math.max(0f, player.getStats().getLevel() - 10f) / 3f >
                (float) Global.getSector().getPersistentData().get("ds_personBountyLevel")) {
            Global.getSector().getPersistentData().put("ds_personBountyLevel",
                                                       Math.max(0f, player.getStats().getLevel() - 10f) / 3f);
        }

        if (Global.getSector().isInFastAdvance() || Global.getSector().isInNewGameAdvance()) {
            endEvent();
            return;
        }

        pickMarketAndFaction();
        if (isDone()) {
            return;
        }

        pickLevel();

        pickBountyType();
        if (isDone()) {
            return;
        }

        if (bountyType == BountyType.PIRATE || bountyType == BountyType.PATHER) {
            qualityFactor = Math.min(1f, thisLevel / 15f);
        } else if (bountyType == BountyType.CABAL) {
            qualityFactor = 1f;
        } else if (bountyType == BountyType.DESERTER) {
            qualityFactor = Math.min(1f, thisLevel / 15f);
        } else if (bountyType == BountyType.ASSASSINATION) {
            qualityFactor = Math.min(1f, thisLevel / 15f);
        } else if (bountyType == BountyType.MERCENARY) {
            qualityFactor = Math.min(1f, thisLevel / 15f);
        }

        if (bountyType == BountyType.ASSASSINATION) {
            pickAssassinastionFaction();
            pickAssassinastionHideoutLocation();
        } else {
            pickHideoutLocation();
        }
        if (isDone()) {
            return;
        }

        initPerson();
        if (isDone()) {
            return;
        }

        initBountyAmount();

        spawnFleet();
        if (isDone()) {
            return;
        }

        initTargetDesc();

        if (bountyType == BountyType.PIRATE) {
            Global.getSector().reportEventStage(this, "start_pirate", null, messagePriority,
                                                new BaseOnMessageDeliveryScript() {
                                                    @Override
                                                    public void beforeDelivery(CommMessageAPI message) {
                                                        if (hideoutLocation.getContainingLocation() instanceof StarSystemAPI) {
                                                            message.setStarSystemId(
                                                                    hideoutLocation.getContainingLocation().getId());
                                                        }
                                                    }
                                                });
        } else if (bountyType == BountyType.PATHER) {
            Global.getSector().reportEventStage(this, "start_pather", null, messagePriority,
                                                new BaseOnMessageDeliveryScript() {
                                                    @Override
                                                    public void beforeDelivery(CommMessageAPI message) {
                                                        if (hideoutLocation.getContainingLocation() instanceof StarSystemAPI) {
                                                            message.setStarSystemId(
                                                                    hideoutLocation.getContainingLocation().getId());
                                                        }
                                                    }
                                                });
        } else if (bountyType == BountyType.CABAL) {
            Global.getSector().reportEventStage(this, "start_cabal", null, messagePriority,
                                                new BaseOnMessageDeliveryScript() {
                                                    @Override
                                                    public void beforeDelivery(CommMessageAPI message) {
                                                        if (hideoutLocation.getContainingLocation() instanceof StarSystemAPI) {
                                                            message.setStarSystemId(
                                                                    hideoutLocation.getContainingLocation().getId());
                                                        }
                                                    }
                                                });
        } else if (bountyType == BountyType.DESERTER) {
            Global.getSector().reportEventStage(this, "start_deserter", null, messagePriority,
                                                new BaseOnMessageDeliveryScript() {
                                                    @Override
                                                    public void beforeDelivery(CommMessageAPI message) {
                                                        if (hideoutLocation.getContainingLocation() instanceof StarSystemAPI) {
                                                            message.setStarSystemId(
                                                                    hideoutLocation.getContainingLocation().getId());
                                                        }
                                                    }
                                                });
        } else if (bountyType == BountyType.ASSASSINATION) {
            Global.getSector().reportEventStage(this, "start_assassination", null, messagePriority,
                                                new BaseOnMessageDeliveryScript() {
                                                    @Override
                                                    public void beforeDelivery(CommMessageAPI message) {
                                                        if (hideoutLocation.getContainingLocation() instanceof StarSystemAPI) {
                                                            message.setStarSystemId(
                                                                    hideoutLocation.getContainingLocation().getId());
                                                        }
                                                    }
                                                });
        } else if (bountyType == BountyType.MERCENARY) {
            Global.getSector().reportEventStage(this, "start_mercenary", null, messagePriority,
                                                new BaseOnMessageDeliveryScript() {
                                                    @Override
                                                    public void beforeDelivery(CommMessageAPI message) {
                                                        if (hideoutLocation.getContainingLocation() instanceof StarSystemAPI) {
                                                            message.setStarSystemId(
                                                                    hideoutLocation.getContainingLocation().getId());
                                                        }
                                                    }
                                                });
        }

        fleet.setFaction(Factions.NEUTRAL, true);

        @SuppressWarnings("unchecked")
        List<DS_PersonBountyEvent> activeBounties =
                                   (List<DS_PersonBountyEvent>) Global.getSector().getPersistentData().get(
                                           "ds_activebounties");
        if (activeBounties == null) {
            activeBounties = new LinkedList<>();
        }
        activeBounties.add(this);
        Global.getSector().getPersistentData().put("ds_activebounties", activeBounties);

        log.info(String.format("Starting person bounty by faction [%s] for person %s", faction.getDisplayName(),
                               person.getName().getFullName()));
    }

    private void endEvent() {
        @SuppressWarnings("unchecked")
        List<DS_PersonBountyEvent> activeBounties =
                                   (List<DS_PersonBountyEvent>) Global.getSector().getPersistentData().get(
                                           "ds_activebounties");
        if (activeBounties != null) {
            Iterator<DS_PersonBountyEvent> iter = activeBounties.iterator();
            while (iter.hasNext()) {
                CampaignEventPlugin event = iter.next();
                if (event.isDone()) {
                    iter.remove();
                }
            }
        }

        data.reportEnded();
        ended = true;

        if (fleet != null) {
            Misc.makeUnimportant(fleet, "pbe");
            fleet.clearAssignments();
            if (hideoutLocation != null) {
                fleet.getAI().addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, hideoutLocation, 1000000f, null);
            } else {
                fleet.despawn();
            }
        }
    }

    private void initBountyAmount() {
        float highStabilityMult = BaseMarketConditionPlugin.getHighStabilityBonusMult(market);

        float base = Global.getSettings().getFloat("basePersonBounty");
        float perLevel = Global.getSettings().getFloat("personBountyPerLevel");

        bountyCredits = (int) (base + perLevel * (thisLevel + 0.5f));
        if (bountyType == BountyType.PIRATE) {
            if (DS_Defs.FACTION_ADJUST.containsKey(Factions.PIRATES)) {
                bountyCredits *= DS_Defs.FACTION_ADJUST.get(Factions.PIRATES);
            }
        } else if (bountyType == BountyType.PATHER) {
            bountyCredits *= 1.25f;
            if (DS_Defs.FACTION_ADJUST.containsKey(Factions.LUDDIC_PATH)) {
                bountyCredits *= DS_Defs.FACTION_ADJUST.get(Factions.LUDDIC_PATH);
            }
        } else if (bountyType == BountyType.CABAL) {
            bountyCredits *= 1.75f;
            if (DS_Defs.FACTION_ADJUST.containsKey("cabal")) {
                bountyCredits *= DS_Defs.FACTION_ADJUST.get("cabal");
            }
        } else if (bountyType == BountyType.DESERTER) {
            bountyCredits *= 1.5f;
            if (DS_Defs.FACTION_ADJUST.containsKey(market.getFactionId())) {
                bountyCredits *= DS_Defs.FACTION_ADJUST.get(market.getFactionId());
            }
        } else if (bountyType == BountyType.MERCENARY) {
            bountyCredits *= 1.5f;
            if (DS_Defs.FACTION_ADJUST.containsKey(Factions.INDEPENDENT)) {
                bountyCredits *= DS_Defs.FACTION_ADJUST.get(Factions.INDEPENDENT);
            }
        } else if (bountyType == BountyType.ASSASSINATION) {
            bountyCredits *= 2f;
            if (DS_Defs.FACTION_ADJUST.containsKey(bountyFaction.getId())) {
                bountyCredits *= DS_Defs.FACTION_ADJUST.get(bountyFaction.getId());
            }
        }

        float distanceBonus = 1f +
              Math.min(1f, Math.max(0f, hideoutLocation.getLocationInHyperspace().length() - 15000f)) / 37000f;

        float stabilityRange = 0.5f * Math.max(0f, 1f - thisLevel / 30f);
        bountyCredits *= 1.5f;
        bountyCredits *= (qualityFactor * 0.25f) + 0.875f;
        float highStabilityBonus = (highStabilityMult - 1f) * stabilityRange + (1f - stabilityRange * 0.5f);
        bountyCredits *= highStabilityBonus * distanceBonus;

        if (bountyCredits < 100000) {
            bountyCredits = Math.round(bountyCredits / 100f) * 100f;
        } else if (bountyCredits < 1000000) {
            bountyCredits = Math.round(bountyCredits / 1000f) * 1000f;
        } else {
            bountyCredits = Math.round(bountyCredits / 10000f) * 10000f;
        }

        payment = bountyCredits;
    }

    private void initPerson() {
        String factionId = Factions.PIRATES;
        if (bountyType == BountyType.PATHER) {
            factionId = Factions.LUDDIC_PATH;
        } else if (bountyType == BountyType.CABAL) {
            factionId = "cabal";
        } else if (bountyType == BountyType.DESERTER) {
            factionId = market.getFactionId();
        } else if (bountyType == BountyType.MERCENARY) {
            factionId = Factions.INDEPENDENT;
        } else if (bountyType == BountyType.ASSASSINATION) {
            factionId = bountyFaction.getId();
        }
        person = OfficerManagerEvent.createOfficer(Global.getSector().getFaction(factionId), 1, true);
    }

    private void initTargetDesc() {
        ShipHullSpecAPI spec = fleet.getFlagship().getVariant().getHullSpec();
        String shipType = spec.getHullNameWithDashClass() + " " + spec.getDesignation().toLowerCase();

        String heOrShe = "he";
        String hisOrHer = "his";
        if (person.isFemale()) {
            heOrShe = "she";
            hisOrHer = "her";
        }

        String levelDesc;
        int personLevel = person.getStats().getLevel();
        if (personLevel <= 5) {
            levelDesc = "an unremarkable officer";
        } else if (personLevel <= 10) {
            levelDesc = "a capable officer";
        } else if (personLevel <= 15) {
            levelDesc = "a highly capable officer";
        } else {
            levelDesc = "an exceptionally capable officer";
        }

        List<String> skillDescList = new ArrayList<>(4);
        if (person.getStats().getSkillLevel(Skills.OFFICER_MANAGEMENT) > 0) {
            skillDescList.add("having a high number of skilled subordinates");
        }
        if (person.getStats().getSkillLevel(Skills.ELECTRONIC_WARFARE) > 0) {
            skillDescList.add("being proficient in electronic warfare");
        }
        if (person.getStats().getSkillLevel(Skills.FIGHTER_DOCTRINE) > 0) {
            skillDescList.add("a noteworthy level of skill in running carrier operations");
        }
        if (person.getStats().getSkillLevel(Skills.COORDINATED_MANEUVERS) > 0) {
            skillDescList.add("a high effectiveness in coordinating the maneuvers of ships during combat");
        }

        String skillDesc = "";
        if (!skillDescList.isEmpty()) {
            skillDesc = Misc.getAndJoined(skillDescList);
        }

        if (!skillDesc.isEmpty() && levelDesc.contains("unremarkable")) {
            levelDesc = "an otherwise unremarkable officer";
        }

        String fleetDesc;
        if (thisLevel < 4) {
            fleetDesc = "small";
        } else if (thisLevel <= 7) {
            fleetDesc = "medium-sized";
        } else if (thisLevel <= 12) {
            fleetDesc = "large";
        } else if (thisLevel <= 18) {
            fleetDesc = "very large";
        } else {
            fleetDesc = "massive";
        }

        targetDesc = String.format("%s is in command of a %s fleet and was last seen using a %s as %s flagship.",
                                   person.getName().getFullName(), fleetDesc, shipType, hisOrHer);

        if (skillDesc.isEmpty()) {
            targetDesc += String.format(" %s is known to be %s.", Misc.ucFirst(heOrShe), levelDesc);
        } else {
            targetDesc += String.format(" %s is %s known for %s.", Misc.ucFirst(heOrShe), levelDesc, skillDesc);
        }
    }

    private void pickAssassinastionFaction() {
        WeightedRandomPicker<FactionAPI> picker = new WeightedRandomPicker<>();
        for (FactionAPI fac : Global.getSector().getAllFactions()) {
            if (!faction.isHostileTo(fac) || DS_Defs.PIRATE_FACTIONS.contains(fac.getId()) ||
                    fac.getId().contentEquals(Factions.LUDDIC_PATH) || fac.getId().contentEquals("cabal")) {
                continue;
            }

            float weight = 1f;
            if (faction.isAtBest(fac, RepLevel.VENGEFUL)) {
                weight *= 2f;
            }

            picker.add(fac, weight);
        }
        bountyFaction = picker.pick();

        if (bountyFaction == null) {
            endEvent();
        }
    }

    private void pickAssassinastionHideoutLocation() {
        WeightedRandomPicker<SectorEntityToken> picker = new WeightedRandomPicker<>();
        for (MarketAPI mkt : Global.getSector().getEconomy().getMarketsCopy()) {
            if (mkt.getFaction() != bountyFaction) {
                continue;
            }
            float weight = mkt.getSize();
            if (mkt.hasCondition(Conditions.MILITARY_BASE)) {
                weight *= 2f;
            }
            if (mkt.hasCondition("ii_militarycomplex")) {
                weight *= 1.5f;
            }
            if (mkt.hasCondition(Conditions.HEADQUARTERS)) {
                weight *= 2f;
            }
            if (mkt.hasCondition(Conditions.SPACEPORT) || mkt.hasCondition(Conditions.ORBITAL_STATION)) {
                weight *= 1.5f;
            }
            if (mkt.hasCondition(Conditions.REGIONAL_CAPITAL)) {
                weight *= 1.5f;
            }

            picker.add(mkt.getPrimaryEntity(), weight);
        }
        hideoutLocation = picker.pick();

        if (hideoutLocation == null) {
            endEvent();
        }
    }

    private void pickBountyType() {
        if (DS_Defs.PIRATE_FACTIONS.contains(faction.getId())) {
            if (faction.isAtWorst(Factions.PLAYER, RepLevel.INHOSPITABLE)) {
                bountyType = BountyType.MERCENARY;
            } else if (faction.isAtWorst(Factions.PLAYER, RepLevel.HOSTILE) && (Math.random() <= 0.3)) {
                bountyType = BountyType.MERCENARY;
            } else {
                endEvent();
            }
            return;
        }

        WeightedRandomPicker<BountyType> picker = new WeightedRandomPicker<>();

        float baseLevel = (float) Global.getSector().getPersistentData().get("ds_personBountyLevel");
        if (faction.isAtBest(Factions.PIRATES, RepLevel.INHOSPITABLE) && thisLevel <= 15) {
            picker.add(BountyType.PIRATE, 10f);
        }
        if (faction.isAtBest(Factions.LUDDIC_PATH, RepLevel.INHOSPITABLE) && thisLevel <= 9) {
            picker.add(BountyType.PATHER, 1.5f);
        }
        if (faction.isAtBest("cabal", RepLevel.INHOSPITABLE) && baseLevel >= 4.5f) {
            picker.add(BountyType.CABAL, 1.5f);
        }
        if (baseLevel >= 3 || picker.isEmpty()) {
            if (thisLevel >= 10) {
                picker.add(BountyType.DESERTER, 30f);
            } else if (thisLevel >= 6) {
                picker.add(BountyType.DESERTER, 20f);
            } else {
                picker.add(BountyType.DESERTER, 10f);
            }
        }
        if ((baseLevel >= 6 || picker.isEmpty()) && Misc.getCommissionFactionId() != null &&
                Misc.getCommissionFactionId().contentEquals(faction.getId())) {
            if (thisLevel >= 15) {
                picker.add(BountyType.ASSASSINATION, 45f);
            } else if (thisLevel >= 10) {
                picker.add(BountyType.ASSASSINATION, 30f);
            } else {
                picker.add(BountyType.ASSASSINATION, 15f);
            }
        } else if ((baseLevel >= 6 || picker.isEmpty()) &&
                !faction.getCustomBoolean(Factions.CUSTOM_OFFERS_COMMISSIONS) &&
                faction.isAtWorst(Factions.PLAYER, RepLevel.WELCOMING)) {
            if (thisLevel >= 15) {
                picker.add(BountyType.ASSASSINATION, 4f);
            } else if (thisLevel >= 10) {
                picker.add(BountyType.ASSASSINATION, 3f);
            } else {
                picker.add(BountyType.ASSASSINATION, 2f);
            }
        }

        bountyType = picker.pick();

        if (bountyType == null) {
            endEvent();
        }
    }

    private void pickHideoutLocation() {
        FactionAPI fac;
        float emptyWeight;
        float occupiedWeight;
        if (bountyType == BountyType.PIRATE || bountyType == BountyType.DESERTER) {
            fac = Global.getSector().getFaction(Factions.PIRATES);
            emptyWeight = 1f;
            occupiedWeight = 1f;
        } else if (bountyType == BountyType.PATHER) {
            fac = Global.getSector().getFaction(Factions.LUDDIC_PATH);
            emptyWeight = 1f;
            occupiedWeight = 1f;
        } else if (bountyType == BountyType.CABAL) {
            fac = Global.getSector().getFaction("cabal");
            emptyWeight = 0.1f;
            occupiedWeight = 1f;
        } else if (bountyType == BountyType.MERCENARY) {
            fac = Global.getSector().getFaction(Factions.INDEPENDENT);
            emptyWeight = 0.2f;
            occupiedWeight = 1f;
        } else {
            fac = Global.getSector().getFaction(bountyFaction.getId());
            emptyWeight = 0f;
            occupiedWeight = 1f;
        }

        occupiedWeight *= Math.max(1f, 10f / Math.max(1f, thisLevel));

        float multMod = 1f;
        while (hideoutLocation == null && multMod <= 5f) {
            WeightedRandomPicker<StarSystemAPI> systemPicker = new WeightedRandomPicker<>();
            for (StarSystemAPI system : Global.getSector().getStarSystems()) {
                float weight = system.getPlanets().size();
                float mult = 0.5f;

                if (system.hasPulsar()) {
                    continue;
                }

                if (system.hasTag(Tags.THEME_MISC_SKIP)) {
                    mult = 1f;
                } else if (system.hasTag(Tags.THEME_MISC)) {
                    mult = 3f;
                } else if (system.hasTag(Tags.THEME_RUINS)) {
                    mult = 7f;
                } else if (system.hasTag(Tags.THEME_REMNANT_DESTROYED)) {
                    mult = 3f;
                } else if (system.hasTag(Tags.THEME_CORE_UNPOPULATED)) {
                    mult = 1f;
                }

                float distToPlayer = Misc.getDistanceToPlayerLY(system.getLocation());
                float noSpawnRange = Global.getSettings().getFloat("personBountyNoSpawnRangeAroundPlayerLY");
                if (distToPlayer < noSpawnRange) {
                    mult = 0f;
                }

                boolean empty = true;
                for (SectorEntityToken planet : system.getPlanets()) {
                    MarketAPI nearestMarket = findNearestHostileMarket(planet, 3000f * (5f / (thisLevel + 5f)), fac);
                    if (nearestMarket != null) {
                        continue;
                    }

                    if (planet.getMarket() != null && !planet.getMarket().isPlanetConditionMarketOnly()) {
                        empty = false;
                        break;
                    }
                }

                if (empty) {
                    mult *= emptyWeight;
                } else {
                    mult *= occupiedWeight;
                }

                if (mult <= 0) {
                    continue;
                }
                float maxDist = 30000f + thisLevel * 3000f;
                maxDist *= multMod;
                float dist = system.getLocation().length();
                float distMult = (Math.max(0, maxDist - dist) / maxDist) * dist;

                systemPicker.add(system, weight * mult * distMult);
            }

            StarSystemAPI system = systemPicker.pick();

            if (system != null) {
                WeightedRandomPicker<SectorEntityToken> picker = new WeightedRandomPicker<>();
                for (SectorEntityToken planet : system.getPlanets()) {
                    if (planet.isStar()) {
                        continue;
                    }
                    MarketAPI nearestMarket = findNearestHostileMarket(planet, 3000f * (5f / (thisLevel + 5f)), fac);
                    if (nearestMarket != null) {
                        continue;
                    }

                    picker.add(planet);
                }
                hideoutLocation = picker.pick();
            }

            multMod += 1f;
        }

        if (hideoutLocation == null) {
            endEvent();
        }
    }

    private void pickMarketAndFaction() {
        String commFacId = Misc.getCommissionFactionId();
        boolean forceCommissionFaction = true;
        if (commFacId != null && data.isParticipating(commFacId)) {
            CampaignEventManagerAPI em = Global.getSector().getEventManager();
            for (CampaignEventPlugin event : em.getOngoingEvents()) {
                if (!(event instanceof DS_PersonBountyEvent)) {
                    continue;
                }
                DS_PersonBountyEvent bounty = (DS_PersonBountyEvent) event;
                if (bounty.faction != null && bounty.faction.getId().equals(commFacId)) {
                    forceCommissionFaction = false;
                }
            }
        } else {
            forceCommissionFaction = false;
        }

        FactionAPI player = Global.getSector().getPlayerFaction();

        WeightedRandomPicker<MarketAPI> picker = new WeightedRandomPicker<>();
        float weightSumCommission = 0f;
        float totalWeight = 0f;
        for (MarketAPI mkt : Global.getSector().getEconomy().getMarketsCopy()) {
            if (!data.isParticipating(mkt.getFactionId()) && !DS_Defs.PIRATE_FACTIONS.contains(mkt.getFactionId())) {
                continue;
            }
            if (mkt.getSize() <= 3 && !DS_Defs.PIRATE_FACTIONS.contains(mkt.getFactionId())) {
                continue;
            }
            if (forceCommissionFaction && !mkt.getFaction().getId().equals(commFacId)) {
                continue;
            }

            float weight = mkt.getSize();
            float weightMod = 1f;
            if (mkt.hasCondition(Conditions.MILITARY_BASE)) {
                weightMod += 1f;
            }
            if (mkt.hasCondition("ii_militarycomplex")) {
                weightMod += 0.5f;
            }
            if (mkt.hasCondition(Conditions.HEADQUARTERS)) {
                weightMod += 1f;
            }
            if (mkt.hasCondition(Conditions.REGIONAL_CAPITAL)) {
                weightMod += 1f;
            }
            weight *= weightMod;

            if (mkt.getFaction() != null) {
                if (mkt.getFaction().isHostileTo(player)) {
                    weight *= 0.5f;
                }
            }

            if (weight <= 0) {
                continue;
            }

            if (commFacId != null && commFacId.contentEquals(mkt.getFactionId())) {
                weight *= 1.25f;
                weightSumCommission += weight;
            }
            totalWeight += weight;

            picker.add(mkt, weight);
        }

        if (picker.isEmpty()) {
            endEvent();
            return;
        }

        if (Misc.getCommissionFactionId() != null) {
            float compensation = Math.max(1f, 0.25f * (totalWeight / weightSumCommission)) - 1f;
            if (compensation > 0f) {
                int size = picker.getItems().size();
                for (int i = 0; i < size; i++) {
                    MarketAPI mkt = picker.getItems().get(i);
                    if (Misc.getCommissionFactionId().contentEquals(mkt.getFactionId())) {
                        picker.add(mkt, picker.getWeight(i) * compensation);
                    }
                }
            }
        }

        market = picker.pick();
        faction = market.getFaction();
        eventTarget = new CampaignEventTarget(market);
    }

    private void spawnFleet() {
        final String fleetFactionId;
        if (bountyType == BountyType.PIRATE) {
            fleetFactionId = Factions.PIRATES;
        } else if (bountyType == BountyType.PATHER) {
            fleetFactionId = Factions.LUDDIC_PATH;
        } else if (bountyType == BountyType.CABAL) {
            fleetFactionId = "cabal";
        } else if (bountyType == BountyType.DESERTER) {
            fleetFactionId = market.getFactionId();
        } else if (bountyType == BountyType.MERCENARY) {
            fleetFactionId = Factions.INDEPENDENT;
        } else {
            fleetFactionId = bountyFaction.getId();
        }

        String fleetName = person.getName().getLast() + "'s" + " Fleet";

        final int maxPts = 5 + (int) (thisLevel * 3.0 + Math.sqrt(thisLevel / 15.0 + 1.0));
        final int level = 5 + (int) thisLevel;

        fleet = DS_FleetFactory.enhancedCreateFleet(faction, Math.max(maxPts, 1), new FleetFactoryDelegate() {
            @Override
            public CampaignFleetAPI createFleet() {
                return FleetFactoryV2.createFleet(new FleetParams(hideoutLocation.getLocationInHyperspace(),
                                                                  market,
                                                                  fleetFactionId,
                                                                  FleetTypes.PERSON_BOUNTY_FLEET,
                                                                  maxPts, // combatPts
                                                                  0f, // freighterPts
                                                                  0f, // tankerPts
                                                                  0f, // transportPts
                                                                  0f, // linerPts
                                                                  0f, // civilianPts
                                                                  0f, // utilityPts
                                                                  0f, // qualityBonus
                                                                  qualityFactor, // qualityOverride
                                                                  1f + thisLevel / 20f, // officer num mult
                                                                  (int) thisLevel / 3, // officer level bonus
                                                                  person,
                                                                  level));
            }
        });

        if (fleet == null) {
            endEvent();
            return;
        }

        fleet.forceSync();

        /* We do this deferred level-up so that the commander's skills reflect the flagship type */
        float weight = FleetFactoryV2.getMemberWeight(fleet.getFlagship());
        float fighters = fleet.getFlagship().getVariant().getFittedWings().size();
        boolean wantCarrierSkills = weight > 0 && fighters / weight >= 0.5f;
        SkillPickPreference pref = SkillPickPreference.NON_CARRIER;
        if (wantCarrierSkills) {
            pref = SkillPickPreference.CARRIER;
        }
        DS_FleetFactory.levelOfficer(person, fleet.getFleetData(), (int) (5 + thisLevel * 1.5f), true, pref,
                                     new Random());

        /* Add in the commander skills, at last */
        FleetFactoryV2.addCommanderSkills(person, fleet, new Random());

        if (thisLevel <= 7f) {
            person.setRankId(Ranks.SPACE_COMMANDER);
        } else if (thisLevel <= 15f) {
            person.setRankId(Ranks.SPACE_CAPTAIN);
        } else {
            person.setRankId(Ranks.SPACE_ADMIRAL);
        }

        @SuppressWarnings("unchecked")
        Map<String, String> archetypeOverride = new HashMap<>(fleet.getFleetData().getOfficersCopy().size() + 1);
        if (thisLevel > 15f) {
            archetypeOverride.put(fleet.getFlagship().getId(), Archetype.ULTIMATE.name());
        } else if (thisLevel > 20f) {
            for (OfficerDataAPI officer : fleet.getFleetData().getOfficersCopy()) {
                FleetMemberAPI member = fleet.getFleetData().getMemberWithCaptain(officer.getPerson());
                if (member == null) {
                    continue;
                }

                archetypeOverride.put(member.getId(), Archetype.ULTIMATE.name());
            }
        }

        Misc.makeImportant(fleet, "pbe", duration);
        fleet.getMemoryWithoutUpdate().set("$level", level);
        fleet.getMemoryWithoutUpdate().set("$qualityFactor", qualityFactor);
        if (bountyType == BountyType.PIRATE || bountyType == BountyType.DESERTER) {
            fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_PIRATE, true);
        }
        if (bountyType == BountyType.PATHER) {
            fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_AGGRESSIVE, true);
        }

        fleet.getMemoryWithoutUpdate().set("$personBountyLevel", thisLevel);
        fleet.getMemoryWithoutUpdate().set("$personBountyFaction", fleetFactionId);
        fleet.getMemoryWithoutUpdate().set("$personBountyType", bountyType);
        fleet.getMemoryWithoutUpdate().set(DS_Defs.MEMORY_KEY_ARCHETYPE_OVERRIDE, archetypeOverride);
        if (bountyType == BountyType.MERCENARY || bountyType == BountyType.DESERTER) {
            Misc.makeLowRepImpact(fleet, "ds_pbe");
        }
        fleet.getMemoryWithoutUpdate().set("$opBonus", Math.max(0f, thisLevel - 15f));

        fleet.getFleetData().sort();
        fleet.setNoFactionInName(true);
        fleet.setName(person.getRank() + " " + fleetName);

        if (bountyType == BountyType.ASSASSINATION) {
            fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_PATROL_FLEET, true);
            fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_PATROL_ALLOW_TOFF, true);
        }

        FleetFactory.finishAndSync(fleet);

        LocationAPI location = hideoutLocation.getContainingLocation();
        location.addEntity(fleet);
        fleet.setLocation(hideoutLocation.getLocation().x - 500, hideoutLocation.getLocation().y + 500);
        fleet.getAI().addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, hideoutLocation, 1000000f, null);
    }

    @SuppressWarnings("unchecked")
    protected void pickLevel() {
        List<DS_PersonBountyEvent> activeBounties =
                                   (List<DS_PersonBountyEvent>) Global.getSector().getPersistentData().get(
                                           "ds_activebounties");
        if (activeBounties == null) {
            activeBounties = new LinkedList<>();
        }

        Float playerStrength = null;
        if (Global.getSector().getPlayerFleet() != null) {
            playerStrength = DS_Util.getNonCivilianFleetPoints(Global.getSector().getPlayerFleet().getFleetData()) *
            (0.3f +
             (float) Math.sqrt(Math.max(0f, Global.getSector().getPlayerPerson().getStats().getLevel() - 10f) / 30f));
        }

        float highStabilityMult = BaseMarketConditionPlugin.getHighStabilityBonusMult(market);
        float beginLevel = (float) Global.getSector().getPersistentData().get("ds_personBountyLevel") +
              (float) MathUtils.getRandom().nextGaussian() * 3f;
        if (beginLevel > 20f) {
            beginLevel = 20f;
        }
        if (beginLevel < 0f) {
            beginLevel = 0f;
        }
        if (beginLevel > market.getSize() * 2f + market.getSize() * market.getStability().getModifiedValue() * 0.15f) {
            beginLevel = market.getSize() * 2f + market.getSize() * market.getStability().getModifiedValue() * 0.15f;
        }
        thisLevel = beginLevel * ((float) Math.random() * 0.25f * (float) Math.sqrt(highStabilityMult) + 0.75f);
        if (playerStrength != null) {
            float desiredLevel = Math.max((playerStrength - 28f) / 17f, 0f);
            int matched = 0;
            for (DS_PersonBountyEvent bounty : activeBounties) {
                if (Math.abs(bounty.thisLevel - desiredLevel) <= 2.5f) {
                    matched++;
                }
            }
            if (matched < 2) {
                thisLevel = desiredLevel * (0.9f + (float) Math.random() * 0.2f);
            }
        }
        thisLevel = Math.max(thisLevel, 0f);
        thisLevel = Math.min(thisLevel, 25f);
    }

    public static enum BountyType {

        PIRATE, DESERTER, ASSASSINATION, MERCENARY, PATHER, CABAL
    }
}
