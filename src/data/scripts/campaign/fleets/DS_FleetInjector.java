package data.scripts.campaign.fleets;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.CampaignEventListener;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.fleets.PatrolFleetManager;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.campaign.DS_FleetFactory;
import data.scripts.campaign.events.DS_PersonBountyEvent.BountyType;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Defs.Archetype;
import data.scripts.util.DS_Defs.FleetStyle;
import data.scripts.util.DS_Util;
import data.scripts.variants.DS_FleetRandomizer;
import exerelin.campaign.AllianceManager;
import exerelin.campaign.alliances.Alliance;
import exerelin.utilities.ExerelinUtilsFleet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Logger;

public class DS_FleetInjector extends BaseCampaignEventListener implements EveryFrameScript {

    public static final Logger log = Global.getLogger(DS_FleetInjector.class);

    public static DS_FleetInjector getInjector() {
        for (EveryFrameScript script : Global.getSector().getScripts()) {
            if (script instanceof DS_FleetInjector) {
                return (DS_FleetInjector) script;
            }
        }

        return null;
    }

    public static void injectFleet(CampaignFleetAPI fleet) {
        String faction = fleet.getFaction().getId();
        MemoryAPI memory = fleet.getMemoryWithoutUpdate();
        MarketAPI market = Misc.getSourceMarket(fleet);

        float stability = market != null ? market.getStabilityValue() : 5f;
        float qualityFactor = market != null ? market.getShipQualityFactor() : 0.5f;

        stability = memory.contains("$stability") ? memory.getFloat("$stability") : stability;
        qualityFactor = memory.contains("$qualityFactor") ? memory.getFloat("$qualityFactor") : qualityFactor;

        Random r;
        if (memory.contains(DS_Defs.MEMORY_KEY_RANDOM_SEED)) {
            long seed = memory.getLong(DS_Defs.MEMORY_KEY_RANDOM_SEED);
            r = new Random(seed);
        } else {
            r = new Random();
        }

        String type =
               memory.contains(MemFlags.MEMORY_KEY_FLEET_TYPE) ? memory.getString(MemFlags.MEMORY_KEY_FLEET_TYPE) : null;
        if (type != null && !type.isEmpty()) {
            if (!DS_Defs.INJECTOR_AFFECTED_FACTIONS.contains(faction)) {
                return;
            }

            switch (type) {
                case FleetTypes.TRADE: {
                    // Trade Fleet
                    if (DS_Defs.INJECTOR_NO_SHARING_MOUNTS.contains(faction)) {
                        randomizeVariants(fleet, faction, null, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.CIVILIAN, faction), true, r);
                    } else {
                        List<String> otherFactions = new ArrayList<>(1);
                        otherFactions.add(Factions.INDEPENDENT);
                        randomizeVariants(fleet, faction, otherFactions, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.CIVILIAN, faction), true, r);
                    }
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, false, r);
                    break;
                }

                case FleetTypes.TRADE_SMUGGLER: {
                    // Smuggler Fleet
                    if (DS_Defs.INJECTOR_NO_SHARING_MOUNTS.contains(faction)) {
                        randomizeVariants(fleet, faction, null, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.STANDARD, faction), true, r);
                    } else {
                        List<String> otherFactions = new ArrayList<>(1);
                        otherFactions.add(Factions.PIRATES);
                        randomizeVariants(fleet, faction, otherFactions, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.STANDARD, faction), true, r);
                    }
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, false, r);
                    break;
                }

                case FleetTypes.TRADE_SMALL: {
                    // Small Trade Fleet
                    if (market == null) {
                        break;
                    }

                    String fromFaction = market.getFactionId();
                    if (!DS_Defs.INJECTOR_AFFECTED_FACTIONS.contains(fromFaction)) {
                        break;
                    }

                    if (DS_Defs.INJECTOR_NO_SHARING_MOUNTS.contains(fromFaction)) {
                        randomizeVariants(fleet, fromFaction, null, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.CIVILIAN, faction), true, r);
                    } else {
                        List<String> otherFactions = new ArrayList<>(1);
                        otherFactions.add(Factions.INDEPENDENT);
                        randomizeVariants(fleet, fromFaction, otherFactions, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.CIVILIAN, faction), true, r);
                    }
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, fromFaction, false, r);
                    break;
                }

                case FleetTypes.TRADE_LINER: {
                    // Liner Fleet
                    if (DS_Defs.INJECTOR_NO_SHARING_MOUNTS.contains(faction)) {
                        randomizeVariants(fleet, faction, null, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.CIVILIAN, faction), true, r);
                    } else {
                        List<String> otherFactions = new ArrayList<>(1);
                        otherFactions.add(Factions.INDEPENDENT);
                        randomizeVariants(fleet, faction, otherFactions, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.CIVILIAN, faction), true, r);
                    }
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, false, r);
                    break;
                }

                case FleetTypes.FOOD_RELIEF_FLEET: {
                    // Relief Fleet from Food Shortage Event
                    if (DS_Defs.INJECTOR_NO_SHARING_MOUNTS.contains(faction)) {
                        randomizeVariants(fleet, faction, null, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.CIVILIAN, faction), true, r);
                    } else {
                        List<String> otherFactions = new ArrayList<>(1);
                        otherFactions.add(Factions.INDEPENDENT);
                        randomizeVariants(fleet, faction, otherFactions, qualityFactor, null, null,
                                          DS_Util.getArchetypeWeights(FleetStyle.CIVILIAN, faction), true, r);
                    }
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, false, r);
                    break;
                }

                case FleetTypes.PATROL_SMALL:
                case FleetTypes.PATROL_MEDIUM:
                case FleetTypes.PATROL_LARGE: {
                    // Patrol Fleets
                    if (fleet.getNameWithFaction().startsWith(
                            Global.getSector().getFaction(Factions.LIONS_GUARD).getDisplayName())) {
                        Archetype theme = pickTheme(Factions.LIONS_GUARD, r);
                        DS_Util.setThemeName(fleet, theme);
                        List<String> extendedTheme = DS_Util.pickExtendedTheme(Factions.LIONS_GUARD, market, r);
                        DS_Util.setExtendedThemeName(fleet, extendedTheme);
                        randomizeVariants(fleet, Factions.LIONS_GUARD, extendedTheme, qualityFactor, null, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.ELITE, Factions.LIONS_GUARD), false, r);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, Factions.LIONS_GUARD, true, r);
                        break;
                    } else if (fleet.getFaction().getId().contentEquals(Factions.LUDDIC_PATH)) {
                        Archetype theme = pickTheme(Factions.LUDDIC_PATH, r);
                        DS_Util.setThemeName(fleet, theme);
                        List<String> extendedTheme = DS_Util.pickExtendedTheme(Factions.LUDDIC_PATH, market, r);
                        DS_Util.setExtendedThemeName(fleet, extendedTheme);
                        randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.RAIDER, faction), false, r);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                        break;
                    }

                    boolean useKOL = false;
                    if (fleet.getFaction().getId().contentEquals(Factions.LUDDIC_CHURCH)) {
                        if (r.nextFloat() < qualityFactor / 2f) {
                            useKOL = true;
                        }
                    }

                    if (useKOL) {
                        fleet.setNoFactionInName(true);
                        fleet.setName("Luddic Knights " + fleet.getName());
                        Archetype theme = pickTheme(Factions.KOL, r);
                        DS_Util.setThemeName(fleet, theme);
                        List<String> extendedTheme = DS_Util.pickExtendedTheme(Factions.KOL, market, r);
                        DS_Util.setExtendedThemeName(fleet, extendedTheme);
                        randomizeVariants(fleet, Factions.KOL, extendedTheme, qualityFactor, null, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.MILITARY, Factions.KOL), false, r);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, Factions.KOL, true, r);
                    } else {
                        Archetype theme = pickTheme(faction, r);
                        DS_Util.setThemeName(fleet, theme);
                        List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                        DS_Util.setExtendedThemeName(fleet, extendedTheme);
                        randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction), false, r);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    }
                    break;
                }

                case FleetTypes.MERC_SCOUT:
                case FleetTypes.MERC_BOUNTY_HUNTER:
                case FleetTypes.MERC_PRIVATEER:
                case FleetTypes.MERC_PATROL:
                case FleetTypes.MERC_ARMADA: {
                    // Merc / Pirate / Avesta Fleets
                    boolean isPirate = memory.contains(MemFlags.MEMORY_KEY_PIRATE) ? memory.getBoolean(
                                       MemFlags.MEMORY_KEY_PIRATE) : false;
                    boolean isAvestan = fleet.getFaction().getId().contentEquals("exipirated");

                    if (isPirate) {
                        if (isAvestan) {
                            Archetype theme = pickTheme("exipirated", r);
                            DS_Util.setThemeName(fleet, theme);
                            List<String> extendedTheme = DS_Util.pickExtendedTheme("exipirated", market, r);
                            DS_Util.setExtendedThemeName(fleet, extendedTheme);
                            switch (type) {
                                case FleetTypes.MERC_SCOUT:
                                case FleetTypes.MERC_BOUNTY_HUNTER:
                                    randomizeVariants(fleet, "exipirated", extendedTheme, qualityFactor, null, theme,
                                                      DS_Util.getArchetypeWeights(FleetStyle.RAIDER, "exipirated"),
                                                      true, r);
                                    break;
                                case FleetTypes.MERC_PRIVATEER:
                                case FleetTypes.MERC_PATROL:
                                case FleetTypes.MERC_ARMADA:
                                    randomizeVariants(fleet, "exipirated", extendedTheme, qualityFactor, null, theme,
                                                      DS_Util.getArchetypeWeights(FleetStyle.PROFESSIONAL, "exipirated"),
                                                      true, r);
                                    break;
                                default:
                                    break;
                            }
                            DS_FleetFactory.finishFleetNonIntrusive(fleet, "exipirated", true, r);
                        } else {
                            Archetype theme = pickTheme(Factions.PIRATES, r);
                            DS_Util.setThemeName(fleet, theme);
                            List<String> extendedTheme = DS_Util.pickExtendedTheme(Factions.PIRATES, market, r);
                            DS_Util.setExtendedThemeName(fleet, extendedTheme);
                            switch (type) {
                                case FleetTypes.MERC_SCOUT:
                                case FleetTypes.MERC_BOUNTY_HUNTER: {
                                    randomizeVariants(fleet, Factions.PIRATES, extendedTheme, qualityFactor, null, theme,
                                                      DS_Util.getArchetypeWeights(FleetStyle.RAIDER, Factions.PIRATES),
                                                      true, r);
                                    break;
                                }
                                case FleetTypes.MERC_PRIVATEER:
                                case FleetTypes.MERC_PATROL:
                                case FleetTypes.MERC_ARMADA: {
                                    randomizeVariants(fleet, Factions.PIRATES, extendedTheme, qualityFactor, null, theme,
                                                      DS_Util.getArchetypeWeights(FleetStyle.STANDARD, Factions.PIRATES),
                                                      true, r);
                                    break;
                                }
                                default:
                                    break;
                            }
                            DS_FleetFactory.finishFleetNonIntrusive(fleet, Factions.PIRATES, true, r);
                        }
                    } else {
                        Archetype theme = pickTheme(faction, r);
                        DS_Util.setThemeName(fleet, theme);
                        List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                        DS_Util.setExtendedThemeName(fleet, extendedTheme);
                        switch (type) {
                            case FleetTypes.MERC_SCOUT:
                            case FleetTypes.MERC_BOUNTY_HUNTER: {
                                if (DS_Defs.INJECTOR_NO_SHARING_MOUNTS.contains(faction)) {
                                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                                      DS_Util.getArchetypeWeights(FleetStyle.RAIDER, faction),
                                                      false, r);
                                } else {
                                    extendedTheme.add(Factions.INDEPENDENT);
                                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                                      DS_Util.getArchetypeWeights(FleetStyle.RAIDER, faction),
                                                      false, r);
                                }
                                break;
                            }
                            case FleetTypes.MERC_PRIVATEER:
                            case FleetTypes.MERC_PATROL:
                            case FleetTypes.MERC_ARMADA: {
                                if (DS_Defs.INJECTOR_NO_SHARING_MOUNTS.contains(faction)) {
                                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                                      DS_Util.getArchetypeWeights(FleetStyle.PROFESSIONAL, faction),
                                                      false, r);
                                } else {
                                    extendedTheme.add(Factions.INDEPENDENT);
                                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                                      DS_Util.getArchetypeWeights(FleetStyle.PROFESSIONAL, faction),
                                                      false, r);
                                }
                                break;
                            }
                            default:
                                break;
                        }
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, false, r);
                    }

                    break;
                }

                case FleetTypes.SCAVENGER_SMALL:
                case FleetTypes.SCAVENGER_MEDIUM:
                case FleetTypes.SCAVENGER_LARGE: {
                    // Scavenger Fleets
                    faction = Factions.SCAVENGERS;

                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.STANDARD, faction), true, r);

                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, false, r);
                    break;
                }

                case FleetTypes.BATTLESTATION: {
                    // Battlestation Entities
                    randomizeVariants(fleet, faction, null, 1f, null, null,
                                      DS_Util.getArchetypeWeights(FleetStyle.ELITE, faction), false, r);

                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case FleetTypes.PERSON_BOUNTY_FLEET: {
                    // Bounty Fleet from Person Bounty Event
                    String bountyFaction;
                    BountyType bountyType;
                    int level;
                    if (fleet.getCommanderStats() != null) {
                        level = fleet.getCommanderStats().getLevel();
                    } else {
                        level = 0;
                    }

                    bountyFaction = memory.contains("$personBountyFaction") ? memory.getString(
                                    "$personBountyFaction") : Factions.PIRATES;
                    bountyType = memory.contains("$personBountyType") ?
                                 (BountyType) memory.get("$personBountyType") :
                                 BountyType.PIRATE;
                    level = memory.contains("$level") ? (int) memory.getFloat("$level") : level;
                    float opBonus = memory.contains("$opBonus") ? memory.getFloat("$opBonus") : 0f;

                    if (!DS_Defs.INJECTOR_AFFECTED_FACTIONS.contains(bountyFaction)) {
                        return;
                    }

                    boolean noobFriendly = false;
                    if (level <= 5f) {
                        noobFriendly = true;
                    }

                    Archetype theme = pickTheme(bountyFaction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(bountyFaction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    if (bountyType == BountyType.PIRATE) {
                        randomizeVariants(fleet, bountyFaction, extendedTheme, qualityFactor, opBonus, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.STANDARD, bountyFaction), level,
                                          noobFriendly, r);
                    } else if (bountyType == BountyType.PATHER) {
                        randomizeVariants(fleet, bountyFaction, extendedTheme, qualityFactor, opBonus, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.RAIDER, bountyFaction), level,
                                          noobFriendly, r);
                    } else if (bountyType == BountyType.CABAL) {
                        randomizeVariants(fleet, bountyFaction, extendedTheme, qualityFactor, opBonus, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.RAIDER, bountyFaction), level,
                                          noobFriendly, r);
                    } else if (bountyType == BountyType.DESERTER) {
                        randomizeVariants(fleet, bountyFaction, extendedTheme, qualityFactor, opBonus, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.MILITARY, bountyFaction), level,
                                          noobFriendly, r);
                    } else if (bountyType == BountyType.MERCENARY) {
                        randomizeVariants(fleet, bountyFaction, extendedTheme, qualityFactor, opBonus, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.PROFESSIONAL, bountyFaction), level,
                                          noobFriendly, r);
                    } else if (bountyType == BountyType.ASSASSINATION) {
                        randomizeVariants(fleet, bountyFaction, extendedTheme, qualityFactor, opBonus, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.ELITE, bountyFaction), level,
                                          noobFriendly, r);
                    }
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, bountyFaction, true, r);
                    break;
                }

                case "famousBounty": {
                    // Famous Bounty Fleet from Famous Bounty Event
                    String bountyFaction = memory.contains("$famousBountyFaction") ? memory.getString(
                                           "$famousBountyFaction") : Factions.PIRATES;
                    int level = 0;
                    if (fleet.getCommanderStats() == null) {
                        level = fleet.getCommanderStats().getLevel();
                    }
                    level = memory.contains("$level") ? (int) memory.getFloat("$level") : level;

                    float opBonus = memory.contains("$opBonus") ? memory.getFloat("$opBonus") : 0f;

                    if (!DS_Defs.INJECTOR_AFFECTED_FACTIONS.contains(bountyFaction)) {
                        return;
                    }

                    randomizeVariants(fleet, bountyFaction, null, qualityFactor, opBonus, null,
                                      DS_Util.getArchetypeWeights(FleetStyle.ELITE, bountyFaction), level, false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, bountyFaction, true, r);
                    break;
                }

                case "vengeanceFleet": {
                    // Faction Vengeance Event Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "cabalFleet": {
                    // Starlight Cabal Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.RAIDER, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "templarAttack1":
                case "templarDefense1":
                case "templarAttack2":
                case "templarDefense2":
                case "templarAttack3":
                case "templarDefense3":
                case "templarAttack4":
                case "templarDefense4": {
                    // Templar Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "tem_responseFleet": {
                    // Crusade Response Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "imperiumISA": {
                    // ISA Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.PROFESSIONAL, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "imperiumSiegeFleet": {
                    // Imperial Siege Fleet
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, null,
                                      DS_Util.getArchetypeWeights(FleetStyle.ELITE, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "imperiumSiegeEscort": {
                    // Imperial Blockade Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.PROFESSIONAL, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "imperialDefense": {
                    // Imperial Defense Fleet
                    randomizeVariants(fleet, faction, null, qualityFactor, null, null,
                                      DS_Util.getArchetypeWeights(FleetStyle.ELITE, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "imperiumHegWarFleet": {
                    // Hegemony War Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "churchPurificationFleet": {
                    // Luddic Church Purification Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "churchEscortFleet": {
                    // Luddic Church Escort Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "exigencyAttack":
                case "exigencyDefense": {
                    // Exigency Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.MILITARY, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "exipiratedCollectors": {
                    // Ahriman Collector Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, null,
                                      DS_Util.getArchetypeWeights(FleetStyle.PROFESSIONAL, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "SCYspyFleet": {
                    // Scy Spy Fleet
                    Archetype theme = pickTheme(faction, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.ELITE, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "tiandongMining": {
                    // Tiandong Mining Fleet
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    extendedTheme.add(Factions.INDEPENDENT);
                    randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, null,
                                      DS_Util.getArchetypeWeights(FleetStyle.STANDARD, faction), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, true, r);
                    break;
                }

                case "uw_dickerson": {
                    Archetype theme = pickTheme(Factions.PIRATES, r);
                    DS_Util.setThemeName(fleet, theme);
                    List<String> extendedTheme = DS_Util.pickExtendedTheme(Factions.PIRATES, market, r);
                    DS_Util.setExtendedThemeName(fleet, extendedTheme);
                    randomizeVariants(fleet, Factions.PIRATES, extendedTheme, qualityFactor, 5f, theme,
                                      DS_Util.getArchetypeWeights(FleetStyle.ELITE, Factions.PIRATES), false, r);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, Factions.PIRATES, true, r);
                    break;
                }

                default: {
                    if (DSModPlugin.isExerelin) {
                        ExerelinUtilsFleet.injectFleet(fleet, market, stability, qualityFactor, type);
                    } else {
                        Archetype theme = pickTheme(faction, r);
                        DS_Util.setThemeName(fleet, theme);
                        List<String> extendedTheme = DS_Util.pickExtendedTheme(faction, market, r);
                        DS_Util.setExtendedThemeName(fleet, extendedTheme);
                        randomizeVariants(fleet, faction, extendedTheme, qualityFactor, null, theme,
                                          DS_Util.getArchetypeWeights(FleetStyle.STANDARD, faction), false, r);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, faction, false, r);
                    }
                }
            }
        }
    }

    public static Archetype pickTheme(String faction, Random r) {
        WeightedRandomPicker<Archetype> picker = new WeightedRandomPicker<>(r);
        if (faction.contentEquals("templars")) {
            picker.add(null, 10f);
        } else {
            picker.add(null, 40f);
        }
        picker.add(Archetype.ARTILLERY, 2f);
        picker.add(Archetype.ASSAULT, 3f);
        picker.add(Archetype.ELITE, 1f);
        picker.add(Archetype.SKIRMISH, 3f);
        picker.add(Archetype.STRIKE, 3f);
        picker.add(Archetype.ULTIMATE, 0.25f);
        return picker.pick();
    }

    public static void prepFleet(CampaignFleetAPI fleet) {
        MemoryAPI memory = fleet.getMemoryWithoutUpdate();

        MarketAPI market = Misc.getSourceMarket(fleet);
        if (market == null && fleet.getContainingLocation() != null) {
            market = DS_Util.findNearestLocalMarketWithSameFaction(fleet);
            if (market != null) {
                memory.set(MemFlags.MEMORY_KEY_SOURCE_MARKET, market.getId());
            }
        }

        memory.set(DS_Defs.MEMORY_KEY_RANDOM_SEED, getInjector().masterSeeder.nextLong());

        if (market != null) {
            if (!memory.contains("$stability")) {
                memory.set("$stability", market.getStabilityValue());
            }
            if (!memory.contains("$qualityFactor")) {
                memory.set("$qualityFactor", market.getShipQualityFactor());
            }
        }
    }

    public static void randomizeVariants(CampaignFleetAPI fleet, String faction, List<String> otherFactions,
                                         float qualityFactor, Float opBonus, Archetype theme,
                                         Map<Archetype, Float> archetypeWeights, boolean noobFriendly, Random r) {
        int level = 0;
        if (fleet.getCommanderStats() != null) {
            level = fleet.getCommanderStats().getLevel();
        }
        randomizeVariants(fleet, faction, otherFactions, qualityFactor, opBonus, theme, archetypeWeights, level,
                          noobFriendly, r);
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static void randomizeVariants(CampaignFleetAPI fleet, String faction, List<String> otherFactions,
                                         float qualityFactor, Float opBonus, Archetype theme,
                                         Map<Archetype, Float> archetypeWeights, int commanderLevel,
                                         boolean noobFriendly, Random r) {
        if (!DSModPlugin.Module_ProceduralVariants) {
            return;
        }

        if (Global.getSector().getPlayerPerson() != null &&
                Global.getSector().getPlayerPerson().getStats().getLevel() >= 40) {
            noobFriendly = false;
        }

        float ordnanceBonus = commanderLevel / 2f;
        if (noobFriendly) {
            ordnanceBonus = 0f;
        }
        if (opBonus != null) {
            ordnanceBonus += opBonus;
        }
        if (!DSModPlugin.Module_ScaledVariants) {
            ordnanceBonus = 0f;
        }

        List<String> factionList = new ArrayList<>(5);
        if (faction != null) {
            factionList.add(faction);
        }
        if (otherFactions != null) {
            factionList.addAll(otherFactions);
        }

        if (DSModPlugin.isExerelin) {
            List<Alliance> alliances = AllianceManager.getAllianceList();
            for (Alliance alliance : alliances) {
                if (alliance.getMembersCopy().contains(faction)) {
                    for (String member : alliance.getMembersCopy()) {
                        if (faction == null || !member.contentEquals(faction)) {
                            /* Add member to list even if it's already in otherFactions, so that the weights compound
                             * together to some degree. */
                            factionList.add(member);
                        }
                    }
                }
            }
        }

        @SuppressWarnings("unchecked")
        Map<String, String> archetypeOverride = (Map<String, String>) fleet.getMemoryWithoutUpdate().get(
                            DS_Defs.MEMORY_KEY_ARCHETYPE_OVERRIDE);

        @SuppressWarnings("unchecked")
        List<String> doNotRandomize = (List<String>) fleet.getMemoryWithoutUpdate().get(
                     DS_Defs.MEMORY_KEY_DO_NOT_RANDOMIZE);

        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            if (doNotRandomize != null && doNotRandomize.contains(member.getId())) {
                continue;
            }

            List<String> roleList = DS_FleetRandomizer.deconstructShip(member.getVariant(), faction, r);
            Archetype type = theme;
            if (archetypeOverride != null && archetypeOverride.containsKey(member.getId())) {
                type = Archetype.valueOf(archetypeOverride.get(member.getId()));
            }

            DS_FleetRandomizer.randomizeVariant(member, factionList, roleList, qualityFactor, type,
                                                archetypeWeights, ordnanceBonus, noobFriendly, r);
        }
    }

    private static void reassignPatrolSpawningScripts() {
        if (!DSModPlugin.isExerelin) {
            for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
                if (market.getPrimaryEntity().hasScriptOfClass(PatrolFleetManager.class) &&
                        !market.getPrimaryEntity().hasScriptOfClass(DS_PatrolFleetManager.class)) {
                    CampaignEventListener listener = null;
                    for (CampaignEventListener l : Global.getSector().getAllListeners()) {
                        if (l instanceof PatrolFleetManager) {
                            listener = l;
                        }
                    }
                    if (listener != null) {
                        Global.getSector().removeListener(listener);
                    }
                    market.getPrimaryEntity().removeScriptsOfClass(PatrolFleetManager.class);
                    market.getPrimaryEntity().addScript(new DS_PatrolFleetManager(market));
                }
            }
        }
    }

    private final List<CampaignFleetAPI> newFleets = new LinkedList<>();
    private final IntervalUtil timer = new IntervalUtil(0.5f, 1.5f);

    protected Random masterSeeder = new Random();

    @SuppressWarnings("LeakingThisInConstructor")
    public DS_FleetInjector() {
        super(true);
    }

    @Override
    public void advance(float amount) {
        Iterator<CampaignFleetAPI> iter = newFleets.iterator();
        while (iter.hasNext()) {
            CampaignFleetAPI fleet = iter.next();
            prepFleet(fleet);
            iter.remove();
        }

        LocationAPI location = Global.getSector().getPlayerFleet().getContainingLocation();
        if (location != null) {
            for (CampaignFleetAPI fleet : location.getFleets()) {
                if (fleet != Global.getSector().getPlayerFleet() && fleet.isVisibleToPlayerFleet()) {
                    if (!fleet.getMemoryWithoutUpdate().getBoolean("$dynasectorInjected")) {
                        injectFleet(fleet);
                        fleet.getMemoryWithoutUpdate().set("$dynasectorInjected", true);
                    }
                }
            }
        }

        float days = Global.getSector().getClock().convertToDays(amount);

        timer.advance(days);
        if (timer.intervalElapsed()) {
            reassignPatrolSpawningScripts();
        }
    }

    public void setMasterSeed(long seed) {
        log.info("Using seed " + seed);
        masterSeeder = new Random(seed);
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public void reportFleetSpawned(CampaignFleetAPI fleet) {
        newFleets.add(fleet);
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }
}
