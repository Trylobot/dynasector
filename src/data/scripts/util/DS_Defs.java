package data.scripts.util;

import com.fs.starfarer.api.impl.campaign.ids.Factions;
import data.scripts.variants.DS_WeaponGrouper.WeaponGroupParam;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DS_Defs {

    public static final Map<String, Set<String>> ADD_FACTION_HULLMOD = new HashMap<>(30);

    public static final Set<String> BANNED_HULLS = new HashSet<>(10);
    public static final Set<String> BANNED_SYSTEMS = new HashSet<>(1);

    public static final Map<Archetype, Set<String>> BLOCK_ARCHETYPE_HULLMOD = new EnumMap<>(Archetype.class);
    public static final Map<String, Set<String>> BLOCK_FACTION_HULLMOD = new HashMap<>(30);

    public static final Map<String, Float> FACTION_ADJUST = new HashMap<>(4);
    public static final Map<String, Float> FACTION_THEMING_CHANCE = new HashMap<>(35);

    public static final Set<String> GROUPER_NO_SPLIT = new HashSet<>(4);
    public static final Map<WeaponGroupParam, Set<String>> GROUPER_TYPE_OVERRIDE = new HashMap<>(15);

    public static final Set<String> INJECTOR_AFFECTED_FACTIONS = new HashSet<>(31);
    public static final Set<String> INJECTOR_NO_MATCHING_WEAPONS = new HashSet<>(6);
    public static final Set<String> INJECTOR_NO_SHARING_MOUNTS = new HashSet<>(5);

    public static final Set<String> MARKET_AFFECTED_FACTIONS = new HashSet<>(24);

    /* Keys must be type Map<String, String>, each entry key is a member ID, each entry value is an enum value */
    public static final String MEMORY_KEY_ARCHETYPE_OVERRIDE = "$ds_archetype_override";

    /* Keys must be type List<String>, each entry value is a member ID */
    public static final String MEMORY_KEY_DO_NOT_RANDOMIZE = "$ds_do_not_randomize";

    /* Keys must be type List<String>, each entry value is a faction ID */
    public static final String MEMORY_KEY_MARKET_INFLUENCES = "$ds_market_influences";

    /* Keys must be type long */
    public static final String MEMORY_KEY_RANDOM_SEED = "$ds_random_seed";

    public static final Set<String> PIRATE_FACTIONS = new HashSet<>(3);

    public static final Map<String, Integer> SHIELD_QUALITY = new HashMap<>(500);

    public static final Set<String> SPECIAL_HULLS = new HashSet<>(36);

    public static final String STAT_BATTLE_DEBRIS_CHANCE = "ds_debris_chance_battle";
    public static final String STAT_BATTLE_DEBRIS_SCALE = "ds_debris_scale_battle";
    public static final String STAT_BATTLE_DERELICTS_CHANCE = "ds_derelicts_chance_battle";
    public static final String STAT_BATTLE_DERELICTS_SCALE = "ds_derelicts_scale_battle";

    public static final String STAT_FLEET_DEBRIS_CHANCE = "ds_debris_chance_fleet";
    public static final String STAT_FLEET_DEBRIS_SCALE = "ds_debris_scale_fleet";
    public static final String STAT_FLEET_DERELICTS_CHANCE = "ds_derelicts_chance_fleet";
    public static final String STAT_FLEET_DERELICTS_SCALE = "ds_derelicts_scale_fleet";

    static {
        FACTION_ADJUST.put(Factions.TRITACHYON, 1.1f);
        FACTION_ADJUST.put("blackrock_driveyards", 1.15f);
        FACTION_ADJUST.put("cabal", 1.25f);
        FACTION_ADJUST.put("templars", 1.5f);
    }

    static {
        FACTION_THEMING_CHANCE.put(Factions.HEGEMONY, 0.05f);
        FACTION_THEMING_CHANCE.put(Factions.DIKTAT, 0.05f);
        FACTION_THEMING_CHANCE.put(Factions.INDEPENDENT, 0.2f);
        FACTION_THEMING_CHANCE.put(Factions.SCAVENGERS, 0.4f);
        FACTION_THEMING_CHANCE.put(Factions.KOL, 0.1f);
        FACTION_THEMING_CHANCE.put(Factions.LIONS_GUARD, 0.1f);
        FACTION_THEMING_CHANCE.put(Factions.LUDDIC_CHURCH, 0.15f);
        FACTION_THEMING_CHANCE.put(Factions.LUDDIC_PATH, 0.2f);
        FACTION_THEMING_CHANCE.put(Factions.NEUTRAL, 0.2f);
        FACTION_THEMING_CHANCE.put(Factions.PIRATES, 0.3f);
        FACTION_THEMING_CHANCE.put(Factions.TRITACHYON, 0.1f);
        FACTION_THEMING_CHANCE.put(Factions.PERSEAN, 0.15f);
        FACTION_THEMING_CHANCE.put(Factions.DERELICT, 0f);
        FACTION_THEMING_CHANCE.put(Factions.REMNANTS, 0.1f);
        FACTION_THEMING_CHANCE.put("cabal", 0.5f);
        FACTION_THEMING_CHANCE.put("interstellarimperium", 0.1f);
        FACTION_THEMING_CHANCE.put("citadeldefenders", 0.2f);
        FACTION_THEMING_CHANCE.put("blackrock_driveyards", 0.05f);
        FACTION_THEMING_CHANCE.put("exigency", 0f);
        FACTION_THEMING_CHANCE.put("exipirated", 0.25f);
        FACTION_THEMING_CHANCE.put("templars", 0f);
        FACTION_THEMING_CHANCE.put("shadow_industry", 0.05f);
        FACTION_THEMING_CHANCE.put("mayorate", 0.15f);
        FACTION_THEMING_CHANCE.put("junk_pirates", 0.5f);
        FACTION_THEMING_CHANCE.put("pack", 0.2f);
        FACTION_THEMING_CHANCE.put("syndicate_asp", 0.15f);
        FACTION_THEMING_CHANCE.put("SCY", 0.05f);
        FACTION_THEMING_CHANCE.put("tiandong", 0.15f);
        FACTION_THEMING_CHANCE.put("diableavionics", 0f);
        FACTION_THEMING_CHANCE.put("ORA", 0.15f);
        FACTION_THEMING_CHANCE.put("player_npc", 0.3f);
        FACTION_THEMING_CHANCE.put("famous_bounty", 0f);
        FACTION_THEMING_CHANCE.put("domain", 0f);
        FACTION_THEMING_CHANCE.put("sector", 0f);
        FACTION_THEMING_CHANCE.put("everything", 0f);
    }

    static {
        BANNED_SYSTEMS.add("Tartarus"); // Kind of a pain
    }

    static {
        PIRATE_FACTIONS.add(Factions.PIRATES);
        PIRATE_FACTIONS.add("exipirated");
        PIRATE_FACTIONS.add("junk_pirates");
    }

    static {
        BANNED_HULLS.add("ssp_superhyperion");
        BANNED_HULLS.add("ssp_oberon");
        BANNED_HULLS.add("ssp_ultron");
        BANNED_HULLS.add("ssp_zeus");
        BANNED_HULLS.add("ssp_ezekiel");
        BANNED_HULLS.add("ssp_cristarium");
        BANNED_HULLS.add("ssp_zero");
        BANNED_HULLS.add("ssp_superzero");
        BANNED_HULLS.add("ssp_hyperzero");
        BANNED_HULLS.add("ssp_archangel");
        BANNED_HULLS.add("swp_banana");
    }

    static {
        for (Archetype archetype : Archetype.values()) {
            BLOCK_ARCHETYPE_HULLMOD.put(archetype, new HashSet<String>(12));
        }
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARCADE).add("cargo_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARCADE).add("additional_crew_quarters");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARCADE).add("supply_conservation_program");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARCADE).add("maximized_ordinance");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARCADE).add("fuel_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARCADE).add("shieldbypass");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARCADE).add("surveying_equipment");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARTILLERY).add("cargo_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARTILLERY).add("additional_crew_quarters");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARTILLERY).add("fuel_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARTILLERY).add("safetyoverrides");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ARTILLERY).add("surveying_equipment");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ASSAULT).add("cargo_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ASSAULT).add("additional_crew_quarters");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ASSAULT).add("fuel_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ASSAULT).add("supply_conservation_program");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ASSAULT).add("SCY_lightArmor");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ASSAULT).add("surveying_equipment");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.BALANCED).add("maximized_ordinance");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.BALANCED).add("shieldbypass");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.BALANCED).add("SCY_lightArmor");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.BALANCED).add("surveying_equipment");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.CLOSE_SUPPORT).add("cargo_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.CLOSE_SUPPORT).add("additional_crew_quarters");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.CLOSE_SUPPORT).add("fuel_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.CLOSE_SUPPORT).add("safetyoverrides");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.CLOSE_SUPPORT).add("surveying_equipment");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("cargo_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("additional_crew_quarters");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("fuel_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("supply_conservation_program");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("shieldbypass");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("blast_doors");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("dedicated_targeting_core");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("solar_shielding");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("unstable_injector");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("surveying_equipment");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("recovery_shuttles");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ELITE).add("augmentedengines");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ESCORT).add("shieldbypass");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ESCORT).add("safetyoverrides");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ESCORT).add("SCY_lightArmor");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ESCORT).add("maximized_ordinance");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("expanded_deck_crew");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("maximized_ordinance");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("shieldbypass");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("brtarget");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("advancedoptics");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("eccm");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("magazines");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("missleracks");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("fluxcoil");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("hardened_subsystems");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("pointdefenseai");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("safetyoverrides");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.FLEET).add("unstable_injector");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.SKIRMISH).add("cargo_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.SKIRMISH).add("additional_crew_quarters");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.SKIRMISH).add("fuel_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.SKIRMISH).add("heavyarmor");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.SKIRMISH).add("ii_energized_armor");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.SKIRMISH).add("ii_fire_control");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.STRIKE).add("cargo_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.STRIKE).add("additional_crew_quarters");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.STRIKE).add("fuel_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.STRIKE).add("ii_fire_control");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.STRIKE).add("surveying_equipment");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.SUPPORT).add("maximized_ordinance");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.SUPPORT).add("shieldbypass");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.SUPPORT).add("safetyoverrides");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("cargo_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("additional_crew_quarters");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("fuel_expansion");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("supply_conservation_program");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("shieldbypass");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("blast_doors");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("dedicated_targeting_core");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("solar_shielding");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("unstable_injector");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("surveying_equipment");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("recovery_shuttles");
        BLOCK_ARCHETYPE_HULLMOD.get(Archetype.ULTIMATE).add("augmentedengines");
    }

    static {
        ADD_FACTION_HULLMOD.put(Factions.DIKTAT, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.HEGEMONY, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.INDEPENDENT, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.PERSEAN, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.KOL, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.LIONS_GUARD, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.LUDDIC_CHURCH, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.LUDDIC_PATH, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.PIRATES, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.TRITACHYON, new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put(Factions.DERELICT, new HashSet<String>(16));
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("turretgyros");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("armoredweapons");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("auxiliarythrusters");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("dedicated_targeting_core");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("eccm");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("magazines");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("missleracks");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("hardened_subsystems");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("insulatedengine");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("reinforcedhull");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("fluxbreakers");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("unstable_injector");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("autorepair");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("fluxcoil");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("fluxdistributor");
        ADD_FACTION_HULLMOD.get(Factions.DERELICT).add("maximized_ordinance");
        ADD_FACTION_HULLMOD.put(Factions.REMNANTS, new HashSet<String>(29));
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("advancedshieldemitter");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("advancedoptics");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("turretgyros");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("auxiliarythrusters");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("eccm");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("missleracks");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("extendedshieldemitter");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("fluxcoil");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("fluxdistributor");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("frontemitter");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("hardenedshieldemitter");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("hardened_subsystems");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("insulatedengine");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("pointdefenseai");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("targetingunit");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("adaptiveshields");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("fluxbreakers");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("stabilizedshieldemitter");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("nav_relay");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("ecm");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("armoredweapons");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("magazines");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("reinforcedhull");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("unstable_injector");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("autorepair");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("heavyarmor");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("solar_shielding");
        ADD_FACTION_HULLMOD.get(Factions.REMNANTS).add("maximized_ordinance");
        ADD_FACTION_HULLMOD.put(Factions.SCAVENGERS, new HashSet<String>(31));
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("turretgyros");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("armoredweapons");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("autorepair");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("auxiliarythrusters");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("blast_doors");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("dedicated_targeting_core");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("eccm");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("magazines");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("missleracks");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("fluxcoil");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("fluxdistributor");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("hardened_subsystems");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("insulatedengine");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("reinforcedhull");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("fluxbreakers");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("surveying_equipment");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("unstable_injector");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("expanded_deck_crew");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("recovery_shuttles");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("operations_center");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("nav_relay");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("ecm");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("cargo_expansion");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("additional_crew_quarters");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("fuel_expansion");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("supply_conservation_program");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("maximized_ordinance");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("advanced_ai_core");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("shieldbypass");
        ADD_FACTION_HULLMOD.get(Factions.SCAVENGERS).add("converted_hanger");
        ADD_FACTION_HULLMOD.put("cabal", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("interstellarimperium", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("citadeldefenders", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("blackrock_driveyards", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("exigency", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("exipirated", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("templars", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("shadow_industry", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("mayorate", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("junk_pirates", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("pack", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("syndicate_asp", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("SCY", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("tiandong", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("diableavionics", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("ORA", new HashSet<String>(0));
        ADD_FACTION_HULLMOD.put("everything", new HashSet<String>(49));
        ADD_FACTION_HULLMOD.get("everything").add("advancedshieldemitter");
        ADD_FACTION_HULLMOD.get("everything").add("advancedoptics");
        ADD_FACTION_HULLMOD.get("everything").add("turretgyros");
        ADD_FACTION_HULLMOD.get("everything").add("armoredweapons");
        ADD_FACTION_HULLMOD.get("everything").add("augmentedengines");
        ADD_FACTION_HULLMOD.get("everything").add("autorepair");
        ADD_FACTION_HULLMOD.get("everything").add("auxiliarythrusters");
        ADD_FACTION_HULLMOD.get("everything").add("blast_doors");
        ADD_FACTION_HULLMOD.get("everything").add("dedicated_targeting_core");
        ADD_FACTION_HULLMOD.get("everything").add("eccm");
        ADD_FACTION_HULLMOD.get("everything").add("magazines");
        ADD_FACTION_HULLMOD.get("everything").add("missleracks");
        ADD_FACTION_HULLMOD.get("everything").add("extendedshieldemitter");
        ADD_FACTION_HULLMOD.get("everything").add("fluxcoil");
        ADD_FACTION_HULLMOD.get("everything").add("fluxdistributor");
        ADD_FACTION_HULLMOD.get("everything").add("frontemitter");
        ADD_FACTION_HULLMOD.get("everything").add("frontshield");
        ADD_FACTION_HULLMOD.get("everything").add("hardenedshieldemitter");
        ADD_FACTION_HULLMOD.get("everything").add("hardened_subsystems");
        ADD_FACTION_HULLMOD.get("everything").add("heavyarmor");
        ADD_FACTION_HULLMOD.get("everything").add("insulatedengine");
        ADD_FACTION_HULLMOD.get("everything").add("pointdefenseai");
        ADD_FACTION_HULLMOD.get("everything").add("targetingunit");
        ADD_FACTION_HULLMOD.get("everything").add("adaptiveshields");
        ADD_FACTION_HULLMOD.get("everything").add("reinforcedhull");
        ADD_FACTION_HULLMOD.get("everything").add("fluxbreakers");
        ADD_FACTION_HULLMOD.get("everything").add("safetyoverrides");
        ADD_FACTION_HULLMOD.get("everything").add("solar_shielding");
        ADD_FACTION_HULLMOD.get("everything").add("stabilizedshieldemitter");
        ADD_FACTION_HULLMOD.get("everything").add("surveying_equipment");
        ADD_FACTION_HULLMOD.get("everything").add("unstable_injector");
        ADD_FACTION_HULLMOD.get("everything").add("expanded_deck_crew");
        ADD_FACTION_HULLMOD.get("everything").add("recovery_shuttles");
        ADD_FACTION_HULLMOD.get("everything").add("operations_center");
        ADD_FACTION_HULLMOD.get("everything").add("nav_relay");
        ADD_FACTION_HULLMOD.get("everything").add("ecm");
        ADD_FACTION_HULLMOD.get("everything").add("ii_energized_armor");
        ADD_FACTION_HULLMOD.get("everything").add("ii_fire_control");
        ADD_FACTION_HULLMOD.get("everything").add("diableavionics_universaldecks");
        ADD_FACTION_HULLMOD.get("everything").add("brassaultop");
        ADD_FACTION_HULLMOD.get("everything").add("brtarget");
        ADD_FACTION_HULLMOD.get("everything").add("brdrive");
        ADD_FACTION_HULLMOD.get("everything").add("cargo_expansion");
        ADD_FACTION_HULLMOD.get("everything").add("additional_crew_quarters");
        ADD_FACTION_HULLMOD.get("everything").add("supply_conservation_program");
        ADD_FACTION_HULLMOD.get("everything").add("maximized_ordinance");
        ADD_FACTION_HULLMOD.get("everything").add("fuel_expansion");
        ADD_FACTION_HULLMOD.get("everything").add("advanced_ai_core");
        ADD_FACTION_HULLMOD.get("everything").add("shieldbypass");
        ADD_FACTION_HULLMOD.get("everything").add("converted_hangar");
        ADD_FACTION_HULLMOD.put("sector", new HashSet<String>(48));
        ADD_FACTION_HULLMOD.get("sector").add("advancedshieldemitter");
        ADD_FACTION_HULLMOD.get("sector").add("advancedoptics");
        ADD_FACTION_HULLMOD.get("sector").add("turretgyros");
        ADD_FACTION_HULLMOD.get("sector").add("armoredweapons");
        ADD_FACTION_HULLMOD.get("sector").add("augmentedengines");
        ADD_FACTION_HULLMOD.get("sector").add("autorepair");
        ADD_FACTION_HULLMOD.get("sector").add("auxiliarythrusters");
        ADD_FACTION_HULLMOD.get("sector").add("blast_doors");
        ADD_FACTION_HULLMOD.get("sector").add("dedicated_targeting_core");
        ADD_FACTION_HULLMOD.get("sector").add("eccm");
        ADD_FACTION_HULLMOD.get("sector").add("magazines");
        ADD_FACTION_HULLMOD.get("sector").add("missleracks");
        ADD_FACTION_HULLMOD.get("sector").add("extendedshieldemitter");
        ADD_FACTION_HULLMOD.get("sector").add("fluxcoil");
        ADD_FACTION_HULLMOD.get("sector").add("fluxdistributor");
        ADD_FACTION_HULLMOD.get("sector").add("frontemitter");
        ADD_FACTION_HULLMOD.get("sector").add("frontshield");
        ADD_FACTION_HULLMOD.get("sector").add("hardenedshieldemitter");
        ADD_FACTION_HULLMOD.get("sector").add("hardened_subsystems");
        ADD_FACTION_HULLMOD.get("sector").add("heavyarmor");
        ADD_FACTION_HULLMOD.get("sector").add("insulatedengine");
        ADD_FACTION_HULLMOD.get("sector").add("pointdefenseai");
        ADD_FACTION_HULLMOD.get("sector").add("targetingunit");
        ADD_FACTION_HULLMOD.get("sector").add("adaptiveshields");
        ADD_FACTION_HULLMOD.get("sector").add("reinforcedhull");
        ADD_FACTION_HULLMOD.get("sector").add("fluxbreakers");
        ADD_FACTION_HULLMOD.get("sector").add("safetyoverrides");
        ADD_FACTION_HULLMOD.get("sector").add("solar_shielding");
        ADD_FACTION_HULLMOD.get("sector").add("stabilizedshieldemitter");
        ADD_FACTION_HULLMOD.get("sector").add("surveying_equipment");
        ADD_FACTION_HULLMOD.get("sector").add("unstable_injector");
        ADD_FACTION_HULLMOD.get("sector").add("expanded_deck_crew");
        ADD_FACTION_HULLMOD.get("sector").add("recovery_shuttles");
        ADD_FACTION_HULLMOD.get("sector").add("operations_center");
        ADD_FACTION_HULLMOD.get("sector").add("nav_relay");
        ADD_FACTION_HULLMOD.get("sector").add("ecm");
        ADD_FACTION_HULLMOD.get("sector").add("ii_energized_armor");
        ADD_FACTION_HULLMOD.get("sector").add("ii_fire_control");
        ADD_FACTION_HULLMOD.get("sector").add("brassaultop");
        ADD_FACTION_HULLMOD.get("sector").add("brtarget");
        ADD_FACTION_HULLMOD.get("sector").add("brdrive");
        ADD_FACTION_HULLMOD.get("sector").add("cargo_expansion");
        ADD_FACTION_HULLMOD.get("sector").add("additional_crew_quarters");
        ADD_FACTION_HULLMOD.get("sector").add("supply_conservation_program");
        ADD_FACTION_HULLMOD.get("sector").add("maximized_ordinance");
        ADD_FACTION_HULLMOD.get("sector").add("fuel_expansion");
        ADD_FACTION_HULLMOD.get("sector").add("advanced_ai_core");
        ADD_FACTION_HULLMOD.get("sector").add("shieldbypass");
        ADD_FACTION_HULLMOD.get("sector").add("converted_hangar");
        ADD_FACTION_HULLMOD.put("domain", new HashSet<String>(45));
        ADD_FACTION_HULLMOD.get("domain").add("advancedshieldemitter");
        ADD_FACTION_HULLMOD.get("domain").add("advancedoptics");
        ADD_FACTION_HULLMOD.get("domain").add("turretgyros");
        ADD_FACTION_HULLMOD.get("domain").add("armoredweapons");
        ADD_FACTION_HULLMOD.get("domain").add("augmentedengines");
        ADD_FACTION_HULLMOD.get("domain").add("autorepair");
        ADD_FACTION_HULLMOD.get("domain").add("auxiliarythrusters");
        ADD_FACTION_HULLMOD.get("domain").add("blast_doors");
        ADD_FACTION_HULLMOD.get("domain").add("dedicated_targeting_core");
        ADD_FACTION_HULLMOD.get("domain").add("eccm");
        ADD_FACTION_HULLMOD.get("domain").add("magazines");
        ADD_FACTION_HULLMOD.get("domain").add("missleracks");
        ADD_FACTION_HULLMOD.get("domain").add("extendedshieldemitter");
        ADD_FACTION_HULLMOD.get("domain").add("fluxcoil");
        ADD_FACTION_HULLMOD.get("domain").add("fluxdistributor");
        ADD_FACTION_HULLMOD.get("domain").add("frontemitter");
        ADD_FACTION_HULLMOD.get("domain").add("frontshield");
        ADD_FACTION_HULLMOD.get("domain").add("hardenedshieldemitter");
        ADD_FACTION_HULLMOD.get("domain").add("hardened_subsystems");
        ADD_FACTION_HULLMOD.get("domain").add("heavyarmor");
        ADD_FACTION_HULLMOD.get("domain").add("insulatedengine");
        ADD_FACTION_HULLMOD.get("domain").add("pointdefenseai");
        ADD_FACTION_HULLMOD.get("domain").add("targetingunit");
        ADD_FACTION_HULLMOD.get("domain").add("adaptiveshields");
        ADD_FACTION_HULLMOD.get("domain").add("reinforcedhull");
        ADD_FACTION_HULLMOD.get("domain").add("fluxbreakers");
        ADD_FACTION_HULLMOD.get("domain").add("safetyoverrides");
        ADD_FACTION_HULLMOD.get("domain").add("solar_shielding");
        ADD_FACTION_HULLMOD.get("domain").add("stabilizedshieldemitter");
        ADD_FACTION_HULLMOD.get("domain").add("surveying_equipment");
        ADD_FACTION_HULLMOD.get("domain").add("unstable_injector");
        ADD_FACTION_HULLMOD.get("domain").add("expanded_deck_crew");
        ADD_FACTION_HULLMOD.get("domain").add("recovery_shuttles");
        ADD_FACTION_HULLMOD.get("domain").add("operations_center");
        ADD_FACTION_HULLMOD.get("domain").add("nav_relay");
        ADD_FACTION_HULLMOD.get("domain").add("ecm");
        ADD_FACTION_HULLMOD.get("domain").add("ii_energized_armor");
        ADD_FACTION_HULLMOD.get("domain").add("ii_fire_control");
        ADD_FACTION_HULLMOD.get("domain").add("cargo_expansion");
        ADD_FACTION_HULLMOD.get("domain").add("additional_crew_quarters");
        ADD_FACTION_HULLMOD.get("domain").add("supply_conservation_program");
        ADD_FACTION_HULLMOD.get("domain").add("maximized_ordinance");
        ADD_FACTION_HULLMOD.get("domain").add("fuel_expansion");
        ADD_FACTION_HULLMOD.get("domain").add("advanced_ai_core");
        ADD_FACTION_HULLMOD.get("domain").add("shieldbypass");
        ADD_FACTION_HULLMOD.get("domain").add("converted_hangar");

        BLOCK_FACTION_HULLMOD.put(Factions.DIKTAT, new HashSet<String>(11));
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("maximized_ordinance");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("advanced_ai_core");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("brassaultop");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("brtarget");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("brdrive");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("pointdefenseai");
        BLOCK_FACTION_HULLMOD.get(Factions.DIKTAT).add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.put(Factions.HEGEMONY, new HashSet<String>(13));
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("maximized_ordinance");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("advanced_ai_core");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("brassaultop");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("brtarget");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("brdrive");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("SCY_lightArmor");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("pointdefenseai");
        BLOCK_FACTION_HULLMOD.get(Factions.HEGEMONY).add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.put(Factions.INDEPENDENT, new HashSet<String>(0));
        BLOCK_FACTION_HULLMOD.put(Factions.PERSEAN, new HashSet<String>(0));
        BLOCK_FACTION_HULLMOD.put(Factions.KOL, new HashSet<String>(9));
        BLOCK_FACTION_HULLMOD.get(Factions.KOL).add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get(Factions.KOL).add("advanced_ai_core");
        BLOCK_FACTION_HULLMOD.get(Factions.KOL).add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get(Factions.KOL).add("pointdefenseai");
        BLOCK_FACTION_HULLMOD.get(Factions.KOL).add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.KOL).add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get(Factions.KOL).add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get(Factions.KOL).add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.KOL).add("SCY_lightArmor");
        BLOCK_FACTION_HULLMOD.put(Factions.LIONS_GUARD, new HashSet<String>(5));
        BLOCK_FACTION_HULLMOD.get(Factions.LIONS_GUARD).add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.LIONS_GUARD).add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get(Factions.LIONS_GUARD).add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get(Factions.LIONS_GUARD).add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.LIONS_GUARD).add("shieldbypass");
        BLOCK_FACTION_HULLMOD.put(Factions.LUDDIC_CHURCH, new HashSet<String>(12));
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("advanced_ai_core");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("pointdefenseai");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("targetingunit");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("frontshield");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("brassaultop");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("brtarget");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("brdrive");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_CHURCH).add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.put(Factions.LUDDIC_PATH, new HashSet<String>(19));
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("advanced_ai_core");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("pointdefenseai");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("targetingunit");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("frontshield");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("autorepair");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("advancedshieldemitter");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("extendedshieldemitter");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("frontemitter");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("hardenedshieldemitter");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("adaptiveshields");
        BLOCK_FACTION_HULLMOD.get(Factions.LUDDIC_PATH).add("stabilizedshieldemitter");
        BLOCK_FACTION_HULLMOD.put(Factions.PIRATES, new HashSet<String>(4));
        BLOCK_FACTION_HULLMOD.get(Factions.PIRATES).add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.PIRATES).add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get(Factions.PIRATES).add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get(Factions.PIRATES).add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.put(Factions.TRITACHYON, new HashSet<String>(9));
        BLOCK_FACTION_HULLMOD.get(Factions.TRITACHYON).add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.get(Factions.TRITACHYON).add("dedicated_targeting_core");
        BLOCK_FACTION_HULLMOD.get(Factions.TRITACHYON).add("shieldbypass");
        BLOCK_FACTION_HULLMOD.get(Factions.TRITACHYON).add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get(Factions.TRITACHYON).add("brassaultop");
        BLOCK_FACTION_HULLMOD.get(Factions.TRITACHYON).add("brtarget");
        BLOCK_FACTION_HULLMOD.get(Factions.TRITACHYON).add("brdrive");
        BLOCK_FACTION_HULLMOD.get(Factions.TRITACHYON).add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get(Factions.TRITACHYON).add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.put(Factions.DERELICT, new HashSet<String>(17));
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("brassaultop");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("brtarget");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("brdrive");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("converted_hangar");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("frontshield");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("pointdefenseai");
        BLOCK_FACTION_HULLMOD.get(Factions.DERELICT).add("targetingunit");
        BLOCK_FACTION_HULLMOD.put(Factions.REMNANTS, new HashSet<String>(13));
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("dedicated_targeting_core");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("shieldbypass");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("brassaultop");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("brtarget");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("brdrive");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get(Factions.REMNANTS).add("converted_hangar");
        BLOCK_FACTION_HULLMOD.put("cabal", new HashSet<String>(6));
        BLOCK_FACTION_HULLMOD.get("cabal").add("dedicated_targeting_core");
        BLOCK_FACTION_HULLMOD.get("cabal").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.get("cabal").add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get("cabal").add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get("cabal").add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get("cabal").add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.put("interstellarimperium", new HashSet<String>(5));
        BLOCK_FACTION_HULLMOD.get("interstellarimperium").add("SCY_lightArmor");
        BLOCK_FACTION_HULLMOD.get("interstellarimperium").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.get("interstellarimperium").add("brassaultop");
        BLOCK_FACTION_HULLMOD.get("interstellarimperium").add("brtarget");
        BLOCK_FACTION_HULLMOD.get("interstellarimperium").add("brdrive");
        BLOCK_FACTION_HULLMOD.put("citadeldefenders", new HashSet<String>(1));
        BLOCK_FACTION_HULLMOD.get("citadeldefenders").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.put("blackrock_driveyards", new HashSet<String>(5));
        BLOCK_FACTION_HULLMOD.get("blackrock_driveyards").add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get("blackrock_driveyards").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.get("blackrock_driveyards").add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get("blackrock_driveyards").add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get("blackrock_driveyards").add("SCY_lightArmor");
        BLOCK_FACTION_HULLMOD.put("exigency", new HashSet<String>(13));
        BLOCK_FACTION_HULLMOD.get("exigency").add("maximized_ordinance");
        BLOCK_FACTION_HULLMOD.get("exigency").add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get("exigency").add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get("exigency").add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get("exigency").add("brassaultop");
        BLOCK_FACTION_HULLMOD.get("exigency").add("brtarget");
        BLOCK_FACTION_HULLMOD.get("exigency").add("brdrive");
        BLOCK_FACTION_HULLMOD.get("exigency").add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get("exigency").add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get("exigency").add("SCY_lightArmor");
        BLOCK_FACTION_HULLMOD.get("exigency").add("frontshield");
        BLOCK_FACTION_HULLMOD.get("exigency").add("dedicated_targeting_core");
        BLOCK_FACTION_HULLMOD.get("exigency").add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.put("exipirated", new HashSet<String>(6));
        BLOCK_FACTION_HULLMOD.get("exipirated").add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get("exipirated").add("SCY_lightArmor");
        BLOCK_FACTION_HULLMOD.get("exipirated").add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get("exipirated").add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get("exipirated").add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get("exipirated").add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.put("templars", new HashSet<String>(17));
        BLOCK_FACTION_HULLMOD.get("templars").add("maximized_ordinance");
        BLOCK_FACTION_HULLMOD.get("templars").add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get("templars").add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get("templars").add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get("templars").add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.get("templars").add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get("templars").add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get("templars").add("SCY_lightArmor");
        BLOCK_FACTION_HULLMOD.get("templars").add("brassaultop");
        BLOCK_FACTION_HULLMOD.get("templars").add("brtarget");
        BLOCK_FACTION_HULLMOD.get("templars").add("brdrive");
        BLOCK_FACTION_HULLMOD.get("templars").add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get("templars").add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get("templars").add("reinforcedhull");
        BLOCK_FACTION_HULLMOD.get("templars").add("dedicated_targeting_core");
        BLOCK_FACTION_HULLMOD.get("templars").add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.get("templars").add("SCY_reactiveArmor");
        BLOCK_FACTION_HULLMOD.put("shadow_industry", new HashSet<String>(6));
        BLOCK_FACTION_HULLMOD.get("shadow_industry").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.get("shadow_industry").add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get("shadow_industry").add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.get("shadow_industry").add("brassaultop");
        BLOCK_FACTION_HULLMOD.get("shadow_industry").add("brtarget");
        BLOCK_FACTION_HULLMOD.get("shadow_industry").add("brdrive");
        BLOCK_FACTION_HULLMOD.put("mayorate", new HashSet<String>(2));
        BLOCK_FACTION_HULLMOD.get("mayorate").add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get("mayorate").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.put("junk_pirates", new HashSet<String>(5));
        BLOCK_FACTION_HULLMOD.get("junk_pirates").add("cargo_expansion");
        BLOCK_FACTION_HULLMOD.get("junk_pirates").add("additional_crew_quarters");
        BLOCK_FACTION_HULLMOD.get("junk_pirates").add("supply_conservation_program");
        BLOCK_FACTION_HULLMOD.get("junk_pirates").add("fuel_expansion");
        BLOCK_FACTION_HULLMOD.get("junk_pirates").add("SCY_lightArmor");
        BLOCK_FACTION_HULLMOD.put("pack", new HashSet<String>(7));
        BLOCK_FACTION_HULLMOD.get("pack").add("maximized_ordinance");
        BLOCK_FACTION_HULLMOD.get("pack").add("brassaultop");
        BLOCK_FACTION_HULLMOD.get("pack").add("brtarget");
        BLOCK_FACTION_HULLMOD.get("pack").add("brdrive");
        BLOCK_FACTION_HULLMOD.get("pack").add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get("pack").add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get("pack").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.put("syndicate_asp", new HashSet<String>(8));
        BLOCK_FACTION_HULLMOD.get("syndicate_asp").add("brassaultop");
        BLOCK_FACTION_HULLMOD.get("syndicate_asp").add("brtarget");
        BLOCK_FACTION_HULLMOD.get("syndicate_asp").add("brdrive");
        BLOCK_FACTION_HULLMOD.get("syndicate_asp").add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get("syndicate_asp").add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get("syndicate_asp").add("maximized_ordinance");
        BLOCK_FACTION_HULLMOD.get("syndicate_asp").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.get("syndicate_asp").add("safetyoverrides");
        BLOCK_FACTION_HULLMOD.put("SCY", new HashSet<String>(8));
        BLOCK_FACTION_HULLMOD.get("SCY").add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get("SCY").add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get("SCY").add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get("SCY").add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get("SCY").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.get("SCY").add("brassaultop");
        BLOCK_FACTION_HULLMOD.get("SCY").add("brtarget");
        BLOCK_FACTION_HULLMOD.get("SCY").add("brdrive");
        BLOCK_FACTION_HULLMOD.put("tiandong", new HashSet<String>(1));
        BLOCK_FACTION_HULLMOD.get("tiandong").add("SCY_lightArmor");
        BLOCK_FACTION_HULLMOD.put("diableavionics", new HashSet<String>(7));
        BLOCK_FACTION_HULLMOD.get("diableavionics").add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get("diableavionics").add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get("diableavionics").add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get("diableavionics").add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get("diableavionics").add("brassaultop");
        BLOCK_FACTION_HULLMOD.get("diableavionics").add("brtarget");
        BLOCK_FACTION_HULLMOD.get("diableavionics").add("brdrive");
        BLOCK_FACTION_HULLMOD.put("ORA", new HashSet<String>(10));
        BLOCK_FACTION_HULLMOD.get("ORA").add("shieldbypass");
        BLOCK_FACTION_HULLMOD.get("ORA").add("frontemitter");
        BLOCK_FACTION_HULLMOD.get("ORA").add("pointdefenseai");
        BLOCK_FACTION_HULLMOD.get("ORA").add("ilk_SensorSuite");
        BLOCK_FACTION_HULLMOD.get("ORA").add("ilk_AICrew");
        BLOCK_FACTION_HULLMOD.get("ORA").add("ii_energized_armor");
        BLOCK_FACTION_HULLMOD.get("ORA").add("ii_fire_control");
        BLOCK_FACTION_HULLMOD.get("ORA").add("brassaultop");
        BLOCK_FACTION_HULLMOD.get("ORA").add("brtarget");
        BLOCK_FACTION_HULLMOD.get("ORA").add("brdrive");
    }

    static {
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.MISSILE, new HashSet<String>(9));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).add("diableavionics_micromissile");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).add("diableavionics_microarray");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).add("homing_laser");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).add("exigency_stinger");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).add("fox_supernova");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).add("ii_apocalypse_w");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).add("ii_supertitan_w");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).add("ii_titan_w");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).add("SCY_singularity");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.NO_AIM, new HashSet<String>(3));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.NO_AIM).add("ii_apocalypse_w");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.NO_AIM).add("ii_supertitan_w");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.NO_AIM).add("ii_titan_w");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.ANTI_FIGHTER, new HashSet<String>(2));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ANTI_FIGHTER).add("devastator");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ANTI_FIGHTER).add("phasecl");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.POINT_DEFENSE, new HashSet<String>(1));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.POINT_DEFENSE).add("phasecl");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.STRIKE, new HashSet<String>(8));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.STRIKE).add("ssp_gungnir");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.STRIKE).add("ssp_gungnir_r");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.STRIKE).add("exigency_repulsor_blaster");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.STRIKE).add("fox_zeus");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.STRIKE).add("ilk_nuke");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.STRIKE).add("ilk_nuke_large");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.STRIKE).add("SCY_singularity");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.STRIKE).add("tem_joyeuse");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.ASSAULT, new HashSet<String>(6));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ASSAULT).add("heavymauler");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ASSAULT).add("phasecl");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ASSAULT).add("brdy_squallgun");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ASSAULT).add("exigency_stinger");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ASSAULT).add("ms_cepc");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ASSAULT).add("tiandong_mauler_battery");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.CLOSE_SUPPORT, new HashSet<String>(8));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.CLOSE_SUPPORT).add("heavymg");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.CLOSE_SUPPORT).add("fox_rotunda");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.CLOSE_SUPPORT).add("fox_rotunda_large");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.CLOSE_SUPPORT).add("ii_mac");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.CLOSE_SUPPORT).add("ilk_graser_light");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.CLOSE_SUPPORT).add("ora_eschewal");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.CLOSE_SUPPORT).add("tem_arondight");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.CLOSE_SUPPORT).add("tem_secace");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.FIRE_SUPPORT, new HashSet<String>(2));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.FIRE_SUPPORT).add("ssp_redeemer");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.FIRE_SUPPORT).add("ora_litany");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.SPECIAL, new HashSet<String>(3));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SPECIAL).add("diableavionics_micromissile");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SPECIAL).add("diableavionics_microarray");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SPECIAL).add("fox_supernova");
        // Cannot be autodetected, but is gathered from csv
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.LOW_FLUX, new HashSet<String>(5));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.LOW_FLUX).add("phasecl");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.LOW_FLUX).add("diableavionics_micromissile");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.LOW_FLUX).add("diableavionics_microarray");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.LOW_FLUX).add("exigency_stinger");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.LOW_FLUX).add("SCY_singularity");
        // Cannot be autodetected, but is gathered from csv
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.SEPARATE_FIRE, new HashSet<String>(2));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SEPARATE_FIRE).add("exigency_repulsor_blaster");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SEPARATE_FIRE).add("ii_mac");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.SUSTAINED_BEAM, new HashSet<String>(7));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).add("mininglaser");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).add("pdlaser");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).add("taclaser");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).add("lrpdlaser");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).add("gravitonbeam");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).add("ionbeam");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).add("hil");
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.BURST_BEAM, new HashSet<String>(5));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).add("pdburst");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).add("phasebeam");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).add("heavyburst");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).add("guardian");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).add("tachyonlance");
        // Cannot be autodetected, but is gathered from csv
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.LIMITED_AMMO, new HashSet<String>(2));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.LIMITED_AMMO).add("ms_cepc");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.LIMITED_AMMO).add("SCY_singularity");
        // Cannot be autodetected
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.INDIRECT_FIRE, new HashSet<String>(20));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("dualflak");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("flak");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("ssp_tripleflak");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("brdy_iwcannon");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("diableavionics_hexafire");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("diableavionics_srab");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("diableavionics_trifire");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("fox_flak");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("fox_miniflak");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("fox_monoLPD");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("ii_sagittarius");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("ilk_shotgun");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("ms_scattercepc");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("ora_litany");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("ora_invocation");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("SCY_flak");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("SCY_scorcher");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("tem_pax");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("tem_sentenia");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).add("tiandong_burstflakcannon");
        // Disables autodetection (and csv data)
        GROUPER_TYPE_OVERRIDE.put(WeaponGroupParam.OVERRIDE, new HashSet<String>(19));
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("phasecl");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("heavymauler");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("ssp_redeemer");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("brdy_squallgun");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("diableavionics_micromissile");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("diableavionics_microarray");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("exigency_repulsor_blaster");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("exigency_stinger");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("fox_supernova");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("fox_rotunda");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("fox_rotunda_large");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("ii_apocalypse_w");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("ii_mac");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("ii_supertitan_w");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("ii_titan_w");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("ilk_graser_light");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("ms_cepc");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("ora_eschewal");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("ora_litany");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("SCY_singularity");
        GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).add("tiandong_mauler_battery");
    }

    static {
        GROUPER_NO_SPLIT.add("ssp_vindicator_o");
        GROUPER_NO_SPLIT.add("diableavionics_versant");
        GROUPER_NO_SPLIT.add("ii_maximus");
        GROUPER_NO_SPLIT.add("ii_adamas");
    }

    static {
        INJECTOR_AFFECTED_FACTIONS.add(Factions.HEGEMONY);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.DIKTAT);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.INDEPENDENT);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.SCAVENGERS);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.KOL);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.LIONS_GUARD);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.LUDDIC_CHURCH);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.LUDDIC_PATH);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.NEUTRAL);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.PIRATES);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.TRITACHYON);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.PERSEAN);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.DERELICT);
        INJECTOR_AFFECTED_FACTIONS.add(Factions.REMNANTS);
        INJECTOR_AFFECTED_FACTIONS.add("cabal");
        INJECTOR_AFFECTED_FACTIONS.add("interstellarimperium");
        INJECTOR_AFFECTED_FACTIONS.add("citadeldefenders");
        INJECTOR_AFFECTED_FACTIONS.add("blackrock_driveyards");
        INJECTOR_AFFECTED_FACTIONS.add("exigency");
        INJECTOR_AFFECTED_FACTIONS.add("exipirated");
        INJECTOR_AFFECTED_FACTIONS.add("templars");
        INJECTOR_AFFECTED_FACTIONS.add("shadow_industry");
        INJECTOR_AFFECTED_FACTIONS.add("mayorate");
        INJECTOR_AFFECTED_FACTIONS.add("junk_pirates");
        INJECTOR_AFFECTED_FACTIONS.add("pack");
        INJECTOR_AFFECTED_FACTIONS.add("syndicate_asp");
        INJECTOR_AFFECTED_FACTIONS.add("SCY");
        INJECTOR_AFFECTED_FACTIONS.add("tiandong");
        INJECTOR_AFFECTED_FACTIONS.add("diableavionics");
        INJECTOR_AFFECTED_FACTIONS.add("ORA");
        INJECTOR_AFFECTED_FACTIONS.add("player_npc");
        INJECTOR_AFFECTED_FACTIONS.add("famous_bounty");
        INJECTOR_AFFECTED_FACTIONS.add("domain");
        INJECTOR_AFFECTED_FACTIONS.add("sector");
        INJECTOR_AFFECTED_FACTIONS.add("everything");
    }

    static {
        INJECTOR_NO_MATCHING_WEAPONS.add(Factions.LUDDIC_PATH);
        INJECTOR_NO_MATCHING_WEAPONS.add(Factions.PIRATES);
        INJECTOR_NO_MATCHING_WEAPONS.add(Factions.DERELICT);
        INJECTOR_NO_MATCHING_WEAPONS.add(Factions.SCAVENGERS);
        INJECTOR_NO_MATCHING_WEAPONS.add("exipirated");
        INJECTOR_NO_MATCHING_WEAPONS.add("junk_pirates");
        INJECTOR_NO_MATCHING_WEAPONS.add("pack");
    }

    static {
        INJECTOR_NO_SHARING_MOUNTS.add(Factions.DERELICT);
        INJECTOR_NO_SHARING_MOUNTS.add(Factions.REMNANTS);
        INJECTOR_NO_SHARING_MOUNTS.add("exigency");
        INJECTOR_NO_SHARING_MOUNTS.add("templars");
        INJECTOR_NO_SHARING_MOUNTS.add("SCY");
        INJECTOR_NO_SHARING_MOUNTS.add("diableavionics");
        INJECTOR_NO_SHARING_MOUNTS.add("blackrock_driveyards");
    }

    static {
        MARKET_AFFECTED_FACTIONS.add(Factions.HEGEMONY);
        MARKET_AFFECTED_FACTIONS.add(Factions.DIKTAT);
        MARKET_AFFECTED_FACTIONS.add(Factions.INDEPENDENT);
        MARKET_AFFECTED_FACTIONS.add(Factions.KOL);
        MARKET_AFFECTED_FACTIONS.add(Factions.LIONS_GUARD);
        MARKET_AFFECTED_FACTIONS.add(Factions.LUDDIC_CHURCH);
        MARKET_AFFECTED_FACTIONS.add(Factions.LUDDIC_PATH);
        MARKET_AFFECTED_FACTIONS.add(Factions.NEUTRAL);
        MARKET_AFFECTED_FACTIONS.add(Factions.PIRATES);
        MARKET_AFFECTED_FACTIONS.add(Factions.TRITACHYON);
        MARKET_AFFECTED_FACTIONS.add(Factions.PERSEAN);
        MARKET_AFFECTED_FACTIONS.add("cabal");
        MARKET_AFFECTED_FACTIONS.add("interstellarimperium");
        MARKET_AFFECTED_FACTIONS.add("citadeldefenders");
        MARKET_AFFECTED_FACTIONS.add("exipirated");
        MARKET_AFFECTED_FACTIONS.add("shadow_industry");
        MARKET_AFFECTED_FACTIONS.add("mayorate");
        MARKET_AFFECTED_FACTIONS.add("junk_pirates");
        MARKET_AFFECTED_FACTIONS.add("pack");
        MARKET_AFFECTED_FACTIONS.add("syndicate_asp");
        MARKET_AFFECTED_FACTIONS.add("SCY");
        MARKET_AFFECTED_FACTIONS.add("tiandong");
        MARKET_AFFECTED_FACTIONS.add("diableavionics");
        MARKET_AFFECTED_FACTIONS.add("ORA");
        MARKET_AFFECTED_FACTIONS.add("player_npc");
    }

    static {
        // -1 = expendable
        // 0 = crap
        // 1 = okay (default)
        // 2 = good
        SHIELD_QUALITY.put("dominator", -1);
        SHIELD_QUALITY.put("mora", -1);
        SHIELD_QUALITY.put("onslaught", -1);
        SHIELD_QUALITY.put("ssp_infernalmachine", -1);
        SHIELD_QUALITY.put("ssp_punisher", -1);
        SHIELD_QUALITY.put("ssp_sidecar", -1);
        SHIELD_QUALITY.put("ssp_boss_dominator", -1);
        SHIELD_QUALITY.put("ssp_boss_dominator_luddic_path", -1);
        SHIELD_QUALITY.put("ssp_boss_lasher_r", -1);
        SHIELD_QUALITY.put("ssp_boss_onslaught", -1);
        SHIELD_QUALITY.put("ssp_boss_onslaught_luddic_path", -1);
        SHIELD_QUALITY.put("diableavionics_draft", -1);
        SHIELD_QUALITY.put("ii_caesar", -1);
        SHIELD_QUALITY.put("ii_dictator", -1);
        SHIELD_QUALITY.put("junk_pirates_boxenstein", -1);
        SHIELD_QUALITY.put("junk_pirates_boxer", -1);
        SHIELD_QUALITY.put("tiandong_hanzhong", -1);
        SHIELD_QUALITY.put("tiandong_nanzhong", -1);
        SHIELD_QUALITY.put("tiandong_xu", -1);

        SHIELD_QUALITY.put("atlas", 0);
        SHIELD_QUALITY.put("bastillon", 0);
        SHIELD_QUALITY.put("berserker", 0);
        SHIELD_QUALITY.put("buffalo2", 0);
        SHIELD_QUALITY.put("cerberus", 0);
        SHIELD_QUALITY.put("colossus", 0);
        SHIELD_QUALITY.put("condor", 0);
        SHIELD_QUALITY.put("conquest", 0);
        SHIELD_QUALITY.put("defender", 0);
        SHIELD_QUALITY.put("dram", 0);
        SHIELD_QUALITY.put("enforcer", 0);
        SHIELD_QUALITY.put("hermes", 0);
        SHIELD_QUALITY.put("hound", 0);
        SHIELD_QUALITY.put("legion", 0);
        SHIELD_QUALITY.put("mudskipper", 0);
        SHIELD_QUALITY.put("mule", 0);
        SHIELD_QUALITY.put("nebula", 0);
        SHIELD_QUALITY.put("phaeton", 0);
        SHIELD_QUALITY.put("picket", 0);
        SHIELD_QUALITY.put("prometheus", 0);
        SHIELD_QUALITY.put("rampart", 0);
        SHIELD_QUALITY.put("remnant_armour1", 0);
        SHIELD_QUALITY.put("remnant_armour2", 0);
        SHIELD_QUALITY.put("remnant_armour3", 0);
        SHIELD_QUALITY.put("sentry", 0);
        SHIELD_QUALITY.put("shepherd", 0);
        SHIELD_QUALITY.put("starliner", 0);
        SHIELD_QUALITY.put("tarsus", 0);
        SHIELD_QUALITY.put("venture", 0);
        SHIELD_QUALITY.put("warden", 0);
        SHIELD_QUALITY.put("ssp_amalgam", 0);
        SHIELD_QUALITY.put("ssp_barbarian", 0);
        SHIELD_QUALITY.put("ssp_boar", 0);
        SHIELD_QUALITY.put("ssp_mongrel", 0);
        SHIELD_QUALITY.put("ssp_renegade", 0);
        SHIELD_QUALITY.put("ssp_shark", 0);
        SHIELD_QUALITY.put("ssp_torch", 0);
        SHIELD_QUALITY.put("ssp_venom", 0);
        SHIELD_QUALITY.put("ssp_vindicator", 0);
        SHIELD_QUALITY.put("swp_wall", 0);
        SHIELD_QUALITY.put("swp_wall_left", 0);
        SHIELD_QUALITY.put("swp_wall_right", 0);
        SHIELD_QUALITY.put("ssp_boss_atlas", 0);
        SHIELD_QUALITY.put("ssp_boss_cerberus", 0);
        SHIELD_QUALITY.put("ssp_boss_conquest", 0);
        SHIELD_QUALITY.put("ssp_boss_mule", 0);
        SHIELD_QUALITY.put("ssp_boss_phaeton", 0);
        SHIELD_QUALITY.put("ssp_boss_tarsus", 0);
        SHIELD_QUALITY.put("swp_boss_frankenstein", 0);
        SHIELD_QUALITY.put("brdy_convergence", 0);
        SHIELD_QUALITY.put("brdy_eschaton", 0);
        SHIELD_QUALITY.put("brdy_gonodactylus", 0);
        SHIELD_QUALITY.put("brdy_locust", 0);
        SHIELD_QUALITY.put("brdy_mantis", 0);
        SHIELD_QUALITY.put("brdy_stenos", 0);
        SHIELD_QUALITY.put("brdy_typheus", 0);
        SHIELD_QUALITY.put("diableavionics_haze", 0);
        SHIELD_QUALITY.put("diableavionics_pandemonium", 0);
        SHIELD_QUALITY.put("exipirated_azryel", 0);
        SHIELD_QUALITY.put("exipirated_gehenna", 0);
        SHIELD_QUALITY.put("exipirated_harinder", 0);
        SHIELD_QUALITY.put("exipirated_kafziel", 0);
        SHIELD_QUALITY.put("exipirated_rauwel", 0);
        SHIELD_QUALITY.put("Fox_Carrier", 0);
        SHIELD_QUALITY.put("Fox_catamaran", 0);
        SHIELD_QUALITY.put("fox_dfrigate", 0);
        SHIELD_QUALITY.put("Fox_Dreadnaught", 0);
        SHIELD_QUALITY.put("Fox_LDestroyer", 0);
        SHIELD_QUALITY.put("Fox_Minelayer", 0);
        SHIELD_QUALITY.put("ii_auriga", 0);
        SHIELD_QUALITY.put("ii_basileus", 0);
        SHIELD_QUALITY.put("ii_basileus_f", 0);
        SHIELD_QUALITY.put("ii_jupiter", 0);
        SHIELD_QUALITY.put("junk_pirates_kraken", 0);
        SHIELD_QUALITY.put("junk_pirates_mandarine", 0);
        SHIELD_QUALITY.put("junk_pirates_satsuma", 0);
        SHIELD_QUALITY.put("junk_pirates_scythe", 0);
        SHIELD_QUALITY.put("junk_pirates_stoatA", 0);
        SHIELD_QUALITY.put("junk_pirates_stoatB", 0);
        SHIELD_QUALITY.put("junk_pirates_the_reaper", 0);
        SHIELD_QUALITY.put("ms_minos", 0);
        SHIELD_QUALITY.put("msp_baseplate", 0);
        SHIELD_QUALITY.put("msp_southpaw", 0);
        SHIELD_QUALITY.put("msp_thresher", 0);
        SHIELD_QUALITY.put("pack_sharpei", 0);
        SHIELD_QUALITY.put("SCY_lamiaA", 0);
        SHIELD_QUALITY.put("SCY_nemeanLion", 0);
        SHIELD_QUALITY.put("tiandong_chengdu", 0);
        SHIELD_QUALITY.put("tiandong_dingjun", 0);
        SHIELD_QUALITY.put("tiandong_guan_du", 0);
        SHIELD_QUALITY.put("tiandong_lao_hu", 0);
        SHIELD_QUALITY.put("tiandong_luo_yang", 0);
        SHIELD_QUALITY.put("tiandong_tianshui", 0);
        SHIELD_QUALITY.put("tiandong_tuolu", 0);
        SHIELD_QUALITY.put("tiandong_wujun", 0);
        SHIELD_QUALITY.put("tiandong_wuzhang", 0);
        SHIELD_QUALITY.put("tiandong_xiakou", 0);
        SHIELD_QUALITY.put("tiandong_boss_wuzhang", 0);

        SHIELD_QUALITY.put("apogee", 2);
        SHIELD_QUALITY.put("astral", 2);
        SHIELD_QUALITY.put("aurora", 2);
        SHIELD_QUALITY.put("brilliant", 2);
        SHIELD_QUALITY.put("fulgent", 2);
        SHIELD_QUALITY.put("glimmer", 2);
        SHIELD_QUALITY.put("hyperion", 2);
        SHIELD_QUALITY.put("lumen", 2);
        SHIELD_QUALITY.put("medusa", 2);
        SHIELD_QUALITY.put("monitor", 2);
        SHIELD_QUALITY.put("odyssey", 2);
        SHIELD_QUALITY.put("omen", 2);
        SHIELD_QUALITY.put("paragon", 2);
        SHIELD_QUALITY.put("remnant_shield1", 2);
        SHIELD_QUALITY.put("remnant_shield2", 2);
        SHIELD_QUALITY.put("scintilla", 2);
        SHIELD_QUALITY.put("sunder", 2);
        SHIELD_QUALITY.put("tempest", 2);
        SHIELD_QUALITY.put("ssp_arachne", 2);
        SHIELD_QUALITY.put("ssp_cathedral", 2);
        SHIELD_QUALITY.put("ssp_circe", 2);
        SHIELD_QUALITY.put("swp_chronos", 2);
        SHIELD_QUALITY.put("swp_shimmer", 2);
        SHIELD_QUALITY.put("ssp_sunder_u", 2);
        SHIELD_QUALITY.put("ssp_venomx", 2);
        SHIELD_QUALITY.put("ssp_vortex", 2);
        SHIELD_QUALITY.put("ssp_boss_astral", 2);
        SHIELD_QUALITY.put("ssp_boss_aurora", 2);
        SHIELD_QUALITY.put("ssp_boss_hyperion", 2);
        SHIELD_QUALITY.put("ssp_boss_lasher_b", 2);
        SHIELD_QUALITY.put("ssp_boss_medusa", 2);
        SHIELD_QUALITY.put("ssp_boss_odyssey", 2);
        SHIELD_QUALITY.put("ssp_boss_paragon", 2);
        SHIELD_QUALITY.put("ssp_boss_sunder", 2);
        SHIELD_QUALITY.put("diableavionics_derecho", 2);
        SHIELD_QUALITY.put("diableavionics_vapor", 2);
        SHIELD_QUALITY.put("Fox_Drone", 2);
        SHIELD_QUALITY.put("Fox_Tank", 2);
        SHIELD_QUALITY.put("ii_jupiter", 2);
        SHIELD_QUALITY.put("ii_maximus", 2);
        SHIELD_QUALITY.put("ilk_cimeterre", 2);
        SHIELD_QUALITY.put("ilk_del_azarchel", 2);
        SHIELD_QUALITY.put("ilk_lilith", 2);
        SHIELD_QUALITY.put("ilk_tiamat", 2);
        SHIELD_QUALITY.put("junk_pirates_clam", 2);
        SHIELD_QUALITY.put("ms_elysium", 2);
        SHIELD_QUALITY.put("ms_elysium_P", 2);
        SHIELD_QUALITY.put("ms_morningstar", 2);
        SHIELD_QUALITY.put("ms_morningstar_L", 2);
        SHIELD_QUALITY.put("ms_tartarus", 2);
        SHIELD_QUALITY.put("ms_skadi", 2);
        SHIELD_QUALITY.put("ora_beatitude", 2);
        SHIELD_QUALITY.put("ora_bliss", 2);
        SHIELD_QUALITY.put("ora_communion", 2);
        SHIELD_QUALITY.put("ora_elevation", 2);
        SHIELD_QUALITY.put("ora_enlightenment", 2);
        SHIELD_QUALITY.put("ora_felicity", 2);
        SHIELD_QUALITY.put("ora_grace", 2);
        SHIELD_QUALITY.put("ora_harmony", 2);
        SHIELD_QUALITY.put("ora_mirth", 2);
        SHIELD_QUALITY.put("ora_revelation", 2);
        SHIELD_QUALITY.put("ora_sanctuary", 2);
        SHIELD_QUALITY.put("pack_ridgeback", 2);
        SHIELD_QUALITY.put("pack_ridgeback_x", 2);
        SHIELD_QUALITY.put("SCY_khalkotauroi", 2);
        SHIELD_QUALITY.put("SCY_manticore", 2);
        SHIELD_QUALITY.put("SCY_tisiphone", 2);
        SHIELD_QUALITY.put("SCY_siren", 2);
        SHIELD_QUALITY.put("SCY_stymphalianbird", 2);
        SHIELD_QUALITY.put("syndicate_asp_diamondback", 2);
        SHIELD_QUALITY.put("syndicate_asp_gigantophis", 2);
    }

    static {
        SPECIAL_HULLS.add("ii_boss_praetorian");
        SPECIAL_HULLS.add("ii_boss_olympus");
        SPECIAL_HULLS.add("ii_boss_dominus");
        SPECIAL_HULLS.add("msp_boss_potniaBis");
        SPECIAL_HULLS.add("ms_boss_charybdis");
        SPECIAL_HULLS.add("ms_boss_mimir");
        SPECIAL_HULLS.add("tem_boss_paladin");
        SPECIAL_HULLS.add("tem_boss_archbishop");
        SPECIAL_HULLS.add("ssp_boss_phaeton");
        SPECIAL_HULLS.add("ssp_boss_hammerhead");
        SPECIAL_HULLS.add("ssp_boss_sunder");
        SPECIAL_HULLS.add("ssp_boss_tarsus");
        SPECIAL_HULLS.add("ssp_boss_medusa");
        SPECIAL_HULLS.add("ssp_boss_falcon");
        SPECIAL_HULLS.add("ssp_boss_hyperion");
        SPECIAL_HULLS.add("ssp_boss_paragon");
        SPECIAL_HULLS.add("ssp_boss_mule");
        SPECIAL_HULLS.add("ssp_boss_aurora");
        SPECIAL_HULLS.add("ssp_boss_odyssey");
        SPECIAL_HULLS.add("ssp_boss_atlas");
        SPECIAL_HULLS.add("ssp_boss_afflictor");
        SPECIAL_HULLS.add("ssp_boss_brawler");
        SPECIAL_HULLS.add("ssp_boss_cerberus");
        SPECIAL_HULLS.add("ssp_boss_dominator");
        SPECIAL_HULLS.add("ssp_boss_doom");
        SPECIAL_HULLS.add("ssp_boss_euryale");
        SPECIAL_HULLS.add("ssp_boss_lasher_b");
        SPECIAL_HULLS.add("ssp_boss_lasher_r");
        SPECIAL_HULLS.add("ssp_boss_onslaught");
        SPECIAL_HULLS.add("ssp_boss_shade");
        SPECIAL_HULLS.add("ssp_boss_eagle");
        SPECIAL_HULLS.add("ssp_boss_beholder");
        SPECIAL_HULLS.add("ssp_boss_dominator_luddic_path");
        SPECIAL_HULLS.add("ssp_boss_onslaught_luddic_path");
        SPECIAL_HULLS.add("ssp_boss_astral");
        SPECIAL_HULLS.add("ssp_boss_conquest");
        SPECIAL_HULLS.add("swp_boss_frankenstein");
        SPECIAL_HULLS.add("swp_boss_sporeship");
        SPECIAL_HULLS.add("tiandong_boss_wuzhang");
        SPECIAL_HULLS.add("pack_bulldog_bullseye");
        SPECIAL_HULLS.add("pack_pitbull_bullseye");
        SPECIAL_HULLS.add("pack_komondor_bullseye");
        SPECIAL_HULLS.add("pack_schnauzer_bullseye");
        SPECIAL_HULLS.add("diableavionics_IBBgulf");
    }

    public static enum FactionStyle {

        HEGEMONY(Factions.HEGEMONY,
                 0.2f, 0.4f, 0.5f, 0.7f), // minQF, lowQF, highQF, maxQF
        DIKTAT(Factions.DIKTAT,
               0f, 0f, 0.6f, 0.8f, // minQF, lowQF, highQF, maxQF
               new ArchetypeWeight(Archetype.BALANCED, 1.5f),
               new ArchetypeWeight(Archetype.ASSAULT, 2f),
               new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 2f),
               new ArchetypeWeight(Archetype.FLEET, 2f)),
        INDEPENDENT(Factions.INDEPENDENT,
                    0f, 0f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                    new ArchetypeWeight(Archetype.BALANCED, 2f),
                    new ArchetypeWeight(Archetype.SKIRMISH, 0.75f),
                    new ArchetypeWeight(Archetype.ARTILLERY, 0.5f),
                    new ArchetypeWeight(Archetype.ESCORT, 1.5f),
                    new ArchetypeWeight(Archetype.FLEET, 1.5f)),
        NEUTRAL(Factions.NEUTRAL,
                0f, 0f, 1f, 1f), // minQF, lowQF, highQF, maxQF
        LIONS_GUARD(Factions.LIONS_GUARD,
                    0.3f, 0.5f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                    new ArchetypeWeight(Archetype.BALANCED, 0.5f),
                    new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                    new ArchetypeWeight(Archetype.ELITE, 2f),
                    new ArchetypeWeight(Archetype.ASSAULT, 0.75f),
                    new ArchetypeWeight(Archetype.ARTILLERY, 0.75f),
                    new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 0.75f),
                    new ArchetypeWeight(Archetype.SUPPORT, 0.75f),
                    new ArchetypeWeight(Archetype.ESCORT, 0.75f),
                    new ArchetypeWeight(Archetype.FLEET, 0.5f),
                    new ArchetypeWeight(Archetype.ULTIMATE, 1.5f)),
        KOL(Factions.KOL,
            0f, 0f, 0.4f, 0.6f, // minQF, lowQF, highQF, maxQF
            new ArchetypeWeight(Archetype.ELITE, 1.5f),
            new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
            new ArchetypeWeight(Archetype.SKIRMISH, 1.5f)),
        LUDDIC_CHURCH(Factions.LUDDIC_CHURCH,
                      0f, 0f, 0.3f, 0.5f, // minQF, lowQF, highQF, maxQF
                      new ArchetypeWeight(Archetype.BALANCED, 2f),
                      new ArchetypeWeight(Archetype.ELITE, 0.5f),
                      new ArchetypeWeight(Archetype.SUPPORT, 2f),
                      new ArchetypeWeight(Archetype.ESCORT, 2f),
                      new ArchetypeWeight(Archetype.FLEET, 2f)),
        LUDDIC_PATH(Factions.LUDDIC_PATH,
                    0f, 0f, 0.2f, 0.4f, // minQF, lowQF, highQF, maxQF
                    new ArchetypeWeight(Archetype.BALANCED, 0.25f),
                    new ArchetypeWeight(Archetype.STRIKE, 2f),
                    new ArchetypeWeight(Archetype.ELITE, 0.25f),
                    new ArchetypeWeight(Archetype.ASSAULT, 2f),
                    new ArchetypeWeight(Archetype.SKIRMISH, 3f),
                    new ArchetypeWeight(Archetype.ARTILLERY, 0.5f),
                    new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 0.5f),
                    new ArchetypeWeight(Archetype.SUPPORT, 0.25f),
                    new ArchetypeWeight(Archetype.ESCORT, 0.25f),
                    new ArchetypeWeight(Archetype.FLEET, 0.25f),
                    new ArchetypeWeight(Archetype.ULTIMATE, 0.25f)),
        PIRATES(Factions.PIRATES,
                0f, 0f, 0f, 0.5f, // minQF, lowQF, highQF, maxQF
                new ArchetypeWeight(Archetype.BALANCED, 0.75f),
                new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                new ArchetypeWeight(Archetype.SKIRMISH, 1.5f),
                new ArchetypeWeight(Archetype.ARTILLERY, 0.5f),
                new ArchetypeWeight(Archetype.SUPPORT, 0.5f),
                new ArchetypeWeight(Archetype.ESCORT, 0.25f),
                new ArchetypeWeight(Archetype.FLEET, 0.25f)),
        TRITACHYON(Factions.TRITACHYON,
                   0.5f, 1f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                   new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                   new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                   new ArchetypeWeight(Archetype.ELITE, 1.5f)),
        PERSEAN(Factions.PERSEAN,
                0.2f, 0.4f, 0.6f, 1f), // minQF, lowQF, highQF, maxQF
        DERELICT(Factions.DERELICT,
                 0f, 0f, 0f, 0.5f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.ARTILLERY, 0f),
                 new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 0f),
                 new ArchetypeWeight(Archetype.ELITE, 0f),
                 new ArchetypeWeight(Archetype.ESCORT, 0f),
                 new ArchetypeWeight(Archetype.SKIRMISH, 0f),
                 new ArchetypeWeight(Archetype.SUPPORT, 0f),
                 new ArchetypeWeight(Archetype.FLEET, 0f),
                 new ArchetypeWeight(Archetype.ULTIMATE, 0f)),
        REMNANTS(Factions.REMNANTS,
                 0.5f, 1f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.BALANCED, 3f),
                 new ArchetypeWeight(Archetype.ULTIMATE, 1.5f),
                 new ArchetypeWeight(Archetype.ELITE, 1.5f)),
        CABAL("cabal",
              0.5f, 1f, 1f, 1f, // minQF, lowQF, highQF, maxQF
              new ArchetypeWeight(Archetype.BALANCED, 1.5f),
              new ArchetypeWeight(Archetype.STRIKE, 1.5f),
              new ArchetypeWeight(Archetype.ELITE, 1.5f),
              new ArchetypeWeight(Archetype.ULTIMATE, 1.5f)),
        IMPERIUM("interstellarimperium",
                 0f, 0f, 0.6f, 0.8f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.STRIKE, 0.75f),
                 new ArchetypeWeight(Archetype.ELITE, 0.5f),
                 new ArchetypeWeight(Archetype.SKIRMISH, 0.5f),
                 new ArchetypeWeight(Archetype.ARTILLERY, 2f),
                 new ArchetypeWeight(Archetype.ULTIMATE, 0.5f)),
        CITADEL("citadeldefenders",
                0.2f, 0.5f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                new ArchetypeWeight(Archetype.ELITE, 1.5f),
                new ArchetypeWeight(Archetype.SKIRMISH, 1.5f)),
        BLACKROCK("blackrock_driveyards",
                  0.2f, 0.4f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                  new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                  new ArchetypeWeight(Archetype.ELITE, 1.5f),
                  new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
                  new ArchetypeWeight(Archetype.SKIRMISH, 2f),
                  new ArchetypeWeight(Archetype.ARTILLERY, 0.5f)),
        EXIGENCY("exigency",
                 0.5f, 1f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                 new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
                 new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 2f),
                 new ArchetypeWeight(Archetype.SUPPORT, 0.5f),
                 new ArchetypeWeight(Archetype.ESCORT, 0.5f),
                 new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        AHRIMAN("exipirated",
                0f, 0f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                new ArchetypeWeight(Archetype.ELITE, 1.5f),
                new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
                new ArchetypeWeight(Archetype.ARTILLERY, 1.5f),
                new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 1.5f),
                new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        TEMPLARS("templars",
                 1f, 1f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.BALANCED, 2f),
                 new ArchetypeWeight(Archetype.ELITE, 2f),
                 new ArchetypeWeight(Archetype.SUPPORT, 0.25f),
                 new ArchetypeWeight(Archetype.ESCORT, 0.25f),
                 new ArchetypeWeight(Archetype.FLEET, 0.25f),
                 new ArchetypeWeight(Archetype.ULTIMATE, 2f)),
        SHADOWYARDS("shadow_industry",
                    0.2f, 0.4f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                    new ArchetypeWeight(Archetype.BALANCED, 2f),
                    new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 1.5f)),
        MAYORATE("mayorate",
                 0.2f, 0.3f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.BALANCED, 0.5f),
                 new ArchetypeWeight(Archetype.ELITE, 1.5f),
                 new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        JUNK_PIRATES("junk_pirates",
                     0.2f, 0.3f, 0.7f, 0.8f, // minQF, lowQF, highQF, maxQF
                     new ArchetypeWeight(Archetype.BALANCED, 0.5f),
                     new ArchetypeWeight(Archetype.ELITE, 2f),
                     new ArchetypeWeight(Archetype.ARTILLERY, 0.5f),
                     new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 0.5f),
                     new ArchetypeWeight(Archetype.SUPPORT, 0.25f),
                     new ArchetypeWeight(Archetype.ESCORT, 0.5f),
                     new ArchetypeWeight(Archetype.FLEET, 0.25f),
                     new ArchetypeWeight(Archetype.ULTIMATE, 1.5f)),
        PACK("pack",
             0.2f, 0.3f, 1f, 1f, // minQF, lowQF, highQF, maxQF
             new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        ASP("syndicate_asp",
            0f, 0f, 1f, 1f, // minQF, lowQF, highQF, maxQF
            new ArchetypeWeight(Archetype.BALANCED, 2f),
            new ArchetypeWeight(Archetype.STRIKE, 0.5f),
            new ArchetypeWeight(Archetype.ELITE, 0.5f),
            new ArchetypeWeight(Archetype.ESCORT, 2f),
            new ArchetypeWeight(Archetype.FLEET, 2f),
            new ArchetypeWeight(Archetype.ULTIMATE, 0.5f)),
        SCY("SCY",
            0f, 0f, 1f, 1f, // minQF, lowQF, highQF, maxQF
            new ArchetypeWeight(Archetype.STRIKE, 1.5f),
            new ArchetypeWeight(Archetype.ELITE, 1.5f),
            new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
            new ArchetypeWeight(Archetype.SKIRMISH, 0.5f)),
        TIANDONG("tiandong",
                 0.2f, 0.4f, 0.6f, 0.8f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                 new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 1.5f),
                 new ArchetypeWeight(Archetype.ESCORT, 1.5f)),
        DIABLE("diableavionics",
               0.2f, 0.6f, 1f, 1f, // minQF, lowQF, highQF, maxQF
               new ArchetypeWeight(Archetype.SKIRMISH, 1.5f),
               new ArchetypeWeight(Archetype.ESCORT, 1.5f)),
        ORA("ORA",
            0.2f, 0.4f, 1f, 1f, // minQF, lowQF, highQF, maxQF
            new ArchetypeWeight(Archetype.STRIKE, 0.5f),
            new ArchetypeWeight(Archetype.ASSAULT, 0.75f),
            new ArchetypeWeight(Archetype.SKIRMISH, 0.75f),
            new ArchetypeWeight(Archetype.ARTILLERY, 2f),
            new ArchetypeWeight(Archetype.SUPPORT, 1.5f),
            new ArchetypeWeight(Archetype.ESCORT, 1.5f)),
        DOMAIN("domain",
               0f, 0f, 1f, 1f), // minQF, lowQF, highQF, maxQF
        SECTOR("sector",
               0f, 0f, 1f, 1f), // minQF, lowQF, highQF, maxQF
        EVERYTHING("everything",
                   0f, 0f, 1f, 1f), // minQF, lowQF, highQF, maxQF
        PLAYER_NPC("player_npc",
                   0f, 0f, 1f, 1f); // minQF, lowQF, highQF, maxQF

        public final String faction;

        public final float minQF;
        public final float lowQF;
        public final float highQF;
        public final float maxQF;

        public final Map<Archetype, Float> archetypeWeights;

        private FactionStyle(String faction, float minQF, float lowQF, float highQF, float maxQF,
                             ArchetypeWeight... weights) {
            this.faction = faction;
            this.minQF = minQF;
            this.lowQF = lowQF;
            this.highQF = highQF;
            this.maxQF = maxQF;
            archetypeWeights = new HashMap<>(weights.length);
            for (ArchetypeWeight archetypeWeight : weights) {
                archetypeWeights.put(archetypeWeight.archetype, archetypeWeight.weight);
            }
        }

        public static FactionStyle getStyle(String faction) {
            for (FactionStyle style : FactionStyle.values()) {
                if (style.faction.contentEquals(faction)) {
                    return style;
                }
            }
            return INDEPENDENT;
        }
    }

    public static enum FleetStyle {

        CIVILIAN(new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                 new ArchetypeWeight(Archetype.STRIKE, 0.5f),
                 new ArchetypeWeight(Archetype.ELITE, 0.25f),
                 new ArchetypeWeight(Archetype.SUPPORT, 1.5f),
                 new ArchetypeWeight(Archetype.ESCORT, 2f),
                 new ArchetypeWeight(Archetype.ULTIMATE, 0f)),
        STANDARD(new ArchetypeWeight(Archetype.SUPPORT, 0.5f)),
        MILITARY(new ArchetypeWeight(Archetype.BALANCED, 1f),
                 new ArchetypeWeight(Archetype.SKIRMISH, 0.75f),
                 new ArchetypeWeight(Archetype.ESCORT, 0.75f),
                 new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        RAIDER(new ArchetypeWeight(Archetype.BALANCED, 0.5f),
               new ArchetypeWeight(Archetype.STRIKE, 1.5f),
               new ArchetypeWeight(Archetype.SKIRMISH, 2f),
               new ArchetypeWeight(Archetype.ASSAULT, 1.25f),
               new ArchetypeWeight(Archetype.SUPPORT, 0.5f),
               new ArchetypeWeight(Archetype.ESCORT, 0.5f),
               new ArchetypeWeight(Archetype.FLEET, 0.25f)),
        ELITE(new ArchetypeWeight(Archetype.BALANCED, 0.5f),
              new ArchetypeWeight(Archetype.ELITE, 1.5f),
              new ArchetypeWeight(Archetype.SUPPORT, 0.5f),
              new ArchetypeWeight(Archetype.ESCORT, 0.5f),
              new ArchetypeWeight(Archetype.FLEET, 0.5f),
              new ArchetypeWeight(Archetype.ULTIMATE, 1.25f)),
        PROFESSIONAL(new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                     new ArchetypeWeight(Archetype.ELITE, 1.25f),
                     new ArchetypeWeight(Archetype.FLEET, 0.75f));

        public final Map<Archetype, Float> archetypeWeights;

        private FleetStyle(ArchetypeWeight... weights) {
            archetypeWeights = new HashMap<>(weights.length);
            for (ArchetypeWeight archetypeWeight : weights) {
                archetypeWeights.put(archetypeWeight.archetype, archetypeWeight.weight);
            }
        }
    }

    public static enum Archetype {

        ARCADE, BALANCED, STRIKE, ELITE, ASSAULT, SKIRMISH, ARTILLERY, CLOSE_SUPPORT, SUPPORT, ESCORT, FLEET, ULTIMATE
    }

    public static enum SlotType {

        FRONT, SIDE, BACK, HEMI, WIDE, BORESIGHT, CROOKED
    }

    public static enum FighterPreference {

        STRIKE
    }

    public static enum WeaponPreference {

        ENERGY, BALLISTIC, MISSILE, STRIKE, PD
    }

    public static enum AsymmetryLevel {

        NONE, LARGE, MEDIUM, FULL
    }

    public static class ArchetypeWeight {

        public final Archetype archetype;
        public final float weight;

        public ArchetypeWeight(Archetype archetype, float weight) {
            this.archetype = archetype;
            this.weight = weight;
        }
    }
}
