package data.scripts.util;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.combat.ShieldAPI.ShieldType;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.HullMods;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs.Archetype;
import data.scripts.util.DS_Defs.AsymmetryLevel;
import data.scripts.util.DS_Defs.FactionStyle;
import data.scripts.util.DS_Defs.FighterPreference;
import data.scripts.util.DS_Defs.FleetStyle;
import data.scripts.util.DS_Defs.WeaponPreference;
import data.scripts.variants.DS_VariantRandomizer.HullmodFactors;
import data.scripts.variants.DS_WeaponGrouper.WeaponGroupConfig;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class DS_Util {

    public static final Comparator<WeaponSlotAPI> SLOT_SORTER = new Comparator<WeaponSlotAPI>() {
        // -1 means slot1 is first, 1 means slot2 is first
        @Override
        public int compare(WeaponSlotAPI slot1, WeaponSlotAPI slot2) {
            int slot1Priority = 0;
            int slot2Priority = 0;
            slot1Priority += (slot1.getWeaponType() == WeaponType.ENERGY) ? 1 : 0;
            slot1Priority += (slot1.getWeaponType() == WeaponType.BALLISTIC) ? 2 : 0;
            slot1Priority += (slot1.getWeaponType() == WeaponType.MISSILE) ? 4 : 0;
            slot1Priority += (slot1.getWeaponType() == WeaponType.HYBRID) ? 8 : 0;
            slot1Priority += (slot1.getWeaponType() == WeaponType.SYNERGY) ? 16 : 0;
            slot1Priority += (slot1.getWeaponType() == WeaponType.COMPOSITE) ? 32 : 0;
            slot1Priority += (slot1.getWeaponType() == WeaponType.UNIVERSAL) ? 64 : 0;
            slot1Priority += slot1.isHardpoint() ? 128 : 0;
            slot1Priority += slot1.isHidden() ? 256 : 0;
            slot1Priority += (slot1.getSlotSize() == WeaponSize.SMALL) ? 512 : 0;
            slot1Priority += (slot1.getSlotSize() == WeaponSize.MEDIUM) ? 1024 : 0;
            slot1Priority += (slot1.getSlotSize() == WeaponSize.LARGE) ? 2048 : 0;
            slot2Priority += (slot2.getWeaponType() == WeaponType.ENERGY) ? 1 : 0;
            slot2Priority += (slot2.getWeaponType() == WeaponType.BALLISTIC) ? 2 : 0;
            slot2Priority += (slot2.getWeaponType() == WeaponType.MISSILE) ? 4 : 0;
            slot2Priority += (slot2.getWeaponType() == WeaponType.HYBRID) ? 8 : 0;
            slot2Priority += (slot2.getWeaponType() == WeaponType.SYNERGY) ? 16 : 0;
            slot2Priority += (slot2.getWeaponType() == WeaponType.COMPOSITE) ? 32 : 0;
            slot2Priority += (slot2.getWeaponType() == WeaponType.UNIVERSAL) ? 64 : 0;
            slot2Priority += slot2.isHardpoint() ? 128 : 0;
            slot2Priority += slot2.isHidden() ? 256 : 0;
            slot2Priority += (slot2.getSlotSize() == WeaponSize.SMALL) ? 512 : 0;
            slot2Priority += (slot2.getSlotSize() == WeaponSize.MEDIUM) ? 1024 : 0;
            slot2Priority += (slot2.getSlotSize() == WeaponSize.LARGE) ? 2048 : 0;
            if (slot1Priority > slot2Priority) {
                return -1;
            } else if (slot2Priority > slot1Priority) {
                return 1;
            }

            if (slot1.getArc() > slot2.getArc()) {
                return -1;
            } else if (slot2.getArc() > slot1.getArc()) {
                return 1;
            }

            if (slot1.getAngle() > slot2.getAngle()) {
                return -1;
            } else if (slot2.getAngle() > slot1.getAngle()) {
                return 1;
            }

            return slot1.getId().compareTo(slot2.getId());
        }
    };

    public static void adjustHullmodFactorsForFaction(HullmodFactors factor, List<String> factionList) {
        float logisticsFactorMod = 1f;
        float eliteFactorMod = 1f;
        float shieldFactorMod = 1f;
        float armorFactorMod = 1f;
        float defenseFactorMod = 1f;
        float fluxFactorMod = 1f;
        float enginesFactorMod = 1f;
        float attackFactorMod = 1f;
        float rangeFactorMod = 1f;
        float capacitorFactorMod = 1f;
        float ventFactorMod = 1f;
        float missileFactorMod = 1f;
        float fightersFactorMod = 1f;

        for (String faction : factionList) {
            switch (faction) {
                case Factions.HEGEMONY:
                    armorFactorMod *= 1.5f;
                    defenseFactorMod *= 1.5f;
                    break;
                case Factions.DIKTAT:
                    defenseFactorMod *= 1.5f;
                    missileFactorMod *= 1.5f;
                    break;
                case Factions.INDEPENDENT:
                    eliteFactorMod *= 0.5f;
                    logisticsFactorMod *= 1.5f;
                    break;
                case Factions.SCAVENGERS:
                    logisticsFactorMod *= 1.5f;
                    break;
                case Factions.PERSEAN:
                    break;
                case Factions.LIONS_GUARD:
                    eliteFactorMod *= 1.5f;
                    enginesFactorMod *= 1.5f;
                    break;
                case Factions.KOL:
                    eliteFactorMod *= 1.5f;
                    enginesFactorMod *= 1.5f;
                    attackFactorMod *= 1.5f;
                    break;
                case Factions.LUDDIC_CHURCH:
                    logisticsFactorMod *= 1.5f;
                    enginesFactorMod *= 0.5f;
                    rangeFactorMod *= 1.5f;
                    capacitorFactorMod *= 1.5f;
                    fightersFactorMod *= 2f;
                    break;
                case Factions.LUDDIC_PATH:
                    logisticsFactorMod *= 0.5f;
                    eliteFactorMod *= 2f;
                    enginesFactorMod *= 2f;
                    fluxFactorMod *= 3f;
                    rangeFactorMod *= 0.5f;
                    capacitorFactorMod *= 0.5f;
                    ventFactorMod *= 0.75f;
                    break;
                case Factions.PIRATES:
                    logisticsFactorMod *= 0.5f;
                    attackFactorMod *= 1.5f;
                    break;
                case Factions.TRITACHYON:
                    fluxFactorMod *= 1.5f;
                    shieldFactorMod *= 1.5f;
                    enginesFactorMod *= 1.5f;
                    fightersFactorMod *= 1.5f;
                    break;
                case Factions.DERELICT:
                    eliteFactorMod *= 0.5f;
                    logisticsFactorMod *= 0.5f;
                    fightersFactorMod *= 0.5f;
                    break;
                case Factions.REMNANTS:
                    logisticsFactorMod *= 0.5f;
                    missileFactorMod *= 2f;
                    break;
                case "cabal":
                    fluxFactorMod *= 1.5f;
                    shieldFactorMod *= 1.5f;
                    enginesFactorMod *= 1.5f;
                    attackFactorMod *= 1.5f;
                    break;
                case "interstellarimperium":
                    armorFactorMod *= 2f;
                    rangeFactorMod *= 2f;
                    missileFactorMod *= 1.5f;
                    break;
                case "citadeldefenders":
                    fluxFactorMod *= 1.5f;
                    enginesFactorMod *= 1.5f;
                    attackFactorMod *= 1.5f;
                    break;
                case "blackrock_driveyards":
                    armorFactorMod *= 1.5f;
                    fluxFactorMod *= 1.5f;
                    enginesFactorMod *= 1.5f;
                    attackFactorMod *= 1.5f;
                    defenseFactorMod *= 1.5f;
                    break;
                case "exigency":
                    fluxFactorMod *= 1.5f;
                    missileFactorMod *= 2f;
                    fightersFactorMod *= 1.5f;
                    break;
                case "exipirated":
                    missileFactorMod *= 1.5f;
                    fightersFactorMod *= 1.5f;
                    break;
                case "templars":
                    logisticsFactorMod *= 0f;
                    fluxFactorMod *= 1.5f;
                    rangeFactorMod *= 2f;
                    capacitorFactorMod *= 1.5f;
                    ventFactorMod *= 1.5f;
                    fightersFactorMod *= 1.5f;
                    break;
                case "shadow_industry":
                    fluxFactorMod *= 1.5f;
                    enginesFactorMod *= 1.5f;
                    rangeFactorMod *= 1.5f;
                    break;
                case "mayorate":
                    enginesFactorMod *= 1.5f;
                    attackFactorMod *= 2f;
                    break;
                case "junk_pirates":
                    logisticsFactorMod *= 0.5f;
                    eliteFactorMod *= 2f;
                    attackFactorMod *= 2f;
                    break;
                case "pack":
                    attackFactorMod *= 2f;
                    fightersFactorMod *= 0.5f;
                    break;
                case "syndicate_asp":
                    logisticsFactorMod *= 2f;
                    eliteFactorMod *= 0.5f;
                    break;
                case "SCY":
                    armorFactorMod *= 1.5f;
                    fluxFactorMod *= 1.5f;
                    enginesFactorMod *= 0.5f;
                    ventFactorMod *= 1.5f;
                    break;
                case "tiandong":
                    armorFactorMod *= 1.5f;
                    fluxFactorMod *= 1.5f;
                    enginesFactorMod *= 1.5f;
                    ventFactorMod *= 2f;
                    break;
                case "diableavionics":
                    armorFactorMod *= 2f;
                    shieldFactorMod *= 2f;
                    rangeFactorMod *= 2f;
                    missileFactorMod *= 1.5f;
                    capacitorFactorMod *= 1.75f;
                    ventFactorMod *= 1.75f;
                    fightersFactorMod *= 1.5f;
                    break;
                case "ORA":
                    shieldFactorMod *= 2f;
                    fluxFactorMod *= 2f;
                    capacitorFactorMod *= 1.75f;
                    ventFactorMod *= 1.75f;
                    enginesFactorMod *= 1.5f;
                    armorFactorMod *= 0.5f;
                    missileFactorMod *= 0.25f;
                    rangeFactorMod *= 1.5f;
                    break;
                default:
                    break;
            }
        }

        factor.logistics *= (float) Math.pow(logisticsFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.elite *= (float) Math.pow(eliteFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.shield *= (float) Math.pow(shieldFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.armor *= (float) Math.pow(armorFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.defense *= (float) Math.pow(defenseFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.fighters *= (float) Math.pow(fightersFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.flux *= (float) Math.pow(fluxFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.engines *= (float) Math.pow(enginesFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.attack *= (float) Math.pow(attackFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.missile *= (float) Math.pow(missileFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.range *= (float) Math.pow(rangeFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.capacitor *= (float) Math.pow(capacitorFactorMod, Math.sqrt(1.0 / factionList.size()));
        factor.vent *= (float) Math.pow(ventFactorMod, Math.sqrt(1.0 / factionList.size()));
    }

    public static void adjustHullmodFactorsForHull(HullmodFactors factor, String baseHullId) {
        switch (baseHullId) {
            case "tem_archbishop":
                factor.range *= 1.5f;
                factor.missile *= 1.5f;
                break;
            case "tem_chevalier":
                factor.range *= 3f;
                factor.missile *= 1.5f;
                factor.engines *= 1.5f;
                break;
            case "tem_crusader":
                factor.armor *= 2f;
                break;
            case "SCY_dracanae":
            case "SCY_hydra":
            case "SCY_megaera":
                factor.range *= 0.5f;
                break;
            case "buffalo2":
            case "gryphon":
            case "venture":
            case "ssp_archer":
            case "ssp_mongrel":
            case "ssp_vindicator":
            case "exigency_carrier":
            case "exigency_compactfrigate":
            case "exigency_cruiser":
            case "exigency_destroyer":
            case "exigency_indra":
            case "exigency_lightcarrier":
            case "exigency_yria":
            case "ii_basileus":
            case "ilk_del_azarchel":
            case "junk_pirates_hammer":
            case "junk_pirates_boss_dugong":
            case "SCY_erymanthianBoar":
            case "SCY_geryon":
            case "SCY_lealaps":
            case "tiandong_dingjun":
            case "tiandong_tuolu":
                factor.missile *= 3f;
                break;
            case "exipirated_azryel":
            case "exipirated_gehenna":
            case "exipirated_harinder":
            case "exipirated_kafziel":
            case "exipirated_rauwel":
            case "ii_lynx":
            case "junk_pirates_dugong":
                factor.missile *= 2f;
                break;
            case "diableavionics_calm":
            case "diableavionics_draft":
            case "diableavionics_storm":
            case "Fox_Cruiser":
                factor.missile *= 1.5f;
                break;
            case "ora_beatitude":
            case "ora_bliss":
            case "ora_communion":
            case "ora_elevation":
            case "ora_enlightenment":
            case "ora_felicity":
            case "ora_grace":
            case "ora_harmony":
            case "ora_mirth":
            case "ora_revelation":
            case "ora_sanctuary":
                factor.range *= 1.5f;
                break;
            default:
                break;
        }
    }

    public static int calculatePowerLevel(CampaignFleetAPI fleet) {
        int power = fleet.getFleetPoints();
        int offLvl = 0;
        int cdrLvl = 0;
        boolean commander = false;
        for (OfficerDataAPI officer : fleet.getFleetData().getOfficersCopy()) {
            if (officer.getPerson() == fleet.getCommander()) {
                commander = true;
                cdrLvl = officer.getPerson().getStats().getLevel();
            } else {
                offLvl += officer.getPerson().getStats().getLevel();
            }
        }
        if (!commander) {
            cdrLvl = fleet.getCommanderStats().getLevel();
        }
        power *= Math.sqrt(cdrLvl / 100f + 1f);
        int flatBonus = cdrLvl + offLvl + 10;
        if (power < flatBonus * 2) {
            flatBonus *= power / (float) (flatBonus * 2);
        }
        power += flatBonus;
        return power;
    }

    public static WeaponGroupConfig chooseWeaponGroupConfig(String baseHullId) {
        switch (baseHullId) {
            case "brdyx_morpheus":
                return WeaponGroupConfig.MORPHEUS;
            case "tem_archbishop":
            case "tem_chevalier":
            case "tem_crusader":
            case "tem_jesuit":
            case "tem_martyr":
            case "tem_paladin":
            case "tem_boss_archbishop":
            case "tem_boss_paladin":
                return WeaponGroupConfig.LINKED_STRIKE;
            case "exigency_carrier":
            case "exigency_compactfrigate":
            case "exigency_cruiser":
            case "exigency_destroyer":
            case "exigency_indra":
            case "exigency_lightcarrier":
            case "exigency_yria":
            case "exipirated_azryel":
            case "exipirated_gehenna":
            case "exipirated_harinder":
            case "exipirated_kafziel":
            case "exipirated_rauwel":
                return WeaponGroupConfig.EXIGENCY;
            case "junk_pirates_dugong":
            case "junk_pirates_boss_dugong":
                return WeaponGroupConfig.MISSILE_BROADSIDE;
            case "conquest":
            case "ssp_torch":
            case "ssp_boss_conquest":
            case "junk_pirates_kraken":
            case "ms_elysium":
            case "ms_elysium_P":
            case "ms_inanna":
            case "ora_beatitude":
            case "ora_bliss":
            case "ora_communion":
            case "ora_enlightenment":
            case "ora_felicity":
            case "ora_harmony":
            case "ora_mirth":
            case "ora_revelation":
            case "ora_sanctuary":
            case "syndicate_asp_gigantophis":
            case "tiandong_xu":
                return WeaponGroupConfig.BROADSIDE;
            case "buffalo2":
            case "gryphon":
            case "venture":
            case "ssp_archer":
            case "ssp_mongrel":
            case "ssp_vindicator":
            case "diableavionics_calm":
            case "diableavionics_draft":
            case "diableavionics_storm":
            case "ii_basileus":
            case "ii_lynx":
            case "ilk_del_azarchel":
            case "Fox_Cruiser":
            case "junk_pirates_hammer":
            case "SCY_erymanthianBoar":
            case "SCY_geryon":
            case "SCY_lealaps":
            case "tiandong_dingjun":
            case "tiandong_tuolu":
                return WeaponGroupConfig.MISSILE;
            default:
                return WeaponGroupConfig.STANDARD;
        }
    }

    public static Color colorBlend(Color a, Color b, float amount) {
        float conjAmount = 1f - amount;
        return new Color((int) Math.max(0, Math.min(255, a.getRed() * conjAmount + b.getRed() * amount)),
                         (int) Math.max(0, Math.min(255, a.getGreen() * conjAmount + b.getGreen() * amount)),
                         (int) Math.max(0, Math.min(255, a.getBlue() * conjAmount + b.getBlue() * amount)),
                         (int) Math.max(0, Math.min(255, a.getAlpha() * conjAmount + b.getAlpha() * amount)));
    }

    public static Color colorJitter(Color color, float amount) {
        return new Color(Math.max(0, Math.min(255, color.getRed() + (int) (((float) Math.random() - 0.5f) * amount))),
                         Math.max(0, Math.min(255, color.getGreen() + (int) (((float) Math.random() - 0.5f) * amount))),
                         Math.max(0, Math.min(255, color.getBlue() + (int) (((float) Math.random() - 0.5f) * amount))),
                         color.getAlpha());
    }

    public static AsymmetryLevel doesHullPreferAsymmetry(String baseHullId) {
        switch (baseHullId) {
            case "berserker":
            case "centurion":
            case "kite":
            case "picket":
            case "scarab":
            case "sentry":
            case "wayfarer":
            case "SCY_megaera":
            case "tiandong_luo_yang":
                return AsymmetryLevel.FULL;
            case "bastillon":
            case "brawler":
            case "harbinger":
            case "hyperion":
            case "ssp_venom":
            case "ssp_venomx":
            case "ssp_boss_hyperion":
            case "ssp_boss_shade":
            case "diableavionics_vapor":
            case "ii_decurion":
            case "tiandong_hujing":
                return AsymmetryLevel.MEDIUM;
            case "legion":
            case "rampart":
            case "tiandong_hanzhong":
                return AsymmetryLevel.LARGE;
            default:
                return AsymmetryLevel.NONE;
        }
    }

    public static boolean doesHullPreferBroadsideWeapons(String baseHullId) {
        switch (baseHullId) {
            case "conquest":
            case "ssp_torch":
            case "ssp_boss_conquest":
            case "junk_pirates_dugong":
            case "junk_pirates_kraken":
            case "junk_pirates_boss_dugong":
            case "ms_elysium":
            case "ms_inanna":
            case "ora_beatitude":
            case "ora_bliss":
            case "ora_communion":
            case "ora_enlightenment":
            case "ora_felicity":
            case "ora_harmony":
            case "ora_mirth":
            case "ora_revelation":
            case "ora_sanctuary":
            case "syndicate_asp_gigantophis":
            case "tiandong_xu":
                return true;
            default:
                return false;
        }
    }

    public static MarketAPI findNearestLocalMarketWithSameFaction(SectorEntityToken token) {
        List<MarketAPI> localMarkets = Misc.getMarketsInLocation(token.getContainingLocation());
        float distToLocalMarket = Float.MAX_VALUE;
        MarketAPI closest = null;
        for (MarketAPI market : localMarkets) {
            if (!market.getFaction().getId().contentEquals(token.getFaction().getId())) {
                continue;
            }
            if (market.getPrimaryEntity() == null) {
                continue;
            }
            if (market.getPrimaryEntity().getContainingLocation() != token.getContainingLocation()) {
                continue;
            }
            float currDist = Misc.getDistance(market.getPrimaryEntity().getLocation(), token.getLocation());
            if (currDist < distToLocalMarket) {
                distToLocalMarket = currDist;
                closest = market;
            }
        }
        return closest;
    }

    public static Map<Archetype, Float> getArchetypeWeights(FleetStyle fleetStyle, String faction) {
        FactionStyle factionStyle = FactionStyle.getStyle(faction);
        Map<Archetype, Float> archetypeWeights =
                              new HashMap<>(factionStyle.archetypeWeights.size() + fleetStyle.archetypeWeights.size());
        for (Map.Entry<Archetype, Float> entry : factionStyle.archetypeWeights.entrySet()) {
            Archetype archetype = entry.getKey();
            float weight = entry.getValue();
            archetypeWeights.put(archetype, weight);
        }
        for (Map.Entry<Archetype, Float> entry : fleetStyle.archetypeWeights.entrySet()) {
            Archetype archetype = entry.getKey();
            float weight = entry.getValue();
            Float currWeight = archetypeWeights.get(archetype);
            if (currWeight != null) {
                archetypeWeights.put(archetype, weight * currWeight);
            } else {
                archetypeWeights.put(archetype, weight);
            }
        }
        return archetypeWeights;
    }

    public static Collection<String> getBuiltInHullMods(ShipVariantAPI ship) {
        ShipVariantAPI tmp = ship.clone();
        tmp.clearHullMods();
        return tmp.getHullMods();
    }

    public static float getEconAvailability(String commodityId) {
        float commodityDemand = 0f;
        float commoditySupply = 0f;

        List<MarketAPI> allMarkets = Global.getSector().getEconomy().getMarketsCopy();
        for (MarketAPI market : allMarkets) {
            for (CommodityOnMarketAPI commodity : market.getAllCommodities()) {
                String id = commodity.getId();
                if (!id.contentEquals(commodityId)) {
                    continue;
                }

                commodity.getCommodity().isExotic();
                commodityDemand += commodity.getDemand().getDemandValue();
                commoditySupply += commodity.getSupplyValue() * commodity.getUtilityOnMarket();
            }
        }

        if (commodityDemand > 1f) {
            return commoditySupply / commodityDemand;
        } else {
            return 0f;
        }
    }

    public static String getFactionName(String faction) {
        switch (faction) {
            case Factions.PIRATES:
                return "Pirate";
            case Factions.HEGEMONY:
                return "Hegemony";
            case Factions.DIKTAT:
                return "Sindrian Diktat";
            case Factions.LIONS_GUARD:
                return "Lion's Guard";
            case Factions.INDEPENDENT:
                return "Independent";
            case Factions.SCAVENGERS:
                return "Scavenger";
            case Factions.TRITACHYON:
                return "Tri-Tachyon";
            case Factions.LUDDIC_PATH:
                return "Pather";
            case Factions.LUDDIC_CHURCH:
                return "Luddic Church";
            case Factions.KOL:
                return "Knights of Ludd";
            case Factions.PERSEAN:
                return "League";
            case Factions.DERELICT:
                return "Automated";
            case Factions.REMNANTS:
                return "Remnant";
            case "cabal":
                return "Starlight";
            case "interstellarimperium":
                return "Imperial";
            case "citadeldefenders":
                return "Citadel";
            case "blackrock_driveyards":
                return "Blackrock";
            case "exipirated":
                return "Association";
            case "exigency":
                return "ExigencyCorp";
            case "templars":
                return "Knights Templar";
            case "shadow_industry":
                return "Shadowyards";
            case "mayorate":
                return "Mayorate";
            case "junk_pirates":
                return "Junk Pirate";
            case "pack":
                return "P.A.C.K.";
            case "syndicate_asp":
                return "Syndicate";
            case "SCY":
                return "Scy";
            case "tiandong":
                return "Tiandong";
            case "diableavionics":
                return "Diable";
            case "ORA":
                return "Alliance";
            default:
                return "Unknown";
        }
    }

    public static String getFactionPrefix(String faction) {
        switch (faction) {
            case Factions.PIRATES:
                return "";
            case Factions.HEGEMONY:
                return "HSS";
            case Factions.DIKTAT:
                return "SDS";
            case Factions.LIONS_GUARD:
                return "LGS";
            case Factions.INDEPENDENT:
                return "ISS";
            case Factions.SCAVENGERS:
                return "ISS";
            case Factions.TRITACHYON:
                return "TTS";
            case Factions.LUDDIC_PATH:
                return "";
            case Factions.LUDDIC_CHURCH:
                return "CGR";
            case Factions.KOL:
                return "LSS";
            case Factions.PERSEAN:
                return "PLS";
            case Factions.DERELICT:
                return "DSS";
            case Factions.REMNANTS:
                return "TTDS";
            case "cabal":
                return "STAR";
            case "interstellarimperium":
                return "ISA";
            case "citadeldefenders":
                return "DCS";
            case "blackrock_driveyards":
                return "BRS";
            case "exipirated":
                return "AA";
            case "exigency":
                return "EXI";
            case "templars":
                return "";
            case "shadow_industry":
                return "SYS";
            case "mayorate":
                return "MNS";
            case "junk_pirates":
                return "RNS";
            case "pack":
                return "PSS";
            case "syndicate_asp":
                return "ASP";
            case "SCY":
                return "SNS";
            case "tiandong":
                return "THI";
            case "diableavionics":
                return "USN";
            case "ORA":
                return "ANS";
            default:
                return "ISS";
        }
    }

    public static FighterPreference getHullFighterPreference(String baseHullId) {
        switch (baseHullId) {
            case "astral":
            case "scintilla":
                return FighterPreference.STRIKE;
            default:
                return null;
        }
    }

    public static WeaponPreference getHullWeaponPreference(String baseHullId) {
        switch (baseHullId) {
            case "aurora":
            case "odyssey":
            case "sunder":
            case "sunder_s":
            case "ssp_excelsior":
            case "ssp_hecate":
            case "ssp_nautilus":
            case "ssp_sunder_u":
            case "ssp_tiger":
            case "ssp_boss_afflictor":
            case "ssp_boss_aurora":
            case "ssp_boss_odyssey":
            case "ssp_boss_paragon":
            case "ssp_boss_sunder":
            case "ssp_boss_tempest":
            case "fox_dfrigate":
            case "ii_barrus":
            case "ii_interrex":
            case "ilk_lilith":
            case "junk_pirates_langoustine":
            case "junk_pirates_kraken":
            case "ms_inanna":
            case "ms_scylla":
            case "ora_harmony":
            case "tem_martyr":
                return WeaponPreference.ENERGY;
            case "hammerhead":
            case "lasher":
            case "ssp_scythe":
            case "ssp_stalker":
            case "ssp_vanguard":
            case "ssp_wall_left":
            case "ssp_wall_right":
            case "ssp_boss_brawler":
            case "ssp_boss_dominator":
            case "ssp_boss_hammerhead":
            case "Fox_Frigate":
            case "ii_dictator":
            case "ii_hammerhead_i":
            case "ii_praetorian":
            case "ii_boss_praetorian":
            case "junk_pirates_dugong":
            case "junk_pirates_boss_dugong":
            case "ms_elysium_P":
            case "pack_komondor":
            case "SCY_alecto":
            case "tiandong_lao_hu":
            case "tiandong_nanzhong":
            case "tiandong_xiakou":
            case "tiandong_boss_wuzhang":
                return WeaponPreference.BALLISTIC;
            case "condor":
            case "gryphon":
            case "venture":
            case "vigilance":
            case "remnant_weapon_platform1":
            case "ssp_archer":
            case "ssp_dragon":
            case "ssp_mongrel":
            case "ssp_vindicator":
            case "ssp_boss_doom":
            case "exigency_compactfrigate":
            case "ii_lynx":
            case "ilk_del_azarchel":
            case "junk_pirates_hammer":
            case "SCY_geryon":
                return WeaponPreference.MISSILE;
            case "hyperion":
            case "ssp_boss_hyperion":
            case "brdy_stenos":
            case "diableavionics_haze":
            case "ora_revelation":
            case "SCY_megaera":
            case "SCY_tisiphone":
                return WeaponPreference.STRIKE;
            case "diableavionics_versant":
                return WeaponPreference.PD;
            default:
                return null;
        }
    }

    public static int getNonCivilianFleetPoints(FleetDataAPI fleetData) {
        int fp = 0;
        for (FleetMemberAPI member : fleetData.getMembersListCopy()) {
            if (member.isMothballed() || member.isCivilian() || member.getHullSpec().getHints().contains(
                    ShipTypeHints.CIVILIAN)) {
                continue;
            }
            fp += member.getFleetPointCost();
        }
        return Math.max(fp, 1);
    }

    public static String getNonDHullId(ShipHullSpecAPI spec) {
        if (spec == null) {
            return null;
        }
        if (spec.getDParentHullId() != null && !spec.getDParentHullId().isEmpty()) {
            return spec.getDParentHullId();
        } else {
            return spec.getHullId();
        }
    }

    public static String getNumerals(CampaignFleetAPI fleet, Random r) {
        String numerals = "";
        int number = 1;
        switch (fleet.getFaction().getId()) {
            case Factions.TRITACHYON:
            case Factions.DERELICT:
            case Factions.REMNANTS:
            case "diableavionics":
            case "citadeldefenders":
            case "blackrock_driveyards":
            case "exigency":
            case "shadow_industry": {
                while (r.nextDouble() <= 0.33) {
                    number++;
                }
                if (number <= 1) {
                    return numerals;
                }
                numerals = "-" + String.format("%03d", number);
                break;
            }
            case "tiandong": {
                while (r.nextDouble() <= 0.33 || number == 4) {
                    number++;
                }
                if (number <= 1) {
                    return numerals;
                }
                numerals = "-" + String.format("%03d", number);
                break;
            }
            default: {
                while (r.nextDouble() <= 0.33) {
                    number++;
                }
                if (number <= 1) {
                    return numerals;
                }
                numerals = " " + Global.getSettings().getRoman(number);
                break;
            }
        }
        return numerals;
    }

    public static boolean isHullmodBlocked(String hullModId, Archetype archetype) {
        Set<String> blockedMods = DS_Defs.BLOCK_ARCHETYPE_HULLMOD.get(archetype);
        if (blockedMods != null) {
            if (blockedMods.contains(hullModId)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isHullmodCompatible(String hullMod, ShipVariantAPI variant) {
        Collection<String> builtIn = getBuiltInHullMods(variant);

        if (variant.hasHullMod(hullMod)) {
            return false;
        }

        switch (hullMod) {
            case "advancedshieldemitter":
                if ((variant.getHullSpec().getDefenseType() != ShieldType.FRONT &&
                     variant.getHullSpec().getDefenseType() != ShieldType.OMNI &&
                     !variant.hasHullMod("frontshield")) ||
                        variant.hasHullMod("shieldbypass")) {
                    return false;
                }
                break;
            case "extendedshieldemitter":
                if ((variant.getHullSpec().getDefenseType() != ShieldType.FRONT &&
                     variant.getHullSpec().getDefenseType() != ShieldType.OMNI &&
                     !variant.hasHullMod("frontshield")) ||
                        variant.hasHullMod("shieldbypass")) {
                    return false;
                }
                break;
            case "frontemitter":
                if ((variant.getHullSpec().getDefenseType() != ShieldType.FRONT &&
                     variant.getHullSpec().getDefenseType() != ShieldType.OMNI &&
                     !variant.hasHullMod("frontshield")) ||
                        variant.getHullSpec().getDefenseType() == ShieldType.FRONT ||
                        variant.hasHullMod("shieldbypass")) {
                    return false;
                }
                break;
            case "frontshield":
                if (variant.getHullSpec().getDefenseType() != ShieldType.NONE ||
                        variant.hasHullMod("shieldbypass") ||
                        variant.hasHullMod("ExigencyRegen") ||
                        variant.hasHullMod("tem_latticeshield")) {
                    return false;
                }
                break;
            case "hardenedshieldemitter":
                if ((variant.getHullSpec().getDefenseType() != ShieldType.FRONT &&
                     variant.getHullSpec().getDefenseType() != ShieldType.OMNI &&
                     !variant.hasHullMod("frontshield")) ||
                        variant.hasHullMod("shieldbypass")) {
                    return false;
                }
                break;
            case "adaptiveshields":
                if ((variant.getHullSpec().getDefenseType() != ShieldType.FRONT &&
                     variant.getHullSpec().getDefenseType() != ShieldType.OMNI &&
                     !variant.hasHullMod("frontshield")) ||
                        variant.getHullSpec().getDefenseType() == ShieldType.OMNI ||
                        variant.hasHullMod("shieldbypass")) {
                    return false;
                }
                break;
            case "stabilizedshieldemitter":
                if ((variant.getHullSpec().getDefenseType() != ShieldType.FRONT &&
                     variant.getHullSpec().getDefenseType() != ShieldType.OMNI &&
                     !variant.hasHullMod("frontshield")) ||
                        variant.hasHullMod("shieldbypass")) {
                    return false;
                }
                break;
            case "shieldbypass":
                if ((variant.getHullSpec().getDefenseType() != ShieldType.FRONT &&
                     variant.getHullSpec().getDefenseType() != ShieldType.OMNI &&
                     !variant.hasHullMod("frontshield")) ||
                        variant.hasHullMod("frontshield") ||
                        (variant.hasHullMod("frontemitter") && !builtIn.contains("frontemitter")) ||
                        (variant.hasHullMod("adaptiveshields") && !builtIn.contains("adaptiveshields")) ||
                        (variant.hasHullMod("stabilizedshieldemitter") && !builtIn.contains("stabilizedshieldemitter")) ||
                        (variant.hasHullMod("hardenedshieldemitter") && !builtIn.contains("hardenedshieldemitter")) ||
                        (variant.hasHullMod("advancedshieldemitter") && !builtIn.contains("advancedshieldemitter")) ||
                        (variant.hasHullMod("extendedshieldemitter") && !builtIn.contains("extendedshieldemitter"))) {
                    return false;
                }
                break;

            case "armoredweapons":
                if (variant.hasHullMod("brimaginosregen")) {
                    return false;
                }
                break;
            case "heavyarmor":
                if (variant.hasHullMod("brimaginosregen")) {
                    return false;
                }
                break;
            case "solar_shielding":
                if (variant.hasHullMod("brimaginosregen") ||
                        variant.hasHullMod("tem_latticeshield")) {
                    return false;
                }
                break;
            case "brassaultop":
                if (!variant.getHullSpec().getHullId().startsWith("brdy_") &&
                        !variant.getHullSpec().getHullId().startsWith("brdyx_")) {
                    return false;
                }
                break;
            case "ii_energized_armor":
                if (!variant.getHullSpec().getHullId().startsWith("ii_")) {
                    return false;
                }
                break;

            case "augmentedengines":
                if (variant.hasHullMod("SCY_engineering") ||
                        variant.hasHullMod("brdrive") ||
                        variant.hasHullMod("tow_cable")) {
                    return false;
                }
                break;
            case "unstable_injector":
                if (variant.hasHullMod("SCY_engineering") ||
                        variant.hasHullMod("brdrive")) {
                    return false;
                }
                break;
            case "brdrive":
                if ((!variant.getHullSpec().getHullId().startsWith("brdy_") &&
                     !variant.getHullSpec().getHullId().startsWith("brdyx_")) ||
                        variant.hasHullMod("augmentedengines") ||
                        variant.hasHullMod("unstable_injector") ||
                        variant.hasHullMod("safetyoverrides")) {
                    return false;
                }
                break;
            case "auxiliarythrusters":
                if (variant.hasHullMod("SCY_engineering")) {
                    return false;
                }
                break;

            case "targetingunit":
                if (variant.hasHullMod("dedicated_targeting_core") ||
                        variant.hasHullMod("brtarget") ||
                        variant.hasHullMod("advancedcore") ||
                        variant.hasHullMod("supercomputer")) {
                    return false;
                }
                break;
            case "dedicated_targeting_core":
                if ((variant.getHullSize() != HullSize.CAPITAL_SHIP && variant.getHullSize() != HullSize.CRUISER) ||
                        variant.hasHullMod("targetingunit") ||
                        variant.hasHullMod("brtarget") ||
                        variant.hasHullMod("advancedcore") ||
                        variant.hasHullMod("supercomputer")) {
                    return false;
                }
                break;
            case "brtarget":
                if ((!variant.getHullSpec().getHullId().startsWith("brdy_") &&
                     !variant.getHullSpec().getHullId().startsWith("brdyx_")) ||
                        variant.hasHullMod("targetingunit") ||
                        variant.hasHullMod("dedicated_targeting_core") ||
                        variant.hasHullMod("advancedcore") ||
                        variant.hasHullMod("supercomputer")) {
                    return false;
                }
                break;
            case "ii_fire_control":
                if (!variant.getHullSpec().getHullId().startsWith("ii_")) {
                    return false;
                }
                break;

            case "converted_hangar":
                if (variant.getHullSpec().getFighterBays() > 0 ||
                        variant.getHullSize() == HullSize.FRIGATE ||
                        variant.hasHullMod(HullMods.PHASE_FIELD)) {
                    return false;
                }
                break;
            case "expanded_deck_crew":
                if (variant.getHullSpec().getFighterBays() == 0) {
                    return false;
                }
                break;
            case "recovery_shuttles":
                if (variant.getHullSpec().getFighterBays() == 0) {
                    return false;
                }
                break;
            case "diableavionics_universaldecks":
                if (!variant.getHullSpec().getHullId().startsWith("diableavionics_") ||
                        (variant.getHullSpec().getFighterBays() == 0)) {
                    return false;
                }
                break;

            case "hardened_subsystems":
                if (!(variant.getHullSpec().getNoCRLossTime() < 10000 ||
                      variant.getHullSpec().getCRLossPerSecond() > 0)) {
                    return false;
                }
                break;
            case "supply_conservation_program":
                if (!(variant.getHullSpec().getNoCRLossTime() < 10000 ||
                      variant.getHullSpec().getCRLossPerSecond() > 0) ||
                        variant.hasHullMod(HullMods.CIVGRADE) ||
                        variant.hasHullMod(HullMods.AUTOMATED) ||
                        variant.hasHullMod("maximized_ordinance")) {
                    return false;
                }
                break;

            case "safetyoverrides":
                if (variant.getHullSize() == HullSize.CAPITAL_SHIP ||
                        variant.hasHullMod(HullMods.CIVGRADE) ||
                        variant.hasHullMod("brdrive")) {
                    return false;
                }
                break;
            case "maximized_ordinance":
                if (variant.hasHullMod(HullMods.CIVGRADE) ||
                        variant.hasHullMod("supply_conservation_program")) {
                    return false;
                }
                break;

            case "additional_crew_quarters":
                if (variant.hasHullMod("fuel_expansion") ||
                        variant.hasHullMod("cargo_expansion") ||
                        variant.hasHullMod("ilk_AICrew") ||
                        variant.hasHullMod(HullMods.AUTOMATED)) {
                    return false;
                }
                break;
            case "cargo_expansion":
                if (variant.hasHullMod("fuel_expansion") ||
                        variant.hasHullMod("additional_crew_quarters")) {
                    return false;
                }
                break;
            case "fuel_expansion":
                if (variant.hasHullMod("additional_crew_quarters") ||
                        variant.hasHullMod("cargo_expansion")) {
                    return false;
                }
                break;

            case "ilk_AICrew":
                if (variant.hasHullMod("additional_crew_quarters")) {
                    return false;
                }
                break;

            default:
                break;
        }

        return true;
    }

    public static boolean isHullmodOffered(List<String> factionList, String hullmod) {
        boolean unlocked = true;
        if (factionList != null && factionList.contains(Factions.PLAYER)) {
            if (Global.getSector() != null && Global.getSector().getCharacterData() != null) {
                if (Global.getSector().getCharacterData().knowsHullMod(hullmod)) {
                    return true;
                }
                unlocked = false;
            }
        }
        if (factionList != null) {
            for (String faction : factionList) {
                if (faction.contentEquals(Factions.PLAYER)) {
                    continue;
                }
                if (DS_Defs.BLOCK_FACTION_HULLMOD.get(faction) != null &&
                        DS_Defs.BLOCK_FACTION_HULLMOD.get(faction).contains(hullmod)) {
                    return false;
                }
                if (DS_Defs.ADD_FACTION_HULLMOD.get(faction) != null &&
                        DS_Defs.ADD_FACTION_HULLMOD.get(faction).contains(hullmod)) {
                    return true;
                }
                if (Global.getSector() != null && Global.getSector().getFaction(faction) != null &&
                        Global.getSector().getFaction(faction).getHullMods() != null) {
                    return Global.getSector().getFaction(faction).getHullMods().contains(hullmod);
                }
            }
        }
        return unlocked;
    }

    public static boolean isHullmodUnlocked(List<String> factionList, String hullmod) {
        boolean unlocked = true;
        if (factionList != null && factionList.contains(Factions.PLAYER)) {
            if (Global.getSector() != null && Global.getSector().getCharacterData() != null) {
                if (Global.getSector().getCharacterData().knowsHullMod(hullmod)) {
                    return true;
                }
                unlocked = false;
            }
        }
        if (factionList != null) {
            for (String faction : factionList) {
                if (faction.contentEquals(Factions.PLAYER)) {
                    continue;
                }
                if (DS_Defs.BLOCK_FACTION_HULLMOD.get(faction) != null &&
                        DS_Defs.BLOCK_FACTION_HULLMOD.get(faction).contains(hullmod)) {
                    return false;
                }
            }
        }
//        if (factionList != null) {
//            for (String faction : factionList) {
//                if (faction.contentEquals(Factions.PLAYER)) {
//                    continue;
//                }
//                if (DS_Defs.ADD_FACTION_HULLMOD.get(faction) != null &&
//                        DS_Defs.ADD_FACTION_HULLMOD.get(faction).contains(hullmod)) {
//                    return true;
//                }
//                if (Global.getSector() != null && Global.getSector().getFaction(faction) != null &&
//                        Global.getSector().getFaction(faction).getHullMods() != null) {
//                    return Global.getSector().getFaction(faction).getHullMods().contains(hullmod);
//                }
//            }
//        }
        return unlocked;
    }

    public static boolean isValidRoman(String num) {
        for (int k = 0; k < num.length(); k++) {
            if (num.charAt(k) != 'I' && num.charAt(k) != 'V' && num.charAt(k) != 'X' && num.charAt(k) != 'L' &&
                    num.charAt(k) != 'C' && num.charAt(k) != 'D' &&
                    num.charAt(k) != 'M') {
                return false;
            }
        }
        return true;
    }

    public static float lerp(float x, float y, float alpha) {
        return (1f - alpha) * x + alpha * y;
    }

    @SuppressWarnings("unchecked")
    public static List<String> pickExtendedTheme(String faction, MarketAPI market, Random r) {
        WeightedRandomPicker<String> picker = new WeightedRandomPicker<>(r);
        if (!faction.contentEquals(Factions.DERELICT) && !faction.contentEquals(Factions.DIKTAT) &&
                !faction.contentEquals(Factions.LUDDIC_CHURCH) && !faction.contentEquals("syndicate_asp")) {
            picker.add(Factions.HEGEMONY, 2.0F);
        }
        if (!faction.contentEquals(Factions.HEGEMONY) && !faction.contentEquals(Factions.KOL) &&
                !faction.contentEquals(Factions.LIONS_GUARD) && !faction.contentEquals(Factions.PERSEAN)) {
            picker.add(Factions.DIKTAT, 2.0F);
        }
        if (!faction.contentEquals(Factions.LIONS_GUARD) && !faction.contentEquals(Factions.REMNANTS)) {
            if (faction.contentEquals("ORA")) {
                picker.add(Factions.TRITACHYON, 6.0F);
            } else {
                picker.add(Factions.TRITACHYON, 3.0F);
            }
        }
        if (!faction.contentEquals(Factions.LUDDIC_PATH) && !faction.contentEquals(Factions.INDEPENDENT) &&
                !faction.contentEquals(Factions.SCAVENGERS) && !faction.contentEquals("junk_pirates")) {
            picker.add(Factions.PIRATES, 2.0F);
        }
        if (!faction.contentEquals(Factions.DIKTAT) && !faction.contentEquals(Factions.KOL)) {
            picker.add(Factions.PERSEAN, 2.0F);
        }
        if (!faction.contentEquals(Factions.DERELICT) && !faction.contentEquals(Factions.HEGEMONY) &&
                !faction.contentEquals(Factions.KOL) && !faction.contentEquals(Factions.LUDDIC_PATH) &&
                !faction.contentEquals(Factions.PIRATES) && !faction.contentEquals("junk_pirates") &&
                !faction.contentEquals("pack")) {
            picker.add(Factions.LUDDIC_CHURCH, 1.0F);
        }
        if (!faction.contentEquals(Factions.DERELICT) && !faction.contentEquals(Factions.KOL) &&
                !faction.contentEquals(Factions.LUDDIC_CHURCH) && !faction.contentEquals(Factions.PIRATES) &&
                !faction.contentEquals("junk_pirates") && !faction.contentEquals("pack") &&
                !faction.contentEquals("syndicate_asp")) {
            picker.add(Factions.LUDDIC_PATH, 0.5F);
        }
        if (!faction.contentEquals(Factions.LUDDIC_CHURCH) && !faction.contentEquals(Factions.PERSEAN) &&
                !faction.contentEquals(Factions.SCAVENGERS) && !faction.contentEquals("pack") &&
                !faction.contentEquals("syndicate_asp")) {
            picker.add(Factions.INDEPENDENT, 1.0F);
        }
        if (!faction.contentEquals(Factions.INDEPENDENT) && !faction.contentEquals(Factions.PIRATES)) {
            picker.add(Factions.SCAVENGERS, 0.5F);
        }
        if (DSModPlugin.hasUnderworld) {
            if (faction.contentEquals(Factions.TRITACHYON)) {
                picker.add("cabal", 0.5F);
            }
        }
        if (DSModPlugin.imperiumExists) {
            picker.add("interstellarimperium", 3.0F);
        }
        if (DSModPlugin.citadelExists) {
            picker.add("citadeldefenders", 2.0F);
        }
        if (DSModPlugin.blackrockExists) {
            picker.add("blackrock_driveyards", 2.0F);
        }
        if (DSModPlugin.exigencyExists) {
            picker.add("exigency", 1.0F);
            if (faction.contentEquals(Factions.PIRATES)) {
                picker.add("exipirated", 3.0F);
            } else {
                picker.add("exipirated", 1.0F);
            }
        }
        if (DSModPlugin.shadowyardsExists) {
            picker.add("shadow_industry", 4.0F);
        }
        if (DSModPlugin.mayorateExists) {
            if (faction.contentEquals(Factions.PIRATES)) {
                picker.add("mayorate", 5.0F);
            } else {
                picker.add("mayorate", 1.0F);
            }
        }
        if (DSModPlugin.junkPiratesExists) {
            if (faction.contentEquals(Factions.PIRATES)) {
                picker.add("exipirated", 3.0F);
            } else {
                picker.add("exipirated", 1.0F);
            }
        }
        if (DSModPlugin.scyExists) {
            picker.add("SCY", 3.0F);
        }
        if (DSModPlugin.tiandongExists) {
            picker.add("tiandong", 2.0F);
        }
        if (DSModPlugin.diableExists) {
            if (faction.contentEquals(Factions.PIRATES)) {
                picker.add("diableavionics", 5.0F);
            } else {
                picker.add("diableavionics", 1.0F);
            }
        }
        if (DSModPlugin.oraExists) {
            if (faction.contentEquals(Factions.TRITACHYON)) {
                picker.add("ORA", 5.0F);
            } else {
                picker.add("ORA", 1.5F);
            }
        }
        if (DSModPlugin.templarsExists) {
            if (faction.contentEquals("cabal")) {
                picker.add("templars", 2.0F);
            }
        }
        if (!faction.contentEquals(Factions.DERELICT) && !faction.contentEquals(Factions.DIKTAT) &&
                !faction.contentEquals(Factions.HEGEMONY) && !faction.contentEquals(Factions.INDEPENDENT) &&
                !faction.contentEquals(Factions.KOL) && !faction.contentEquals(Factions.LIONS_GUARD) &&
                !faction.contentEquals(Factions.LUDDIC_CHURCH) && !faction.contentEquals(Factions.LUDDIC_PATH) &&
                !faction.contentEquals(Factions.PERSEAN) && !faction.contentEquals(Factions.PIRATES) &&
                !faction.contentEquals(Factions.REMNANTS) && !faction.contentEquals(Factions.SCAVENGERS) &&
                !faction.contentEquals(Factions.TRITACHYON) && !faction.contentEquals("junk_pirates") &&
                !faction.contentEquals("pack") && !faction.contentEquals("syndicate_asp")) {
            picker.add("domain", 3.0F);
        }
        picker.add("sector", 4.0F);
        picker.add("everything", 2.0F);
        picker.remove(faction);

        List<String> factionList = new ArrayList<>(3);
        Float themeChance = DS_Defs.FACTION_THEMING_CHANCE.get(faction);
        if (themeChance != null) {
            if (market != null) {
                List<String> toAdd = new ArrayList<>(4);
                List<String> influences = (List<String>) market.getMemoryWithoutUpdate().get(
                             DS_Defs.MEMORY_KEY_MARKET_INFLUENCES);
                if (influences != null) {
                    toAdd.addAll(influences);
                }
                if (market.getMemoryWithoutUpdate().contains("$startingFactionId")) {
                    String startingFaction = market.getMemoryWithoutUpdate().getString("$startingFactionId");
                    if (!startingFaction.contentEquals(faction)) {
                        toAdd.add(startingFaction);
                    }
                }
                for (String addedFac : toAdd) {
                    if (factionList.size() >= 3) {
                        break;
                    }
                    if (addedFac.contentEquals(faction) || !DS_Defs.INJECTOR_AFFECTED_FACTIONS.contains(addedFac)) {
                        continue;
                    }

                    float adjustedChance = 1f - (float) Math.pow(1.0 - ((themeChance + 0.1) / 1.1), 3.0);
                    if (r.nextFloat() < adjustedChance) {
                        factionList.add(addedFac);
                        picker.remove(addedFac);
                    }
                }
            }

            while (!picker.isEmpty() && factionList.size() < 3) {
                if (r.nextFloat() < themeChance) {
                    factionList.add(picker.pickAndRemove());
                } else {
                    break;
                }
            }
        }

        return factionList;
    }

    public static void removeMarketInfluence(MarketAPI market, String... factions) {
        if (market == null) {
            return;
        }

        @SuppressWarnings("unchecked")
        List<String> influences = (List<String>) market.getMemoryWithoutUpdate().get(
                     DS_Defs.MEMORY_KEY_MARKET_INFLUENCES);

        for (String faction : factions) {
            if (influences != null) {
                influences.remove(faction);
            }
        }
    }

    /* Pyrolistical on StackExchange */
    public static double roundToSignificantFigures(double num, int n) {
        if (num == 0) {
            return 0;
        }

        final double d = Math.ceil(Math.log10(num < 0 ? -num : num));
        final int power = n - (int) d;

        final double magnitude = Math.pow(10, power);
        final long shifted = Math.round(num * magnitude);
        return shifted / magnitude;
    }

    public static long roundToSignificantFiguresLong(double num, int n) {
        return Math.round(roundToSignificantFigures(num, n));
    }

    public static void setExtendedThemeName(CampaignFleetAPI fleet, List<String> extendedTheme) {
        if (!DSModPlugin.Module_ProceduralVariants) {
            return;
        }
        if (extendedTheme == null || extendedTheme.isEmpty()) {
            return;
        }

        String name = "";
        int idx = 0;
        for (String faction : extendedTheme) {
            String fullName;
            String shortName;
            switch (faction) {
                case Factions.HEGEMONY:
                    fullName = "Hegemony";
                    shortName = "Heg";
                    break;
                case Factions.DIKTAT:
                    fullName = "Diktat";
                    shortName = "SD";
                    break;
                case Factions.LIONS_GUARD:
                    fullName = "Lion";
                    shortName = "LG";
                    break;
                case Factions.INDEPENDENT:
                    fullName = "Independent";
                    shortName = "Ind";
                    break;
                case Factions.LUDDIC_CHURCH:
                    fullName = "Luddic";
                    shortName = "LC";
                    break;
                case Factions.LUDDIC_PATH:
                    fullName = "Pather";
                    shortName = "LP";
                    break;
                case Factions.KOL:
                    fullName = "KOL";
                    shortName = "KOL";
                    break;
                case Factions.TRITACHYON:
                    fullName = "Tri-Tachyon";
                    shortName = "TT";
                    break;
                case Factions.PERSEAN:
                    fullName = "League";
                    shortName = "PL";
                    break;
                case Factions.PIRATES:
                    fullName = "Pirate";
                    shortName = "Pir";
                    break;
                case Factions.SCAVENGERS:
                    fullName = "Scavenged";
                    shortName = "Scav";
                    break;
                case Factions.DERELICT:
                    fullName = "Automated";
                    shortName = "Auto";
                    break;
                case Factions.REMNANTS:
                    fullName = "Remnant";
                    shortName = "Rem";
                    break;
                case "cabal":
                    fullName = "Starlight";
                    shortName = "STAR";
                    break;
                case "interstellarimperium":
                    fullName = "Imperial";
                    shortName = "II";
                    break;
                case "citadeldefenders":
                    fullName = "Citadel";
                    shortName = "Cit";
                    break;
                case "shadow_industry":
                    fullName = "Yardie";
                    shortName = "SRA";
                    break;
                case "blackrock_driveyards":
                    fullName = "Blackrock";
                    shortName = "BRDY";
                    break;
                case "exigency":
                    fullName = "Exigency";
                    shortName = "Exi";
                    break;
                case "exipirated":
                    fullName = "Ahriman";
                    shortName = "AA";
                    break;
                case "templars":
                    fullName = "Templar";
                    shortName = "KT";
                    break;
                case "mayorate":
                    fullName = "Mayorate";
                    shortName = "Mayo";
                    break;
                case "tiandong":
                    fullName = "Tiandong";
                    shortName = "THI";
                    break;
                case "junk_pirates":
                    fullName = "Junk";
                    shortName = "JP";
                    break;
                case "pack":
                    fullName = "PACK";
                    shortName = "PACK";
                    break;
                case "syndicate_asp":
                    fullName = "Syndicate";
                    shortName = "ASP";
                    break;
                case "SCY":
                    fullName = "Scyan";
                    shortName = "SCY";
                    break;
                case "diableavionics":
                    fullName = "Diable";
                    shortName = "DA";
                    break;
                case "ORA":
                    fullName = "Alliance";
                    shortName = "ORA";
                    break;
                case "domain":
                    fullName = "Standard";
                    shortName = "Std";
                    break;
                case "sector":
                    fullName = "Mixed";
                    shortName = "Mix";
                    break;
                case "everything":
                    fullName = "Exotic";
                    shortName = "Exo";
                    break;
                default:
                    fullName = Global.getSector().getFaction(faction).getDisplayName();
                    shortName = fullName.substring(0, Math.min(3, fullName.length()));
                    break;
            }

            if (extendedTheme.size() == 1) {
                name = fullName;
            } else if (idx == 0) {
                name = shortName;
            } else {
                name += "/" + shortName;
            }
            idx++;
        }

        if (fleet.getName().endsWith("(Long Range)")) {
            fleet.setName(fleet.getName().replace("(Long Range)", "(LR)"));
        }
        if (fleet.getName().endsWith("(Assault)")) {
            fleet.setName(fleet.getName().replace("(Assault)", "(A)"));
        }
        if (fleet.getName().endsWith("(Elite)")) {
            fleet.setName(fleet.getName().replace("(Elite)", "(E)"));
        }
        if (fleet.getName().endsWith("(Fast)")) {
            fleet.setName(fleet.getName().replace("(Fast)", "(F)"));
        }
        if (fleet.getName().endsWith("(Strike)")) {
            fleet.setName(fleet.getName().replace("(Strike)", "(S)"));
        }
        if (fleet.getName().endsWith("(Ultra)")) {
            fleet.setName(fleet.getName().replace("(Ultra)", "(U)"));
        }

        fleet.setName(fleet.getName() + " (" + name + ")");
    }

    public static void setMarketInfluence(MarketAPI market, String... factions) {
        if (market == null) {
            return;
        }

        @SuppressWarnings("unchecked")
        List<String> influences = (List<String>) market.getMemoryWithoutUpdate().get(
                     DS_Defs.MEMORY_KEY_MARKET_INFLUENCES);

        if (influences == null) {
            influences = new ArrayList<>(3);
            market.getMemoryWithoutUpdate().set(DS_Defs.MEMORY_KEY_MARKET_INFLUENCES, influences);
        }

        for (String faction : factions) {
            if (!influences.contains(faction)) {
                influences.add(faction);
            }
        }
    }

    public static void setThemeName(CampaignFleetAPI fleet, Archetype archetype) {
        if (!DSModPlugin.Module_ProceduralVariants) {
            return;
        }

        if (archetype == null) {
            return;
        }
        String name;
        switch (archetype) {
            case ARTILLERY:
                name = "Long Range";
                break;
            case ASSAULT:
                name = "Assault";
                break;
            case ELITE:
                name = "Elite";
                break;
            case SKIRMISH:
                name = "Fast";
                break;
            case STRIKE:
                name = "Strike";
                break;
            case ULTIMATE:
                name = "Ultra";
                break;
            default:
                return;
        }
        fleet.setName(fleet.getName() + " (" + name + ")");
    }

    public static void stripVariant(ShipVariantAPI variant) {
        for (String id : variant.getFittedWeaponSlots()) {
            WeaponSlotAPI slot = variant.getSlot(id);
            if (slot.isDecorative() || slot.isBuiltIn() || slot.isHidden() || slot.isSystemSlot() ||
                    slot.isStationModule()) {
                continue;
            }
            variant.clearSlot(slot.getId());
        }
        int numBays = 20; // well above whatever it might actually be
        for (int i = 0; i < numBays; i++) {
            if (variant.getWingId(i) != null) {
                variant.setWingId(i, "");
            }
        }
        variant.setNumFluxCapacitors(0);
        variant.setNumFluxVents(0);
        variant.clearHullMods();
    }

    public static enum RequiredFaction {

        NONE, JUNK_PIRATES, TIANDONG, SHADOWYARDS, IMPERIUM, TEMPLARS, DIABLE, BLACKROCK, CITADEL, EXIGENCY, MAYORATE,
        SCY, CABAL, ORA;

        public boolean isLoaded() {
            switch (this) {
                case JUNK_PIRATES:
                    return DSModPlugin.junkPiratesExists;
                case TIANDONG:
                    return DSModPlugin.tiandongExists;
                case SHADOWYARDS:
                    return DSModPlugin.shadowyardsExists;
                case IMPERIUM:
                    return DSModPlugin.imperiumExists;
                case TEMPLARS:
                    return DSModPlugin.templarsExists;
                case DIABLE:
                    return DSModPlugin.diableExists;
                case BLACKROCK:
                    return DSModPlugin.blackrockExists;
                case CITADEL:
                    return DSModPlugin.citadelExists;
                case EXIGENCY:
                    return DSModPlugin.exigencyExists;
                case MAYORATE:
                    return DSModPlugin.mayorateExists;
                case SCY:
                    return DSModPlugin.scyExists;
                case CABAL:
                    return DSModPlugin.hasUnderworld;
                case ORA:
                    return DSModPlugin.oraExists;
                case NONE:
                default:
                    return true;
            }
        }
    }

}
