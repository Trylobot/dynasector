package data.scripts.variants;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.SettingsAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;
import com.fs.starfarer.api.loading.RoleEntryAPI;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Note: the player_npc faction MUST be last in load order!
public class DS_Database {

    public static final Map<String, Float> factionFighterAverage = new LinkedHashMap<>(30);
    public static final Map<String, Map<String, Float>> factionFighters = new LinkedHashMap<>(30);
    public static final Map<String, Map<SlotType, Float>> factionWeaponAverages = new LinkedHashMap<>(30);
    public static final Map<String, Map<String, Float>> factionWeapons = new LinkedHashMap<>(30);

    public static final Map<String, Map<String, Float>> hullRoles = new HashMap<>(500);
    public static final Map<String, Boolean> hullSortSlots = new HashMap<>(500);

    public static final Map<String, DS_FighterEntry> masterFighterList = new LinkedHashMap<>(300);
    public static final Map<String, DS_WeaponEntry> masterWeaponList = new LinkedHashMap<>(300);

    // Ranged 0 to ~1, biased around 0.5
    public static final Map<String, EnumMap<RandomizerFighterType, Float>> shipFighterCoefficients =
                                                                           new HashMap<>(500);
    // Ranged 0 to ~1, biased around 0.5
    public static final Map<String, Map<String, EnumMap<RandomizerWeaponType, Float>>> shipSlotCoefficients =
                                                                                       new HashMap<>(500);

    public static final Map<String, Float> variantQuality = new HashMap<>(500); // Actually mostly ship hull IDs

    private static final String NEXERELIN_SETTINGS = "nexerelinOptions.json";
    private static final String PLAYER_FACTION = "player_npc";

    private static final Set<String> invalidFighterHeaders = new HashSet<>(19);
    private static final Set<String> invalidWeaponHeaders = new HashSet<>(22);
    private static final Logger log = Global.getLogger(DS_Database.class);
    private static final Map<String, Float> playerWeightMap = new HashMap<>(30);
    private static final List<String> roleHeaders = new ArrayList<>(29);

    static final Set<String> FIGHTER_IGNORE = new HashSet<>(1);
    static final Set<String> HULL_IGNORE = new HashSet<>(65);
    static final Set<String> NEVER_SPAWN = new HashSet<>(3);
    static final Set<String> STATION_MODULES = new HashSet<>(16);
    static final Set<String> WEAPON_IGNORE = new HashSet<>(12);

    static {
        log.setLevel(Level.ALL);
    }

    static {
        STATION_MODULES.add("derelict_mothership_bow_module_Hull");
        STATION_MODULES.add("derelict_mothership_pod_engine_left_Hull");
        STATION_MODULES.add("derelict_mothership_pod_m_left_Hull");
        STATION_MODULES.add("derelict_mothership_pod_m_right_Hull");
        STATION_MODULES.add("derelict_mothership_pod_s_right_Hull");
        STATION_MODULES.add("derelict_mothership_sensor_mast_Hull");
        STATION_MODULES.add("remnant_armour1_Hull");
        STATION_MODULES.add("remnant_armour2_Hull");
        STATION_MODULES.add("remnant_armour3_Hull");
        STATION_MODULES.add("remnant_hangar1_Hull");
        STATION_MODULES.add("remnant_shield1_Hull");
        STATION_MODULES.add("remnant_shield2_Hull");
        STATION_MODULES.add("remnant_weapon_platform1_Hull");
        STATION_MODULES.add("remnant_weapon_platform2_Hull");
        STATION_MODULES.add("remnant_weapon_platform3_Hull");
        STATION_MODULES.add("platform1_Hull");
        STATION_MODULES.add("swp_wall_left_Hull");
        STATION_MODULES.add("swp_wall_right_Hull");
        STATION_MODULES.add("swp_boss_sporeship_bl_Hull");
        STATION_MODULES.add("swp_boss_sporeship_br_Hull");
        STATION_MODULES.add("swp_boss_sporeship_ll_Hull");
        STATION_MODULES.add("swp_boss_sporeship_lr_Hull");
        STATION_MODULES.add("swp_boss_sporeship_ml_Hull");
        STATION_MODULES.add("swp_boss_sporeship_mr_Hull");
        STATION_MODULES.add("swp_boss_sporeship_ul_Hull");
        STATION_MODULES.add("swp_boss_sporeship_ur_Hull");
        STATION_MODULES.add("swp_boss_sporeship_top_Hull");
        STATION_MODULES.add("SCY_corA_HullL");
        STATION_MODULES.add("SCY_corAF_Hull");
        STATION_MODULES.add("SCY_corAFL_Hull");
        STATION_MODULES.add("SCY_corAFR_Hull");
        STATION_MODULES.add("SCY_corAL_Hull");
        STATION_MODULES.add("SCY_corAR_Hull");
        STATION_MODULES.add("SCY_ketoAL_Hull");
        STATION_MODULES.add("SCY_ketoAR_Hull");
        STATION_MODULES.add("SCY_khalkPA_Hull");
        STATION_MODULES.add("SCY_khalkPB_Hull");
        STATION_MODULES.add("SCY_khalkPC_Hull");
        STATION_MODULES.add("SCY_khalkPD_Hull");
        STATION_MODULES.add("SCY_khalkPE_Hull");
        STATION_MODULES.add("SCY_khalkPF_Hull");
        STATION_MODULES.add("SCY_lamiaAF_Hull");
        STATION_MODULES.add("SCY_lamiaAL_Hull");
        STATION_MODULES.add("SCY_lamiaAR_Hull");
        STATION_MODULES.add("SCY_lionFL_Hull");
        STATION_MODULES.add("SCY_lionFR_Hull");
        STATION_MODULES.add("SCY_lionRL_Hull");
        STATION_MODULES.add("SCY_lionRR_Hull");

        FIGHTER_IGNORE.add("ssp_boss_wasp_wing");

        NEVER_SPAWN.add("station_derelict_survey_mothership_Hull");
        NEVER_SPAWN.add("remnant_station2_Hull");
        NEVER_SPAWN.add("station_small_Hull");

        HULL_IGNORE.add("remnant_station1_Hull");
        HULL_IGNORE.add("ssp_cristarium_Hull");
        HULL_IGNORE.add("ssp_archangel_Hull");
        HULL_IGNORE.add("ssp_ultron_Hull");
        HULL_IGNORE.add("ssp_hyperzero_Hull");
        HULL_IGNORE.add("ssp_superhyperion_Hull");
        HULL_IGNORE.add("ssp_oberon_Hull");
        HULL_IGNORE.add("ssp_zeus_Hull");
        HULL_IGNORE.add("ssp_ezekiel_Hull");
        HULL_IGNORE.add("ssp_zero_Hull");
        HULL_IGNORE.add("ssp_superzero_Hull");
        HULL_IGNORE.add("swp_banana_Hull");
        HULL_IGNORE.add("ii_boss_olympus_Hull");
        HULL_IGNORE.add("ii_boss_dominus_Hull");
        HULL_IGNORE.add("ii_boss_praetorian_Hull");
        HULL_IGNORE.add("ms_boss_charybdis_Hull");
        HULL_IGNORE.add("ms_boss_mimir_Hull");
        HULL_IGNORE.add("msp_boss_potniaBis_Hull");
        HULL_IGNORE.add("ssp_boss_doom_Hull");
        HULL_IGNORE.add("ssp_boss_onslaught_Hull");
        HULL_IGNORE.add("ssp_boss_mule_Hull");
        HULL_IGNORE.add("ssp_boss_hammerhead_Hull");
        HULL_IGNORE.add("ssp_boss_aurora_Hull");
        HULL_IGNORE.add("ssp_boss_medusa_Hull");
        HULL_IGNORE.add("ssp_boss_sunder_Hull");
        HULL_IGNORE.add("ssp_boss_lasher_r_Hull");
        HULL_IGNORE.add("ssp_boss_falcon_Hull");
        HULL_IGNORE.add("ssp_boss_shade_Hull");
        HULL_IGNORE.add("ssp_boss_tarsus_Hull");
        HULL_IGNORE.add("ssp_boss_euryale_Hull");
        HULL_IGNORE.add("ssp_boss_brawler_Hull");
        HULL_IGNORE.add("ssp_boss_eagle_Hull");
        HULL_IGNORE.add("ssp_boss_cerberus_Hull");
        HULL_IGNORE.add("ssp_boss_dominator_Hull");
        HULL_IGNORE.add("ssp_boss_afflictor_Hull");
        HULL_IGNORE.add("ssp_boss_odyssey_Hull");
        HULL_IGNORE.add("ssp_boss_phaeton_Hull");
        HULL_IGNORE.add("ssp_boss_hyperion_Hull");
        HULL_IGNORE.add("ssp_boss_atlas_Hull");
        HULL_IGNORE.add("ssp_boss_lasher_b_Hull");
        HULL_IGNORE.add("ssp_boss_paragon_Hull");
        HULL_IGNORE.add("ssp_boss_beholder_Hull");
        HULL_IGNORE.add("ssp_boss_dominator_luddic_path_Hull");
        HULL_IGNORE.add("ssp_boss_onslaught_luddic_path_Hull");
        HULL_IGNORE.add("ssp_boss_astral_Hull");
        HULL_IGNORE.add("ssp_boss_conquest_Hull");
        HULL_IGNORE.add("swp_boss_frankenstein_Hull");
        HULL_IGNORE.add("swp_boss_sporeship_Hull");
        HULL_IGNORE.add("tem_boss_archbishop_Hull");
        HULL_IGNORE.add("tem_boss_paladin_Hull");
        HULL_IGNORE.add("tiandong_boss_wuzhang_Hull");
        HULL_IGNORE.add("pack_bulldog_bullseye_Hull");
        HULL_IGNORE.add("pack_pitbull_bullseye_Hull");
        HULL_IGNORE.add("pack_komondor_bullseye_Hull");
        HULL_IGNORE.add("pack_schnauzer_bullseye_Hull");
        HULL_IGNORE.add("diableavionics_IBBgulf_Hull");
        HULL_IGNORE.add("fox_meatship_Hull");
        HULL_IGNORE.add("fox_meatship_internal_Hull");
        HULL_IGNORE.add("fox_meatship2_Hull");
        HULL_IGNORE.add("fox_meatship3_Hull");
        HULL_IGNORE.add("fox_troll_Hull");
        HULL_IGNORE.add("ii_titan_Hull");
        HULL_IGNORE.add("ii_mirv_Hull");
        HULL_IGNORE.add("junk_pirates_onslaught_overdrive_Hull");
        HULL_IGNORE.add("TAR_meatshipHT_Hull");
        HULL_IGNORE.add("TAR_meatshipLT_Hull");
        HULL_IGNORE.add("TAR_meatshipMT_Hull");
        HULL_IGNORE.add("TAR_meatshipMT_H_Hull");

        WEAPON_IGNORE.add("megapulse");
        WEAPON_IGNORE.add("omegaorb2");
        WEAPON_IGNORE.add("wraithii");
        WEAPON_IGNORE.add("zeroannihilator");
        WEAPON_IGNORE.add("zeroannihilator2");
        WEAPON_IGNORE.add("zeroannihilator3");
        WEAPON_IGNORE.add("zerobeam");
        WEAPON_IGNORE.add("zerocannon");
        WEAPON_IGNORE.add("empbomb");
        WEAPON_IGNORE.add("godmode");
        WEAPON_IGNORE.add("tiandong_boss_liberty_flare");
        WEAPON_IGNORE.add("fox_idiot_main");
    }

    static {
        invalidFighterHeaders.add("name");
        invalidFighterHeaders.add("id");
        invalidFighterHeaders.add("builtin");
        invalidFighterHeaders.add("tier");
        invalidFighterHeaders.add("OPs");
        invalidFighterHeaders.add("kinetic");
        invalidFighterHeaders.add("highExplosive");
        invalidFighterHeaders.add("energy");
        invalidFighterHeaders.add("fragmentation");
        invalidFighterHeaders.add("attack");
        invalidFighterHeaders.add("interfere");
        invalidFighterHeaders.add("alpha");
        invalidFighterHeaders.add("defense");
        invalidFighterHeaders.add("support");
        invalidFighterHeaders.add("closeRange");
        invalidFighterHeaders.add("longRange");
        invalidFighterHeaders.add("disable");
        invalidFighterHeaders.add("techType");
        invalidFighterHeaders.add("leetness");
        invalidWeaponHeaders.add("name");
        invalidWeaponHeaders.add("id");
        invalidWeaponHeaders.add("builtin");
        invalidWeaponHeaders.add("size");
        invalidWeaponHeaders.add("type");
        invalidWeaponHeaders.add("tier");
        invalidWeaponHeaders.add("OPs");
        invalidWeaponHeaders.add("limitedAmmo");
        invalidWeaponHeaders.add("lowFlux");
        invalidWeaponHeaders.add("separateFire");
        invalidWeaponHeaders.add("frontOnly");
        invalidWeaponHeaders.add("turretOnly");
        invalidWeaponHeaders.add("damageType");
        invalidWeaponHeaders.add("attack");
        invalidWeaponHeaders.add("standoff");
        invalidWeaponHeaders.add("alpha");
        invalidWeaponHeaders.add("defense");
        invalidWeaponHeaders.add("closeRange");
        invalidWeaponHeaders.add("longRange");
        invalidWeaponHeaders.add("disable");
        invalidWeaponHeaders.add("techType");
        invalidWeaponHeaders.add("leetness");
        roleHeaders.add("interceptor");
        roleHeaders.add("fighter");
        roleHeaders.add("bomber");
        roleHeaders.add("fastAttack");
        roleHeaders.add("escortSmall");
        roleHeaders.add("escortMedium");
        roleHeaders.add("combatSmall");
        roleHeaders.add("combatMedium");
        roleHeaders.add("combatLarge");
        roleHeaders.add("combatCapital");
        roleHeaders.add("combatFreighterSmall");
        roleHeaders.add("combatFreighterMedium");
        roleHeaders.add("combatFreighterLarge");
        roleHeaders.add("civilianRandom");
        roleHeaders.add("carrierSmall");
        roleHeaders.add("carrierMedium");
        roleHeaders.add("carrierLarge");
        roleHeaders.add("freighterSmall");
        roleHeaders.add("freighterMedium");
        roleHeaders.add("freighterLarge");
        roleHeaders.add("tankerSmall");
        roleHeaders.add("tankerMedium");
        roleHeaders.add("tankerLarge");
        roleHeaders.add("personnelSmall");
        roleHeaders.add("personnelMedium");
        roleHeaders.add("personnelLarge");
        roleHeaders.add("linerSmall");
        roleHeaders.add("linerMedium");
        roleHeaders.add("linerLarge");
        roleHeaders.add("tug");
        roleHeaders.add("crig");
        roleHeaders.add("utility");
        roleHeaders.add("miningSmall");
        roleHeaders.add("miningMedium");
        roleHeaders.add("miningLarge");
    }

    public static void checkForUnusedFighters(SettingsAPI settings) {
        Set<String> warned = new HashSet<>(100);
        for (String wingId : Global.getSector().getAllFighterWingIds()) {
            if (FIGHTER_IGNORE.contains(wingId)) {
                continue;
            }

            FighterWingSpecAPI fighterSpec = settings.getFighterWingSpec(wingId);
            boolean system = (fighterSpec.hasTag(Tags.WING_NO_DROP) && fighterSpec.hasTag(Tags.WING_NO_SELL));

            DS_FighterEntry fighter = masterFighterList.get(wingId);
            if (fighter == null) {
                log.warn("No fighter definition for " + wingId);
            }
            if (fighter == null) {
                warned.add(wingId);
                if (system) {
                    log.warn("No fighter definition for (system): " + wingId + " (" +
                            fighterSpec.getVariant().getDesignation() + " " +
                            fighterSpec.getVariant().getDisplayName() + " Wing)");
                } else {
                    log.warn("No fighter definition for: " + wingId + " (" +
                            fighterSpec.getVariant().getDesignation() + " " +
                            fighterSpec.getVariant().getDisplayName() + " Wing)");
                }
            } else if (fighter.builtIn && !system) {
                warned.add(wingId);
                log.warn("Fighter wing marked as system but is modular: " + wingId + " (" +
                        fighterSpec.getVariant().getDesignation() + " " +
                        fighterSpec.getVariant().getDisplayName() + " Wing)");
            }
        }
        for (String hullId : Global.getSector().getAllEmptyVariantIds()) {
            ShipVariantAPI variant = Global.getSettings().getVariant(hullId);
            for (int i = 0; i < 20; i++) {
                FighterWingSpecAPI fighterSpec = variant.getWing(i);
                if (fighterSpec == null) {
                    continue;
                }

                String wingId = fighterSpec.getId();
                if (warned.contains(wingId)) {
                    continue;
                }

                if (FIGHTER_IGNORE.contains(wingId)) {
                    continue;
                }

                boolean system = (fighterSpec.hasTag(Tags.WING_NO_DROP) && fighterSpec.hasTag(Tags.WING_NO_SELL));

                DS_FighterEntry fighter = masterFighterList.get(wingId);
                if (fighter == null) {
                    warned.add(wingId);
                    if (system) {
                        log.warn("No fighter definition for (system, built-in): " + wingId + " (" +
                                fighterSpec.getVariant().getDesignation() + " " +
                                fighterSpec.getVariant().getDisplayName() + " Wing)");
                    } else {
                        log.warn("No fighter definition for (built-in): " + wingId + " (" +
                                fighterSpec.getVariant().getDesignation() + " " +
                                fighterSpec.getVariant().getDisplayName() + " Wing)");
                    }
                } else if (!fighter.builtIn && system) {
                    warned.add(wingId);
                    log.warn("Fighter wing marked as system but is modular: " + wingId + " (" +
                            fighterSpec.getVariant().getDesignation() + " " +
                            fighterSpec.getVariant().getDisplayName() + " Wing)");
                }
            }
        }
    }

    public static void checkForUnusedHulls(SettingsAPI settings) {
        for (String hullId : Global.getSector().getAllEmptyVariantIds()) {
            if (HULL_IGNORE.contains(hullId) || STATION_MODULES.contains(hullId)) {
                continue;
            }

            Float quality = variantQuality.get(hullId);
            if (quality == null) {
                ShipVariantAPI variant = Global.getSettings().getVariant(hullId);
                log.warn("No hull definition for " + variant.getHullSpec().getHullId());
            }
        }
    }

    public static void checkForUnusedWeapons(SettingsAPI settings) {
        Set<String> warned = new HashSet<>(100);
        for (String weaponId : Global.getSector().getAllWeaponIds()) {
            if (WEAPON_IGNORE.contains(weaponId)) {
                continue;
            }

            WeaponSpecAPI weaponSpec = settings.getWeaponSpec(weaponId);
            if (weaponSpec.getType() == WeaponType.DECORATIVE) {
                continue;
            }

            boolean system = (weaponSpec.getAIHints().contains(AIHints.SYSTEM) || weaponSpec.getType() ==
                              WeaponType.SYSTEM);

            DS_WeaponEntry weapon = masterWeaponList.get(weaponId);
            if (weapon == null) {
                warned.add(weaponId);
                if (system) {
                    log.warn("No weapon definition for " + weaponSpec.getSize().getDisplayName() + " " +
                            weaponSpec.getType().getDisplayName() + " (system): " +
                            weaponId + " (" + weaponSpec.getWeaponName() + ")");
                } else {
                    log.warn("No weapon definition for " + weaponSpec.getSize().getDisplayName() + " " +
                            weaponSpec.getType().getDisplayName() + ": " +
                            weaponId + " (" + weaponSpec.getWeaponName() + ")");
                }
            } else if (weapon.builtIn && !system) {
                warned.add(weaponId);
                log.warn(weaponSpec.getSize().getDisplayName() + " " + weaponSpec.getType().getDisplayName() +
                        " weapon marked as system but is modular: " +
                        weaponId + " (" + weaponSpec.getWeaponName() + ")");
            }
        }
        for (String hullId : Global.getSector().getAllEmptyVariantIds()) {
            ShipVariantAPI variant = Global.getSettings().getVariant(hullId);
            for (String slot : variant.getFittedWeaponSlots()) {
                WeaponSpecAPI weaponSpec = variant.getWeaponSpec(slot);
                String weaponId = weaponSpec.getWeaponId();
                if (warned.contains(weaponId)) {
                    continue;
                }

                if (WEAPON_IGNORE.contains(weaponId)) {
                    continue;
                }

                boolean system = (weaponSpec.getAIHints().contains(AIHints.SYSTEM) || weaponSpec.getType() ==
                                  WeaponType.SYSTEM);

                DS_WeaponEntry weapon = masterWeaponList.get(weaponId);
                if (weapon == null) {
                    warned.add(weaponId);
                    if (system) {
                        log.warn("No weapon definition for " + weaponSpec.getSize().getDisplayName() + " " +
                                weaponSpec.getType().getDisplayName() +
                                " (system, built-in): " + weaponId + " (" + weaponSpec.getWeaponName() + ")");
                    } else {
                        log.warn("No weapon definition for " + weaponSpec.getSize().getDisplayName() + " " +
                                weaponSpec.getType().getDisplayName() +
                                " (built-in): " + weaponId + " (" + weaponSpec.getWeaponName() + ")");
                    }
                } else if (!weapon.builtIn && system) {
                    warned.add(weaponId);
                    log.warn(
                            weaponSpec.getSize().getDisplayName() + " " + weaponSpec.getType().getDisplayName() +
                            " weapon is system but marked as modular: " +
                            weaponId + " (" + weaponSpec.getWeaponName() + ")");
                }
            }
        }
    }

    public static void generateFighterCoefficients(SettingsAPI settings) {
        /* Map variants to hull */
        Map<String, List<ShipVariantAPI>> junctionTable = new HashMap<>(500);
        for (String variantId : settings.getAllVariantIds()) {
            ShipVariantAPI variant = settings.getVariant(variantId);

            // It's important that fighters are skipped!
            if (variant == null || variant.isEmptyHullVariant() || variant.isFighter() || !variant.isStockVariant()) {
                continue;
            }
            String hullId = variant.getHullSpec().getHullId();
            List<ShipVariantAPI> shipVariants = junctionTable.get(hullId);
            if (shipVariants == null) {
                shipVariants = new ArrayList<>(10);
                junctionTable.put(hullId, shipVariants);
            }
            shipVariants.add(variant);
        }

        /* Get a coefficient for each fighter type, to avoid over-/under-representation of certain types */
        EnumMap<RandomizerFighterType, Float> masterCoefficients = new EnumMap<>(RandomizerFighterType.class);
        for (RandomizerFighterType type : RandomizerFighterType.values()) {
            masterCoefficients.put(type, 0f);
        }
        int totalFighters = 0;
        for (DS_FighterEntry fighterEntry : masterFighterList.values()) {
            if (fighterEntry.builtIn) {
                continue;
            }

            if (fighterEntry.alpha) {
                float curr = masterCoefficients.get(RandomizerFighterType.ALPHA);
                masterCoefficients.put(RandomizerFighterType.ALPHA, curr + 1f);
            }
            if (fighterEntry.attack) {
                float curr = masterCoefficients.get(RandomizerFighterType.ATTACK);
                masterCoefficients.put(RandomizerFighterType.ATTACK, curr + 1f);
            }
            if (fighterEntry.defense) {
                float curr = masterCoefficients.get(RandomizerFighterType.DEFENSE);
                masterCoefficients.put(RandomizerFighterType.DEFENSE, curr + 1f);
            }
            if (fighterEntry.disable) {
                float curr = masterCoefficients.get(RandomizerFighterType.DISABLE);
                masterCoefficients.put(RandomizerFighterType.DISABLE, curr + 1f);
            }
            if (fighterEntry.interfere) {
                float curr = masterCoefficients.get(RandomizerFighterType.INTERFERE);
                masterCoefficients.put(RandomizerFighterType.INTERFERE, curr + 1f);
            }
            if (fighterEntry.support) {
                float curr = masterCoefficients.get(RandomizerFighterType.SUPPORT);
                masterCoefficients.put(RandomizerFighterType.SUPPORT, curr + 1f);
            }
            if (fighterEntry.closeRange) {
                float curr = masterCoefficients.get(RandomizerFighterType.CLOSE_RANGE);
                masterCoefficients.put(RandomizerFighterType.CLOSE_RANGE, curr + 1f);
            }
            if (fighterEntry.longRange) {
                float curr = masterCoefficients.get(RandomizerFighterType.LONG_RANGE);
                masterCoefficients.put(RandomizerFighterType.LONG_RANGE, curr + 1f);
            }
            totalFighters++;
        }
        float totalFighterCoeff = 0f;
        for (RandomizerFighterType type : RandomizerFighterType.values()) {
            float curr = (float) Math.sqrt(Math.max(1f, masterCoefficients.get(type)));
            float coeff = totalFighters / curr;
            totalFighterCoeff += coeff;
        }
        totalFighterCoeff /= RandomizerFighterType.values().length;
        for (RandomizerFighterType type : RandomizerFighterType.values()) {
            float curr = (float) Math.sqrt(Math.max(1f, masterCoefficients.get(type)));
            float coeff = (totalFighters / curr) / totalFighterCoeff;
            masterCoefficients.put(type, coeff);
            log.info("Master coefficient for fighter role " + type.toString() + ": " + coeff);
        }

        Set<String> allHulls = new HashSet<>(variantQuality.size() + STATION_MODULES.size());
        allHulls.addAll(variantQuality.keySet());
        allHulls.addAll(STATION_MODULES);

        int i = 0;
        for (String hullVariantId : allHulls) {
            i++;
            if (i % 50 == 0) {
                DSModPlugin.refresh();
            }

            String hullId = hullVariantId;
            if (hullId.endsWith("_Hull")) {
                hullId = hullId.replace("_Hull", "");
            }
            List<ShipVariantAPI> variants = junctionTable.get(hullId);
            if (variants == null || variants.size() <= 0) {
                // No variants to scan, no data to find
                continue;
            }

            EnumMap<RandomizerFighterType, Float> fighterCoefficients = new EnumMap<>(RandomizerFighterType.class);
            for (RandomizerFighterType type : RandomizerFighterType.values()) {
                fighterCoefficients.put(type, masterCoefficients.get(type));
            }

            for (ShipVariantAPI variant : variants) {
                for (int bay = 0; bay < 20; bay++) {
                    FighterWingSpecAPI wing = variant.getWing(bay);
                    if (wing == null) {
                        // Don't add anything
                        continue;
                    }

                    DS_FighterEntry fighterEntry = masterFighterList.get(wing.getId());
                    if (fighterEntry == null) {
                        // No info on this fighter; assume average
                        for (RandomizerFighterType type : RandomizerFighterType.values()) {
                            float curr = fighterCoefficients.get(type);
                            fighterCoefficients.put(type, curr + 0.5f * masterCoefficients.get(type));
                        }
                        continue;
                    }

                    if (fighterEntry.alpha) {
                        float curr = fighterCoefficients.get(RandomizerFighterType.ALPHA);
                        fighterCoefficients.put(RandomizerFighterType.ALPHA, curr + masterCoefficients.get(
                                                RandomizerFighterType.ALPHA));
                    }
                    if (fighterEntry.attack) {
                        float curr = fighterCoefficients.get(RandomizerFighterType.ATTACK);
                        fighterCoefficients.put(RandomizerFighterType.ATTACK, curr + masterCoefficients.get(
                                                RandomizerFighterType.ATTACK));
                    }
                    if (fighterEntry.defense) {
                        float curr = fighterCoefficients.get(RandomizerFighterType.DEFENSE);
                        fighterCoefficients.put(RandomizerFighterType.DEFENSE, curr + masterCoefficients.get(
                                                RandomizerFighterType.DEFENSE));
                    }
                    if (fighterEntry.disable) {
                        float curr = fighterCoefficients.get(RandomizerFighterType.DISABLE);
                        fighterCoefficients.put(RandomizerFighterType.DISABLE, curr + masterCoefficients.get(
                                                RandomizerFighterType.DISABLE));
                    }
                    if (fighterEntry.interfere) {
                        float curr = fighterCoefficients.get(RandomizerFighterType.INTERFERE);
                        fighterCoefficients.put(RandomizerFighterType.INTERFERE, curr + masterCoefficients.get(
                                                RandomizerFighterType.INTERFERE));
                    }
                    if (fighterEntry.support) {
                        float curr = fighterCoefficients.get(RandomizerFighterType.SUPPORT);
                        fighterCoefficients.put(RandomizerFighterType.SUPPORT, curr + masterCoefficients.get(
                                                RandomizerFighterType.SUPPORT));
                    }
                    if (fighterEntry.closeRange) {
                        float curr = fighterCoefficients.get(RandomizerFighterType.CLOSE_RANGE);
                        fighterCoefficients.put(RandomizerFighterType.CLOSE_RANGE, curr + masterCoefficients.get(
                                                RandomizerFighterType.CLOSE_RANGE));
                    }
                    if (fighterEntry.longRange) {
                        float curr = fighterCoefficients.get(RandomizerFighterType.LONG_RANGE);
                        fighterCoefficients.put(RandomizerFighterType.LONG_RANGE, curr + masterCoefficients.get(
                                                RandomizerFighterType.LONG_RANGE));
                    }
                }
            }

            for (RandomizerFighterType type : RandomizerFighterType.values()) {
                float total = fighterCoefficients.get(type);
                float avg = total / (variants.size() + 1);
                fighterCoefficients.put(type, avg);
                if (DSModPlugin.SHOW_DEBUG_INFO) {
                    log.info("Coefficient for " + hullId + " " + type.toString() + ": " + avg);
                }
            }

            shipFighterCoefficients.put(hullId, fighterCoefficients);
        }
    }

    public static void generateSlotCoefficients(SettingsAPI settings) {
        /* Map variants to hull */
        Map<String, List<ShipVariantAPI>> junctionTable = new HashMap<>(500);
        for (String variantId : settings.getAllVariantIds()) {
            ShipVariantAPI variant = settings.getVariant(variantId);

            // It's important that fighters are skipped!
            if (variant == null || variant.isEmptyHullVariant() || variant.isFighter() || !variant.isStockVariant()) {
                continue;
            }
            String hullId = variant.getHullSpec().getHullId();
            List<ShipVariantAPI> shipVariants = junctionTable.get(hullId);
            if (shipVariants == null) {
                shipVariants = new ArrayList<>(10);
                junctionTable.put(hullId, shipVariants);
            }
            shipVariants.add(variant);
        }

        /* Get a coefficient for each weapon type, to avoid over-/under-representation of certain types */
        EnumMap<RandomizerWeaponType, Float> masterCoefficients = new EnumMap<>(RandomizerWeaponType.class);
        for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
            masterCoefficients.put(type, 0f);
        }
        int totalWeapons = 0;
        for (DS_WeaponEntry weaponEntry : masterWeaponList.values()) {
            if (weaponEntry.builtIn) {
                continue;
            }

            if (weaponEntry.alpha) {
                float curr = masterCoefficients.get(RandomizerWeaponType.ALPHA);
                masterCoefficients.put(RandomizerWeaponType.ALPHA, curr + 1f);
            }
            if (weaponEntry.attack) {
                float curr = masterCoefficients.get(RandomizerWeaponType.ATTACK);
                masterCoefficients.put(RandomizerWeaponType.ATTACK, curr + 1f);
            }
            if (weaponEntry.defense) {
                float curr = masterCoefficients.get(RandomizerWeaponType.DEFENSE);
                masterCoefficients.put(RandomizerWeaponType.DEFENSE, curr + 1f);
            }
            if (weaponEntry.disable) {
                float curr = masterCoefficients.get(RandomizerWeaponType.DISABLE);
                masterCoefficients.put(RandomizerWeaponType.DISABLE, curr + 1f);
            }
            if (weaponEntry.standoff) {
                float curr = masterCoefficients.get(RandomizerWeaponType.STANDOFF);
                masterCoefficients.put(RandomizerWeaponType.STANDOFF, curr + 1f);
            }
            if (weaponEntry.closeRange) {
                float curr = masterCoefficients.get(RandomizerWeaponType.CLOSE_RANGE);
                masterCoefficients.put(RandomizerWeaponType.CLOSE_RANGE, curr + 1f);
            }
            if (weaponEntry.longRange) {
                float curr = masterCoefficients.get(RandomizerWeaponType.LONG_RANGE);
                masterCoefficients.put(RandomizerWeaponType.LONG_RANGE, curr + 1f);
            }
            totalWeapons++;
        }
        float totalWeaponCoeff = 0f;
        for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
            float curr = (float) Math.sqrt(Math.max(1f, masterCoefficients.get(type)));
            float coeff = totalWeapons / curr;
            totalWeaponCoeff += coeff;
        }
        totalWeaponCoeff /= RandomizerWeaponType.values().length;
        for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
            float curr = (float) Math.sqrt(Math.max(1f, masterCoefficients.get(type)));
            float coeff = (totalWeapons / curr) / totalWeaponCoeff;
            masterCoefficients.put(type, coeff);
            log.info("Master coefficient for weapon role " + type.toString() + ": " + coeff);
        }

        Set<String> allHulls = new HashSet<>(variantQuality.size() + STATION_MODULES.size());
        allHulls.addAll(variantQuality.keySet());
        allHulls.addAll(STATION_MODULES);

        int i = 0;
        for (String hullVariantId : allHulls) {
            i++;
            if (i % 50 == 0) {
                DSModPlugin.refresh();
            }

            String hullId = hullVariantId;
            if (hullId.endsWith("_Hull")) {
                hullId = hullId.replace("_Hull", "");
            }
            List<ShipVariantAPI> variants = junctionTable.get(hullId);
            if (variants == null || variants.size() <= 0) {
                // No variants to scan, no data to find
                continue;
            }

            ShipHullSpecAPI hullSpec = variants.get(0).getHullSpec();
            List<WeaponSlotAPI> slots = hullSpec.getAllWeaponSlotsCopy();

            Map<String, EnumMap<RandomizerWeaponType, Float>> shipSlotsDef = new HashMap<>(slots.size());
            for (WeaponSlotAPI slot : slots) {
                if (slot.isBuiltIn() || slot.isDecorative() || slot.isSystemSlot() || slot.getWeaponType() ==
                        WeaponType.LAUNCH_BAY) {
                    continue;
                }

                EnumMap<RandomizerWeaponType, Float> slotCoefficients = new EnumMap<>(RandomizerWeaponType.class);
                for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
                    slotCoefficients.put(type, masterCoefficients.get(type));
                }

                for (ShipVariantAPI variant : variants) {
                    WeaponSpecAPI weapon = variant.getWeaponSpec(slot.getId());
                    if (weapon == null) {
                        // Don't add anything; coefficients will drop across the board for this slot
                        continue;
                    }

                    DS_WeaponEntry weaponEntry = masterWeaponList.get(weapon.getWeaponId());
                    if (weaponEntry == null) {
                        // No info on this weapon; assume average
                        for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
                            float curr = slotCoefficients.get(type);
                            slotCoefficients.put(type, curr + 0.5f * masterCoefficients.get(type));
                        }
                        continue;
                    }

                    if (weaponEntry.alpha) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.ALPHA);
                        slotCoefficients.put(RandomizerWeaponType.ALPHA, curr + masterCoefficients.get(
                                             RandomizerWeaponType.ALPHA));
                    }
                    if (weaponEntry.attack) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.ATTACK);
                        slotCoefficients.put(RandomizerWeaponType.ATTACK, curr + masterCoefficients.get(
                                             RandomizerWeaponType.ATTACK));
                    }
                    if (weaponEntry.defense) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.DEFENSE);
                        slotCoefficients.put(RandomizerWeaponType.DEFENSE, curr + masterCoefficients.get(
                                             RandomizerWeaponType.DEFENSE));
                    }
                    if (weaponEntry.disable) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.DISABLE);
                        slotCoefficients.put(RandomizerWeaponType.DISABLE, curr + masterCoefficients.get(
                                             RandomizerWeaponType.DISABLE));
                    }
                    if (weaponEntry.standoff) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.STANDOFF);
                        slotCoefficients.put(RandomizerWeaponType.STANDOFF, curr + masterCoefficients.get(
                                             RandomizerWeaponType.STANDOFF));
                    }
                    if (weaponEntry.closeRange) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.CLOSE_RANGE);
                        slotCoefficients.put(RandomizerWeaponType.CLOSE_RANGE, curr + masterCoefficients.get(
                                             RandomizerWeaponType.CLOSE_RANGE));
                    }
                    if (weaponEntry.longRange) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.LONG_RANGE);
                        slotCoefficients.put(RandomizerWeaponType.LONG_RANGE, curr + masterCoefficients.get(
                                             RandomizerWeaponType.LONG_RANGE));
                    }
                }

                for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
                    float total = slotCoefficients.get(type);
                    float avg = total / (variants.size() + 1);
                    slotCoefficients.put(type, avg);
                    if (DSModPlugin.SHOW_DEBUG_INFO) {
                        log.info("Coefficient for " + hullId + " " + slot.getId() + " " + type.toString() + ": " + avg);
                    }
                }

                shipSlotsDef.put(slot.getId(), slotCoefficients);
            }

            shipSlotCoefficients.put(hullId, shipSlotsDef);
        }
    }

    public static void init() {
        List<String> urls = DS_Database.readFactionsCSV(Global.getSettings(), "data/factions/factions.csv");
        for (String json : urls) {
            JSONObject nexerelin = null;
            JSONObject body = null;

            try {
                body = Global.getSettings().loadJSON(json);
                nexerelin = Global.getSettings().loadJSON(NEXERELIN_SETTINGS);
            } catch (IOException | JSONException e) {
                log.log(Level.ERROR, "JSON Loading Failed! " + e.getMessage());
            }

            if (body == null || nexerelin == null) {
                continue;
            }

            try {
                String factionID = body.getString("FactionID");

                playerWeightMap.put(factionID, (float) nexerelin.optDouble(factionID + "FactionTech", 0f));
            } catch (JSONException e) {
                log.log(Level.ERROR, "Weapon Loading Failed! " + e.getMessage());
            }
        }

        readFightersCSV(Global.getSettings(), "data/factions/fighter_wings.csv");
        DSModPlugin.refresh();

        readWeaponsCSV(Global.getSettings(), "data/factions/weapon_categories.csv");
        DSModPlugin.refresh();

        readAllFactions(Global.getSettings(), urls);
        DSModPlugin.refresh();

        readShipsCSV(Global.getSettings(), "data/factions/ship_roles.csv");
        DSModPlugin.refresh();

        if (DSModPlugin.Module_ProceduralVariants) {
            generateSlotCoefficients(Global.getSettings());
        }

        evaluateFleetCompositions(Global.getSettings());
    }

    public static void readAllFactions(SettingsAPI settings, List<String> urls) {
        for (String json : urls) {
            readFactionJSON(settings, json);
        }
    }

    public static void readFactionJSON(SettingsAPI settings, String urlToJSON) {
        String factionID;

        JSONObject body = null;

        try {
            body = settings.loadJSON(urlToJSON);
        } catch (IOException | JSONException e) {
            log.log(Level.ERROR, "JSON Loading Failed! " + e.getMessage());
        }

        if (body == null) {
            return;
        }

        try {
            factionID = body.getString("FactionID");

            JSONObject techWeightsWeapon = body.getJSONObject("TechWeightsWeapon");
            Map<String, Float> weightsWeapon = new HashMap<>(techWeightsWeapon.length());
            Iterator weightsIteratorWeapon = techWeightsWeapon.keys();
            while (weightsIteratorWeapon.hasNext()) {
                String key = (String) weightsIteratorWeapon.next();
                weightsWeapon.put(key, (float) techWeightsWeapon.optDouble(key, 1.0));
            }

            JSONObject techWeightsFighter = body.getJSONObject("TechWeightsFighter");
            Map<String, Float> weightsFighter = new HashMap<>(techWeightsFighter.length());
            Iterator weightsIteratorFighter = techWeightsFighter.keys();
            while (weightsIteratorFighter.hasNext()) {
                String key = (String) weightsIteratorFighter.next();
                weightsFighter.put(key, (float) techWeightsFighter.optDouble(key, 1.0));
            }

            float masterWeaponScale = (float) body.optDouble("MasterWeaponScale", 1.0);
            float masterFighterScale = (float) body.optDouble("MasterFighterScale", 1.0);

            JSONObject tierWeights = body.getJSONObject("TierWeights");

            float tier[] = new float[6];
            tier[0] = (float) tierWeights.optDouble("0", 0.0);
            tier[1] = (float) tierWeights.optDouble("1", 0.0);
            tier[2] = (float) tierWeights.optDouble("2", 0.0);
            tier[3] = (float) tierWeights.optDouble("3", 0.0);
            tier[4] = (float) tierWeights.optDouble("4", 0.0);
            tier[5] = (float) tierWeights.optDouble("5", 0.0);

            Map<String, Float> thisFactionWeapons = new LinkedHashMap<>(300);
            Map<SlotType, Float> thisFactionWeaponAverages = new EnumMap<>(SlotType.class);
            Map<SlotType, Float> thisFactionWeaponTotalWeights = new EnumMap<>(SlotType.class);

            for (Entry<String, DS_WeaponEntry> entry : masterWeaponList.entrySet()) {
                DS_WeaponEntry weaponEntry = entry.getValue();

                float weight = 0f;
                if (weaponEntry.factionWeights.containsKey(factionID)) {
                    weight = tier[weaponEntry.tier] * weaponEntry.factionWeights.get(factionID);
                }
                if (!weaponEntry.techType.isEmpty() && weightsWeapon.containsKey(weaponEntry.techType)) {
                    weight *= weightsWeapon.get(weaponEntry.techType);
                }
                weight *= masterWeaponScale;

                if (weight > 0f) {
                    thisFactionWeapons.put(entry.getKey(), weight);
                } else {
                    continue;
                }

                List<SlotType> slots = SlotType.getSlotTypesForWeapon(weaponEntry.size, weaponEntry.type);
                for (SlotType slot : slots) {
                    Float total = thisFactionWeaponAverages.get(slot);
                    Float totalWeight = thisFactionWeaponTotalWeights.get(slot);
                    if (total == null) {
                        total = 0f;
                    }
                    if (totalWeight == null) {
                        totalWeight = 0f;
                    }
                    total += weaponEntry.OP * weight;
                    totalWeight += weight;
                    thisFactionWeaponAverages.put(slot, total);
                    thisFactionWeaponTotalWeights.put(slot, totalWeight);
                }
            }

            for (SlotType slot : SlotType.values()) {
                Float total = thisFactionWeaponAverages.get(slot);
                Float totalWeight = thisFactionWeaponTotalWeights.get(slot);
                if (total != null && totalWeight != null && totalWeight > 0f) {
                    float average = total / totalWeight;
                    thisFactionWeaponAverages.put(slot, average);
                    log.info(factionID + " " + slot.toString() + " average OP: " + String.format("%.1f", average));
                } else {
                    if (slot.size == WeaponSize.SMALL) {
                        thisFactionWeaponAverages.put(slot, 5f);
                    } else if (slot.size == WeaponSize.MEDIUM) {
                        thisFactionWeaponAverages.put(slot, 10f);
                    } else if (slot.size == WeaponSize.LARGE) {
                        thisFactionWeaponAverages.put(slot, 20f);
                    }
                }
            }

            factionWeapons.put(factionID, thisFactionWeapons);
            factionWeaponAverages.put(factionID, thisFactionWeaponAverages);

            Map<String, Float> thisFactionFighters = new LinkedHashMap<>(300);
            float thisFactionFighterAverage = 0f;
            float thisFactionFighterTotalWeight = 0f;

            for (Entry<String, DS_FighterEntry> entry : masterFighterList.entrySet()) {
                DS_FighterEntry fighterEntry = entry.getValue();

                float weight = 0f;
                if (fighterEntry.factionWeights.containsKey(factionID)) {
                    weight = tier[fighterEntry.tier] * fighterEntry.factionWeights.get(factionID);
                }
                if (!fighterEntry.techType.isEmpty() && weightsFighter.containsKey(fighterEntry.techType)) {
                    weight *= weightsFighter.get(fighterEntry.techType);
                }
                weight *= masterFighterScale;

                if (weight > 0f) {
                    thisFactionFighters.put(entry.getKey(), weight);
                } else {
                    continue;
                }

                thisFactionFighterAverage += fighterEntry.OP * weight;
                thisFactionFighterTotalWeight += weight;
            }

            if (thisFactionFighterTotalWeight > 0f) {
                thisFactionFighterAverage /= thisFactionFighterTotalWeight;
                log.info(factionID + " fighter average OP: " + String.format("%.1f", thisFactionFighterAverage));
            } else {
                thisFactionFighterAverage = 10f;
            }

            factionFighters.put(factionID, thisFactionFighters);
            factionFighterAverage.put(factionID, thisFactionFighterAverage);
        } catch (JSONException e) {
            log.log(Level.ERROR, "Weapon and LPC Loading Failed! " + e.getMessage());
        }
    }

    public static List<String> readFactionsCSV(SettingsAPI settings, String SettingID) {
        JSONArray listURLS;
        List<String> tempList = new ArrayList<>(30);

        try {
            listURLS = settings.loadCSV(SettingID);

            for (int count = 0; count < listURLS.length(); count++) {
                tempList.add(listURLS.getJSONObject(count).getString("url"));
            }
        } catch (IOException | JSONException e) {
            log.log(Level.ERROR, "Faction CSV Loading Failed! " + e.getMessage());
        }

        return tempList;
    }

    public static void readFightersCSV(SettingsAPI settings, String SettingID) {
        JSONArray rows;

        try {
            rows = settings.loadCSV(SettingID);
            for (int i = 0; i < rows.length(); i++) {
                JSONObject row = rows.getJSONObject(i);
                String id = row.optString("id");

                if (!id.isEmpty()) {
                    FighterWingSpecAPI spec = null;
                    try {
                        spec = Global.getSettings().getFighterWingSpec(id);
                    } catch (Exception e) {
                    }
                    if (spec == null) {
                        continue;
                    }

                    boolean builtIn = row.optBoolean("builtin", false);
                    int tier = row.optInt("tier", spec.getTier());
                    int OP = row.optInt("OPs", Math.round(spec.getOpCost()));
                    boolean kinetic = row.optBoolean("kinetic", false);
                    boolean highExplosive = row.optBoolean("highExplosive", false);
                    boolean energy = row.optBoolean("energy", false);
                    boolean fragmentation = row.optBoolean("fragmentation", false);
                    boolean attack = row.optBoolean("attack", false);
                    boolean interfere = row.optBoolean("interfere", false);
                    boolean alpha = row.optBoolean("alpha", false);
                    boolean defense = row.optBoolean("defense", false);
                    boolean support = row.optBoolean("support", false);
                    boolean closeRange = row.optBoolean("closeRange", false);
                    boolean longRange = row.optBoolean("longRange", false);
                    boolean disable = row.optBoolean("disable", false);
                    String techType = row.optString("techType", "");
                    float leetness = (float) row.optDouble("leetness", 1f);

                    DS_FighterEntry fighterEntry =
                                    new DS_FighterEntry(builtIn, tier, OP, kinetic, highExplosive, energy, fragmentation,
                                                        attack, interfere, alpha, defense, support, closeRange,
                                                        longRange, disable, techType, leetness);

                    int count = 0;
                    float playerWeight = 0f;
                    for (int j = 0; j < row.length(); j++) {
                        String factionID = row.names().getString(j);
                        if (!invalidFighterHeaders.contains(factionID)) {
                            count++;
                            float weight = (float) row.optDouble(factionID, 0.0);
                            if (weight > 0f) {
                                fighterEntry.factionWeights.put(factionID, weight);
                            }
                            weight *= playerWeightMap.containsKey(factionID) ? playerWeightMap.get(factionID) : 0f;
                            if (weight > 0f) {
                                playerWeight += weight;
                            }
                        }
                    }
                    if (playerWeight > 0f) {
                        playerWeight /= count;
                        fighterEntry.factionWeights.put(PLAYER_FACTION, playerWeight);
                    }

                    masterFighterList.put(id, fighterEntry);
                }
            }
        } catch (IOException | JSONException e) {
            log.log(Level.ERROR, "Fighter CSV Loading Failed! " + e.getMessage());
        }
    }

    public static void readShipsCSV(SettingsAPI settings, String SettingID) {
        JSONArray rows;

        try {
            rows = settings.loadCSV(SettingID);
            for (int i = 0; i < rows.length(); i++) {
                JSONObject row = rows.getJSONObject(i);
                String id = row.optString("id");
                if (id.endsWith("_Hull")) {
                    id = id.replace("_Hull", "");
                }

                if (!id.isEmpty()) {
                    String idStripped = id;
                    if (!idStripped.endsWith("_wing") && !idStripped.endsWith("_Hull")) {
                        idStripped += "_Hull";
                    }

                    if (!idStripped.endsWith("_wing")) {
                        try {
                            ShipVariantAPI variant = settings.getVariant(idStripped);
                        } catch (Exception ex) {
                            continue;
                        }
                    }

                    float qualityFactorMin = (float) row.optDouble("qfMin", 0.25);
                    float qualityFactorMax = (float) row.optDouble("qfMax", 0.75);
                    variantQuality.put(idStripped, (qualityFactorMin + qualityFactorMax) / 2f);

                    boolean sortSlots = row.optBoolean("sortSlots", false);
                    hullSortSlots.put(id, sortSlots);

                    Map<String, Float> roles = new HashMap<>(2);
                    for (int k = 0; k < row.length(); k++) {
                        String shipRole = row.names().getString(k);
                        if (roleHeaders.contains(shipRole)) {
                            float roleWeight = (float) row.optDouble(shipRole, 0.0);
                            if (roleWeight <= 0f) {
                                continue;
                            }

                            roles.put(id, roleWeight);
                        }
                    }

                    hullRoles.put(id, roles);
                }
            }
        } catch (IOException | JSONException e) {
            log.log(Level.ERROR, "Ship CSV Loading Failed! " + e.getMessage());
        }
    }

    public static void readWeaponsCSV(SettingsAPI settings, String SettingID) {
        JSONArray rows;

        try {
            rows = settings.loadCSV(SettingID);
            for (int i = 0; i < rows.length(); i++) {
                JSONObject row = rows.getJSONObject(i);
                String id = row.optString("id");

                if (!id.isEmpty()) {
                    WeaponSpecAPI spec = null;
                    try {
                        spec = Global.getSettings().getWeaponSpec(id);
                    } catch (Exception e) {
                    }
                    if (spec == null) {
                        continue;
                    }

                    boolean builtIn = row.optBoolean("builtin", false);

                    String sizeString = row.optString("size", spec.getSize().getDisplayName().toLowerCase());
                    WeaponSize size;
                    switch (sizeString) {
                        case "small":
                            size = WeaponSize.SMALL;
                            break;
                        case "medium":
                            size = WeaponSize.MEDIUM;
                            break;
                        case "large":
                            size = WeaponSize.LARGE;
                            break;
                        default:
                            continue;
                    }
                    String typeString = row.optString("type", spec.getType().getDisplayName().toLowerCase());
                    WeaponType type;
                    switch (typeString) {
                        case "ballistic":
                            type = WeaponType.BALLISTIC;
                            break;
                        case "missile":
                            type = WeaponType.MISSILE;
                            break;
                        case "energy":
                            type = WeaponType.ENERGY;
                            break;
                        case "universal":
                            type = WeaponType.UNIVERSAL;
                            break;
                        case "hybrid":
                            type = WeaponType.HYBRID;
                            break;
                        case "synergy":
                            type = WeaponType.SYNERGY;
                            break;
                        case "composite":
                            type = WeaponType.COMPOSITE;
                            break;
                        case "system":
                            type = WeaponType.SYSTEM;
                            break;
                        case "decorative":
                            type = WeaponType.DECORATIVE;
                            break;
                        case "launch_bay":
                            type = WeaponType.LAUNCH_BAY;
                            break;
                        case "built_in":
                            type = WeaponType.BUILT_IN;
                            break;
                        default:
                            continue;
                    }
                    int tier = row.optInt("tier", spec.getTier());
                    int OP = row.optInt("OPs", Math.round(spec.getOrdnancePointCost(null)));
                    String damageTypeString = row.optString("damageType");
                    boolean limitedAmmo = row.optBoolean("limitedAmmo", false);
                    boolean lowFlux = row.optBoolean("lowFlux", false);
                    boolean separateFire = row.optBoolean("separateFire", false);
                    boolean frontOnly = row.optBoolean("frontOnly", false);
                    boolean turretOnly = row.optBoolean("turretOnly", false);
                    DamageType damageType;
                    switch (damageTypeString) {
                        case "HIGH_EXPLOSIVE":
                            damageType = DamageType.HIGH_EXPLOSIVE;
                            break;
                        case "KINETIC":
                            damageType = DamageType.KINETIC;
                            break;
                        case "ENERGY":
                            damageType = DamageType.ENERGY;
                            break;
                        case "FRAGMENTATION":
                            damageType = DamageType.FRAGMENTATION;
                            break;
                        case "OTHER":
                            damageType = DamageType.OTHER;
                            break;
                        default:
                            continue;
                    }
                    boolean attack = row.optBoolean("attack", false);
                    boolean standoff = row.optBoolean("standoff", false);
                    boolean alpha = row.optBoolean("alpha", false);
                    boolean defense = row.optBoolean("defense", false);
                    boolean closeRange = row.optBoolean("closeRange", false);
                    boolean longRange = row.optBoolean("longRange", false);
                    boolean disable = row.optBoolean("disable", false);
                    String techType = row.optString("techType", "");
                    float leetness = (float) row.optDouble("leetness", 1f);

                    DS_WeaponEntry weaponEntry = new DS_WeaponEntry(builtIn, size, type, tier, OP, limitedAmmo, lowFlux,
                                                                    separateFire, frontOnly, turretOnly, damageType,
                                                                    attack, standoff, alpha, defense, closeRange,
                                                                    longRange, disable, techType, leetness);

                    int count = 0;
                    float playerWeight = 0f;
                    for (int j = 0; j < row.length(); j++) {
                        String factionID = row.names().getString(j);
                        if (!invalidWeaponHeaders.contains(factionID)) {
                            count++;
                            float weight = (float) row.optDouble(factionID, 0.0);
                            if (weight > 0f) {
                                weaponEntry.factionWeights.put(factionID, weight);
                            }
                            weight *= playerWeightMap.containsKey(factionID) ? playerWeightMap.get(factionID) : 0f;
                            if (weight > 0f) {
                                playerWeight += weight;
                            }
                        }
                    }
                    if (playerWeight > 0f) {
                        playerWeight /= count;
                        weaponEntry.factionWeights.put(PLAYER_FACTION, playerWeight);
                    }

                    masterWeaponList.put(id, weaponEntry);
                }
            }
        } catch (IOException | JSONException e) {
            log.log(Level.ERROR, "Weapon CSV Loading Failed! " + e.getMessage());
        }
    }

    public static int shieldQuality(String id) {
        Integer quality = DS_Defs.SHIELD_QUALITY.get(id);
        if (quality == null) {
            return 1;
        } else {
            return quality;
        }
    }

    private static void evaluateFleetCompositions(SettingsAPI settings) {
        for (FactionAPI faction : Global.getSector().getAllFactions()) {
            for (String roleId : roleHeaders) {
                List<RoleEntryAPI> roleEntries = settings.getEntriesForRole(faction.getId(), roleId);
                if (roleEntries.isEmpty()) {
                    continue;
                }

                log.log(Level.INFO, "" + faction.getDisplayName() + " " + roleId + "...");
                Map<String, Float> weightMap = new HashMap<>(roleEntries.size());
                List<String> hullIds = new ArrayList<>(roleEntries.size());
                float totalWeight = 0f;
                for (RoleEntryAPI roleEntry : roleEntries) {
                    String variantId = roleEntry.getVariantId();
                    ShipVariantAPI variant = null;
                    try {
                        variant = settings.getVariant(variantId);
                    } catch (RuntimeException e) {
                    }
                    if (variant == null) {
                        continue;
                    }

                    float weight = roleEntry.getWeight();
                    String hullId = variant.getHullSpec().getHullId();
                    if (weightMap.containsKey(hullId)) {
                        float existingWeight = weightMap.get(hullId);
                        weightMap.put(hullId, existingWeight + weight);
                    } else {
                        weightMap.put(hullId, weight);
                        hullIds.add(hullId);
                    }

                    totalWeight += weight;
                }

                if (weightMap.isEmpty()) {
                    log.log(Level.INFO, "... empty");
                    continue;
                }

                Collections.sort(hullIds, new HullRoleSorter(weightMap));
                for (String hullId : hullIds) {
                    ShipHullSpecAPI hull = settings.getHullSpec(hullId);
                    float weight = weightMap.get(hullId);
                    float ratio = weight / totalWeight;
                    log.log(Level.INFO, String.format("... %s: %.2f (%.2f%%)",
                                                      hull.getHullName(), weight, ratio * 100f));
                }
            }
        }
    }

    public static enum SlotType {

        LARGE_UNIVERSAL(WeaponSize.LARGE, WeaponType.UNIVERSAL),
        MEDIUM_UNIVERSAL(WeaponSize.MEDIUM, WeaponType.UNIVERSAL),
        SMALL_UNIVERSAL(WeaponSize.SMALL, WeaponType.UNIVERSAL),
        LARGE_HYBRID(WeaponSize.LARGE, WeaponType.HYBRID, LARGE_UNIVERSAL),
        MEDIUM_HYBRID(WeaponSize.MEDIUM, WeaponType.HYBRID, MEDIUM_UNIVERSAL),
        SMALL_HYBRID(WeaponSize.SMALL, WeaponType.HYBRID, SMALL_UNIVERSAL),
        LARGE_SYNERGY(WeaponSize.LARGE, WeaponType.SYNERGY, LARGE_UNIVERSAL),
        MEDIUM_SYNERGY(WeaponSize.MEDIUM, WeaponType.SYNERGY, MEDIUM_UNIVERSAL),
        SMALL_SYNERGY(WeaponSize.SMALL, WeaponType.SYNERGY, SMALL_UNIVERSAL),
        LARGE_COMPOSITE(WeaponSize.LARGE, WeaponType.COMPOSITE, LARGE_UNIVERSAL),
        MEDIUM_COMPOSITE(WeaponSize.MEDIUM, WeaponType.COMPOSITE, MEDIUM_UNIVERSAL),
        SMALL_COMPOSITE(WeaponSize.SMALL, WeaponType.COMPOSITE, SMALL_UNIVERSAL),
        LARGE_BALLISTIC(WeaponSize.LARGE, WeaponType.BALLISTIC, LARGE_UNIVERSAL, LARGE_HYBRID, LARGE_COMPOSITE),
        MEDIUM_BALLISTIC(WeaponSize.MEDIUM, WeaponType.BALLISTIC, MEDIUM_UNIVERSAL, MEDIUM_HYBRID, MEDIUM_COMPOSITE),
        SMALL_BALLISTIC(WeaponSize.SMALL, WeaponType.BALLISTIC, SMALL_UNIVERSAL, SMALL_HYBRID, SMALL_COMPOSITE),
        LARGE_MISSILE(WeaponSize.LARGE, WeaponType.MISSILE, LARGE_UNIVERSAL, LARGE_SYNERGY, LARGE_COMPOSITE),
        MEDIUM_MISSILE(WeaponSize.MEDIUM, WeaponType.MISSILE, MEDIUM_UNIVERSAL, MEDIUM_SYNERGY, MEDIUM_COMPOSITE),
        SMALL_MISSILE(WeaponSize.SMALL, WeaponType.MISSILE, SMALL_UNIVERSAL, SMALL_SYNERGY, SMALL_COMPOSITE),
        LARGE_ENERGY(WeaponSize.LARGE, WeaponType.ENERGY, LARGE_UNIVERSAL, LARGE_HYBRID, LARGE_SYNERGY),
        MEDIUM_ENERGY(WeaponSize.MEDIUM, WeaponType.ENERGY, MEDIUM_UNIVERSAL, MEDIUM_HYBRID, MEDIUM_SYNERGY),
        SMALL_ENERGY(WeaponSize.SMALL, WeaponType.ENERGY, SMALL_UNIVERSAL, SMALL_HYBRID, SMALL_SYNERGY);

        final WeaponSize size;
        final WeaponType type;
        final List<SlotType> compatibleSlots;

        SlotType(WeaponSize size, WeaponType type, SlotType... compatibleSlots) {
            this.size = size;
            this.type = type;
            this.compatibleSlots = new ArrayList<>(compatibleSlots.length + 1);
            this.compatibleSlots.addAll(Arrays.asList(compatibleSlots));
            init();
        }

        private void init() {
            if (type != WeaponType.HYBRID && type != WeaponType.SYNERGY && type != WeaponType.COMPOSITE) {
                this.compatibleSlots.add(this);
            }
        }

        public static SlotType getSlotType(WeaponSize size, WeaponType type) {
            for (SlotType slot : SlotType.values()) {
                if (slot.size == size && slot.type == type) {
                    return slot;
                }
            }
            return null;
        }

        public static List<SlotType> getSlotTypesForWeapon(WeaponSize size, WeaponType type) {
            for (SlotType slot : SlotType.values()) {
                if (slot.size == size && slot.type == type) {
                    return Collections.unmodifiableList(slot.compatibleSlots);
                }
            }
            return null;
        }
    }

    public static enum RandomizerFighterType {

        ALPHA, ATTACK, DEFENSE, DISABLE, INTERFERE, SUPPORT,
        CLOSE_RANGE, LONG_RANGE
    }

    public static enum RandomizerWeaponType {

        ALPHA, ATTACK, DEFENSE, DISABLE, STANDOFF,
        CLOSE_RANGE, LONG_RANGE
    }

    private static final class HullRoleSorter implements Comparator<String> {

        private final Map<String, Float> weightMap;

        HullRoleSorter(Map<String, Float> weightMap) {
            this.weightMap = weightMap;
        }

        // -1 means hullId1, is first, 1 means hullId2 is first
        @Override
        public int compare(String hullId1, String hullId2) {
            float weight1 = weightMap.get(hullId1);
            float weight2 = weightMap.get(hullId2);
            if (weight1 > weight2) {
                return -1;
            } else if (weight2 > weight1) {
                return 1;
            }
            return hullId1.compareTo(hullId2);
        }
    };
}
