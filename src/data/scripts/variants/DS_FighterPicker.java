package data.scripts.variants;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Defs.FighterPreference;
import data.scripts.variants.DS_Database.RandomizerFighterType;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Logger;

public class DS_FighterPicker {

    private static final Logger log = Global.getLogger(DS_FighterPicker.class);

    public static String chooseFighter(List<String> factionList, EnumMap<RandomizerFighterType, Float> coefficients,
                                       List<Map<String, Float>> fighterDataList, String preferredFighter,
                                       FighterPreference preference, DamageType preferredDamageType, float qualityFactor,
                                       int remainingOP, int lastResortOP, float attack, float interfere, float alpha,
                                       float defense, float support, float closeRange, float longRange, float disable,
                                       float budgetFactor, boolean station, boolean noobFriendly,
                                       MutableShipStatsAPI stats, Random r) {
        WeightedRandomPicker<String> randomizer = new WeightedRandomPicker<>(r);

        float bayOP = 0f;
        for (String faction : factionList) {
            bayOP += getOPTargetForBay(faction, stats);
        }
        float targetOP = (bayOP / factionList.size()) * budgetFactor;

        for (Map<String, Float> fighterData : fighterDataList) {
            for (Map.Entry<String, Float> entry : fighterData.entrySet()) {
                String id = entry.getKey();
                float weight = calculateFighterWeight(id, entry.getValue(), targetOP, factionList, coefficients,
                                                      preferredFighter, preference, preferredDamageType, qualityFactor,
                                                      remainingOP, lastResortOP, attack, interfere, alpha, defense,
                                                      support, closeRange, longRange, disable, station, noobFriendly,
                                                      stats);

                if (weight <= 0f) {
                    continue;
                }

                randomizer.add(id, weight);
            }
        }

        boolean sharingFighters = false;
        for (String faction : factionList) {
            if (!DS_Defs.INJECTOR_NO_SHARING_MOUNTS.contains(faction)) {
                sharingFighters = true;
                break;
            }
        }

        if (randomizer.isEmpty() && sharingFighters) {
            List<String> factionListNew = new ArrayList<>(1);
            List<Map<String, Float>> fighterDataListNew = new ArrayList<>(1);
            switch (factionList.get(0)) {
                case "everything":
                    return null;
                case "sector": {
                    factionListNew.add("everything");
                    fighterDataListNew.add(DS_Database.factionFighters.get("everything"));
                    return chooseFighter(factionListNew, coefficients, fighterDataListNew, preferredFighter, preference,
                                         preferredDamageType, qualityFactor, remainingOP, lastResortOP, attack,
                                         interfere, alpha, defense, support, closeRange, longRange, disable,
                                         budgetFactor, station, noobFriendly, stats, r);
                }
                case "domain": {
                    factionListNew.add("sector");
                    fighterDataListNew.add(DS_Database.factionFighters.get("sector"));
                    return chooseFighter(factionListNew, coefficients, fighterDataListNew, preferredFighter, preference,
                                         preferredDamageType, qualityFactor, remainingOP, lastResortOP, attack,
                                         interfere, alpha, defense, support, closeRange, longRange, disable,
                                         budgetFactor, station, noobFriendly, stats, r);
                }
                default: {
                    factionListNew.add("domain");
                    fighterDataListNew.add(DS_Database.factionFighters.get("domain"));
                    return chooseFighter(factionListNew, coefficients, fighterDataListNew, preferredFighter, preference,
                                         preferredDamageType, qualityFactor, remainingOP, lastResortOP, attack,
                                         interfere, alpha, defense, support, closeRange, longRange, disable,
                                         budgetFactor, station, noobFriendly, stats, r);
                }
            }
        }

        String id = randomizer.pick();

        if (id == null) {
            return null;
        }

        return id;
    }

    public static float getOPTargetForBay(String faction, MutableShipStatsAPI stats) {
        Float fighterAverage = DS_Database.factionFighterAverage.get(faction);
        if (fighterAverage == null) {
            return 10f;
        }

        return fighterAverage;
    }

    public static String pickRandomFighter(String faction, Map<String, Float> fighterData, float qualityFactor,
                                           int maxTier, Random r) {
        WeightedRandomPicker<String> randomizer = new WeightedRandomPicker<>(r);

        for (Map.Entry<String, Float> entry : fighterData.entrySet()) {
            String id = entry.getKey();
            float weight = entry.getValue();
            DS_FighterEntry fighter = DS_Database.masterFighterList.get(id);

            if (fighter.tier > maxTier) {
                continue;
            }

            if (fighter.tier == 0) { // qF 0 = wt 3, qF 0.5 = wt 1, qf 1 = wt 0.33
                if (qualityFactor < 0.5f) {
                    weight *= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
                } else {
                    weight /= 1f + (qualityFactor - 0.5f) / 0.25f;
                }
            } else if (fighter.tier == 1) { // qF 0 = wt 2, qF 0.5 = wt 1, qf 1 = wt 0.5
                if (qualityFactor < 0.5f) {
                    weight *= 1f / (Math.max(qualityFactor, -0.45f) + 0.5f);
                } else {
                    weight /= 1f + (qualityFactor - 0.5f) / 0.5f;
                }
            } else if (fighter.tier == 2) { // qF 0 = wt 0.33, qF 0.5 = wt 1, qf 1 = wt 2
                if (qualityFactor < 0.5f) {
                    weight /= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
                } else {
                    weight *= 1f + (qualityFactor - 0.5f) / 0.5f;
                }
            } else if (fighter.tier == 3) { // qF 0 = wt 0.05, qF 0.5 = wt 1, qf 1 = wt 3
                if (qualityFactor < 0.5f) {
                    weight /= 1f / (Math.max(qualityFactor, 0.05f));
                } else {
                    weight *= 1f + (qualityFactor - 0.5f) / 0.25f;
                }
            }

            if (weight <= 0f) {
                continue;
            }

            randomizer.add(id, weight);
        }

        if (randomizer.isEmpty() && !faction.contentEquals("exigency") && !faction.contentEquals("templars")) {
            switch (faction) {
                case "everything":
                    return null;
                case "sector":
                    return pickRandomFighter("everything", DS_Database.factionFighters.get("everything"), qualityFactor,
                                             maxTier, r);
                case "domain":
                    return pickRandomFighter("sector", DS_Database.factionFighters.get("sector"), qualityFactor, maxTier,
                                             r);
                default:
                    return pickRandomFighter("domain", DS_Database.factionFighters.get("domain"), qualityFactor, maxTier,
                                             r);
            }
        }

        return randomizer.pick();
    }

    private static float calculateFighterWeight(String id, float defaultWeight, float targetOP, List<String> factionList,
                                                EnumMap<RandomizerFighterType, Float> coefficients,
                                                String preferredFighter, FighterPreference preference,
                                                DamageType preferredDamageType, float qualityFactor, int remainingOP,
                                                int lastResortOP, float attack, float interfere, float alpha,
                                                float defense, float support, float closeRange, float longRange,
                                                float disable, boolean station, boolean noobFriendly,
                                                MutableShipStatsAPI stats) {
        float weight = defaultWeight;
        float opTarget = targetOP;
        DS_FighterEntry fighter = DS_Database.masterFighterList.get(id);
        FighterWingSpecAPI spec = Global.getSettings().getFighterWingSpec(id);

        if (factionList.contains("SCY") && id.startsWith("SCY_")) {
            weight *= 4f;
        }

        if (factionList.contains("shadow_industry") && id.startsWith("ms_")) {
            weight *= 4f;
        }

        if (preference == FighterPreference.STRIKE) {
            if (fighter.alpha) {
                weight *= 2f;
                opTarget *= 1.25f;
            }
        }

        float diff = opTarget - fighter.OP;
        if (diff < 0f) {
            weight *= Math.exp(-diff * diff / 60f);
        } else {
            weight *= Math.exp(-diff * diff / 360f);
        }

        if (fighter.OP > remainingOP) {
            if (fighter.OP <= lastResortOP || fighter.OP <= 0) {
                weight *= 0.001f;
            } else {
                return -1f;
            }
        }

        if (preferredFighter != null && id.contentEquals(preferredFighter)) {
            weight *= 200000f;
        }

        if (noobFriendly) {
            weight /= fighter.leetness;
        }

        if (fighter.tier == 0) { // qF 0 = wt 3, qF 0.5 = wt 1, qf 1 = wt 0.33
            if (qualityFactor < 0.5f) {
                weight *= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
            } else {
                weight /= 1f + (qualityFactor - 0.5f) / 0.25f;
            }
        } else if (fighter.tier == 1) { // qF 0 = wt 2, qF 0.5 = wt 1, qf 1 = wt 0.5
            if (qualityFactor < 0.5f) {
                weight *= 1f / (Math.max(qualityFactor, -0.45f) + 0.5f);
            } else {
                weight /= 1f + (qualityFactor - 0.5f) / 0.5f;
            }
        } else if (fighter.tier == 2) { // qF 0 = wt 0.33, qF 0.5 = wt 1, qf 1 = wt 2
            if (qualityFactor < 0.5f) {
                weight /= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
            } else {
                weight *= 1f + (qualityFactor - 0.5f) / 0.5f;
            }
        } else if (fighter.tier == 3) { // qF 0 = wt 0.05, qF 0.5 = wt 0.5, qf 1 = wt 3
            if (qualityFactor < 0.5f) {
                weight /= 1f / (Math.max(qualityFactor, 0.05f));
            } else {
                weight *= 0.5f + (qualityFactor - 0.5f) / 0.2f;
            }
        } else if (fighter.tier == 4) { // qF 0 = wt 0.033, qF 0.5 = wt 0.33, qf 1 = wt 4
            if (qualityFactor < 0.5f) {
                weight /= 1.5f / (Math.max(qualityFactor, 0.05f));
            } else {
                weight *= 0.33f + (qualityFactor - 0.5f) / 0.136f;
            }
        }

        if (preferredDamageType != null) {
            if ((fighter.highExplosive && DamageType.HIGH_EXPLOSIVE == preferredDamageType) ||
                    (fighter.kinetic && DamageType.KINETIC == preferredDamageType) ||
                    (fighter.energy && DamageType.ENERGY == preferredDamageType) ||
                    (fighter.fragmentation && DamageType.FRAGMENTATION == preferredDamageType)) {
                weight *= 2f;
            }
        }

        float boost = 1f;
        if (fighter.attack) {
            boost += attack / boost;
        }
        if (fighter.interfere) {
            boost += interfere / boost;
        }
        if (fighter.alpha) {
            boost += alpha / boost;
        }
        if (fighter.defense) {
            boost += defense / boost;
        }
        if (fighter.support) {
            boost += support / boost;
        }
        if (fighter.longRange) {
            boost += longRange / boost;
        }
        if (fighter.closeRange) {
            boost += closeRange / boost;
        }
        if (fighter.disable) {
            boost += disable / boost;
        }
        boost -= 1f;
        weight *= boost;

        if (coefficients != null) {
            if (fighter.alpha) {
                weight *= coefficients.get(RandomizerFighterType.ALPHA);
            }
            if (fighter.attack) {
                weight *= coefficients.get(RandomizerFighterType.ATTACK);
            }
            if (fighter.defense) {
                weight *= coefficients.get(RandomizerFighterType.DEFENSE);
            }
            if (fighter.disable) {
                weight *= coefficients.get(RandomizerFighterType.DISABLE);
            }
            if (fighter.interfere) {
                weight *= coefficients.get(RandomizerFighterType.INTERFERE);
            }
            if (fighter.support) {
                weight *= coefficients.get(RandomizerFighterType.SUPPORT);
            }
            if (fighter.closeRange) {
                weight *= coefficients.get(RandomizerFighterType.CLOSE_RANGE);
            }
            if (fighter.longRange) {
                weight *= coefficients.get(RandomizerFighterType.LONG_RANGE);
            }
        }

        return weight;
    }
}
