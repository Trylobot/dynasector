package data.scripts.variants;

import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Defs.Archetype;
import data.scripts.util.DS_Defs.FactionStyle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class DS_FleetRandomizer {

    public static final Comparator<FleetMemberAPI> PRIORITY = new Comparator<FleetMemberAPI>() {
        // -1 means member1 is first, 1 means member2 is first
        @Override
        public int compare(FleetMemberAPI member1, FleetMemberAPI member2) {
            if (DS_Defs.SPECIAL_HULLS.contains(member1.getHullId())) {
                if (!DS_Defs.SPECIAL_HULLS.contains(member2.getHullId())) {
                    return -1;
                }
            } else {
                if (DS_Defs.SPECIAL_HULLS.contains(member2.getHullId())) {
                    return 1;
                }
            }
            if (member1.isFlagship()) {
                if (!member2.isFlagship()) {
                    return -1;
                }
            } else {
                if (member2.isFlagship()) {
                    return 1;
                }
            }
            if (!member1.isCivilian()) {
                if (member2.isCivilian()) {
                    return -1;
                }
            } else {
                if (!member2.isCivilian()) {
                    return 1;
                }
            }
            if (!member1.isFighterWing()) {
                if (member2.isFighterWing()) {
                    return -1;
                }
            } else {
                if (!member2.isFighterWing()) {
                    return 1;
                }
            }
            if (member1.getFleetPointCost() > member2.getFleetPointCost()) {
                return -1;
            } else if (member1.getFleetPointCost() < member2.getFleetPointCost()) {
                return 1;
            }
            if (member1.getCaptain() != null) {
                if (member2.getCaptain() == null) {
                    return -1;
                }
            } else {
                if (member2.getCaptain() != null) {
                    return 1;
                }
            }
            if (!member1.isFrigate()) {
                if (member2.isFrigate()) {
                    return -1;
                }
            } else {
                if (!member2.isFrigate()) {
                    return 1;
                }
            }
            if (!member1.isDestroyer()) {
                if (member2.isDestroyer()) {
                    return -1;
                }
            } else {
                if (!member2.isDestroyer()) {
                    return 1;
                }
            }
            if (!member1.isCruiser()) {
                if (member2.isCruiser()) {
                    return -1;
                }
            } else {
                if (!member2.isCruiser()) {
                    return 1;
                }
            }
            if (!member1.isCapital()) {
                if (member2.isCapital()) {
                    return -1;
                }
            } else {
                if (!member2.isCapital()) {
                    return 1;
                }
            }
            return member1.getSpecId().compareTo(member2.getSpecId());
        }
    };

    private static final float ALMOST_ZERO = 0.00001f;

    public static int countFPInFleetData(List<FleetMemberAPI> data) {
        int count = 0;
        for (FleetMemberAPI member : data) {
            count += member.getFleetPointCost();
        }

        return count;
    }

    public static int countPtsInFleetData(List<FleetMemberAPI> data) {
        int pts = 0;
        for (FleetMemberAPI member : data) {
            if (member.isFighterWing() || member.isFrigate()) {
                pts += 1;
            } else if (member.isDestroyer()) {
                pts += 2;
            } else if (member.isCruiser()) {
                pts += 4;
            } else if (member.isCapital()) {
                pts += 8;
            }
        }

        return pts;
    }

    public static List<String> deconstructShip(ShipVariantAPI variant, String faction, Random r) {
        List<String> roleList = new ArrayList<>(25);
        Map<String, Float> roleListFractions = new LinkedHashMap<>(55);

        String id = variant.getHullSpec().getHullId();
        if (variant.isFighter()) {
            id += "_wing";
        }

        Map<String, Float> roles = DS_Database.hullRoles.get(id);
        if (roles == null) {
            id = variant.getHullSpec().getDParentHullId();
            if (variant.isFighter()) {
                id += "_wing";
            }

            roles = DS_Database.hullRoles.get(id);
        }
        if (roles == null) {
            id = variant.getHullSpec().getBaseHullId();
            if (variant.isFighter()) {
                id += "_wing";
            }

            roles = DS_Database.hullRoles.get(id);
        }
        if (roles == null) {
            return roleList; // guess we couldn't find anything...
        }

        for (Entry<String, Float> e : roles.entrySet()) {
            String role = e.getKey();
            float weight = e.getValue();

            Float fraction = roleListFractions.get(role);
            if (fraction == null) {
                roleListFractions.put(role, weight);
            } else {
                roleListFractions.put(role, weight + fraction);
            }
        }

        float fracRemainder = 0f;
        for (Map.Entry<String, Float> fraction : roleListFractions.entrySet()) {
            String role = fraction.getKey();
            float frac = fraction.getValue();

            int count = (int) Math.floor(frac);
            frac -= count;

            if (r.nextFloat() < frac + fracRemainder) {
                count++;
                fracRemainder = 0f;
            } else {
                fracRemainder += frac;
            }

            for (int i = 0; i < count; i++) {
                roleList.add(role);
            }
        }

        return roleList;
    }

    public static Archetype getArchetypeFromRole(List<String> roleList, float qualityFactor,
                                                 Archetype preferredArchetype, Map<Archetype, Float> archetypeWeights,
                                                 Random r) {
        WeightedRandomPicker<Archetype> archetypes = new WeightedRandomPicker<>(r);
        if (roleList != null) {
            for (String role : roleList) {
                switch (role) {
                    case ShipRoles.FAST_ATTACK:
                        archetypes.add(Archetype.ASSAULT, 3f);
                        archetypes.add(Archetype.BALANCED, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.SKIRMISH, 2f);
                        archetypes.add(Archetype.STRIKE, 2f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    case ShipRoles.ESCORT_SMALL:
                        archetypes.add(Archetype.ARTILLERY, 0.5f);
                        archetypes.add(Archetype.BALANCED, 2f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.ESCORT, 3f);
                        archetypes.add(Archetype.SUPPORT, 2f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    case ShipRoles.ESCORT_MEDIUM:
                        archetypes.add(Archetype.ARTILLERY, 1f);
                        archetypes.add(Archetype.BALANCED, 2f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.ESCORT, 3f);
                        archetypes.add(Archetype.SUPPORT, 2f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    case ShipRoles.COMBAT_SMALL:
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 2f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.SKIRMISH, 0.5f);
                        archetypes.add(Archetype.STRIKE, 1f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    default:
                    case ShipRoles.COMBAT_MEDIUM:
                        archetypes.add(Archetype.ARTILLERY, 0.5f);
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 2f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.SKIRMISH, 0.5f);
                        archetypes.add(Archetype.STRIKE, 1f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    case ShipRoles.COMBAT_LARGE:
                        archetypes.add(Archetype.ARTILLERY, 1f);
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 2f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.STRIKE, 0.5f);
                        archetypes.add(Archetype.ESCORT, 0.5f);
                        archetypes.add(Archetype.SUPPORT, 0.5f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    case ShipRoles.COMBAT_CAPITAL:
                        archetypes.add(Archetype.ARTILLERY, 1f);
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 2f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.ESCORT, 0.5f);
                        archetypes.add(Archetype.SUPPORT, 0.5f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    case "miningSmall":
                    case ShipRoles.COMBAT_FREIGHTER_SMALL:
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 2f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.ESCORT, 1f);
                        archetypes.add(Archetype.SKIRMISH, 0.5f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case "miningMedium":
                    case ShipRoles.COMBAT_FREIGHTER_MEDIUM:
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 2f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.ESCORT, 1f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case "miningLarge":
                    case ShipRoles.COMBAT_FREIGHTER_LARGE:
                        archetypes.add(Archetype.ARTILLERY, 1f);
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 2f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.ESCORT, 1f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.CIV_RANDOM:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ESCORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SKIRMISH, 1f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.CARRIER_SMALL:
                        archetypes.add(Archetype.ARTILLERY, 1f);
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.SKIRMISH, 0.5f);
                        archetypes.add(Archetype.ESCORT, 1f);
                        archetypes.add(Archetype.STRIKE, 1f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    case ShipRoles.CARRIER_MEDIUM:
                        archetypes.add(Archetype.ARTILLERY, 1f);
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.SKIRMISH, 0.5f);
                        archetypes.add(Archetype.ESCORT, 1f);
                        archetypes.add(Archetype.STRIKE, 1f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    case ShipRoles.CARRIER_LARGE:
                        archetypes.add(Archetype.ARTILLERY, 1f);
                        archetypes.add(Archetype.ASSAULT, 1f);
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                        archetypes.add(Archetype.SKIRMISH, 0.5f);
                        archetypes.add(Archetype.ESCORT, 1f);
                        archetypes.add(Archetype.STRIKE, 1f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                        break;
                    case ShipRoles.FREIGHTER_SMALL:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.FREIGHTER_MEDIUM:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.FREIGHTER_LARGE:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.TANKER_SMALL:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.TANKER_MEDIUM:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.TANKER_LARGE:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.PERSONNEL_SMALL:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.PERSONNEL_MEDIUM:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.PERSONNEL_LARGE:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.LINER_SMALL:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.LINER_MEDIUM:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.LINER_LARGE:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.TUG:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.CRIG:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                    case ShipRoles.UTILITY:
                        archetypes.add(Archetype.BALANCED, 3f);
                        archetypes.add(Archetype.FLEET, 3f);
                        archetypes.add(Archetype.SUPPORT, 1f);
                        break;
                }
            }
        }

        if (archetypes.isEmpty()) {
            if (preferredArchetype != null) {
                return preferredArchetype;
            }
            return Archetype.BALANCED;
        }

        if (archetypeWeights != null) {
            for (Map.Entry<Archetype, Float> entry : archetypeWeights.entrySet()) {
                Archetype archetype = entry.getKey();
                float weight = entry.getValue();
                if (archetypes.getItems().contains(archetype)) {
                    float currWeight = archetypes.getWeight(archetypes.getItems().indexOf(archetype));
                    archetypes.remove(archetype);
                    archetypes.add(archetype, currWeight * weight);
                }
            }
        }

        if (preferredArchetype != null) {
            for (int i = 0; i < archetypes.getItems().size(); i++) {
                Archetype archetype = archetypes.getItems().get(i);
                if (archetype == preferredArchetype) {
                    archetypes.add(archetype, archetypes.getWeight(i) * 9f + 5f);
                    break;
                }
            }
            archetypes.add(preferredArchetype, 5f);
        }

        return archetypes.pick();
    }

    public static void randomizeVariant(FleetMemberAPI member, List<String> factionList, List<String> roleList,
                                        float qualityFactor, Archetype preferredArchetype,
                                        Map<Archetype, Float> archetypeWeights, float opBonus, boolean noobFriendly,
                                        Random r) {
        if (factionList.isEmpty()) {
            factionList.add(Factions.INDEPENDENT);
        }

        float avgVariantQF = 0f;
        for (String faction : factionList) {
            float variantQF = qualityFactor;
            FactionStyle style = FactionStyle.getStyle(faction);
            if (style != null) {
                if (qualityFactor < style.lowQF) {
                    if (style.lowQF - style.minQF <= ALMOST_ZERO) {
                        variantQF = style.minQF;
                    } else {
                        float a = (style.lowQF - qualityFactor) / (style.lowQF - style.minQF);
                        variantQF = (style.lowQF * (1f - a)) + (style.minQF * a);
                    }
                }
                if (qualityFactor > style.highQF) {
                    if (style.maxQF - style.highQF <= ALMOST_ZERO) {
                        variantQF = style.maxQF;
                    } else {
                        float a = (qualityFactor - style.highQF) / (style.maxQF - style.highQF);
                        variantQF = (style.highQF * (1f - a)) + (style.maxQF * a);
                    }
                }
            }
            avgVariantQF += variantQF;
        }
        avgVariantQF /= factionList.size();

        DS_VariantRandomizer.setVariant(member, factionList, null, roleList, avgVariantQF, preferredArchetype,
                                        archetypeWeights, opBonus, noobFriendly, r);
    }
}
