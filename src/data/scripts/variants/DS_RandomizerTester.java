package data.scripts.variants;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import data.scripts.util.DS_Defs.Archetype;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DS_RandomizerTester implements EveryFrameScript {

    @Override
    public void advance(float amount) {
        CampaignFleetAPI player = Global.getSector().getPlayerFleet();
        if (player == null) {
            return;
        }

        FleetDataAPI data = player.getFleetData();
        for (FleetMemberAPI member : data.getMembersListCopy()) {
            ShipVariantAPI variant = member.getVariant();

            String name = member.getShipName();
            if (name == null || name.length() <= 0) {
                continue;
            }
            if (name.indexOf('-') <= 0) {
                continue;
            }
            String faction = name.substring(0, name.indexOf('-'));
            if (Global.getSector().getFaction(faction) == null) {
                continue;
            }

            variant.clearHullMods();
            for (WeaponSlotAPI slot : variant.getHullSpec().getAllWeaponSlotsCopy()) {
                if (!slot.isBuiltIn() && !slot.isDecorative() && !slot.isSystemSlot()) {
                    variant.clearSlot(slot.getId());
                }
            }
            variant.setNumFluxCapacitors(0);
            variant.setNumFluxVents(0);
            variant.autoGenerateWeaponGroups();

            List<String> factionList = new ArrayList<>(1);
            factionList.add(faction);

            if (name.endsWith("-R")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.ARTILLERY, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-A")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.ASSAULT, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-B")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.BALANCED, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-C")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.CLOSE_SUPPORT, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-L")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.ELITE, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-E")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.ESCORT, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-F")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.FLEET, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-K")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.SKIRMISH, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-S")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.STRIKE, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-P")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.SUPPORT, 0.5f, 0f, false, new Random());
            } else if (name.endsWith("-U")) {
                DS_VariantRandomizer.setVariant(member, factionList, player.getCommanderStats(),
                                                Archetype.ULTIMATE, 0.5f, 0f, false, new Random());
            }
        }
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }
}
