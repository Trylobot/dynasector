package data.scripts.variants;

import com.fs.starfarer.api.impl.campaign.ids.Factions;
import data.scripts.util.DS_Defs.Archetype;
import java.util.Random;

public class DS_VariantNamer {

    public static String getVariantName(String faction, Archetype archetype, float qualityFactor, Random r) {
        switch (archetype) {
            case ARCADE:
                return "Arcade";
            case BALANCED:
                switch (faction) {
                    case Factions.KOL:
                    case Factions.LUDDIC_CHURCH:
                    case Factions.LUDDIC_PATH:
                        return "Luddic";
                    case "exipirated":
                    case Factions.PIRATES:
                        return "Stolen";
                    case "templars":
                        return "Estendre";
                    case "SCY":
                        return "Combat";
                    case "diableavionics":
                    case "cabal":
                        if (qualityFactor < 0.25f) {
                            return "Outdated";
                        } else {
                            if (r.nextDouble() > 0.5) {
                                return "Multirole";
                            } else {
                                return "Standard";
                            }
                        }
                    case "ORA":
                        return "Standard";
                    default:
                        if (qualityFactor < 0.25f) {
                            return "Outdated";
                        } else {
                            if (r.nextDouble() > 0.5) {
                                return "Balanced";
                            } else {
                                return "Standard";
                            }
                        }
                }
            case STRIKE:
                switch (faction) {
                    case Factions.KOL:
                    case Factions.LUDDIC_CHURCH:
                    case Factions.LUDDIC_PATH:
                        return "Smiter";
                    case "exipirated":
                    case "mayorate":
                    case Factions.PIRATES:
                        return "Lancer";
                    case "templars":
                        return "Mittere";
                    case "SCY":
                        return "Brawler";
                    case "diableavionics":
                    case "cabal":
                        return "Blitz";
                    case "ORA":
                        return "Blaster";
                    default:
                        return "Strike";
                }
            case ELITE:
                switch (faction) {
                    case Factions.KOL:
                    case Factions.LUDDIC_CHURCH:
                    case Factions.LUDDIC_PATH:
                    case "cabal":
                        return "Holy";
                    case "exipirated":
                    case "mayorate":
                    case Factions.PIRATES:
                    case "tiandong":
                        return "Modified";
                    case "templars":
                        return "Eligere";
                    case "SCY":
                    case "diableavionics":
                        return "Veteran";
                    case "ORA":
                        return "Elite";
                    default:
                        if (qualityFactor < 0.25f) {
                            return "Standard";
                        } else {
                            return "Elite";
                        }
                }
            case ASSAULT:
                switch (faction) {
                    case Factions.KOL:
                    case Factions.LUDDIC_CHURCH:
                    case Factions.LUDDIC_PATH:
                        return "Evangelist";
                    case "exigency":
                    case "mayorate":
                    case Factions.PIRATES:
                    case "shadow_industry":
                    case Factions.TRITACHYON:
                    case "SCY":
                    case Factions.DERELICT:
                    case Factions.REMNANTS:
                        return "Attack";
                    case "citadeldefenders":
                    case "cabal":
                        return "Breaker";
                    case "templars":
                        return "Saltare";
                    case "tiandong":
                    case "ORA":
                        if (r.nextDouble() > 0.5) {
                            return "Assault";
                        } else {
                            return "Brawler";
                        }
                    case "diableavionics":
                        return "Frontline";
                    default:
                        return "Assault";
                }
            case SKIRMISH:
                switch (faction) {
                    case "blackrock_driveyards":
                    case Factions.INDEPENDENT:
                    case Factions.TRITACHYON:
                    case Factions.DERELICT:
                    case Factions.REMNANTS:
                        return "Fast Attack";
                    case "exigency":
                    case "exipirated":
                    case "mayorate":
                    case Factions.PIRATES:
                        return "Raider";
                    case Factions.HEGEMONY:
                    case "interstellarimperium":
                    case Factions.DIKTAT:
                        return "Patrol";
                    case "citadeldefenders":
                        return "Mobile";
                    case "templars":
                        return "Agilis";
                    case "SCY":
                        return "Recon";
                    case "diableavionics":
                    case "cabal":
                        return "Harasser";
                    default:
                        return "Skirmish";
                }
            case ARTILLERY:
                switch (faction) {
                    case "citadeldefenders":
                    case "interstellarimperium":
                    case Factions.KOL:
                    case Factions.LUDDIC_CHURCH:
                    case Factions.LUDDIC_PATH:
                    case Factions.DERELICT:
                    case Factions.REMNANTS:
                        return "Siege";
                    case "exipirated":
                    case "mayorate":
                    case Factions.PIRATES:
                    case "cabal":
                        return "Bombardment";
                    case Factions.HEGEMONY:
                    case Factions.INDEPENDENT:
                    case "shadow_industry":
                    case Factions.TRITACHYON:
                    case "tiandong":
                        if (r.nextDouble() > 0.5) {
                            return "Standoff";
                        } else {
                            return "Fire Support";
                        }
                    case "templars":
                        return "Atirier";
                    default:
                        return "Fire Support";
                }
            case CLOSE_SUPPORT:
                switch (faction) {
                    case "templars":
                        return "Fusiller";
                    default:
                        return "Close Support";
                }
            case SUPPORT:
                switch (faction) {
                    case "exipirated":
                    case Factions.PIRATES:
                    case "cabal":
                        return "Retrofitted";
                    case "templars":
                        return "Portare";
                    case "diableavionics":
                        return "Relief";
                    default:
                        return "Support";
                }
            case ESCORT:
                switch (faction) {
                    case Factions.KOL:
                    case Factions.LUDDIC_CHURCH:
                    case Factions.LUDDIC_PATH:
                    case "cabal":
                        return "Protector";
                    case "exipirated":
                    case Factions.PIRATES:
                        return "Watchdog";
                    case "templars":
                        return "Defensa";
                    case "citadeldefenders":
                        return "Guardian";
                    case "SCY":
                        return "Guard";
                    case "diableavionics":
                        return "Reinforcement";
                    case "ORA":
                    case Factions.DERELICT:
                    case Factions.REMNANTS:
                        return "Warden";
                    default:
                        return "Escort";
                }
            case FLEET:
                switch (faction) {
                    case "templars":
                        return "Loger";
                    case "blackrock_driveyards":
                    case "citadeldefenders":
                    case "exigency":
                    case "shadow_industry":
                    case Factions.TRITACHYON:
                        return "Corporate";
                    case "SCY":
                        return "Support";
                    case "diableavionics":
                    case "cabal":
                        return "Reserve";
                    case Factions.DERELICT:
                    case Factions.REMNANTS:
                        return "Autonomous";
                    default:
                        return "Fleet";
                }
            case ULTIMATE:
                switch (faction) {
                    case "blackrock_driveyards":
                    case "cabal":
                        return "Prototype";
                    case Factions.INDEPENDENT:
                    case "tiandong":
                        return "Master";
                    case "interstellarimperium":
                        if (qualityFactor < 0.5f) {
                            return "Imperial";
                        } else {
                            return "Maximum";
                        }
                    case Factions.KOL:
                    case Factions.LUDDIC_CHURCH:
                    case Factions.LUDDIC_PATH:
                        return "Exalted";
                    case "exipirated":
                    case "mayorate":
                    case Factions.PIRATES:
                        return "Heavily Modified";
                    case "citadeldefenders":
                    case "exigency":
                    case Factions.TRITACHYON:
                    case Factions.DERELICT:
                    case Factions.REMNANTS:
                        return "Super";
                    case "shadow_industry":
                        return "Shadow";
                    case "templars":
                        return "Magister";
                    case "SCY":
                        return "Army";
                    case "diableavionics":
                        return "Experimental";
                    case "ORA":
                        return "Ultimate";
                    default:
                        if (qualityFactor < 0.25f) {
                            return "Elite";
                        } else {
                            return "Ultimate";
                        }
                }
            default:
                return "Custom";
        }
    }
}
