package data.scripts.variants;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.ShieldAPI.ShieldType;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.loading.HullModSpecAPI;
import com.fs.starfarer.api.loading.VariantSource;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Defs.Archetype;
import data.scripts.util.DS_Defs.AsymmetryLevel;
import data.scripts.util.DS_Defs.FighterPreference;
import data.scripts.util.DS_Defs.SlotType;
import data.scripts.util.DS_Defs.WeaponPreference;
import data.scripts.util.DS_Util;
import data.scripts.variants.DS_Database.RandomizerFighterType;
import data.scripts.variants.DS_Database.RandomizerWeaponType;
import data.scripts.variants.DS_WeaponGrouper.WeaponGroupParam;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Logger;

public class DS_VariantRandomizer {

    public static final String RANDOMIZED_VARIANT_TAG = "ds_randomized";

    private static final Logger log = Global.getLogger(DS_VariantRandomizer.class);

    public static void setVariant(FleetMemberAPI ship, List<String> factionListInput, MutableCharacterStatsAPI stats,
                                  Archetype archetype, float qualityFactor, float bonusOP, boolean noobFriendly,
                                  Random r) {
        setVariant(ship, factionListInput, stats, null, qualityFactor, archetype, null, bonusOP, noobFriendly, r);
    }

    public static void setVariant(FleetMemberAPI ship, List<String> factionListInput, MutableCharacterStatsAPI stats,
                                  List<String> roleList, float qualityFactor, Archetype preferredArchetype,
                                  Map<Archetype, Float> archetypeWeights, float bonusOP, boolean noobFriendly,
                                  Random r) {
        List<String> factionList = new ArrayList<>(5);
        for (String faction : factionListInput) {
            if (!DS_Database.factionWeapons.containsKey(faction)) {
                factionList.add(Factions.INDEPENDENT);
            } else {
                factionList.add(faction);
            }
        }

        if (factionList.isEmpty()) {
            factionList.add(Factions.INDEPENDENT);
        }

        if (ship.isFighterWing()) {
            return;
        }

        ShipVariantAPI newVariant = ship.getVariant().clone();
        newVariant.setSource(VariantSource.REFIT);
        DS_Util.stripVariant(newVariant);
        newVariant.addTag(RANDOMIZED_VARIANT_TAG);

        Archetype archetype = DS_FleetRandomizer.getArchetypeFromRole(roleList, qualityFactor, preferredArchetype,
                                                                      archetypeWeights, r);

        ship.setVariant(createVariant(newVariant, factionList, stats, archetype, qualityFactor, bonusOP, noobFriendly,
                                      ship.isFlagship(), ship.isStation(), r),
                        false, true);

        if (ship.getVariant().getStationModules() != null) {
            for (String moduleSlot : ship.getVariant().getStationModules().keySet()) {
                ShipVariantAPI module = ship.getModuleVariant(moduleSlot);
                if (module.getHullSpec().getOrdnancePoints(stats) <= 0) {
                    continue;
                }

                ShipVariantAPI newModule = module.clone();
                newModule.setSource(VariantSource.REFIT);
                DS_Util.stripVariant(newModule);
                newModule.addTag(RANDOMIZED_VARIANT_TAG);

                //List<String> moduleRoleList = DS_FleetRandomizer.deconstructShip(module, factionList.get(0));
                Archetype moduleArchetype = DS_FleetRandomizer.getArchetypeFromRole(roleList, qualityFactor,
                                                                                    preferredArchetype, archetypeWeights,
                                                                                    r);

                newModule = createVariant(newModule, factionListInput, stats, moduleArchetype, qualityFactor, bonusOP,
                                          noobFriendly, false, ship.isStation(), r);
                ship.setModuleVariant(moduleSlot, newModule);
            }
        }
    }

    public static void upgradeVariant(FleetMemberAPI ship, List<String> factionListInput,
                                      MutableCharacterStatsAPI stats, Archetype archetype, float qualityFactor,
                                      float bonusOP, Random r) {
        upgradeVariant(ship, factionListInput, stats, null, qualityFactor, archetype, null, bonusOP, r);
    }

    public static void upgradeVariant(FleetMemberAPI ship, List<String> factionListInput,
                                      MutableCharacterStatsAPI stats, List<String> roleList,
                                      float qualityFactor, Archetype preferredArchetype,
                                      Map<Archetype, Float> archetypeWeights, float bonusOP, Random r) {
        List<String> factionList = new ArrayList<>(5);
        for (String faction : factionListInput) {
            if (DS_Database.factionWeapons.containsKey(faction)) {
                factionList.add(faction);
            }
        }
        if (factionList.isEmpty() || ship.isFighterWing()) {
            return;
        }

        Archetype archetype = DS_FleetRandomizer.getArchetypeFromRole(roleList, qualityFactor, preferredArchetype,
                                                                      archetypeWeights, r);

        ship.setVariant(doUpgrades(ship.getVariant(), factionList, stats, archetype, true, true, qualityFactor, bonusOP,
                                   ship.isFlagship(), ship.isStation(), r), false, true);

        if (ship.getVariant().getStationModules() != null) {
            for (String moduleSlot : ship.getVariant().getStationModules().keySet()) {
                ShipVariantAPI module = ship.getModuleVariant(moduleSlot);
                if (module.getHullSpec().getOrdnancePoints(stats) <= 0) {
                    continue;
                }

                Archetype moduleArchetype = DS_FleetRandomizer.getArchetypeFromRole(roleList, qualityFactor,
                                                                                    preferredArchetype, archetypeWeights,
                                                                                    r);

                ShipVariantAPI newModule = doUpgrades(module, factionList, stats, moduleArchetype, true, true,
                                                      qualityFactor, bonusOP, false, ship.isStation(), r);
                ship.setModuleVariant(moduleSlot, newModule);
            }
        }
    }

    private static ShipVariantAPI createVariant(ShipVariantAPI variant, List<String> factionList,
                                                MutableCharacterStatsAPI stats, Archetype archetype, float qualityFactor,
                                                float bonusOP, boolean noobFriendly, boolean flagship, boolean station,
                                                Random r) {
        ShipHullSpecAPI hullSpec = variant.getHullSpec();

        List<Map<String, Float>> weaponDataList = new ArrayList<>(5);
        for (String faction : factionList) {
            if (faction.contentEquals("player") ||
                    (faction.contentEquals("SCY") && !hullSpec.getHullId().startsWith("SCY_")) ||
                    (faction.contentEquals("shadow_industry") && !hullSpec.getHullId().startsWith("ms_"))) {
                weaponDataList.add(DS_Database.factionWeapons.get(Factions.INDEPENDENT));
            } else {
                Map<String, Float> list = DS_Database.factionWeapons.get(faction);
                if (list == null) {
                    continue;
                }
                weaponDataList.add(list);
            }
        }

        List<Map<String, Float>> fighterDataList = new ArrayList<>(5);
        for (String faction : factionList) {
            if (faction.contentEquals("player") ||
                    (faction.contentEquals("SCY") && !hullSpec.getHullId().startsWith("SCY_")) ||
                    (faction.contentEquals("shadow_industry") && !hullSpec.getHullId().startsWith("ms_"))) {
                fighterDataList.add(DS_Database.factionFighters.get(Factions.INDEPENDENT));
            } else {
                Map<String, Float> list = DS_Database.factionFighters.get(faction);
                if (list == null) {
                    continue;
                }
                fighterDataList.add(list);
            }
        }

        float attack, minAttack;
        float standoff, minStandoff;
        float alpha, minAlpha;
        float defense, minDefense;
        float closeRange, minCloseRange;
        float longRange, minLongRange;
        float disable, minDisable;
        float desiredHE;
        float desiredK;
        boolean highTier;
        int allowedMountsOP;

        int originalOP = variant.getUnusedOP(stats);
        int totalOP;
        if (stats == null) {
            totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
        } else {
            totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                              (stats.getShipOrdnancePointBonus().percentMod + bonusOP) /
                              100f) * stats.getShipOrdnancePointBonus().mult);
        }

        int totalHE = 0;
        int totalK = 0;
        int totalE = 0;
        int totalF = 0;
        int totalAttack = 0;
        int totalStandoff = 0;
        int totalAlpha = 0;
        int totalDefense = 0;
        int totalCloseRange = 0;
        int totalLongRange = 0;
        int totalDisable = 0;

        float interfere, minInterfere;
        float support, minSupport;
        int totalInterfere = 0;
        int totalSupport = 0;

        int sizeScalar;
        if (hullSpec.getHullSize() == HullSize.CAPITAL_SHIP) {
            sizeScalar = 100;
        } else if (hullSpec.getHullSize() == HullSize.CRUISER) {
            sizeScalar = 60;
        } else if (hullSpec.getHullSize() == HullSize.DESTROYER) {
            sizeScalar = 40;
        } else {
            sizeScalar = 20;
        }

        int bays = hullSpec.getFighterBays();
        if (variant.hasHullMod("converted_hangar")) {
            bays++;
        }
        boolean isCarrier = false;
        if ((variant.getHullSize() == HullSize.FRIGATE || variant.getHullSize() == HullSize.DESTROYER) && bays > 0) {
            isCarrier = true;
        }
        if (variant.getHullSize() == HullSize.CRUISER) {
            if (bays > 1) {
                isCarrier = true;
            }
        }
        if (variant.getHullSize() == HullSize.CAPITAL_SHIP) {
            if (bays > 2) {
                isCarrier = true;
            }
        }

        if (factionList.get(0).contentEquals(Factions.DERELICT)) {
            sizeScalar *= 0.5;
        }

        if (isCarrier) {
            sizeScalar *= 0.75;
        }

        HullmodFactors factor = new HullmodFactors(archetype);
        DS_Util.adjustHullmodFactorsForFaction(factor, factionList);
        DS_Util.adjustHullmodFactorsForHull(factor, hullSpec.getBaseHullId());

        float targetWeaponsOP = 0f;
        for (WeaponSlotAPI slot : hullSpec.getAllWeaponSlotsCopy()) {
            float slotOP = 0f;
            for (String faction : factionList) {
                slotOP += DS_WeaponPicker.getOPTargetForSlot(faction, slot.getSlotSize(), slot.getWeaponType(),
                                                             variant.getStatsForOpCosts());
            }
            targetWeaponsOP += slotOP / factionList.size();
        }

        float targetFightersOP = 0f;
        for (int i = 0; i < hullSpec.getFighterBays(); i++) {
            float bayOP = 0f;
            for (String faction : factionList) {
                bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
            }
            targetFightersOP += bayOP / factionList.size();
        }
        float targetMountsOP = targetWeaponsOP + targetFightersOP;

        variant.setVariantDisplayName(DS_VariantNamer.getVariantName(factionList.get(0), archetype, qualityFactor, r));

        if (archetype == Archetype.ARCADE) {
            attack = 1f;
            minAttack = 0f;

            standoff = 1f;
            minStandoff = 0f;

            alpha = 1f;
            minAlpha = 0f;

            defense = 1f;
            minDefense = 0f;

            closeRange = 1f;
            minCloseRange = 0f;

            longRange = 1f;
            minLongRange = 0f;

            disable = 1f;
            minDisable = 0f;

            interfere = 1f;
            minInterfere = 0f;

            support = 1f;
            minSupport = 0f;

            desiredHE = 0f;
            desiredK = 0f;

            highTier = false;

            totalOP = (int) (hullSpec.getOrdnancePoints(null) * 1.1f);

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.05 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = (int) (hullSpec.getOrdnancePoints(null) * 1.1f);
                    totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 20f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 30f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 50f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.025 * Math.min(1.0, opBufferFactor) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = (int) (hullSpec.getOrdnancePoints(null) * 1.1f);
                    totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.8f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP * 2f) / 3f);
        } else if (archetype == Archetype.BALANCED) {
            attack = 1f;
            minAttack = 0.25f;

            standoff = 1f;
            minStandoff = 0.25f;

            alpha = 0.75f;
            minAlpha = 0f;

            defense = 1f;
            minDefense = 0.1f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 0.5f;
            minLongRange = 0f;

            disable = 0.25f;
            minDisable = 0f;

            interfere = 0.5f;
            minInterfere = 0f;

            support = 0.5f;
            minSupport = 0f;

            desiredHE = 0.15f;
            desiredK = 0.3f;

            highTier = false;

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.05 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 20f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 30f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 50f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.1 * Math.min(1.0, opBufferFactor - 0.25) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.8f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP * 2f) / 3f);
        } else if (archetype == Archetype.STRIKE) {
            attack = 0.5f;
            minAttack = 0f;

            standoff = 0.5f;
            minStandoff = 0f;

            alpha = 3f;
            minAlpha = 0.6f;

            defense = 0.5f;
            minDefense = 0f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 0f;
            minLongRange = 0f;

            disable = 0.5f;
            minDisable = 0f;

            interfere = 1.5f;
            minInterfere = 0.3f;

            support = 0.25f;
            minSupport = 0f;

            desiredHE = 0.2f;
            desiredK = 0f;

            highTier = false;

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.075 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 20f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 30f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 50f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.05 * Math.min(1.0, opBufferFactor - 0.5) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.75f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP * 2f) / 3f);
        } else if (archetype == Archetype.ELITE) {
            attack = 1f;
            minAttack = 0.2f;

            standoff = 0.75f;
            minStandoff = 0.1f;

            alpha = 1f;
            minAlpha = 0.2f;

            defense = 0.75f;
            minDefense = 0.1f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 0.25f;
            minLongRange = 0f;

            disable = 1f;
            minDisable = 0f;

            interfere = 0.5f;
            minInterfere = 0f;

            support = 0.25f;
            minSupport = 0f;

            desiredHE = 0.1f;
            desiredK = 0.2f;

            highTier = true;

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.1 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 20f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 30f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 50f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.05 * Math.min(1.0, opBufferFactor - 0.5) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.75f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP * 3f) / 4f);
        } else if (archetype == Archetype.ASSAULT) {
            attack = 3f;
            minAttack = 0.5f;

            standoff = 0.5f;
            minStandoff = 0f;

            alpha = 0.5f;
            minAlpha = 0f;

            defense = 0.75f;
            minDefense = 0.1f;

            closeRange = 0.75f;
            minCloseRange = 0f;

            longRange = 0.25f;
            minLongRange = 0f;

            disable = 0.25f;
            minDisable = 0f;

            interfere = 0.25f;
            minInterfere = 0f;

            support = 0f;
            minSupport = 0f;

            desiredHE = 0.15f;
            desiredK = 0.2f;

            highTier = false;

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.05 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 20f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 30f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 50f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.05 * Math.min(1.0, opBufferFactor - 0.5) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.8f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP * 2f) / 3f);
        } else if (archetype == Archetype.SKIRMISH) {
            attack = 0.75f;
            minAttack = 0.15f;

            standoff = 0.75f;
            minStandoff = 0.15f;

            alpha = 0.5f;
            minAlpha = 0f;

            defense = 0.5f;
            minDefense = 0f;

            closeRange = 0f;
            minCloseRange = 0f;

            longRange = 1f;
            minLongRange = 0.2f;

            disable = 0.75f;
            minDisable = 0f;

            interfere = 1f;
            minInterfere = 0f;

            support = 0.5f;
            minSupport = 0f;

            desiredHE = 0f;
            desiredK = 0.25f;

            highTier = false;

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.075 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 20f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 30f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 50f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.075 * Math.min(1.0, opBufferFactor - 0.5) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.7f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP * 2f) / 3f);
        } else if (archetype == Archetype.ARTILLERY) {
            attack = 0.25f;
            minAttack = 0f;

            standoff = 0.5f;
            minStandoff = 0f;

            alpha = 0.25f;
            minAlpha = 0f;

            defense = 0.75f;
            minDefense = 0f;

            closeRange = 0f;
            minCloseRange = 0f;

            longRange = 3f;
            minLongRange = 0.6f;

            disable = 0.25f;
            minDisable = 0f;

            interfere = 0.5f;
            minInterfere = 0f;

            support = 0.25f;
            minSupport = 0f;

            desiredHE = 0.05f;
            desiredK = 0.25f;

            highTier = false;

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.025 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 20f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 30f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 50f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.15 * Math.min(1.0, opBufferFactor - 0.25) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.85f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP * 2f) / 3f);
        } else if (archetype == Archetype.CLOSE_SUPPORT) {
            attack = 1f;
            minAttack = 0.1f;

            standoff = 3f;
            minStandoff = 0.6f;

            alpha = 0.25f;
            minAlpha = 0f;

            defense = 1f;
            minDefense = 0.1f;

            closeRange = 0f;
            minCloseRange = 0f;

            longRange = 0.75f;
            minLongRange = 0f;

            disable = 0.75f;
            minDisable = 0f;

            interfere = 1f;
            minInterfere = 0f;

            support = 1f;
            minSupport = 0.1f;

            desiredHE = 0f;
            desiredK = 0.4f;

            highTier = false;

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.025 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 20f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 30f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 50f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.1 * Math.min(1.0, opBufferFactor - 0.25) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.8f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP * 2f) / 3f);
        } else if (archetype == Archetype.SUPPORT) {
            attack = 0.25f;
            minAttack = 0f;

            standoff = 1f;
            minStandoff = 0.1f;

            alpha = 0.25f;
            minAlpha = 0f;

            defense = 1f;
            minDefense = 0.1f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 1f;
            minLongRange = 0.1f;

            disable = 2f;
            minDisable = 0.1f;

            interfere = 1.5f;
            minInterfere = 0f;

            support = 2f;
            minSupport = 0.3f;

            desiredHE = 0f;
            desiredK = 0.2f;

            highTier = false;

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.025 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 10f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 15f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 25f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.15 * Math.min(1.0, opBufferFactor) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.75f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP) / 2f);
        } else if (archetype == Archetype.ESCORT) {
            attack = 0.5f;
            minAttack = 0f;

            standoff = 1f;
            minStandoff = 0.1f;

            alpha = 0f;
            minAlpha = 0f;

            defense = 3f;
            minDefense = 0.6f;

            closeRange = 0.5f;
            minCloseRange = 0f;

            longRange = 0f;
            minLongRange = 0f;

            disable = 1f;
            minDisable = 0.1f;

            interfere = 1f;
            minInterfere = 0f;

            support = 1f;
            minSupport = 0.2f;

            desiredHE = 0f;
            desiredK = 0f;

            highTier = false;

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.025 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 12f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 18f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 30f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.15 * Math.min(1.0, opBufferFactor - 0.5) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.7f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP) / 2f);
        } else if (archetype == Archetype.FLEET) {
            attack = 0.25f;
            minAttack = 0f;

            standoff = 0.5f;
            minStandoff = 0f;

            alpha = 0.25f;
            minAlpha = 0f;

            defense = 2f;
            minDefense = 0.25f;

            closeRange = 0f;
            minCloseRange = 0f;

            longRange = 1f;
            minLongRange = 0f;

            disable = 1f;
            minDisable = 0f;

            interfere = 0.5f;
            minInterfere = 0f;

            support = 0.75f;
            minSupport = 0f;

            desiredHE = 0f;
            desiredK = 0f;

            highTier = false;

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 10f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 15f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 25f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.05 * Math.min(1.0, opBufferFactor - 0.25) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.45f) + (totalOP - sizeScalar)) / 2f);
        } else if (archetype == Archetype.ULTIMATE) {
            attack = 1f;
            minAttack = 0.2f;

            standoff = 1f;
            minStandoff = 0.2f;

            alpha = 1f;
            minAlpha = 0.2f;

            defense = 0.75f;
            minDefense = 0f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 0.25f;
            minLongRange = 0f;

            disable = 0.5f;
            minDisable = 0f;

            interfere = 0.5f;
            minInterfere = 0f;

            support = 0.25f;
            minSupport = 0f;

            desiredHE = 0.1f;
            desiredK = 0.2f;

            highTier = true;

            if (stats == null) {
                totalOP = (int) (originalOP + originalOP * (bonusOP + 10f) / 100f);
            } else {
                totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                  (stats.getShipOrdnancePointBonus().percentMod +
                                   bonusOP + 10f) / 100f) *
                                 stats.getShipOrdnancePointBonus().mult);
            }

            if (DSModPlugin.hasSWP && DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance") &&
                    !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                    DS_Util.isHullmodCompatible("maximized_ordinance", variant)) {
                float mult = 0.75f * factor.elite;
                if (DS_Util.isHullmodOffered(factionList, "maximized_ordinance")) {
                    mult *= 2f;
                }
                if (mult * 0.1 >= r.nextDouble()) {
                    variant.addMod("maximized_ordinance");
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            if (DS_Util.isHullmodUnlocked(factionList, "converted_hangar") &&
                    !DS_Util.isHullmodBlocked("converted_hangar", archetype) &&
                    DS_Util.isHullmodCompatible("converted_hangar", variant)) {
                float hullmodCost;
                float desiredBuffer;
                switch (hullSpec.getHullSize()) {
                    case DESTROYER:
                        hullmodCost = 10f;
                        desiredBuffer = 30f;
                        break;
                    case CRUISER:
                        hullmodCost = 15f;
                        desiredBuffer = 45f;
                        break;
                    default:
                    case CAPITAL_SHIP:
                        hullmodCost = 20f;
                        desiredBuffer = 75f;
                        break;
                }
                float opBufferFactor = Math.max(0.25f, (totalOP - targetMountsOP - hullmodCost) / desiredBuffer);
                float mult = 1.5f * factor.fighters / (1f + factor.logistics);
                if (DS_Util.isHullmodOffered(factionList, "converted_hangar")) {
                    mult *= 2f;
                }
                if (mult * 0.05 * Math.min(1.0, opBufferFactor - 0.5) >= r.nextDouble()) {
                    variant.addMod("converted_hangar");
                    if (variant.computeOPCost(stats) >= totalOP) {
                        variant.removeMod("converted_hangar");
                    } else {
                        float bayOP = 0f;
                        for (String faction : factionList) {
                            bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
                        }
                        targetFightersOP += bayOP / factionList.size();
                        targetMountsOP = targetWeaponsOP + targetFightersOP;
                    }
                    originalOP = variant.getUnusedOP(stats);
                    if (stats == null) {
                        totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
                    } else {
                        totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                          (stats.getShipOrdnancePointBonus().percentMod + bonusOP) / 100f) *
                                         stats.getShipOrdnancePointBonus().mult);
                    }
                }
            }

            allowedMountsOP = (int) ((totalOP * (r.nextFloat() * 0.1f + 0.75f) +
                                      Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedMountsOP = (int) Math.max(allowedMountsOP, (allowedMountsOP + targetMountsOP * 3f) / 4f);
        } else {
            return variant;
        }

        if (factionList.get(0).contentEquals(Factions.LUDDIC_PATH)) {
            allowedMountsOP *= 0.75f;
            targetWeaponsOP *= 0.75f;
            targetFightersOP *= 0.75f;
            targetMountsOP = targetWeaponsOP + targetFightersOP;
        }

        if (factionList.get(0).contentEquals("diableavionics")) {
            int allowedMountsIncrease = Math.round((totalOP - allowedMountsOP) * 0.33f);
            allowedMountsOP += allowedMountsIncrease;

            float fightersRatio = targetFightersOP / Math.max(targetMountsOP, 1f);
            float targetFightersIncrease = Math.min((totalOP - targetFightersOP) * 0.5f * fightersRatio,
                                                    allowedMountsIncrease);
            targetFightersOP += targetFightersIncrease;
            targetWeaponsOP += Math.min((totalOP - targetWeaponsOP) * 0.33f,
                                        allowedMountsIncrease - targetFightersIncrease);
            targetMountsOP = targetWeaponsOP + targetFightersOP;
        }

        if (hullSpec.getBaseHullId().startsWith("SCY_")) {
            allowedMountsOP += Math.round((totalOP - allowedMountsOP) * 0.33f);
            targetWeaponsOP += (totalOP - targetWeaponsOP) * 0.33f;
            targetMountsOP = targetWeaponsOP + targetFightersOP;
        }

        if (hullSpec.getBaseHullId().startsWith("ora_") || factionList.get(0).contentEquals("ORA")) {
            longRange += 0.5f;
            longRange *= 1.25f;
            minLongRange += 0.1f;
        }

        if (allowedMountsOP <= 0) {
            allowedMountsOP = 1;
        }

        if (allowedMountsOP > totalOP) {
            allowedMountsOP = totalOP;
        }

        if (DSModPlugin.SHOW_DEBUG_INFO) {
            log.info("Creating " + hullSpec.getHullName() + " " + archetype.toString() + " variant" +
                    " (Total OP: " + totalOP + ")" +
                    " (Allowed Mounts OP: " + allowedMountsOP + ")" +
                    " (Target Weapons OP: " + targetWeaponsOP + ")" +
                    " (Target Fighters OP: " + targetFightersOP + ")"
            );
        }

        FighterPreference fighterPreference = DS_Util.getHullFighterPreference(hullSpec.getBaseHullId());
        WeaponPreference weaponPreference = DS_Util.getHullWeaponPreference(hullSpec.getBaseHullId());
        AsymmetryLevel asymmetric = DS_Util.doesHullPreferAsymmetry(hullSpec.getBaseHullId());
        boolean broadside = DS_Util.doesHullPreferBroadsideWeapons(hullSpec.getBaseHullId());

        if (factionList.get(0).contentEquals("SCY")) {
            minDefense = (minDefense < 0.5f) ? (minDefense + 0.5f) / 2f : minDefense;
        }

        int currentMountsOP;

        float fightersRatio = targetFightersOP / Math.max(targetMountsOP, 1f);
        float ratioFudge = ((float) Math.sqrt(factor.fighters) - 1f) * 0.05f + 0.05f;
        fightersRatio = Math.max(Math.min(fightersRatio + ratioFudge, 1f), fightersRatio / 2f);
        int allowedFightersOP = Math.round(allowedMountsOP * fightersRatio);
        int beforeFightersOP = variant.computeOPCost(stats);

        bays = hullSpec.getFighterBays();
        if (variant.hasHullMod("converted_hangar")) {
            bays++;
        }

        for (String wingId : variant.getWings()) {
            if (wingId.isEmpty() || variant.getNonBuiltInWings().contains(wingId)) {
                continue;
            }

            DS_FighterEntry fighter = DS_Database.masterFighterList.get(wingId);

            if (fighter == null) {
                log.warn("Found unrecognized built-in wing on " + hullSpec.getHullName() + ": " + wingId);
                continue;
            }

            int opImpact = fighter.OP + 10;
            totalHE += fighter.highExplosive ? opImpact : 0;
            totalK += fighter.kinetic ? opImpact : 0;
            totalE += fighter.energy ? opImpact : 0;
            totalF += fighter.fragmentation ? opImpact : 0;
            totalAttack += fighter.attack ? opImpact : 0;
            totalInterfere += fighter.interfere ? opImpact : 0;
            totalAlpha += fighter.alpha ? opImpact : 0;
            totalDefense += fighter.defense ? opImpact : 0;
            totalSupport += fighter.support ? opImpact : 0;
            totalCloseRange += fighter.closeRange ? opImpact : 0;
            totalLongRange += fighter.longRange ? opImpact : 0;
            totalDisable += fighter.disable ? opImpact : 0;
        }

        for (int i = 0; i < bays; i++) {
            /* Skip built-ins! */
            if (variant.getWing(i) != null) {
                continue;
            }

            int currentFightersOP = variant.computeOPCost(stats) - beforeFightersOP;
            currentMountsOP = variant.computeOPCost(stats) - beforeFightersOP;

            int allowedOpForModLevels = allowedFightersOP + 10 * bays;
            float minAttackFighters = minAttack * 0.5f;
            float minCloseRangeFighters = minCloseRange * 0.5f;
            float minLongRangeFighters = minLongRange * 0.5f;
            float minDisableFighters = minDisable * 0.5f;

            float modAttack = attack;
            if (totalAttack < (int) (allowedOpForModLevels * minAttackFighters)) {
                modAttack *= allowedOpForModLevels / (float) (allowedOpForModLevels + 10 + totalAttack -
                                                              currentFightersOP);
                if (currentFightersOP < (int) (allowedOpForModLevels * minAttackFighters)) {
                    modAttack /= Math.max(1f - minAttackFighters, 0.1f);
                }
            }
            float modInterfere = interfere;
            if (totalInterfere < (int) (allowedOpForModLevels * minInterfere)) {
                modInterfere *= allowedOpForModLevels / (float) (allowedOpForModLevels + 10 + totalInterfere -
                                                                 currentFightersOP);
                if (currentFightersOP < (int) (allowedOpForModLevels * minInterfere)) {
                    modInterfere /= Math.max(1f - minInterfere, 0.1f);
                }
            }
            float modAlpha = alpha;
            if (totalAlpha < (int) (allowedOpForModLevels * minAlpha)) {
                modAlpha *= allowedOpForModLevels / (float) (allowedOpForModLevels + 2 + totalAlpha - currentFightersOP);
                if (currentFightersOP < (int) (allowedOpForModLevels * minAlpha)) {
                    modAlpha /= Math.max(1f - minAlpha, 0.1f);
                }
            }
            float modDefense = defense;
            if (totalDefense < (int) (allowedOpForModLevels * minDefense)) {
                modDefense *= allowedOpForModLevels / (float) (allowedOpForModLevels + 4 + totalDefense -
                                                               currentFightersOP);
                if (currentFightersOP < (int) (allowedOpForModLevels * minDefense)) {
                    modDefense /= Math.max(1f - minDefense, 0.1f);
                }
            }
            float modSupport = support;
            if (totalSupport < (int) (allowedOpForModLevels * minSupport)) {
                modSupport *= allowedOpForModLevels / (float) (allowedOpForModLevels + 4 + totalSupport -
                                                               currentFightersOP);
                if (currentFightersOP < (int) (allowedOpForModLevels * minSupport)) {
                    modSupport /= Math.max(1f - minSupport, 0.1f);
                }
            }
            float modCloseRange = closeRange;
            if (totalCloseRange < (int) (allowedOpForModLevels * minCloseRangeFighters)) {
                modCloseRange *= allowedOpForModLevels / (float) (allowedOpForModLevels + 4 + totalCloseRange -
                                                                  currentFightersOP);
                if (currentFightersOP < (int) (allowedOpForModLevels * minCloseRangeFighters)) {
                    modCloseRange /= Math.max(1f - minCloseRangeFighters, 0.1f);
                }
            }
            float modLongRange = longRange;
            if (totalLongRange < (int) (allowedOpForModLevels * minLongRangeFighters)) {
                modLongRange *= allowedOpForModLevels / (float) (allowedOpForModLevels + 6 + totalLongRange -
                                                                 currentFightersOP);
                if (currentFightersOP < (int) (allowedOpForModLevels * minLongRangeFighters)) {
                    modLongRange /= Math.max(1f - minLongRangeFighters, 0.1f);
                }
            }
            float modDisable = disable;
            if (totalDisable < (int) (allowedOpForModLevels * minDisableFighters)) {
                modDisable *= allowedOpForModLevels / (float) (allowedOpForModLevels + 8 + totalDisable -
                                                               currentFightersOP);
                if (currentFightersOP < (int) (allowedOpForModLevels * minDisableFighters)) {
                    modDisable /= Math.max(1f - minDisableFighters, 0.1f);
                }
            }

            if (variant.hasHullMod("converted_hangar")) {
                modAlpha *= 0.25f;
            }

            DamageType preferredDamageType = null;
            float modDesiredHE = desiredHE;
            if (totalHE + totalE / 2 < (int) (allowedOpForModLevels * desiredHE)) {
                modDesiredHE *= allowedOpForModLevels / (float) (allowedOpForModLevels + 10 + totalHE + totalE / 2 -
                                                                 currentFightersOP);
            }
            float modDesiredK = desiredK;
            if (totalK + totalE / 2 < (int) (allowedOpForModLevels * desiredK)) {
                modDesiredK *= allowedOpForModLevels / (float) (allowedOpForModLevels + 10 + totalK + totalE / 2 -
                                                                currentFightersOP);
            }
            if (modDesiredHE > modDesiredK && modDesiredHE >= 0.4f) {
                preferredDamageType = DamageType.HIGH_EXPLOSIVE;
            } else if (modDesiredK > modDesiredHE && modDesiredK >= 0.4f) {
                preferredDamageType = DamageType.KINETIC;
            }

            // Can't really go higher than 2.5x price (~25 OP)
            float fightersBudgetFactor = Math.min(Math.max((allowedFightersOP - currentFightersOP) /
                  Math.max(targetFightersOP, 1f), 0.05f), 2.5f);

            String wingId = null;

            EnumMap<RandomizerFighterType, Float> coefficients = DS_Database.shipFighterCoefficients.get(
                                                  DS_Util.getNonDHullId(hullSpec));

            wingId =
            DS_FighterPicker.chooseFighter(factionList, coefficients, fighterDataList, wingId, fighterPreference,
                                           preferredDamageType,
                                           (highTier ? ((1f + qualityFactor) / 2f) : qualityFactor),
                                           (allowedFightersOP - currentFightersOP), (totalOP - currentMountsOP),
                                           modAttack, modInterfere, modAlpha, modDefense, modSupport,
                                           modCloseRange, modLongRange, modDisable, fightersBudgetFactor,
                                           station, noobFriendly, variant.getStatsForOpCosts(), r);

            if (wingId != null) {
                DS_FighterEntry fighter = DS_Database.masterFighterList.get(wingId);

                int opImpact = fighter.OP + 10;
                totalHE += fighter.highExplosive ? opImpact : 0;
                totalK += fighter.kinetic ? opImpact : 0;
                totalE += fighter.energy ? opImpact : 0;
                totalF += fighter.fragmentation ? opImpact : 0;
                totalAttack += fighter.attack ? opImpact : 0;
                totalInterfere += fighter.interfere ? opImpact : 0;
                totalAlpha += fighter.alpha ? opImpact : 0;
                totalDefense += fighter.defense ? opImpact : 0;
                totalSupport += fighter.support ? opImpact : 0;
                totalCloseRange += fighter.closeRange ? opImpact : 0;
                totalLongRange += fighter.longRange ? opImpact : 0;
                totalDisable += fighter.disable ? opImpact : 0;

                variant.setWingId(i, wingId);
            }

            float bayOP = 0f;
            for (String faction : factionList) {
                bayOP += DS_FighterPicker.getOPTargetForBay(faction, variant.getStatsForOpCosts());
            }
            targetFightersOP -= bayOP / factionList.size();
        }

        /* Don't skew weapon choices too heavily on carriers */
        float fighterFulfilmentFactor = 0.5f;
        totalHE *= fighterFulfilmentFactor;
        totalK *= fighterFulfilmentFactor;
        totalE *= fighterFulfilmentFactor;
        totalAttack *= fighterFulfilmentFactor;
        totalAlpha *= fighterFulfilmentFactor;
        totalDefense *= fighterFulfilmentFactor;
        totalCloseRange *= fighterFulfilmentFactor;
        totalLongRange *= fighterFulfilmentFactor;
        totalDisable *= fighterFulfilmentFactor;

        Map<WeaponSize, Map<WeaponType, Map<Float, String>>> masterSlotsMap = new EnumMap<>(WeaponSize.class);
        for (WeaponSize size : WeaponSize.values()) {
            Map<WeaponType, Map<Float, String>> typeMap = new EnumMap<>(WeaponType.class);
            for (WeaponType type : WeaponType.values()) {
                typeMap.put(type, new HashMap<Float, String>(20));
            }
            masterSlotsMap.put(size, typeMap);
        }

        currentMountsOP = variant.computeOPCost(stats) - beforeFightersOP;
        int allowedWeaponsOP = allowedMountsOP - currentMountsOP;
        int beforeWeaponsOP = variant.computeOPCost(stats);

        List<WeaponSlotAPI> slots = hullSpec.getAllWeaponSlotsCopy();
        Boolean sortSlots = DS_Database.hullSortSlots.get(DS_Util.getNonDHullId(hullSpec));
        if (sortSlots == null || sortSlots == true) {
            Collections.sort(slots, DS_Util.SLOT_SORTER);
        }

        Map<Integer, List<String>> weaponChoiceMap = new HashMap<>(slots.size());
        for (WeaponSlotAPI slot : slots) {
            if (slot.isDecorative() || slot.isSystemSlot() || slot.isStationModule() ||
                    slot.getWeaponType() == WeaponType.LAUNCH_BAY) {
                continue;
            }

            String slotId = slot.getId();

            if (slot.isBuiltIn()) {
                if (variant.getWeaponId(slotId) == null) {
                    continue;
                }

                DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(variant.getWeaponId(slotId));

                if (weapon == null) {
                    log.warn("Found unrecognized built-in weapon on " + hullSpec.getHullName() + ": " +
                            variant.getWeaponId(slotId));
                    continue;
                }

                totalHE += weapon.damageType == DamageType.HIGH_EXPLOSIVE ? weapon.OP : 0;
                totalK += weapon.damageType == DamageType.KINETIC ? weapon.OP : 0;
                totalE += weapon.damageType == DamageType.ENERGY ? weapon.OP : 0;
                totalF += weapon.damageType == DamageType.FRAGMENTATION ? weapon.OP : 0;
                totalAttack += weapon.attack ? weapon.OP : 0;
                totalStandoff += weapon.standoff ? weapon.OP : 0;
                totalAlpha += weapon.alpha ? weapon.OP : 0;
                totalDefense += weapon.defense ? weapon.OP : 0;
                totalCloseRange += weapon.closeRange ? weapon.OP : 0;
                totalLongRange += weapon.longRange ? weapon.OP : 0;
                totalDisable += weapon.disable ? weapon.OP : 0;

                continue;
            }

            float normalizedAngle = 0;
            if (slot.getAngle() > 180) {
                normalizedAngle = 360 - slot.getAngle();
            } else if (slot.getAngle() > 0) {
                normalizedAngle = slot.getAngle();
            } else if (slot.getAngle() < 0) {
                normalizedAngle = -slot.getAngle();
            }
            float uniqueKey = normalizedAngle * 360f + slot.getArc();

            int currentWeaponsOP = variant.computeOPCost(stats) - beforeWeaponsOP;
            currentMountsOP = variant.computeOPCost(stats) - beforeFightersOP;

            SlotType slotType;
            boolean slotFront = Math.abs(slot.getAngle()) - slot.getArc() / 2f <= 0f || slot.getArc() >= 315f;
            boolean slotTurret = slot.getArc() >= 45f;
            if (slot.getArc() <= 120f && slot.getArc() >= 45f && Math.abs(normalizedAngle) <= slot.getArc() / 2f - 22.5f) {
                slotType = SlotType.FRONT;
            } else if (slot.getArc() <= 225f && slot.getArc() >= 45f && Math.abs(normalizedAngle) >= 135f) {
                slotType = SlotType.BACK;
            } else if (slot.getArc() <= 120f && slot.getArc() >= 45f) {
                slotType = SlotType.SIDE;
            } else if (slot.getArc() <= 225 && slot.getArc() >= 120f) {
                slotType = SlotType.HEMI;
            } else if (slot.getArc() >= 225f) {
                slotType = SlotType.WIDE;
            } else if (normalizedAngle >= 22.5f) {
                slotType = SlotType.CROOKED;
            } else {
                slotType = SlotType.BORESIGHT;
            }

            float modAttack = attack;
            if (totalAttack < (int) (allowedWeaponsOP * minAttack)) {
                modAttack *= allowedWeaponsOP / (float) (allowedWeaponsOP + 10 + totalAttack - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minAttack)) {
                    modAttack /= Math.max(1f - minAttack, 0.1f);
                }
            }
            float modStandoff = standoff;
            if (totalStandoff < (int) (allowedWeaponsOP * minStandoff)) {
                modStandoff *= allowedWeaponsOP / (float) (allowedWeaponsOP + 10 + totalStandoff - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minStandoff)) {
                    modStandoff /= Math.max(1f - minStandoff, 0.1f);
                }
            }
            float modAlpha = alpha;
            if (totalAlpha < (int) (allowedWeaponsOP * minAlpha)) {
                modAlpha *= allowedWeaponsOP / (float) (allowedWeaponsOP + 2 + totalAlpha - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minAlpha)) {
                    modAlpha /= Math.max(1f - minAlpha, 0.1f);
                }
            }
            float modDefense = defense;
            if (totalDefense < (int) (allowedWeaponsOP * minDefense)) {
                modDefense *= allowedWeaponsOP / (float) (allowedWeaponsOP + 4 + totalDefense - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minDefense)) {
                    modDefense /= Math.max(1f - minDefense, 0.1f);
                }
            }
            float modCloseRange = closeRange;
            if (totalCloseRange < (int) (allowedWeaponsOP * minCloseRange)) {
                modCloseRange *= allowedWeaponsOP / (float) (allowedWeaponsOP + 4 + totalCloseRange - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minCloseRange)) {
                    modCloseRange /= Math.max(1f - minCloseRange, 0.1f);
                }
            }
            float modLongRange = longRange;
            if (totalLongRange < (int) (allowedWeaponsOP * minLongRange)) {
                modLongRange *= allowedWeaponsOP / (float) (allowedWeaponsOP + 6 + totalLongRange - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minLongRange)) {
                    modLongRange /= Math.max(1f - minLongRange, 0.1f);
                }
            }
            float modDisable = disable;
            if (totalDisable < (int) (allowedWeaponsOP * minDisable)) {
                modDisable *= allowedWeaponsOP / (float) (allowedWeaponsOP + 8 + totalDisable - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minDisable)) {
                    modDisable /= Math.max(1f - minDisable, 0.1f);
                }
            }

            DamageType preferredDamageType = null;
            float modDesiredHE = desiredHE;
            if (totalHE + totalE / 2 + totalF / 4 < (int) (allowedWeaponsOP * desiredHE)) {
                modDesiredHE *= allowedWeaponsOP / (float) (allowedWeaponsOP + 10 + totalHE + totalE / 2 + totalF / 4 -
                                                            currentWeaponsOP);
            }
            float modDesiredK = desiredK;
            if (totalK + totalE / 2 + totalF / 4 < (int) (allowedWeaponsOP * desiredK)) {
                modDesiredK *= allowedWeaponsOP / (float) (allowedWeaponsOP + 10 + totalK + totalE / 2 + totalF / 4 -
                                                           currentWeaponsOP);
            }
            if (modDesiredHE > modDesiredK && modDesiredHE >= 0.4f) {
                preferredDamageType = DamageType.HIGH_EXPLOSIVE;
            } else if (modDesiredK > modDesiredHE && modDesiredK >= 0.4f) {
                preferredDamageType = DamageType.KINETIC;
            }

            Map<Float, String> slotsMap = masterSlotsMap.get(slot.getSlotSize()).get(slot.getWeaponType());
            if (slotsMap == null) {
                continue;
            }

            // Can't really go higher than 2x price (~10/20/40 OP)
            float weaponsBudgetFactor = Math.min(Math.max((allowedWeaponsOP - currentWeaponsOP) / Math.max(
                  targetWeaponsOP, 1f), 0.25f), 2f);

            String weaponId = null;
            if (slotsMap.containsKey(uniqueKey)) {
                weaponId = slotsMap.get(uniqueKey);
            }

            Map<String, EnumMap<RandomizerWeaponType, Float>> shipSlotDef = DS_Database.shipSlotCoefficients.get(
                                                              DS_Util.getNonDHullId(hullSpec));
            EnumMap<RandomizerWeaponType, Float> coefficients;
            if (shipSlotDef == null) {
                coefficients = null;
            } else {
                coefficients = shipSlotDef.get(slotId);
            }

            if (DSModPlugin.SHOW_DEBUG_INFO) {
                log.info("Picking for slot " + slotId +
                        " (Remaining Allowed OP: " + (allowedWeaponsOP - currentWeaponsOP) + ")" +
                        " (Remaining Total OP: " + (totalOP - currentMountsOP) + ")" +
                        " (Target OP: " + targetWeaponsOP + ")" +
                        " (Budget Factor: " + weaponsBudgetFactor + ")" +
                        " (Preference: " + weaponId + ")"
                );
            }

            weaponId = DS_WeaponPicker.chooseWeapon(factionList, coefficients, weaponDataList, weaponId, slotType,
                                                    slotFront, slotTurret, weaponPreference, slot.getSlotSize(),
                                                    slot.getWeaponType(), preferredDamageType,
                                                    (highTier ? ((1f + qualityFactor) / 2f) : qualityFactor),
                                                    (allowedWeaponsOP - currentWeaponsOP), (totalOP - currentMountsOP),
                                                    modAttack, modStandoff, modAlpha, modDefense, modCloseRange,
                                                    modLongRange, modDisable, weaponsBudgetFactor, asymmetric, broadside,
                                                    station, noobFriendly, weaponChoiceMap, variant.getStatsForOpCosts(),
                                                    r);

            if (DSModPlugin.SHOW_DEBUG_INFO) {
                log.info("Picked " + weaponId);
            }

            if (weaponId != null) {
                DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(weaponId);

                totalHE += weapon.damageType == DamageType.HIGH_EXPLOSIVE ? weapon.OP : 0;
                totalK += weapon.damageType == DamageType.KINETIC ? weapon.OP : 0;
                totalE += weapon.damageType == DamageType.ENERGY ? weapon.OP : 0;
                totalAttack += weapon.attack ? weapon.OP : 0;
                totalStandoff += weapon.standoff ? weapon.OP : 0;
                totalAlpha += weapon.alpha ? weapon.OP : 0;
                totalDefense += weapon.defense ? weapon.OP : 0;
                totalCloseRange += weapon.closeRange ? weapon.OP : 0;
                totalLongRange += weapon.longRange ? weapon.OP : 0;
                totalDisable += weapon.disable ? weapon.OP : 0;

                switch (weapon.size) {
                    case SMALL:
                        switch (weapon.type) {
                            case BALLISTIC:
                                masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.BALLISTIC).put(uniqueKey, weaponId);
                                if (slot.getSlotSize() == WeaponSize.MEDIUM) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.BALLISTIC).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.COMPOSITE) {
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.HYBRID) {
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.UNIVERSAL) {
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.UNIVERSAL).
                                            put(uniqueKey, weaponId);
                                }
                                break;
                            case ENERGY:
                                masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.ENERGY).put(uniqueKey, weaponId);
                                if (slot.getSlotSize() == WeaponSize.MEDIUM) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.ENERGY).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.HYBRID) {
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.SYNERGY) {
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.UNIVERSAL) {
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.UNIVERSAL).
                                            put(uniqueKey, weaponId);
                                }
                                break;
                            case MISSILE:
                                masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.MISSILE).put(uniqueKey, weaponId);
                                if (slot.getSlotSize() == WeaponSize.MEDIUM) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.MISSILE).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.COMPOSITE) {
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.SYNERGY) {
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.UNIVERSAL) {
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.SMALL).get(WeaponType.UNIVERSAL).
                                            put(uniqueKey, weaponId);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case MEDIUM:
                        switch (weapon.type) {
                            case BALLISTIC:
                                masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.BALLISTIC).put(uniqueKey, weaponId);
                                if (slot.getSlotSize() == WeaponSize.LARGE) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.BALLISTIC).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.COMPOSITE) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.HYBRID) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.UNIVERSAL) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.UNIVERSAL).
                                            put(uniqueKey, weaponId);
                                }
                                break;
                            case ENERGY:
                                masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.ENERGY).put(uniqueKey, weaponId);
                                if (slot.getSlotSize() == WeaponSize.LARGE) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.ENERGY).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.HYBRID) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.SYNERGY) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.UNIVERSAL) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.UNIVERSAL).
                                            put(uniqueKey, weaponId);
                                }
                                break;
                            case MISSILE:
                                masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.MISSILE).put(uniqueKey, weaponId);
                                if (slot.getSlotSize() == WeaponSize.LARGE) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.MISSILE).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.COMPOSITE) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.SYNERGY) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.UNIVERSAL) {
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.MEDIUM).get(WeaponType.UNIVERSAL).
                                            put(uniqueKey, weaponId);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case LARGE:
                        switch (weapon.type) {
                            case BALLISTIC:
                                masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.BALLISTIC).put(uniqueKey, weaponId);
                                if (slot.getWeaponType() == WeaponType.COMPOSITE) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.HYBRID) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.UNIVERSAL) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.UNIVERSAL).
                                            put(uniqueKey, weaponId);
                                }
                                break;
                            case ENERGY:
                                masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.ENERGY).put(uniqueKey, weaponId);
                                if (slot.getWeaponType() == WeaponType.HYBRID) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.SYNERGY) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.UNIVERSAL) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.HYBRID).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.UNIVERSAL).
                                            put(uniqueKey, weaponId);
                                }
                                break;
                            case MISSILE:
                                masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.MISSILE).put(uniqueKey, weaponId);
                                if (slot.getWeaponType() == WeaponType.COMPOSITE) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.SYNERGY) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                } else if (slot.getWeaponType() == WeaponType.UNIVERSAL) {
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.COMPOSITE).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.SYNERGY).
                                            put(uniqueKey, weaponId);
                                    masterSlotsMap.get(WeaponSize.LARGE).get(WeaponType.UNIVERSAL).
                                            put(uniqueKey, weaponId);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                }

                variant.addWeapon(slotId, weaponId);
            }

            float slotOP = 0f;
            for (String faction : factionList) {
                slotOP += DS_WeaponPicker.getOPTargetForSlot(faction, slot.getSlotSize(), slot.getWeaponType(),
                                                             variant.getStatsForOpCosts());
            }
            targetWeaponsOP -= slotOP / factionList.size();
        }

        return doUpgrades(variant, factionList, stats, archetype, false, false, qualityFactor, bonusOP, flagship,
                          station, r);
    }

    private static ShipVariantAPI doUpgrades(ShipVariantAPI variant, List<String> factionList,
                                             MutableCharacterStatsAPI stats, Archetype archetype, boolean createNew,
                                             boolean onlyUpgrade, float qualityFactor, float bonusOP, boolean flagship,
                                             boolean station, Random r) {
        int beamMounts = 0;
        int missMounts = 0;
        int ituMounts = 0;
        int fcsMounts = 0;
        int trackingMounts = 0;
        int ipdaiMounts = 0; // Does not include PD or Strike
        int limitedAmmoMounts = 0;
        int limitedAmmoMissMounts = 0;
        int turretMounts = 0;
        ShipVariantAPI newVariant;
        if (createNew) {
            newVariant = variant.clone();
            newVariant.setSource(VariantSource.REFIT);
            newVariant.addTag(RANDOMIZED_VARIANT_TAG);
        } else {
            newVariant = variant;
        }

        ShipHullSpecAPI hullSpec = variant.getHullSpec();

        for (WeaponSlotAPI slot : hullSpec.getAllWeaponSlotsCopy()) {
            String weaponId = newVariant.getWeaponId(slot.getId());
            DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(weaponId);
            if (weapon == null) {
                continue;
            }

            if (slot.getArc() >= 45f) {
                turretMounts += 1;
            }

            EnumSet<AIHints> hints = Global.getSettings().getWeaponSpec(weaponId).getAIHints();
            if (slot.getArc() >= 45f &&
                    (weapon.type == WeaponType.BALLISTIC || weapon.type == WeaponType.ENERGY) &&
                    !weapon.alpha &&
                    !DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.INDIRECT_FIRE).contains(weaponId) &&
                    !DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).contains(weaponId) &&
                    !DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).contains(weaponId)) {
                if (weapon.size == WeaponSize.SMALL) {
                    trackingMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    trackingMounts += 2;
                } else {
                    trackingMounts += 4;
                }
            }
            if (slot.getArc() >= 45f &&
                    (weapon.type == WeaponType.BALLISTIC || weapon.type == WeaponType.ENERGY) &&
                    !hints.contains(AIHints.PD) &&
                    !hints.contains(AIHints.PD_ONLY) &&
                    !hints.contains(AIHints.STRIKE) &&
                    weapon.size == WeaponSize.SMALL) {
                ipdaiMounts += 1;
                if (weapon.defense) {
                    ipdaiMounts += 1;
                }
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).contains(weaponId) ||
                    DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).contains(weaponId)) {
                if (weapon.size == WeaponSize.SMALL) {
                    beamMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    beamMounts += 2;
                } else {
                    beamMounts += 4;
                }
            }
            if (weapon.type == WeaponType.MISSILE) {
                if (weapon.size == WeaponSize.SMALL) {
                    missMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    missMounts += 2;
                } else {
                    missMounts += 4;
                }
            }
            if (weapon.limitedAmmo && weapon.type == WeaponType.MISSILE) {
                if (weapon.size == WeaponSize.SMALL) {
                    limitedAmmoMissMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    limitedAmmoMissMounts += 2;
                } else {
                    limitedAmmoMissMounts += 4;
                }
            }
            if (weapon.limitedAmmo &&
                    weapon.type != WeaponType.MISSILE) {
                if (weapon.size == WeaponSize.SMALL) {
                    limitedAmmoMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    limitedAmmoMounts += 2;
                } else {
                    limitedAmmoMounts += 4;
                }
            }
            if (weapon.type == WeaponType.BALLISTIC || weapon.type == WeaponType.ENERGY) {
                ituMounts += 1;
            }
            if ((weapon.type == WeaponType.BALLISTIC || weapon.type == WeaponType.ENERGY) &&
                    !DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).contains(weaponId) &&
                    !DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).contains(weaponId)) {
                fcsMounts += 1;
            }
        }

        String hullId = hullSpec.getBaseHullId();

        int originalOP = hullSpec.getOrdnancePoints(null);
        int totalOP;
        if (stats == null) {
            totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
        } else {
            totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                              (stats.getShipOrdnancePointBonus().percentMod + bonusOP) /
                              100f) * stats.getShipOrdnancePointBonus().mult);
        }

        int weapOPcost = newVariant.computeWeaponOPCost(stats);
        int freeOP = (totalOP - newVariant.computeOPCost(stats));

        float sizeFactor;
        switch (hullSpec.getHullSize()) {
            default:
            case FRIGATE:
                sizeFactor = 0.5f;
                break;
            case DESTROYER:
                sizeFactor = 1f;
                break;
            case CRUISER:
                sizeFactor = 1.5f;
                break;
            case CAPITAL_SHIP:
                sizeFactor = 2.5f;
                break;
        }

        HullmodFactors factor = new HullmodFactors(archetype);
        if (archetype == Archetype.ULTIMATE) {
            if (stats == null) {
                totalOP = (int) (originalOP + originalOP * (bonusOP + 10f) / 100f);
            } else {
                totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP *
                                  (stats.getShipOrdnancePointBonus().percentMod + bonusOP + 10f) / 100f) *
                                 stats.getShipOrdnancePointBonus().mult);
            }
        }

        DS_Util.adjustHullmodFactorsForFaction(factor, factionList);
        DS_Util.adjustHullmodFactorsForHull(factor, hullId);

        if (!newVariant.getFittedWeaponSlots().isEmpty()) {
            newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, DS_Util.chooseWeaponGroupConfig(hullId));
        }

        if (DSModPlugin.hasSWP && !DS_Util.isHullmodBlocked("maximized_ordinance", archetype) &&
                DS_Util.isHullmodCompatible("maximized_ordinance", newVariant) && onlyUpgrade &&
                DS_Util.isHullmodUnlocked(factionList, "maximized_ordinance")) {
            if (Math.pow(weapOPcost / (double) totalOP, 1.0 / (factor.elite + 0.1)) >= (1f - r.nextFloat() *
                                                                                        r.nextFloat() *
                                                                                        r.nextFloat())) {
                newVariant.addMod("maximized_ordinance");
                freeOP = totalOP - newVariant.computeOPCost(stats);
            }
        }

        if (freeOP > 0) {
            Collection<String> builtIn = DS_Util.getBuiltInHullMods(variant);

            int maxVents;
            if (hullSpec.getHullSize() == HullSize.CAPITAL_SHIP) {
                maxVents = 50;
            } else if (hullSpec.getHullSize() == HullSize.CRUISER) {
                maxVents = 30;
            } else if (hullSpec.getHullSize() == HullSize.DESTROYER) {
                maxVents = 20;
            } else {
                maxVents = 10;
            }
            if ((stats == null && bonusOP >= 6.66f) || (stats != null && stats.getSkillLevel("ship_design") >= 2)) {
                maxVents = Math.round(maxVents * 1.2f);
            }

            int maxCapacitors;
            if (hullSpec.getHullSize() == HullSize.CAPITAL_SHIP) {
                maxCapacitors = 50;
            } else if (hullSpec.getHullSize() == HullSize.CRUISER) {
                maxCapacitors = 30;
            } else if (hullSpec.getHullSize() == HullSize.DESTROYER) {
                maxCapacitors = 20;
            } else {
                maxCapacitors = 10;
            }
            if ((stats == null && bonusOP >= 3.33f) || (stats != null && stats.getSkillLevel("ship_design") >= 1)) {
                maxCapacitors = Math.round(maxCapacitors * 1.2f);
            }

            boolean neverVent = hullId.contentEquals("ssp_excelsior");

            if (neverVent) {
                factor.capacitor += factor.vent;
                factor.vent = 0f;
            }

            int capacitors = Math.min((int) (freeOP / 6f * (r.nextFloat() + 0.75f) * factor.capacitor),
                                      maxCapacitors);
            int vents =
                Math.min((int) (freeOP / 3f * (r.nextFloat() + 1.5f) * factor.vent - capacitors * 0.8f),
                         maxVents);
            if (capacitors + vents > freeOP) {
                capacitors = (int) Math.floor(capacitors * (float) (freeOP / (capacitors + vents)));
                vents = (int) Math.floor(vents * (float) (freeOP / (capacitors + vents)));
            }
            newVariant.setNumFluxCapacitors(Math.max(capacitors, newVariant.getNumFluxCapacitors()));
            newVariant.setNumFluxVents(Math.max(vents, newVariant.getNumFluxVents()));

            boolean shields = ((hullSpec.getDefenseType().equals(ShieldType.FRONT)) ||
                               (newVariant.getHullSpec().getDefenseType().equals(ShieldType.OMNI)));
            boolean phase = (hullSpec.getDefenseType().equals(ShieldType.PHASE));
            boolean repulsor = hullId.startsWith("exigency_");
            boolean priwen = hullId.startsWith("tem_");
            int shieldQuality = DS_Database.shieldQuality(hullId);
            boolean goodShields = (shieldQuality == 2);
            boolean badShields = (shieldQuality == 0 || shieldQuality == -1);
            boolean expendableShields = (shieldQuality == -1);
            boolean lotsOfBeamMounts = (beamMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 3f);
            boolean lotsOfMissMounts = (missMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 2f);
            boolean lotsOfTrackingMounts = (trackingMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 3f);
            boolean lotsOfLimitedAmmoMounts = (limitedAmmoMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 3f);
            boolean lotsOfLimitedAmmoMissMounts = (limitedAmmoMissMounts >=
                                                   (newVariant.getNonBuiltInWeaponSlots().size()) / 2f);
            boolean tonsOfLimitedAmmoMissMounts = (limitedAmmoMissMounts >=
                                                   (newVariant.getNonBuiltInWeaponSlots().size()) / 1f);
            boolean lotsOfTurretMounts = (turretMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 1.5f);
            boolean lotsOfITUMounts = (ituMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 2f);
            boolean lotsOfFCSMounts = (fcsMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 2.5f);
            float shieldArc = hullSpec.getShieldSpec().getArc();

            boolean lotsOfIPDAIMounts;
            if (factionList.get(0).contentEquals("SCY")) {
                lotsOfIPDAIMounts = (ipdaiMounts >= (newVariant.getNonBuiltInWeaponSlots().size() / 4f));
            } else {
                lotsOfIPDAIMounts = (ipdaiMounts >= (newVariant.getNonBuiltInWeaponSlots().size() / 3.5f));
            }
            boolean tonsOfIPDAIMounts = (ipdaiMounts >= (newVariant.getNonBuiltInWeaponSlots().size() / 2f));

            int bays = hullSpec.getFighterBays();
            if (newVariant.hasHullMod("converted_hangar")) {
                bays++;
            }
            boolean isCarrier = false;
            boolean isLiteCarrier = false;
            if ((variant.getHullSize() == HullSize.FRIGATE || variant.getHullSize() == HullSize.DESTROYER) && bays > 0) {
                isCarrier = true;
            }
            if (variant.getHullSize() == HullSize.CRUISER) {
                if (bays > 1) {
                    isCarrier = true;
                } else if (bays > 0) {
                    isLiteCarrier = true;
                }
            }
            if (variant.getHullSize() == HullSize.CAPITAL_SHIP) {
                if (bays > 2) {
                    isCarrier = true;
                } else if (bays > 0) {
                    isLiteCarrier = true;
                }
            }

            int numWanzers = 0;
            for (String wing : newVariant.getFittedWings()) {
                if (wing.startsWith("diableavionics_")) {
                    numWanzers++;
                }
            }
            boolean hasWanzers = (numWanzers >= (newVariant.getFittedWings().size() / 2f));

            if (repulsor) {
                shields = false;
                phase = false;
                goodShields = false;
                badShields = true;
            }

            if (priwen) {
                shields = false;
                phase = false;
                goodShields = false;
                badShields = true;
            }

            if (phase) {
                goodShields = false;
                badShields = true;
            }

            if (DSModPlugin.hasSWP && expendableShields && shields &&
                    DS_Util.isHullmodUnlocked(factionList, "shieldbypass") &&
                    !DS_Util.isHullmodBlocked("shieldbypass", archetype)) {
                float target = 1f;
                if (variant.getHullSize() == HullSize.FRIGATE) {
                    target = 20f / (hullSpec.getFluxDissipation() * 0.3f);
                }
                if (variant.getHullSize() == HullSize.DESTROYER) {
                    target = 40f / (hullSpec.getFluxDissipation() * 0.3f);
                }
                if (variant.getHullSize() == HullSize.CRUISER) {
                    target = 60f / (hullSpec.getFluxDissipation() * 0.3f);
                }
                if (variant.getHullSize() == HullSize.CAPITAL_SHIP) {
                    target = 100f / (hullSpec.getFluxDissipation() * 0.3f);
                }

                if ((float) (r.nextDouble() * r.nextDouble()) >= target && !neverVent) {
                    boolean added = false;
                    if (!builtIn.contains("shieldbypass")) {
                        newVariant.addMod("shieldbypass");
                        added = true;
                        shields = false;
                    }
                    if (newVariant.computeOPCost(stats) >= totalOP) {
                        newVariant.removeMod("shieldbypass");
                        added = false;
                        shields = true;
                    }
                    if (added) {
                        factor.shield = 0f;
                        factor.armor *= 3f;
                    }
                }
            }

            WeightedRandomPicker<String> picker = new WeightedRandomPicker<>(r);
            if (shields) {
                if (DS_Util.isHullmodUnlocked(factionList, "frontemitter") &&
                        hullSpec.getDefenseType().equals(ShieldType.OMNI) &&
                        shieldArc < 360f &&
                        !station) {
                    float weight = 1f * factor.shield;
                    if (weight > 0.01f) {
                        picker.add("frontemitter", weight);
                    }
                }
                if (DS_Util.isHullmodUnlocked(factionList, "adaptiveshields") &&
                        hullSpec.getDefenseType().equals(ShieldType.FRONT) &&
                        !station) {
                    float weight = 2f * factor.shield;
                    if (weight > 0.01f) {
                        picker.add("adaptiveshields", weight);
                    }
                }
                if (DS_Util.isHullmodUnlocked(factionList, "extendedshieldemitter") &&
                        shieldArc < 360f) {
                    float weight = 2f * factor.shield;
                    if (weight > 0.01f) {
                        picker.add("extendedshieldemitter", weight);
                    }
                }
                if (DS_Util.isHullmodUnlocked(factionList, "hardenedshieldemitter") &&
                        Global.getSettings().getHullModSpec("hardenedshieldemitter") != null) {
                    float fluxFromCapsBase = Global.getSettings().getHullModSpec("hardenedshieldemitter").getCostFor(
                          hullSpec.getHullSize()) * 200f;
                    float fluxFromCaps =
                          Math.min((maxCapacitors - newVariant.getNumFluxCapacitors()) * 200f, fluxFromCapsBase);
                    float fluxFromMod =
                          (hullSpec.getFluxCapacity() + newVariant.getNumFluxCapacitors() * 200f) * 1f / 3f;
                    if (fluxFromMod > fluxFromCaps) {
                        float weight;
                        if (goodShields) {
                            weight = 3f * factor.shield;
                        } else if (badShields) {
                            weight = 1f * factor.shield;
                        } else {
                            weight = 2f * factor.shield;
                        }
                        weight *= fluxFromMod / fluxFromCapsBase;
                        if (weight > 0.01f) {
                            picker.add("hardenedshieldemitter", weight);
                        }
                    }
                }
                if (DS_Util.isHullmodUnlocked(factionList, "advancedshieldemitter")) {
                    float weight;
                    if (goodShields) {
                        weight = 1f * factor.shield;
                    } else if (badShields) {
                        weight = 0.25f * factor.shield;
                    } else {
                        weight = 0.5f * factor.shield;
                    }
                    if (weight > 0.01f) {
                        picker.add("advancedshieldemitter", weight);
                    }
                }
                if (DS_Util.isHullmodUnlocked(factionList, "stabilizedshieldemitter") &&
                        Global.getSettings().getHullModSpec("stabilizedshieldemitter") != null &&
                        !neverVent) {
                    float fluxFromVentsBase = Global.getSettings().getHullModSpec("stabilizedshieldemitter").getCostFor(
                          hullSpec.getHullSize()) * 10f;
                    float fluxFromVents = Math.min((maxVents - newVariant.getNumFluxVents()) * 10f, fluxFromVentsBase);
                    float fluxFromMod = hullSpec.getShieldSpec().getUpkeepCost() * 0.5f;
                    if (fluxFromMod > fluxFromVents) {
                        float weight = 1.5f * factor.shield * factor.flux;
                        weight *= fluxFromMod / fluxFromVentsBase;
                        if (weight > 0.01f) {
                            picker.add("stabilizedshieldemitter", weight);
                        }
                    }
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "fluxbreakers")) {
                float weight;
                if (phase) {
                    weight = 4f * factor.flux * factor.armor;
                } else if (builtIn.contains("safetyoverrides")) {
                    weight = 1f * factor.armor;
                } else {
                    weight = 2f * factor.flux * factor.armor;
                }
                if (weight > 0.01f) {
                    picker.add("fluxbreakers", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "frontshield") &&
                    !shields && !phase && !repulsor && !priwen && !station) {
                float weight = 3f * factor.shield / (0.5f + factor.engines);
                if (weight > 0.01f) {
                    picker.add("frontshield", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "blast_doors")) {
                float weight;
                if (!goodShields || phase) {
                    if (badShields) {
                        weight = 1f * factor.armor * factor.logistics;
                    } else {
                        weight = 0.75f * factor.armor * factor.logistics;
                    }
                } else {
                    weight = 0.5f * factor.armor * factor.logistics;
                }
                if (weight > 0.01f) {
                    picker.add("blast_doors", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "reinforcedhull")) {
                float weight;
                if (!goodShields || phase) {
                    if (badShields) {
                        weight = 2f * factor.armor * (1f + factor.logistics);
                    } else {
                        weight = 1.5f * factor.armor * (1f + factor.logistics);
                    }
                } else {
                    weight = 1f * factor.armor * (1f + factor.logistics);
                }
                if (weight > 0.01f) {
                    picker.add("reinforcedhull", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "armoredweapons")) {
                float weight;
                if (!goodShields || phase) {
                    if (badShields) {
                        weight = 3f * factor.armor;
                    } else {
                        weight = 1.5f * factor.armor;
                    }
                } else {
                    weight = 0.5f * factor.armor;
                }
                if (weight > 0.01f) {
                    picker.add("armoredweapons", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "autorepair")) {
                float weight;
                if (!goodShields || phase) {
                    if (badShields) {
                        weight = 3f * factor.armor * factor.elite / (0.5f + factor.logistics);
                    } else {
                        weight = 2f * factor.armor * factor.elite / (0.5f + factor.logistics);
                    }
                } else {
                    weight = 1f * factor.armor * factor.elite / (0.5f + factor.logistics);
                }
                if (weight > 0.01f) {
                    picker.add("autorepair", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "heavyarmor")) {
                float weight;
                if (!goodShields || phase) {
                    if (badShields) {
                        weight = 4f * factor.armor;
                    } else {
                        weight = 2f * factor.armor;
                    }
                } else {
                    weight = 1f * factor.armor;
                }
                if (weight > 0.01f) {
                    picker.add("heavyarmor", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "insulatedengine")) {
                float weight;
                if (!goodShields || phase) {
                    if (badShields) {
                        weight = 2f * factor.armor * factor.engines;
                    } else {
                        weight = 1f * factor.armor * factor.engines;
                    }
                } else {
                    weight = 0.5f * factor.armor * factor.engines;
                }
                if (weight > 0.01f) {
                    picker.add("insulatedengine", weight);
                }
            }
            if (DSModPlugin.imperiumExists &&
                    DS_Util.isHullmodUnlocked(factionList, "ii_energized_armor")) {
                float weight;
                if (!goodShields || phase) {
                    if (badShields) {
                        weight = 4f * factor.armor * factor.flux * (float) Math.sqrt(sizeFactor);
                    } else {
                        weight = 2f * factor.armor * factor.flux * (float) Math.sqrt(sizeFactor);
                    }
                } else {
                    weight = 0.5f * factor.armor * factor.flux * (float) Math.sqrt(sizeFactor);
                }
                if (factionList.contains("interstellarimperium")) {
                    weight *= 2f;
                }
                if (weight > 0.01f) {
                    picker.add("ii_energized_armor", weight);
                }
            }
            if (DSModPlugin.scyExists &&
                    DS_Util.isHullmodUnlocked(factionList, "SCY_reactiveArmor") &&
                    (!goodShields || phase)) {
                float weight;
                if (badShields) {
                    weight = 2f * factor.armor;
                } else {
                    weight = 1f * factor.armor;
                }
                if (weight > 0.01f) {
                    picker.add("SCY_reactiveArmor", weight);
                }
            }
            if (DSModPlugin.scyExists &&
                    DS_Util.isHullmodUnlocked(factionList, "SCY_lightArmor")) {
                float weight = 0.5f * factor.engines / (1f + factor.armor);
                if (goodShields && !phase) {
                    weight *= 2f;
                } else if (badShields && !phase) {
                    weight *= 0.5f;
                } else if (!shields) {
                    weight *= 0f;
                }
                if (weight > 0.01f) {
                    picker.add("SCY_lightArmor", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "solar_shielding")) {
                float weight = 0.5f * factor.armor;
                if (factionList.contains(Factions.DIKTAT)) {
                    weight *= 3f;
                }
                if (weight > 0.01f) {
                    picker.add("solar_shielding", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "auxiliarythrusters")) {
                float weight = 3f * factor.engines;
                if (weight > 0.01f) {
                    picker.add("auxiliarythrusters", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "advancedoptics") &&
                    beamMounts > 0) {
                float weight;
                if (lotsOfBeamMounts) {
                    weight = 4f * factor.range;
                } else {
                    weight = 0.5f * factor.range;
                }
                if (weight > 0.01f) {
                    picker.add("advancedoptics", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "eccm")) {
                float weight;
                float basicFactor = (factor.range / 2f) / (0.5f + factor.elite);
                if (lotsOfMissMounts) {
                    weight = 2f * (1f * factor.missile + basicFactor);
                } else if (missMounts >= 1) {
                    weight = 1f * (0.5f * factor.missile + basicFactor);
                } else {
                    weight = 0.75f * basicFactor;
                }
                if (weight > 0.01f) {
                    picker.add("eccm", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "ecm")) {
                float weight = 1f * ((factor.range / 2f + factor.logistics - 1f) / (0.5f + factor.elite));
                if (weight > 0.01f) {
                    picker.add("ecm", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "nav_relay")) {
                float weight = 1f * ((factor.engines / 2f + factor.logistics - 1f) / (0.5f + factor.elite));
                if (weight > 0.01f) {
                    picker.add("nav_relay", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "operations_center") &&
                    flagship) {
                float weight = 0.5f * (factor.elite + factor.logistics);
                if (weight > 0.01f) {
                    picker.add("operations_center", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "surveying_equipment")) {
                float weight = 0.5f * factor.logistics / (1f + factor.elite);
                if (weight > 0.01f) {
                    picker.add("surveying_equipment", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "recovery_shuttles") &&
                    (isCarrier || isLiteCarrier)) {
                float weight = 1f * factor.logistics * (factor.fighters - 0.5f) / (1f + factor.elite);
                if (weight > 0.01f) {
                    picker.add("recovery_shuttles", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "expanded_deck_crew") &&
                    (isCarrier || isLiteCarrier)) {
                float weight = 5f * factor.fighters;
                if (isLiteCarrier) {
                    weight *= 0.33f;
                }
                if (weight > 0.01f) {
                    picker.add("expanded_deck_crew", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "missleracks") &&
                    missMounts > 0) {
                float weight = 0f;
                if (tonsOfLimitedAmmoMissMounts) {
                    weight = 8f * (factor.missile + 1f);
                } else if (lotsOfLimitedAmmoMissMounts) {
                    weight = 4f * (factor.missile + 0.5f);
                } else if (limitedAmmoMissMounts >= 1) {
                    weight = 1f * factor.missile;
                }
                if (weight > 0.01f) {
                    picker.add("missleracks", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "pointdefenseai") &&
                    ipdaiMounts > 0) {
                float weight;
                if (tonsOfIPDAIMounts) {
                    weight = 5f * factor.defense;
                } else if (lotsOfIPDAIMounts) {
                    weight = 3f * factor.defense;
                } else {
                    weight = 0.2f * factor.defense;
                }
                if (factionList.contains("templars") || factionList.contains("SCY")) {
                    weight *= 2f;
                }
                if (weight > 0.01f) {
                    picker.add("pointdefenseai", weight);
                }
            }
            if (DSModPlugin.hasSWP &&
                    DS_Util.isHullmodUnlocked(factionList, "advanced_ai_core") &&
                    trackingMounts > 0) {
                float weight;
                if (lotsOfTrackingMounts) {
                    weight = 2f * factor.defense * (float) Math.sqrt(factor.range);
                } else {
                    weight = 0.2f * factor.defense * (float) Math.sqrt(factor.range);
                }
                if (factionList.contains("SCY")) {
                    weight *= 0.5f;
                }
                if (weight > 0.01f) {
                    picker.add("advanced_ai_core", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "targetingunit") &&
                    !DS_Util.isHullmodBlocked("targetingunit", archetype) &&
                    DS_Util.isHullmodCompatible("targetingunit", newVariant) &&
                    ituMounts > 0) {
                float weight;
                if (lotsOfITUMounts) {
                    weight = 7.5f * factor.range;
                } else {
                    weight = 1f * factor.range;
                }
                if (weight > 0.01f) {
                    picker.add("targetingunit", weight);
                }
            } else if (DS_Util.isHullmodUnlocked(factionList, "dedicated_targeting_core") &&
                    !DS_Util.isHullmodBlocked("dedicated_targeting_core", archetype) &&
                    DS_Util.isHullmodCompatible("dedicated_targeting_core", newVariant) &&
                    ituMounts > 0) {
                float weight;
                if (lotsOfITUMounts) {
                    weight = 7f * factor.range;
                } else {
                    weight = 1f * factor.range;
                }
                if (weight > 0.01f) {
                    picker.add("dedicated_targeting_core", weight);
                }
            }
            if (DSModPlugin.blackrockExists &&
                    DS_Util.isHullmodUnlocked(factionList, "brtarget")) {
                float weight = 3f * factor.attack;
                if (weight > 0.01f) {
                    picker.add("brtarget", weight);
                }
            }
            if (DSModPlugin.imperiumExists &&
                    DS_Util.isHullmodUnlocked(factionList, "ii_fire_control") &&
                    fcsMounts > 0) {
                float weight;
                if (lotsOfFCSMounts) {
                    weight = 3f * factor.range;
                } else {
                    weight = 0.5f * factor.range;
                }
                if (factionList.contains("interstellarimperium")) {
                    weight *= 2f;
                }
                if (weight > 0.01f) {
                    picker.add("ii_fire_control", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "turretgyros") &&
                    turretMounts > 0) {
                float weight;
                if (lotsOfTurretMounts) {
                    weight = 1f * factor.attack * factor.defense;
                } else {
                    weight = 0.2f * factor.attack * factor.defense;
                }
                if (weight > 0.01f) {
                    picker.add("turretgyros", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "augmentedengines")) {
                float weight = 0.5f * (factor.logistics - 0.5f);
                if (weight > 0.01f) {
                    picker.add("augmentedengines", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "unstable_injector")) {
                float weight = 5f * factor.engines / (0.5f + factor.range);
                if (weight > 0.01f) {
                    picker.add("unstable_injector", weight);
                }
            }
            if (DSModPlugin.blackrockExists &&
                    DS_Util.isHullmodUnlocked(factionList, "brdrive")) {
                float weight = 3f * factor.engines;
                if (weight > 0.01f) {
                    picker.add("brdrive", weight);
                }
            }
            if (DSModPlugin.diableExists &&
                    DS_Util.isHullmodUnlocked(factionList, "diableavionics_universaldecks") &&
                    (isCarrier || isLiteCarrier) &&
                    hasWanzers) {
                float weight = 2f * factor.fighters;
                if (isLiteCarrier) {
                    weight *= 0.5f;
                }
                if (newVariant.hasHullMod("converted_hangar")) {
                    weight *= 0.25f;
                }
                if (factionList.contains("diableavionics")) {
                    weight *= 2f;
                }
                if (weight > 0.01f) {
                    picker.add("diableavionics_universaldecks", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "magazines") &&
                    limitedAmmoMounts > 0) {
                float weight;
                if (lotsOfLimitedAmmoMounts) {
                    weight = 4f * factor.attack;
                } else {
                    weight = 0.5f * factor.attack;
                }
                if (weight > 0.01f) {
                    picker.add("magazines", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "hardened_subsystems")) {
                float weight = 1f * factor.logistics;
                if (weight > 0.01f) {
                    picker.add("hardened_subsystems", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "safetyoverrides")) {
                float weight = 3f * factor.elite * Math.max(factor.vent, factor.flux) *
                      factor.engines / ((0.5f + factor.logistics) * (0.5f + factor.range));
                if (weight > 0.01f) {
                    picker.add("safetyoverrides", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "fluxcoil") &&
                    (newVariant.getNumFluxCapacitors() > Math.round(maxCapacitors * 0.7))) {
                float weight = 3f * factor.flux * factor.capacitor *
                      (float) Math.pow((newVariant.getNumFluxCapacitors() / (double) maxCapacitors), 2.0);
                if (weight > 0.01f) {
                    picker.add("fluxcoil", weight);
                }
            }
            if (DS_Util.isHullmodUnlocked(factionList, "fluxdistributor") &&
                    (newVariant.getNumFluxVents() > Math.round(maxVents * 0.7)) &&
                    !neverVent) {
                float weight = 2f * factor.flux * factor.vent *
                      (float) Math.pow((newVariant.getNumFluxVents() / (double) maxVents), 2.0);
                if (weight > 0.01f) {
                    picker.add("fluxdistributor", weight);
                }
            }
            if (DSModPlugin.hasSWP &&
                    DS_Util.isHullmodUnlocked(factionList, "supply_conservation_program")) {
                float weight = 0.5f * (factor.logistics - 1f);
                if (weight > 0.01f) {
                    picker.add("supply_conservation_program", weight);
                }
            }
            if (DSModPlugin.hasSWP &&
                    DS_Util.isHullmodUnlocked(factionList, "cargo_expansion")) {
                float weight;
                if (hullSpec.getHints().contains(ShipTypeHints.CIVILIAN)) {
                    weight = 2f * factor.logistics;
                } else {
                    weight = 1f * (factor.logistics - 1f);
                }
                if (weight > 0.01f) {
                    picker.add("cargo_expansion", weight);
                }
            }
            if (DSModPlugin.hasSWP &&
                    DS_Util.isHullmodUnlocked(factionList, "fuel_expansion")) {
                float weight;
                if (hullSpec.getHints().contains(ShipTypeHints.CIVILIAN)) {
                    weight = 2f * factor.logistics;
                } else {
                    weight = 1f * (factor.logistics - 1f);
                }
                if (weight > 0.01f) {
                    picker.add("fuel_expansion", weight);
                }
            }
            if (DSModPlugin.hasSWP &&
                    DS_Util.isHullmodUnlocked(factionList, "additional_crew_quarters")) {
                float weight;
                if (hullSpec.getHints().contains(ShipTypeHints.CIVILIAN)) {
                    weight = 2f * factor.logistics;
                } else {
                    weight = 1f * (factor.logistics - 1f);
                }
                if (weight > 0.01f) {
                    picker.add("additional_crew_quarters", weight);
                }
            }
            if (DSModPlugin.blackrockExists &&
                    DS_Util.isHullmodUnlocked(factionList, "brassaultop")) {
                float weight = 3f * factor.armor;
                if (weight > 0.01f) {
                    picker.add("brassaultop", weight);
                }
            }
            if (DSModPlugin.mayorateExists &&
                    DS_Util.isHullmodUnlocked(factionList, "ilk_SensorSuite") &&
                    ituMounts > 0) {
                float weight;
                if (factionList.contains("mayorate")) {
                    weight = 2f * factor.range;
                } else {
                    weight = 1f * factor.range;
                }
                if (weight > 0.01f) {
                    picker.add("ilk_SensorSuite", weight);
                }
            }
            if (DSModPlugin.mayorateExists &&
                    DS_Util.isHullmodUnlocked(factionList, "ilk_AICrew")) {
                float weight;
                if (factionList.contains("mayorate")) {
                    weight = 2f * (float) Math.sqrt(Math.max(factor.attack * factor.engines * factor.logistics, 0));
                } else {
                    weight = 0.5f * (float) Math.sqrt(Math.max(factor.attack * factor.engines * factor.logistics, 0));
                }
                if (factionList.contains(Factions.LUDDIC_PATH) || factionList.contains(Factions.LUDDIC_CHURCH)) {
                    weight = 0f;
                }
                if (weight > 0.01f) {
                    picker.add("ilk_AICrew", weight);
                }
            }

            /* Apply faction preferences */
            for (int i = 0; i < picker.getItems().size(); i++) {
                float weight = picker.getWeight(i);
                String id = picker.getItems().get(i);
                if (!DS_Util.isHullmodOffered(factionList, id)) {
                    picker.setWeight(i, weight * 0.5f);
                }
            }

            /* Apply weight changes based on high-priority hull mods */
            freeOP = totalOP - newVariant.computeOPCost(stats);
            for (int i = 0; i < picker.getItems().size(); i++) {
                float weight = picker.getWeight(i);
                String id = picker.getItems().get(i);
                HullModSpecAPI spec = Global.getSettings().getHullModSpec(id);
                if (spec == null) {
                    continue;
                }
                int cost = spec.getCostFor(newVariant.getHullSize());
                int buffer = newVariant.getNumFluxVents() + newVariant.getNumFluxCapacitors();
                buffer = Math.max(0, Math.round(buffer - maxVents * 0.5f - maxCapacitors * 0.25f));
                switch (id) {
                    case "fluxcoil":
                    case "fluxdistributor":
                    case "safetyoverrides":
                        buffer = 0;
                        break;
                    default:
                        buffer *= 0.5;
                        break;
                }
                float urgencyFactor = Math.max(Math.min(cost / Math.max(1f, freeOP + buffer), 1f), 0f);
                picker.setWeight(i, weight * (1f + urgencyFactor));
            }

            while (newVariant.computeOPCost(stats) < totalOP) {
                if (picker.isEmpty()) {
                    int remainingOP = totalOP - newVariant.computeOPCost(stats);
                    int moreVents = (int) (remainingOP * factor.vent / (factor.capacitor + factor.vent));
                    int moreCapacitors = remainingOP - moreVents;
                    if (moreVents + newVariant.getNumFluxVents() > maxVents) {
                        moreCapacitors += moreVents + newVariant.getNumFluxVents() - maxVents;
                        moreVents = maxVents - newVariant.getNumFluxVents();
                    }
                    if (moreCapacitors + newVariant.getNumFluxCapacitors() > maxCapacitors) {
                        moreVents += moreCapacitors + newVariant.getNumFluxCapacitors() - maxCapacitors;
                        moreCapacitors = maxCapacitors - newVariant.getNumFluxCapacitors();
                    }
                    if (moreVents + newVariant.getNumFluxVents() > maxVents) {
                        moreVents = maxVents - newVariant.getNumFluxVents();
                    }
                    newVariant.setNumFluxVents(newVariant.getNumFluxVents() + moreVents);
                    newVariant.setNumFluxCapacitors(newVariant.getNumFluxCapacitors() + moreCapacitors);
                    break;
                } else {
                    String pick = picker.pickAndRemove();
                    if (DS_Util.isHullmodBlocked(pick, archetype)) {
                        continue;
                    }
                    if (!DS_Util.isHullmodCompatible(pick, variant)) {
                        continue;
                    }

                    HullModSpecAPI spec = Global.getSettings().getHullModSpec(pick);
                    if (spec == null) {
                        continue;
                    }
                    int cost = spec.getCostFor(newVariant.getHullSize());
                    int buffer = newVariant.getNumFluxVents() + newVariant.getNumFluxCapacitors();
                    buffer = Math.max(0, Math.round(buffer - maxVents * 0.5f - maxCapacitors * 0.25f));
                    switch (pick) {
                        case "fluxcoil":
                        case "fluxdistributor":
                            buffer = 0;
                            break;
                        case "safetyoverrides":
                            buffer *= 1;
                            break;
                        default:
                            buffer *= 0.5;
                            break;
                    }
                    if ((newVariant.computeOPCost(stats) + cost) > (totalOP + buffer)) {
                        continue;
                    }

                    if (!builtIn.contains(pick)) {
                        newVariant.addMod(pick);
                    }

                    if (newVariant.hasHullMod("extendedshieldemitter") && newVariant.hasHullMod("frontemitter") &&
                            shieldArc >= 180f) {
                        newVariant.removeMod("extendedshieldemitter");
                    }
                }
            }

            for (int i = 0; i < 100; i++) {
                if (newVariant.computeOPCost(stats) > totalOP) {
                    if (r.nextDouble() > 0.5) {
                        if (newVariant.getNumFluxVents() > 0) {
                            newVariant.setNumFluxVents(newVariant.getNumFluxVents() - 1);
                        }
                    } else {
                        if (newVariant.getNumFluxCapacitors() > 0) {
                            newVariant.setNumFluxCapacitors(newVariant.getNumFluxCapacitors() - 1);
                        }
                    }
                }
            }
        }

        return newVariant;
    }

    public static class HullmodFactors {

        public float armor = 1f;
        public float attack = 1f;
        public float capacitor = 1f;
        public float defense = 1f;
        public float elite = 1f;
        public float engines = 1f;
        public float fighters = 1f;
        public float flux = 1f;
        public float logistics = 1f;
        public float missile = 1f;
        public float range = 1f;
        public float shield = 1f;
        public float vent = 1f;

        public HullmodFactors(Archetype archetype) {
            if (archetype == Archetype.BALANCED) {
                logistics = 1f;
                elite = 1f;
                shield = 1f;
                armor = 1f;
                defense = 1f;
                fighters = 1f;
                flux = 1f;
                engines = 1f;
                attack = 1f;
                missile = 1f;
                range = 1f;
                capacitor = 1f;
                vent = 1f;
            } else if (archetype == Archetype.STRIKE) {
                logistics = 0.25f;
                elite = 1f;
                shield = 1f;
                armor = 1f;
                defense = 0.5f;
                fighters = 1f;
                flux = 1.5f;
                engines = 1.5f;
                attack = 1.5f;
                missile = 1.5f;
                range = 1f;
                capacitor = 1.5f;
                vent = 1f;
            } else if (archetype == Archetype.ELITE) {
                logistics = 0f;
                elite = 2f;
                shield = 1f;
                armor = 1f;
                defense = 1f;
                fighters = 1f;
                flux = 1f;
                engines = 1f;
                attack = 1f;
                missile = 1f;
                range = 1f;
                capacitor = 1f;
                vent = 1f;
            } else if (archetype == Archetype.ASSAULT) {
                logistics = 0.5f;
                elite = 0.5f;
                shield = 0.5f;
                armor = 1.5f;
                defense = 0.5f;
                fighters = 1f;
                flux = 1f;
                engines = 0.5f;
                attack = 2f;
                missile = 1f;
                range = 1.25f;
                capacitor = 0.75f;
                vent = 1.5f;
            } else if (archetype == Archetype.SKIRMISH) {
                logistics = 0.5f;
                elite = 0.75f;
                shield = 1f;
                armor = 0.5f;
                defense = 1f;
                fighters = 0.5f;
                flux = 0.5f;
                engines = 3f;
                attack = 0.5f;
                missile = 1.5f;
                range = 1f;
                capacitor = 0.75f;
                vent = 0.75f;
            } else if (archetype == Archetype.ARTILLERY) {
                logistics = 1f;
                elite = 0.5f;
                shield = 1f;
                armor = 1f;
                defense = 1f;
                fighters = 1.5f;
                flux = 0.5f;
                engines = 0.25f;
                attack = 1.5f;
                missile = 0.75f;
                range = 5f;
                capacitor = 0.75f;
                vent = 0.5f;
            } else if (archetype == Archetype.CLOSE_SUPPORT) {
                logistics = 1f;
                elite = 0.5f;
                shield = 1f;
                armor = 1f;
                defense = 1f;
                fighters = 1f;
                flux = 1f;
                engines = 0.5f;
                attack = 1f;
                missile = 1.5f;
                range = 1.5f;
                capacitor = 1.25f;
                vent = 0.75f;
            } else if (archetype == Archetype.SUPPORT) {
                logistics = 0.75f;
                elite = 0.25f;
                shield = 1f;
                armor = 0.5f;
                defense = 1f;
                fighters = 2f;
                flux = 0.5f;
                engines = 1f;
                attack = 0.5f;
                missile = 1.5f;
                range = 1f;
                capacitor = 0.75f;
                vent = 0.75f;
            } else if (archetype == Archetype.ESCORT) {
                logistics = 1f;
                elite = 0.25f;
                shield = 1.5f;
                armor = 1f;
                defense = 3f;
                fighters = 1.5f;
                flux = 0.75f;
                engines = 1.5f;
                attack = 1f;
                missile = 0.75f;
                range = 2f;
                capacitor = 1.25f;
                vent = 0.75f;
            } else if (archetype == Archetype.FLEET) {
                logistics = 5f;
                elite = 0f;
                shield = 1f;
                armor = 1f;
                defense = 0.5f;
                fighters = 1.5f;
                flux = 0.25f;
                engines = 2f;
                attack = 0.5f;
                missile = 0.5f;
                range = 0.5f;
                capacitor = 0.25f;
                vent = 0.25f;
            } else if (archetype == Archetype.ULTIMATE) {
                logistics = 0f;
                elite = 2f;
                shield = 1f;
                armor = 1f;
                defense = 1f;
                fighters = 1f;
                flux = 1f;
                engines = 1f;
                attack = 1f;
                missile = 1f;
                range = 1f;
                capacitor = 1f;
                vent = 1f;
            }
        }
    }
}
