package data.scripts.variants;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.ModSpecAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.loading.Description;
import com.fs.starfarer.api.loading.WeaponGroupSpec;
import com.fs.starfarer.api.loading.WeaponGroupType;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DS_WeaponGrouper {

    private static final Logger log = Global.getLogger(DS_Database.class);

    public static ShipVariantAPI generateWeaponGroups(ShipVariantAPI variant, WeaponGroupConfig config) {
        // Clean up the existing groups
        // We go through this whole song and dance rather than just flushing and making new groups for reasons of maximum compatability
        for (WeaponGroupSpec group : variant.getWeaponGroups()) {
            WeaponGroupSpec clone = group.clone();
            for (String slot : clone.getSlots()) {
                group.removeSlot(slot);
            }
        }

        // These are programmable variables passed from the WeaponGroupConfig
        // This will change how the algorithm will behave
        boolean splitMissiles = false;
        boolean enforceSides = false;
        boolean linkedStrike = false;
        boolean splitBeams = false;
        boolean exigency = false;

        switch (config) {
            case MISSILE:
                splitMissiles = true;
                break;
            case BROADSIDE:
                enforceSides = true;
                break;
            case MISSILE_BROADSIDE:
                splitMissiles = true;
                enforceSides = true;
                break;
            case EXIGENCY:
                linkedStrike = true;
                exigency = true;
                break;
            case LINKED_STRIKE:
                linkedStrike = true;
                break;
            case MORPHEUS:
                splitBeams = true;
                linkedStrike = true;
                break;
            case STANDARD:
            default:
        }

        // Now we define all of the possible weapon groups that the variant can use
        List<WeaponGroupSpec> groupList = new ArrayList<>(18);

        WeaponGroupSpec PDGroup = new WeaponGroupSpec();
        PDGroup.setType(WeaponGroupType.LINKED);
        PDGroup.setAutofireOnByDefault(true);
        groupList.add(PDGroup);

        WeaponGroupSpec AFGroup = new WeaponGroupSpec();
        AFGroup.setType(WeaponGroupType.LINKED);
        AFGroup.setAutofireOnByDefault(true);
        groupList.add(AFGroup);

        WeaponGroupSpec AssHGroup = new WeaponGroupSpec();
        AssHGroup.setType(WeaponGroupType.LINKED);
        AssHGroup.setAutofireOnByDefault(false);
        groupList.add(AssHGroup);

        WeaponGroupSpec AssTGroup = new WeaponGroupSpec();
        AssTGroup.setType(WeaponGroupType.LINKED);
        AssTGroup.setAutofireOnByDefault(true);
        groupList.add(AssTGroup);

        WeaponGroupSpec CSHGroup = new WeaponGroupSpec();
        CSHGroup.setType(WeaponGroupType.LINKED);
        CSHGroup.setAutofireOnByDefault(true);
        groupList.add(CSHGroup);

        WeaponGroupSpec CSTGroup = new WeaponGroupSpec();
        CSTGroup.setType(WeaponGroupType.LINKED);
        CSTGroup.setAutofireOnByDefault(true);
        groupList.add(CSTGroup);

        WeaponGroupSpec SupGroup = new WeaponGroupSpec();
        SupGroup.setType(WeaponGroupType.LINKED);
        SupGroup.setAutofireOnByDefault(true);
        groupList.add(SupGroup);

        WeaponGroupSpec HvyHGroup = new WeaponGroupSpec();
        if (linkedStrike || enforceSides) {
            HvyHGroup.setType(WeaponGroupType.LINKED);
        } else {
            HvyHGroup.setType(WeaponGroupType.ALTERNATING);
        }
        HvyHGroup.setAutofireOnByDefault(false);
        groupList.add(HvyHGroup);

        WeaponGroupSpec HvyTGroup = new WeaponGroupSpec();
        if (linkedStrike || enforceSides) {
            HvyTGroup.setType(WeaponGroupType.LINKED);
        } else {
            HvyTGroup.setType(WeaponGroupType.ALTERNATING);
        }
        HvyTGroup.setAutofireOnByDefault(false);
        groupList.add(HvyTGroup);

        WeaponGroupSpec BeaHGroup = new WeaponGroupSpec();
        BeaHGroup.setType(WeaponGroupType.LINKED);
        BeaHGroup.setAutofireOnByDefault(true);
        groupList.add(BeaHGroup);

        WeaponGroupSpec BeaTGroup = new WeaponGroupSpec();
        BeaTGroup.setType(WeaponGroupType.LINKED);
        BeaTGroup.setAutofireOnByDefault(false);
        groupList.add(BeaTGroup);

        WeaponGroupSpec StrHGroup = new WeaponGroupSpec();
        if (linkedStrike) {
            StrHGroup.setType(WeaponGroupType.LINKED);
        } else {
            StrHGroup.setType(WeaponGroupType.ALTERNATING);
        }
        StrHGroup.setAutofireOnByDefault(false);
        groupList.add(StrHGroup);

        WeaponGroupSpec StrTGroup = new WeaponGroupSpec();
        if (linkedStrike || variant.getHullSize() == HullSize.CRUISER || variant.getHullSize() == HullSize.CAPITAL_SHIP) {
            StrTGroup.setType(WeaponGroupType.LINKED);
        } else {
            StrTGroup.setType(WeaponGroupType.ALTERNATING);
        }
        StrTGroup.setAutofireOnByDefault(false);
        groupList.add(StrTGroup);

        WeaponGroupSpec MisGroup = new WeaponGroupSpec();
        if (linkedStrike) {
            MisGroup.setType(WeaponGroupType.LINKED);
        } else {
            MisGroup.setType(WeaponGroupType.ALTERNATING);
        }
        if (exigency) {
            MisGroup.setAutofireOnByDefault(true);
        } else {
            MisGroup.setAutofireOnByDefault(false);
        }
        groupList.add(MisGroup);

        WeaponGroupSpec SpamMisGroup = new WeaponGroupSpec();
        if (exigency) {
            SpamMisGroup.setType(WeaponGroupType.ALTERNATING);
        } else {
            SpamMisGroup.setType(WeaponGroupType.LINKED);
        }
        SpamMisGroup.setAutofireOnByDefault(true);
        groupList.add(SpamMisGroup);

        WeaponGroupSpec StrMisGroup = new WeaponGroupSpec();
        if (linkedStrike) {
            StrMisGroup.setType(WeaponGroupType.LINKED);
        } else {
            StrMisGroup.setType(WeaponGroupType.ALTERNATING);
        }
        StrMisGroup.setAutofireOnByDefault(false);
        groupList.add(StrMisGroup);

        WeaponGroupSpec LGroup = new WeaponGroupSpec();
        LGroup.setType(WeaponGroupType.LINKED);
        LGroup.setAutofireOnByDefault(false);
        groupList.add(LGroup);

        WeaponGroupSpec RGroup = new WeaponGroupSpec();
        RGroup.setType(WeaponGroupType.LINKED);
        RGroup.setAutofireOnByDefault(false);
        groupList.add(RGroup);

        // This loops through all of the weapons and individually assigns them to initial groups
        // Most weapon groupings are auto-detected based on AI hints, weapon data, and description strings
        // The remaning groupings are enforced via overrides and configuration parameters
        List<WeaponSlotAPI> slots = variant.getHullSpec().getAllWeaponSlotsCopy();
        Collection<String> fittedSlots = variant.getFittedWeaponSlots();
        for (String fittedSlot : fittedSlots) {
            String weapon = variant.getWeaponId(fittedSlot);
            String type = Global.getSettings().getDescription(weapon, Description.Type.WEAPON).getText2();
            WeaponSlotAPI slot = null;
            for (WeaponSlotAPI slott : slots) {
                if (slott.getId().contentEquals(fittedSlot)) {
                    slot = slott;
                }
            }

            // These are the various weapon properties that are checked when making the groupings
            boolean antiFighter = false;
            boolean pointDefense = false;
            boolean pdAlso = false;
            boolean strike = false;
            boolean noAim = false;
            boolean assault = false;
            boolean closeSupport = false;
            boolean fireSupport = false;
            boolean missile = false;
            boolean lowFlux = false;
            boolean separateFire = false;
            boolean hardpoint = false;
            boolean limitedAmmo = false;
            boolean beam = false;
            boolean special = false;
            boolean front = false;
            boolean back = false;
            boolean left = false;
            boolean right = false;
            WeaponSize size = WeaponSize.SMALL;
            DS_WeaponEntry entry = DS_Database.masterWeaponList.get(weapon);

            if (slot != null) {
                if (slot.getArc() <= 15f) {
                    hardpoint = true;
                }
                float angle = slot.getAngle();
                if (angle < -180f) {
                    angle += 360f;
                }
                if (angle > 180f) {
                    angle -= 360f;
                }
                if (angle <= 45f && angle >= -45f) {
                    front = true;
                } else if (angle >= 45f && angle <= 135f) {
                    left = true;
                } else if (angle > 135f || angle < -135f) {
                    back = true;
                } else {
                    right = true;
                }
                size = slot.getSlotSize();
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.MISSILE).contains(weapon)) {
                missile = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.NO_AIM).contains(weapon)) {
                noAim = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ANTI_FIGHTER).contains(weapon)) {
                antiFighter = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.POINT_DEFENSE).contains(weapon)) {
                pointDefense = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.STRIKE).contains(weapon)) {
                strike = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.ASSAULT).contains(weapon)) {
                assault = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.CLOSE_SUPPORT).contains(weapon)) {
                closeSupport = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.FIRE_SUPPORT).contains(weapon)) {
                fireSupport = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SPECIAL).contains(weapon)) {
                special = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.LOW_FLUX).contains(weapon)) {
                lowFlux = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SEPARATE_FIRE).contains(weapon)) {
                separateFire = true;
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).contains(weapon)) {
                beam = true;
            }
            /*
             if (getWeaponList("Burst Beam").contains(weapon)) { }
             */
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.LIMITED_AMMO).contains(weapon)) {
                limitedAmmo = true;
            }

            // If "Override" is set for the weapon, skip this auto-detection stuff and only use the overrides
            // Weapons without "Override" set will use both auto-detected stuff and overrides
            if (!DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.OVERRIDE).contains(weapon)) {
                if (entry != null && entry.type == WeaponType.MISSILE) {
                    missile = true;
                }
                if (entry != null && entry.limitedAmmo && missile) {
                    limitedAmmo = true;
                }
                if (entry != null && entry.lowFlux) {
                    lowFlux = true;
                }
                if (entry != null && entry.separateFire) {
                    separateFire = true;
                }
                EnumSet<AIHints> hints = variant.getWeaponSpec(fittedSlot).getAIHints();
                for (AIHints hint : hints) {
                    if (hint == AIHints.ANTI_FTR) {
                        antiFighter = true;
                    }
                    if (hint == AIHints.PD || hint == AIHints.PD_ONLY) {
                        pointDefense = true;
                    }
                    if (hint == AIHints.STRIKE) {
                        strike = true;
                    }
                    if (hint == AIHints.DO_NOT_AIM || hint == AIHints.HEATSEEKER) {
                        noAim = true;
                    }
                }
                for (AIHints hint : hints) {
                    if (hint == AIHints.PD_ALSO) {
                        pointDefense = false;
                        pdAlso = true;
                    }
                }
                if (type.contentEquals("Anti-Fighter")) {
                    antiFighter = true;
                }
                if (type.contentEquals("Point Defense")) {
                    pointDefense = true;
                }
                if (type.contentEquals("Strike")) {
                    strike = true;
                }
                if (type.contentEquals("Assault")) {
                    assault = true;
                }
                if (type.contentEquals("Close Support") || type.contentEquals("Suppression") ||
                        type.contentEquals("Support")) {
                    closeSupport = true;
                }
                if (type.contentEquals("Fire Support")) {
                    fireSupport = true;
                }
                if (type.contentEquals("Special")) {
                    special = true;
                }
            }

            boolean verySignificantSize =
                    ((separateFire && !((variant.getHullSize() == HullSize.CRUISER &&
                                         size == WeaponSize.SMALL) ||
                                        (variant.getHullSize() == HullSize.CAPITAL_SHIP &&
                                         (size == WeaponSize.SMALL || size == WeaponSize.MEDIUM)))) ||
                     (size == WeaponSize.LARGE &&
                      (variant.getHullSize() == HullSize.FRIGATE || variant.getHullSize() == HullSize.DESTROYER)));

            boolean significantSize =
                    (separateFire ||
                     (size == WeaponSize.LARGE ||
                      (size == WeaponSize.MEDIUM &&
                       (variant.getHullSize() != HullSize.CAPITAL_SHIP)) ||
                      (size == WeaponSize.SMALL &&
                       (variant.getHullSize() == HullSize.FRIGATE || variant.getHullSize() == HullSize.DESTROYER))));

            // This is the logic for assigning weapons to general groups
            // Make sure you know what you are doing before changing this stuff around
            // The order of operations is important!
            if (hardpoint && verySignificantSize) {
                if (!beam) {
                    HvyHGroup.addSlot(fittedSlot);
                } else {
                    BeaHGroup.addSlot(fittedSlot);
                }
            } else if (!hardpoint && verySignificantSize) {
                if (!beam) {
                    HvyTGroup.addSlot(fittedSlot);
                } else {
                    BeaTGroup.addSlot(fittedSlot);
                }
            } else if (missile && left && enforceSides && !noAim && !limitedAmmo) {
                LGroup.addSlot(fittedSlot);
            } else if (missile && right && enforceSides && !noAim && !limitedAmmo) {
                RGroup.addSlot(fittedSlot);
            } else if (missile && !limitedAmmo && !strike && !antiFighter && !pointDefense &&
                    (splitMissiles || hardpoint)) {
                SpamMisGroup.addSlot(fittedSlot);
            } else if (missile && (limitedAmmo || (hardpoint && !noAim)) && !strike && !pointDefense && !antiFighter) {
                MisGroup.addSlot(fittedSlot);
            } else if (missile && strike) {
                StrMisGroup.addSlot(fittedSlot);
            } else if (pointDefense && !verySignificantSize && (!hardpoint || noAim) && !limitedAmmo) {
                PDGroup.addSlot(fittedSlot);
            } else if (pdAlso && !significantSize && (!hardpoint || noAim) && !limitedAmmo) {
                PDGroup.addSlot(fittedSlot);
            } else if (antiFighter && !verySignificantSize) {
                AFGroup.addSlot(fittedSlot);
            } else if (left && (hardpoint || enforceSides) && (!lowFlux || missile)) {
                LGroup.addSlot(fittedSlot);
            } else if (right && (hardpoint || enforceSides) && (!lowFlux || missile)) {
                RGroup.addSlot(fittedSlot);
            } else if (assault && hardpoint && (!lowFlux || missile)) {
                AssHGroup.addSlot(fittedSlot);
            } else if (assault && !hardpoint && (!lowFlux || missile)) {
                AssTGroup.addSlot(fittedSlot);
            } else if (closeSupport && hardpoint && (!lowFlux || missile)) {
                CSHGroup.addSlot(fittedSlot);
            } else if (closeSupport && !hardpoint && (!lowFlux || missile)) {
                CSTGroup.addSlot(fittedSlot);
            } else if (strike || limitedAmmo) {
                if (!beam) {
                    if (hardpoint) {
                        StrHGroup.addSlot(fittedSlot);
                    } else {
                        StrTGroup.addSlot(fittedSlot);
                    }
                } else {
                    if (hardpoint) {
                        BeaHGroup.addSlot(fittedSlot);
                    } else {
                        BeaTGroup.addSlot(fittedSlot);
                    }
                }
            } else if (fireSupport || lowFlux || special) {
                SupGroup.addSlot(fittedSlot);
            } else {
                SupGroup.addSlot(fittedSlot); // This should always be at the end
            }
        }

        // Now we consolidate the general weapon groups into five final weapon groups
        // This is only done if the number of general weapon groups is greater than 5
        // If Alex ever increases the maximum number of weapon groups, this number can be changed
        int groupCount = 0;
        for (WeaponGroupSpec group : groupList) {
            if (!group.getSlots().isEmpty()) {
                groupCount++;
            }
        }

        // Combine groups that the AI can't handle separately
        if (!AssHGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!CSHGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : AssHGroup.getSlots()) {
                    CSHGroup.addSlot(toAdd);
                }
                AssHGroup = null;
            }
        } else {
            AssHGroup = null;
        }
        if (!HvyHGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!StrHGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : HvyHGroup.getSlots()) {
                    StrHGroup.addSlot(toAdd);
                }
                HvyHGroup = null;
            }
        } else {
            HvyHGroup = null;
        }

        // This section is the most difficult part of the algorithm and can make or break large ships like the Onslaught
        // Make sure you know what you are doing when adding logic here
        // Do not pass weapons into a group that may have already become null; NetBeans should warn you about this
        // Order of operations is extremely important
        if (!AFGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!PDGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : AFGroup.getSlots()) {
                    PDGroup.addSlot(toAdd);
                }
                AFGroup = null;
            }
        } else {
            AFGroup = null;
        }
        if (!splitMissiles) {
            if (!StrMisGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!MisGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : StrMisGroup.getSlots()) {
                        MisGroup.addSlot(toAdd);
                    }
                    StrMisGroup = null;
                }
            } else {
                StrMisGroup = null;
            }
        }
        if (!PDGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!SupGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : PDGroup.getSlots()) {
                    SupGroup.addSlot(toAdd);
                }
                PDGroup = null;
            }
        } else {
            PDGroup = null;
        }
        if (!splitMissiles) {
            if (!SpamMisGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!SupGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : SpamMisGroup.getSlots()) {
                        SupGroup.addSlot(toAdd);
                    }
                    SpamMisGroup = null;
                }
            } else {
                SpamMisGroup = null;
            }
        }
        if (!AssTGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!CSTGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : AssTGroup.getSlots()) {
                    CSTGroup.addSlot(toAdd);
                }
                AssTGroup = null;
            }
        } else {
            AssTGroup = null;
        }
        if (!splitBeams) {
            if (!BeaTGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!CSTGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : BeaTGroup.getSlots()) {
                        CSTGroup.addSlot(toAdd);
                    }
                    BeaTGroup = null;
                }
            } else {
                BeaTGroup = null;
            }
            if (!BeaHGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!CSHGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : BeaHGroup.getSlots()) {
                        CSHGroup.addSlot(toAdd);
                    }
                    BeaHGroup = null;
                }
            } else {
                BeaHGroup = null;
            }
        }
        if (!enforceSides) {
            if (!LGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!RGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : LGroup.getSlots()) {
                        RGroup.addSlot(toAdd);
                    }
                    LGroup = null;
                }
            } else {
                LGroup = null;
            }
        }
        if (!CSTGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!SupGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : CSTGroup.getSlots()) {
                    SupGroup.addSlot(toAdd);
                }
                CSTGroup = null;
            }
        } else {
            CSTGroup = null;
        }
        if (!HvyTGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!StrTGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : HvyTGroup.getSlots()) {
                    StrTGroup.addSlot(toAdd);
                }
                HvyTGroup = null;
            }
        } else {
            HvyTGroup = null;
        }
        if (splitMissiles && StrMisGroup != null) {
            if (!StrMisGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!MisGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : StrMisGroup.getSlots()) {
                        MisGroup.addSlot(toAdd);
                    }
                    StrMisGroup = null;
                }
            } else {
                StrMisGroup = null;
            }
        }
        if (splitMissiles && SpamMisGroup != null) {
            if (!SpamMisGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!SupGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : SpamMisGroup.getSlots()) {
                        SupGroup.addSlot(toAdd);
                    }
                    SpamMisGroup = null;
                }
            } else {
                SpamMisGroup = null;
            }
        }
        if (splitBeams && BeaTGroup != null) {
            if (!BeaTGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!StrHGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : BeaTGroup.getSlots()) {
                        StrHGroup.addSlot(toAdd);
                    }
                    BeaTGroup = null;
                }
            } else {
                BeaTGroup = null;
            }
        }
        if (splitBeams && BeaHGroup != null) {
            if (!BeaHGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!CSHGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : BeaHGroup.getSlots()) {
                        CSHGroup.addSlot(toAdd);
                    }
                    BeaHGroup = null;
                }
            } else {
                BeaHGroup = null;
            }
        }
        if (!StrTGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!StrHGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : StrTGroup.getSlots()) {
                    StrHGroup.addSlot(toAdd);
                }
                StrTGroup = null;
            }
        } else {
            StrTGroup = null;
        }
        if (enforceSides) {
            if (!CSHGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!SupGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : CSHGroup.getSlots()) {
                        SupGroup.addSlot(toAdd);
                    }
                    CSHGroup = null;
                }
            } else {
                CSHGroup = null;
            }
        } else {
            if (CSHGroup.getSlots().isEmpty()) {
                CSHGroup = null;
            }
        }
        if (enforceSides && LGroup != null) {
            if (LGroup.getSlots().isEmpty()) {
                LGroup = null;
            }
        }
        if (RGroup.getSlots().isEmpty()) {
            RGroup = null;
        }
        if (StrHGroup.getSlots().isEmpty()) {
            StrHGroup = null;
        }
        if (SupGroup.getSlots().isEmpty()) {
            SupGroup = null;
        }
        if (MisGroup.getSlots().isEmpty()) {
            MisGroup = null;
        }

        if (variant.getWeaponGroups().size() < 5) {
            for (int i = variant.getWeaponGroups().size(); i < 5; i++) {
                variant.addWeaponGroup(new WeaponGroupSpec());
            }
        }

        // I didn't make a loop for this because the order in which you put these functions determines their order on the ship
        int currentGroup = 0;
        int tries = 0;
        while (groupCount < 5 && tries < 5) {
            if (HvyHGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(HvyHGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (HvyTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(HvyTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (StrHGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(StrHGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (StrTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(StrTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (BeaHGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(BeaHGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (BeaTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(BeaTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (LGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(LGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (RGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(RGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (AssHGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(AssHGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (CSHGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(CSHGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (AssTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(AssTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (CSTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(CSTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (StrMisGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(StrMisGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (MisGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(MisGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (SpamMisGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(SpamMisGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (SupGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(SupGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (AFGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(AFGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (PDGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(PDGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            tries++;
        }

        if (HvyHGroup != null) {
            integrateGroup(HvyHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (HvyTGroup != null) {
            integrateGroup(HvyTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (StrHGroup != null) {
            integrateGroup(StrHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (StrTGroup != null) {
            integrateGroup(StrTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (BeaHGroup != null) {
            integrateGroup(BeaHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (BeaTGroup != null) {
            integrateGroup(BeaTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (LGroup != null) {
            integrateGroup(LGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (RGroup != null) {
            integrateGroup(RGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (AssHGroup != null) {
            integrateGroup(AssHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (CSHGroup != null) {
            integrateGroup(CSHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (AssTGroup != null) {
            integrateGroup(AssTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (CSTGroup != null) {
            integrateGroup(CSTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (StrMisGroup != null) {
            integrateGroup(StrMisGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (MisGroup != null) {
            integrateGroup(MisGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (SpamMisGroup != null) {
            integrateGroup(SpamMisGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (SupGroup != null) {
            integrateGroup(SupGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (AFGroup != null) {
            integrateGroup(AFGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (PDGroup != null) {
            integrateGroup(PDGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }

        return variant;
    }

    public static void init() throws IOException, JSONException {
        for (ModSpecAPI mod : Global.getSettings().getModManager().getEnabledModsCopy()) {
            JSONArray rows;
            try {
                rows = Global.getSettings().loadCSV("data/weapons/weapon_data.csv", mod.getId());
            } catch (RuntimeException e) {
                log.warn("Fail to load weapon_data.csv for " + mod.getId());
                continue;
            }
            loadWeaponData(rows);
        }

        /* Apparently doesn't work yet on vanilla :( */
        loadWeaponData(Global.getSettings().loadCSV("data/weapons/weapon_data.csv"));
    }

    public static void loadWeaponData(JSONArray rows) throws JSONException {
        for (int i = 0; i < rows.length(); i++) {
            JSONObject row = rows.getJSONObject(i);
            double dps = row.optDouble("damage/second", -1.0);
            double fps = row.optDouble("energy/second", -1.0);
            double burstSize = row.optDouble("burst size", -1.0);
            boolean beam = false;
            boolean burst = false;
            if (dps > 0.0 || fps > 0.0) {
                beam = true;
            }
            if (burstSize > 0.0) {
                burst = true;
            }

            if (beam && !burst) {
                log.info(row.getString("id") + " added as sustained beam");
                DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).add(row.getString("id"));
            }
            if (beam && burst) {
                log.info(row.getString("id") + " added as burst beam");
                DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).add(row.getString("id"));
            }
        }
    }

    private static void integrateGroup(WeaponGroupSpec source, WeaponGroupSpec destination) {
        for (String slot : source.getSlots()) {
            destination.addSlot(slot);
        }
        destination.setType(source.getType());
        destination.setAutofireOnByDefault(source.isAutofireOnByDefault());
    }

    private static WeaponGroupSpec splitGroup(WeaponGroupSpec spec, ShipVariantAPI variant) {
        if (DS_Defs.GROUPER_NO_SPLIT.contains(DS_Util.getNonDHullId(variant.getHullSpec()))) {
            return null;
        }

        Set<String> idSet = new LinkedHashSet<>(20);
        for (String slot : spec.getSlots()) {
            idSet.add(variant.getWeaponId(slot));
        }
        if (idSet.size() > 1) {
            int i = (int) Math.ceil(idSet.size() / 2f);
            Iterator<String> iter = idSet.iterator();
            while (iter.hasNext()) {
                iter.next();
                if (i > 0) {
                    i--;
                } else {
                    iter.remove();
                }
            }

            WeaponGroupSpec newSpec = new WeaponGroupSpec();
            newSpec.setType(spec.getType());
            newSpec.setAutofireOnByDefault(spec.isAutofireOnByDefault());

            iter = spec.getSlots().iterator();
            while (iter.hasNext()) {
                String slot = iter.next();
                if (idSet.contains(variant.getWeaponId(slot))) {
                    newSpec.addSlot(slot);
                    iter.remove();
                }
            }

            return newSpec;
        } else {
            return null;
        }
    }

    public enum WeaponGroupConfig {

        STANDARD, MISSILE, BROADSIDE, MISSILE_BROADSIDE, EXIGENCY, LINKED_STRIKE, MORPHEUS
    }

    public enum WeaponGroupParam {

        MISSILE, NO_AIM, ANTI_FIGHTER, POINT_DEFENSE, STRIKE, ASSAULT, CLOSE_SUPPORT, FIRE_SUPPORT, SPECIAL, LOW_FLUX,
        SEPARATE_FIRE, SUSTAINED_BEAM, BURST_BEAM, LIMITED_AMMO, INDIRECT_FIRE, OVERRIDE
    }
}
