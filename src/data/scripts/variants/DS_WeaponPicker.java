package data.scripts.variants;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Defs.AsymmetryLevel;
import data.scripts.util.DS_Defs.SlotType;
import data.scripts.util.DS_Defs.WeaponPreference;
import data.scripts.variants.DS_Database.RandomizerWeaponType;
import data.scripts.variants.DS_WeaponGrouper.WeaponGroupParam;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Logger;

public class DS_WeaponPicker {

    private static final Logger log = Global.getLogger(DS_WeaponPicker.class);

    public static String chooseWeapon(List<String> factionList, EnumMap<RandomizerWeaponType, Float> coefficients,
                                      List<Map<String, Float>> weaponDataList, String preferredWeapon, SlotType slotType,
                                      boolean front, boolean turret, WeaponPreference preference, WeaponSize size,
                                      WeaponType type, DamageType preferredDamageType, float qualityFactor,
                                      int remainingOP, int lastResortOP, float attack, float standoff, float alpha,
                                      float defense, float closeRange, float longRange, float disable,
                                      float budgetFactor, AsymmetryLevel asymmetric, boolean broadside, boolean station,
                                      boolean noobFriendly, Map<Integer, List<String>> weaponChoiceMap,
                                      MutableShipStatsAPI stats, Random r) {
        WeightedRandomPicker<String> randomizer = new WeightedRandomPicker<>(r);

        float slotOP = 0f;
        for (String faction : factionList) {
            slotOP += getOPTargetForSlot(faction, size, type, stats);
        }
        float targetOP = (slotOP / factionList.size()) * budgetFactor;

        for (Map<String, Float> weaponData : weaponDataList) {
            for (Map.Entry<String, Float> entry : weaponData.entrySet()) {
                String id = entry.getKey();
                float weight = calculateWeaponWeight(id, entry.getValue(), targetOP, factionList, coefficients,
                                                     preferredWeapon, slotType, front, turret, preference, size, type,
                                                     preferredDamageType, qualityFactor, remainingOP, lastResortOP,
                                                     attack, standoff, alpha, defense, closeRange, longRange, disable,
                                                     asymmetric, broadside, station, noobFriendly, stats);

                if (weight <= 0f) {
                    continue;
                }

                randomizer.add(id, weight);
            }
        }

        boolean sharingWeapons = false;
        for (String faction : factionList) {
            if (!DS_Defs.INJECTOR_NO_SHARING_MOUNTS.contains(faction)) {
                sharingWeapons = true;
                break;
            }
        }

        if (randomizer.isEmpty() && sharingWeapons) {
            List<String> factionListNew = new ArrayList<>(1);
            List<Map<String, Float>> weaponDataListNew = new ArrayList<>(1);
            switch (factionList.get(0)) {
                case "everything":
                    return null;
                case "sector": {
                    factionListNew.add("everything");
                    weaponDataListNew.add(DS_Database.factionWeapons.get("everything"));
                    return chooseWeapon(factionListNew, coefficients, weaponDataListNew, preferredWeapon, slotType,
                                        front, turret, preference, size, type, preferredDamageType, qualityFactor,
                                        remainingOP, lastResortOP, attack, standoff, alpha, defense, closeRange,
                                        longRange, disable, budgetFactor, asymmetric, broadside, station, noobFriendly,
                                        weaponChoiceMap, stats, r);
                }
                case "domain": {
                    factionListNew.add("sector");
                    weaponDataListNew.add(DS_Database.factionWeapons.get("sector"));
                    return chooseWeapon(factionListNew, coefficients, weaponDataListNew, preferredWeapon, slotType,
                                        front, turret, preference, size, type, preferredDamageType, qualityFactor,
                                        remainingOP, lastResortOP, attack, standoff, alpha, defense, closeRange,
                                        longRange, disable, budgetFactor, asymmetric, broadside, station, noobFriendly,
                                        weaponChoiceMap, stats, r);
                }
                default: {
                    factionListNew.add("domain");
                    weaponDataListNew.add(DS_Database.factionWeapons.get("domain"));
                    return chooseWeapon(factionListNew, coefficients, weaponDataListNew, preferredWeapon, slotType,
                                        front, turret, preference, size, type, preferredDamageType, qualityFactor,
                                        remainingOP, lastResortOP, attack, standoff, alpha, defense, closeRange,
                                        longRange, disable, budgetFactor, asymmetric, broadside, station, noobFriendly,
                                        weaponChoiceMap, stats, r);
                }
            }
        }

        String id = randomizer.pick();

        if (id == null) {
            return null;
        }

        DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(id);
        int roleCode = 0;
        if (weapon.attack) {
            roleCode |= 1;
        }
        if (weapon.standoff) {
            roleCode |= 1 << 1;
        }
        if (weapon.alpha) {
            roleCode |= 1 << 2;
        }
        if (weapon.defense) {
            roleCode |= 1 << 3;
        }
        if (weapon.disable) {
            roleCode |= 1 << 4;
        }
        if ((!weapon.attack && !weapon.standoff && !weapon.alpha && !weapon.defense) &&
                weapon.closeRange) {
            roleCode |= 1 << 5;
        }
        if (((!weapon.attack && !weapon.standoff && !weapon.alpha && !weapon.defense) ||
             weapon.type == WeaponType.MISSILE) &&
                weapon.longRange) {
            roleCode |= 1 << 6;
        }
        if (weapon.damageType == DamageType.HIGH_EXPLOSIVE) {
            roleCode |= 1 << 7;
        }
        if (weapon.damageType == DamageType.KINETIC) {
            roleCode |= 1 << 8;
        }
        if (weapon.damageType == DamageType.ENERGY || weapon.damageType == DamageType.FRAGMENTATION) {
            roleCode |= 1 << 9;
        }

        if (roleCode != 0 && !DS_Defs.INJECTOR_NO_MATCHING_WEAPONS.contains(factionList.get(0))) {
            List<String> existingIds = weaponChoiceMap.get(roleCode);
            if (existingIds == null) {
                existingIds = new ArrayList<>(3);
                existingIds.add(id);
                weaponChoiceMap.put(roleCode, existingIds);
            } else {
                if (preferredWeapon == null || !id.contentEquals(preferredWeapon)) {
weaponLoop:         for (String existingId : existingIds) {
                        DS_WeaponEntry existingWeapon = DS_Database.masterWeaponList.get(existingId);

                        float trueWeaponOP = getTrueOPCost(existingId, existingWeapon.OP, existingWeapon.type,
                                                           existingWeapon.size, stats);

                        if (trueWeaponOP > lastResortOP) {
                            continue;
                        }

                        switch (type) {
                            case BALLISTIC:
                                if (existingWeapon.size == weapon.size &&
                                        existingWeapon.type == WeaponType.BALLISTIC) {
                                    id = existingId;
                                    break weaponLoop;
                                }
                                break;
                            case ENERGY:
                                if (existingWeapon.size == weapon.size &&
                                        existingWeapon.type == WeaponType.ENERGY) {
                                    id = existingId;
                                    break weaponLoop;
                                }
                                break;
                            case MISSILE:
                                if (existingWeapon.size == weapon.size &&
                                        existingWeapon.type == WeaponType.MISSILE) {
                                    id = existingId;
                                    break weaponLoop;
                                }
                                break;
                            case COMPOSITE:
                                if (existingWeapon.size == weapon.size &&
                                        (existingWeapon.type == WeaponType.BALLISTIC ||
                                         existingWeapon.type == WeaponType.MISSILE)) {
                                    id = existingId;
                                    break weaponLoop;
                                }
                                break;
                            case HYBRID:
                                if (existingWeapon.size == weapon.size &&
                                        (existingWeapon.type == WeaponType.BALLISTIC ||
                                         existingWeapon.type == WeaponType.ENERGY)) {
                                    id = existingId;
                                    break weaponLoop;
                                }
                                break;
                            case SYNERGY:
                                if (existingWeapon.size == weapon.size &&
                                        (existingWeapon.type == WeaponType.ENERGY ||
                                         existingWeapon.type == WeaponType.MISSILE)) {
                                    id = existingId;
                                    break weaponLoop;
                                }
                                break;
                            case UNIVERSAL:
                                if (existingWeapon.size == weapon.size) {
                                    id = existingId;
                                    break weaponLoop;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            if (!existingIds.contains(id)) {
                existingIds.add(id);
            }
        }

        return id;
    }

    public static float getOPTargetForSlot(String faction, WeaponSize size, WeaponType type, MutableShipStatsAPI stats) {
        if (type == WeaponType.BUILT_IN || type == WeaponType.DECORATIVE || type == WeaponType.LAUNCH_BAY ||
                type == WeaponType.SYSTEM || type == WeaponType.STATION_MODULE) {
            return 0f;
        }

        Map<DS_Database.SlotType, Float> weaponAverages = DS_Database.factionWeaponAverages.get(faction);
        if (weaponAverages == null) {
            if (size == WeaponSize.SMALL) {
                if (stats != null) {
                    switch (type) {
                        default:
                        case BALLISTIC:
                            return stats.getDynamic().getValue(Stats.SMALL_BALLISTIC_MOD, 5f);
                        case MISSILE:
                            return stats.getDynamic().getValue(Stats.SMALL_MISSILE_MOD, 5f);
                        case ENERGY:
                            return stats.getDynamic().getValue(Stats.SMALL_ENERGY_MOD, 5f);
                    }
                } else {
                    return 5f;
                }
            } else if (size == WeaponSize.MEDIUM) {
                if (stats != null) {
                    switch (type) {
                        default:
                        case BALLISTIC:
                            return stats.getDynamic().getValue(Stats.MEDIUM_BALLISTIC_MOD, 10f);
                        case MISSILE:
                            return stats.getDynamic().getValue(Stats.MEDIUM_MISSILE_MOD, 10f);
                        case ENERGY:
                            return stats.getDynamic().getValue(Stats.MEDIUM_ENERGY_MOD, 10f);
                    }
                } else {
                    return 10f;
                }
            } else {
                if (stats != null) {
                    switch (type) {
                        default:
                        case BALLISTIC:
                            return stats.getDynamic().getValue(Stats.LARGE_BALLISTIC_MOD, 20f);
                        case MISSILE:
                            return stats.getDynamic().getValue(Stats.LARGE_MISSILE_MOD, 20f);
                        case ENERGY:
                            return stats.getDynamic().getValue(Stats.LARGE_ENERGY_MOD, 20f);
                    }
                } else {
                    return 20f;
                }
            }
        }

        DS_Database.SlotType slot = DS_Database.SlotType.getSlotType(size, type);
        if (slot == null) {
            return 0f;
        }
        Float op = weaponAverages.get(slot);
        if (op == null) {
            if (size == WeaponSize.SMALL) {
                if (stats != null) {
                    switch (type) {
                        default:
                        case BALLISTIC:
                            return stats.getDynamic().getValue(Stats.SMALL_BALLISTIC_MOD, 5f);
                        case MISSILE:
                            return stats.getDynamic().getValue(Stats.SMALL_MISSILE_MOD, 5f);
                        case ENERGY:
                            return stats.getDynamic().getValue(Stats.SMALL_ENERGY_MOD, 5f);
                    }
                } else {
                    return 5f;
                }
            } else if (size == WeaponSize.MEDIUM) {
                if (stats != null) {
                    switch (type) {
                        default:
                        case BALLISTIC:
                            return stats.getDynamic().getValue(Stats.MEDIUM_BALLISTIC_MOD, 10f);
                        case MISSILE:
                            return stats.getDynamic().getValue(Stats.MEDIUM_MISSILE_MOD, 10f);
                        case ENERGY:
                            return stats.getDynamic().getValue(Stats.MEDIUM_ENERGY_MOD, 10f);
                    }
                } else {
                    return 10f;
                }
            } else {
                if (stats != null) {
                    switch (type) {
                        default:
                        case BALLISTIC:
                            return stats.getDynamic().getValue(Stats.LARGE_BALLISTIC_MOD, 20f);
                        case MISSILE:
                            return stats.getDynamic().getValue(Stats.LARGE_MISSILE_MOD, 20f);
                        case ENERGY:
                            return stats.getDynamic().getValue(Stats.LARGE_ENERGY_MOD, 20f);
                    }
                } else {
                    return 20f;
                }
            }
        }

        if (stats != null) {
            if (size == WeaponSize.SMALL) {
                switch (type) {
                    default:
                    case BALLISTIC:
                        return stats.getDynamic().getValue(Stats.SMALL_BALLISTIC_MOD, op);
                    case MISSILE:
                        return stats.getDynamic().getValue(Stats.SMALL_MISSILE_MOD, op);
                    case ENERGY:
                        return stats.getDynamic().getValue(Stats.SMALL_ENERGY_MOD, op);
                }
            } else if (size == WeaponSize.MEDIUM) {
                switch (type) {
                    default:
                    case BALLISTIC:
                        return stats.getDynamic().getValue(Stats.MEDIUM_BALLISTIC_MOD, op);
                    case MISSILE:
                        return stats.getDynamic().getValue(Stats.MEDIUM_MISSILE_MOD, op);
                    case ENERGY:
                        return stats.getDynamic().getValue(Stats.MEDIUM_ENERGY_MOD, op);
                }
            } else {
                switch (type) {
                    default:
                    case BALLISTIC:
                        return stats.getDynamic().getValue(Stats.LARGE_BALLISTIC_MOD, op);
                    case MISSILE:
                        return stats.getDynamic().getValue(Stats.LARGE_MISSILE_MOD, op);
                    case ENERGY:
                        return stats.getDynamic().getValue(Stats.LARGE_ENERGY_MOD, op);
                }
            }
        }
        return op;
    }

    public static String pickRandomWeapon(String faction, Map<String, Float> weaponData, WeaponSize size,
                                          float qualityFactor, int maxTier, Random r) {
        WeightedRandomPicker<String> randomizer = new WeightedRandomPicker<>(r);

        for (Map.Entry<String, Float> entry : weaponData.entrySet()) {
            String id = entry.getKey();
            float weight = entry.getValue();
            DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(id);

            if (weapon.size != size) {
                continue;
            }

            if (weapon.tier > maxTier) {
                continue;
            }

            if (weapon.tier == 0) { // qF 0 = wt 3, qF 0.5 = wt 1, qf 1 = wt 0.33
                if (qualityFactor < 0.5f) {
                    weight *= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
                } else {
                    weight /= 1f + (qualityFactor - 0.5f) / 0.25f;
                }
            } else if (weapon.tier == 1) { // qF 0 = wt 2, qF 0.5 = wt 1, qf 1 = wt 0.5
                if (qualityFactor < 0.5f) {
                    weight *= 1f / (Math.max(qualityFactor, -0.45f) + 0.5f);
                } else {
                    weight /= 1f + (qualityFactor - 0.5f) / 0.5f;
                }
            } else if (weapon.tier == 2) { // qF 0 = wt 0.33, qF 0.5 = wt 1, qf 1 = wt 2
                if (qualityFactor < 0.5f) {
                    weight /= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
                } else {
                    weight *= 1f + (qualityFactor - 0.5f) / 0.5f;
                }
            } else if (weapon.tier == 3) { // qF 0 = wt 0.05, qF 0.5 = wt 1, qf 1 = wt 3
                if (qualityFactor < 0.5f) {
                    weight /= 1f / (Math.max(qualityFactor, 0.05f));
                } else {
                    weight *= 1f + (qualityFactor - 0.5f) / 0.25f;
                }
            }

            if (weight <= 0f) {
                continue;
            }

            randomizer.add(id, weight);
        }

        if (randomizer.isEmpty() && !faction.contentEquals("exigency") && !faction.contentEquals("templars")) {
            switch (faction) {
                case "everything":
                    return null;
                case "sector":
                    return pickRandomWeapon("everything", DS_Database.factionWeapons.get("everything"), size,
                                            qualityFactor, maxTier, r);
                case "domain":
                    return pickRandomWeapon("sector", DS_Database.factionWeapons.get("sector"), size, qualityFactor,
                                            maxTier, r);
                default:
                    return pickRandomWeapon("domain", DS_Database.factionWeapons.get("domain"), size, qualityFactor,
                                            maxTier, r);
            }
        }

        return randomizer.pick();
    }

    private static float calculateWeaponWeight(String id, float defaultWeight, float targetOP, List<String> factionList,
                                               EnumMap<RandomizerWeaponType, Float> coefficients, String preferredWeapon,
                                               SlotType slotType, boolean front, boolean turret,
                                               WeaponPreference preference, WeaponSize size, WeaponType type,
                                               DamageType preferredDamageType, float qualityFactor, int remainingOP,
                                               int lastResortOP, float attack, float standoff, float alpha,
                                               float defense, float closeRange, float longRange, float disable,
                                               AsymmetryLevel asymmetric, boolean broadside, boolean station,
                                               boolean noobFriendly, MutableShipStatsAPI stats) {
        float weight = defaultWeight;
        float opTarget = targetOP;
        DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(id);
        WeaponSpecAPI spec = Global.getSettings().getWeaponSpec(id);

        float trueWeaponOP = getTrueOPCost(id, weapon.OP, weapon.type, weapon.size, stats);
        float weaponCostFactor = trueWeaponOP / Math.max(weapon.OP, 1f);

        if (factionList.contains("SCY") && id.startsWith("SCY_")) {
            weight *= 4f;
        }

        if (factionList.contains("shadow_industry") && id.startsWith("ms_")) {
            weight *= 4f;
        }

        if (preference == WeaponPreference.BALLISTIC) {
            if (weapon.type == WeaponType.BALLISTIC) {
                weight *= 5f;
                opTarget *= 1.25f;
            }
        } else if (preference == WeaponPreference.ENERGY) {
            if (weapon.type == WeaponType.ENERGY) {
                weight *= 5f;
                opTarget *= 1.25f;
            }
        } else if (preference == WeaponPreference.MISSILE) {
            if (weapon.type == WeaponType.MISSILE) {
                weight *= 5f;
                opTarget *= 1.25f;
            }
        } else if (preference == WeaponPreference.STRIKE) {
            if (weapon.alpha) {
                weight *= 2f;
                opTarget *= 1.25f;
            }
        }

        if (type == WeaponType.UNIVERSAL) {
            if (weapon.size != size) {
                return -1f;
            }
        } else if (type == WeaponType.HYBRID) {
            if (weapon.size != size) {
                return -1f;
            }
            if (weapon.type != WeaponType.BALLISTIC && weapon.type != WeaponType.ENERGY && weapon.type !=
                    WeaponType.HYBRID) {
                return -1f;
            }
        } else if (type == WeaponType.SYNERGY) {
            if (weapon.size != size) {
                return -1f;
            }
            if (weapon.type != WeaponType.MISSILE && weapon.type != WeaponType.ENERGY && weapon.type !=
                    WeaponType.SYNERGY) {
                return -1f;
            }
        } else if (type == WeaponType.COMPOSITE) {
            if (weapon.size != size) {
                return -1f;
            }
            if (weapon.type != WeaponType.BALLISTIC && weapon.type != WeaponType.MISSILE && weapon.type !=
                    WeaponType.COMPOSITE) {
                return -1f;
            }
        } else {
            if (weapon.type != type) {
                return -1f;
            }
            if (weapon.size == WeaponSize.MEDIUM && size == WeaponSize.LARGE) {
                weight *= 0.05f * weaponCostFactor * weaponCostFactor;
            } else if (weapon.size == WeaponSize.SMALL && size == WeaponSize.MEDIUM) {
                weight *= 0.05f * weaponCostFactor * weaponCostFactor;
            } else if (weapon.size != size) {
                return -1f;
            }
        }

        float diff = opTarget - trueWeaponOP;
        if (size == WeaponSize.SMALL) {
            diff *= 1.5f;
        } else if (size == WeaponSize.LARGE) {
            diff /= 1.5f;
        }
        if (diff < 0f) {
            weight *= Math.exp(-diff * diff / 10f);
        } else {
            if (diff < 5f) {
                weight *= Math.exp(-diff * diff / 20f);
            } else if (diff < 10f) {
                diff = Math.min(diff, 10f);
                weight *= Math.exp(-diff * diff / 40f - 0.625f);
            } else {
                diff = Math.min(diff, 10f);
                weight *= Math.exp(-diff * diff / 80f - 1.875f);
            }
        }

        if (trueWeaponOP > remainingOP) {
            if (trueWeaponOP <= lastResortOP && preferredWeapon != null && id.contentEquals(preferredWeapon)) {
                weight *= 0.001f;
            } else {
                return -1f;
            }
        }

        if (preferredWeapon != null && id.contentEquals(preferredWeapon)) {
            if ((asymmetric == AsymmetryLevel.FULL) ||
                    ((weapon.size == WeaponSize.LARGE) && (asymmetric == AsymmetryLevel.LARGE)) ||
                    ((weapon.size == WeaponSize.LARGE) || (weapon.size == WeaponSize.MEDIUM) &&
                     (asymmetric == AsymmetryLevel.MEDIUM))) {
                weight *= 3f;
            } else {
                weight *= 200000f;
            }
        }

        if (noobFriendly) {
            weight /= weapon.leetness;
        }

        if (weapon.tier == 0) { // qF 0 = wt 3, qF 0.5 = wt 1, qf 1 = wt 0.33
            if (qualityFactor < 0.5f) {
                weight *= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
            } else {
                weight /= 1f + (qualityFactor - 0.5f) / 0.25f;
            }
        } else if (weapon.tier == 1) { // qF 0 = wt 2, qF 0.5 = wt 1, qf 1 = wt 0.5
            if (qualityFactor < 0.5f) {
                weight *= 1f / (Math.max(qualityFactor, -0.45f) + 0.5f);
            } else {
                weight /= 1f + (qualityFactor - 0.5f) / 0.5f;
            }
        } else if (weapon.tier == 2) { // qF 0 = wt 0.33, qF 0.5 = wt 1, qf 1 = wt 2
            if (qualityFactor < 0.5f) {
                weight /= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
            } else {
                weight *= 1f + (qualityFactor - 0.5f) / 0.5f;
            }
        } else if (weapon.tier == 3) { // qF 0 = wt 0.05, qF 0.5 = wt 0.5, qf 1 = wt 3
            if (qualityFactor < 0.5f) {
                weight /= 1f / (Math.max(qualityFactor, 0.05f));
            } else {
                weight *= 0.5f + (qualityFactor - 0.5f) / 0.2f;
            }
        } else if (weapon.tier == 4) { // qF 0 = wt 0.033, qF 0.5 = wt 0.33, qf 1 = wt 4
            if (qualityFactor < 0.5f) {
                weight /= 1.5f / (Math.max(qualityFactor, 0.05f));
            } else {
                weight *= 0.33f + (qualityFactor - 0.5f) / 0.136f;
            }
        }

        if (preferredDamageType != null) {
            if (weapon.damageType == preferredDamageType) {
                if (weapon.type == WeaponType.BALLISTIC) {
                    weight *= 2f;
                } else if (weapon.type == WeaponType.MISSILE) {
                    weight *= 1.5f;
                } else if (weapon.type == WeaponType.ENERGY) {
                    weight *= 1.25f;
                }
            }
        }

        float boost = 1f;
        if (weapon.attack) {
            boost += attack / boost;
        }
        if (weapon.standoff) {
            boost += standoff / boost;
        }
        if (weapon.alpha) {
            boost += alpha / boost;
        }
        if (weapon.defense) {
            boost += defense / boost;
        }
        if (weapon.longRange) {
            boost += longRange / boost;
        }
        if (weapon.closeRange) {
            boost += closeRange / boost;
        }
        if (weapon.disable) {
            boost += disable / boost;
        }
        boost -= 1f;
        weight *= boost;

        if (station && !turret) {
            if (spec != null && (spec.getAIHints().contains(WeaponAPI.AIHints.GUIDED_POOR) ||
                                 spec.getAIHints().contains(WeaponAPI.AIHints.DO_NOT_AIM) ||
                                 spec.getAIHints().contains(WeaponAPI.AIHints.HEATSEEKER))) {
                weight *= 1f;
            } else {
                weight *= 0f;
            }
        }

        if (slotType == SlotType.FRONT) {
            if (weapon.defense) {
                weight *= 0.67f;
            }
            if (broadside && !station) {
                if (weapon.longRange && weapon.type != WeaponType.MISSILE) {
                    weight *= 0.25f;
                }
            }
        } else if (slotType == SlotType.SIDE) {
            if (!broadside || station) {
                if (weapon.standoff) {
                    weight *= 0.75f;
                }
                if (weapon.alpha) {
                    weight *= 0.5f;
                }
                if (weapon.defense) {
                    weight *= 1.5f;
                }
                if (weapon.closeRange) {
                    weight *= 0.5f;
                }
            } else {
                if (weapon.alpha) {
                    weight *= 0.75f;
                }
                if (weapon.closeRange) {
                    weight *= 0.75f;
                }
                if (weapon.longRange && weapon.type != WeaponType.MISSILE) {
                    weight *= 1.5f;
                }
            }
        } else if (slotType == SlotType.BACK && !station) {
            if (weapon.attack) {
                weight *= 0.5f;
            }
            if (weapon.standoff) {
                weight *= 0.75f;
            }
            if (weapon.alpha) {
                weight *= 0.5f;
            }
            if (weapon.defense) {
                weight *= 2f;
            }
            if (weapon.closeRange) {
                weight *= 0.25f;
            }
        } else if (slotType == SlotType.HEMI) {
            if (weapon.attack) {
                weight *= 0.75f;
            }
            if (weapon.alpha) {
                weight *= 0.5f;
            }
            if (weapon.standoff) {
                weight *= 1.25f;
            }
            if (weapon.longRange) {
                weight *= 1.25f;
            }
            if (weapon.defense) {
                weight *= 1.5f;
            }
            if (weapon.type == WeaponType.MISSILE) {
                weight *= 0.5f;
            }
            if (broadside && !station) {
                if (weapon.longRange && weapon.type != WeaponType.MISSILE) {
                    weight *= 0.5f;
                }
            }
        } else if (slotType == SlotType.WIDE) {
            if (weapon.defense) {
                weight *= 1.5f;
            }
            if (weapon.type == WeaponType.MISSILE) {
                weight *= 0.25f;
            }
            if (broadside && !station) {
                if (weapon.longRange && weapon.type != WeaponType.MISSILE) {
                    weight *= 0.75f;
                }
            }
        } else if (slotType == SlotType.BORESIGHT && !station) {
            if (!broadside || station) {
                if (weapon.alpha) {
                    weight *= 2f;
                }
                if (weapon.defense && weapon.type != WeaponType.MISSILE) {
                    weight *= 0.33f;
                }
                if (weapon.closeRange) {
                    weight *= 0.75f;
                }
                if (weapon.longRange) {
                    weight *= 1.33f;
                }
                if (weapon.type == WeaponType.MISSILE) {
                    weight *= 1.5f;
                }
            } else {
                if (weapon.defense && weapon.type != WeaponType.MISSILE) {
                    weight *= 0.5f;
                }
                if (weapon.type == WeaponType.MISSILE) {
                    weight *= 1.5f;
                }
                if (weapon.longRange && weapon.type != WeaponType.MISSILE) {
                    weight *= 0.25f;
                }
            }
        } else if (slotType == SlotType.CROOKED) {
            if (weapon.closeRange) {
                weight *= 0.5f;
            }
            if (weapon.longRange) {
                weight *= 2f;
            }
            if (weapon.type != WeaponType.MISSILE) {
                weight *= 0.25f;
            }
        }

        if (!broadside) {
            if (!front && weapon.frontOnly) {
                weight *= 0f;
            }
        } else {
            if (!front && weapon.frontOnly &&
                    (slotType != SlotType.HEMI) && (slotType != SlotType.WIDE) &&
                    (slotType != SlotType.SIDE) && (slotType != SlotType.CROOKED)) {
                weight *= 0f;
            }
        }
        if (!turret && weapon.turretOnly) {
            weight *= 0f;
        }

        if (coefficients != null) {
            if (weapon.alpha) {
                weight *= coefficients.get(RandomizerWeaponType.ALPHA);
            }
            if (weapon.attack) {
                weight *= coefficients.get(RandomizerWeaponType.ATTACK);
            }
            if (weapon.defense) {
                weight *= coefficients.get(RandomizerWeaponType.DEFENSE);
            }
            if (weapon.disable) {
                weight *= coefficients.get(RandomizerWeaponType.DISABLE);
            }
            if (weapon.standoff) {
                weight *= coefficients.get(RandomizerWeaponType.STANDOFF);
            }
            if (weapon.closeRange) {
                weight *= coefficients.get(RandomizerWeaponType.CLOSE_RANGE);
            }
            if (weapon.longRange) {
                weight *= coefficients.get(RandomizerWeaponType.LONG_RANGE);
            }
        }

        return weight;
    }

    private static float getTrueOPCost(String id, int baseOP, WeaponType type, WeaponSize size,
                                       MutableShipStatsAPI stats) {
        float trueWeaponOP = baseOP;
        if (stats != null) {
            switch (type) {
                default:
                case BALLISTIC:
                    switch (size) {
                        default:
                        case SMALL:
                            trueWeaponOP = stats.getDynamic().getValue(Stats.SMALL_BALLISTIC_MOD,
                                                                       trueWeaponOP);
                            break;
                        case MEDIUM:
                            trueWeaponOP = stats.getDynamic().getValue(Stats.MEDIUM_BALLISTIC_MOD,
                                                                       trueWeaponOP);
                            break;
                        case LARGE:
                            trueWeaponOP = stats.getDynamic().getValue(Stats.LARGE_BALLISTIC_MOD,
                                                                       trueWeaponOP);
                            break;
                    }
                    break;
                case MISSILE:
                    switch (size) {
                        default:
                        case SMALL:
                            trueWeaponOP = stats.getDynamic().getValue(Stats.SMALL_MISSILE_MOD,
                                                                       trueWeaponOP);
                            break;
                        case MEDIUM:
                            trueWeaponOP = stats.getDynamic().getValue(Stats.MEDIUM_MISSILE_MOD,
                                                                       trueWeaponOP);
                            break;
                        case LARGE:
                            trueWeaponOP = stats.getDynamic().getValue(Stats.LARGE_MISSILE_MOD,
                                                                       trueWeaponOP);
                            break;
                    }
                    break;
                case ENERGY:
                    switch (size) {
                        default:
                        case SMALL:
                            trueWeaponOP = stats.getDynamic().getValue(Stats.SMALL_MISSILE_MOD,
                                                                       trueWeaponOP);
                            break;
                        case MEDIUM:
                            trueWeaponOP = stats.getDynamic().getValue(Stats.MEDIUM_MISSILE_MOD,
                                                                       trueWeaponOP);
                            break;
                        case LARGE:
                            trueWeaponOP = stats.getDynamic().getValue(Stats.LARGE_MISSILE_MOD,
                                                                       trueWeaponOP);
                            break;
                    }
                    break;
            }
            WeaponSpecAPI spec = Global.getSettings().getWeaponSpec(id);
            if (spec != null && (spec.getAIHints().contains(AIHints.PD) ||
                                 spec.getAIHints().contains(AIHints.PD_ALSO) ||
                                 spec.getAIHints().contains(AIHints.PD_ONLY))) {
                switch (size) {
                    default:
                    case SMALL:
                        trueWeaponOP = stats.getDynamic().getValue(Stats.SMALL_PD_MOD,
                                                                   trueWeaponOP);
                        break;
                    case MEDIUM:
                        trueWeaponOP = stats.getDynamic().getValue(Stats.MEDIUM_PD_MOD,
                                                                   trueWeaponOP);
                        break;
                    case LARGE:
                        trueWeaponOP = stats.getDynamic().getValue(Stats.LARGE_PD_MOD,
                                                                   trueWeaponOP);
                        break;
                }
            }
            if (DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.SUSTAINED_BEAM).contains(id) ||
                    DS_Defs.GROUPER_TYPE_OVERRIDE.get(WeaponGroupParam.BURST_BEAM).contains(id)) {
                switch (size) {
                    default:
                    case SMALL:
                        trueWeaponOP = stats.getDynamic().getValue(Stats.SMALL_BEAM_MOD,
                                                                   trueWeaponOP);
                        break;
                    case MEDIUM:
                        trueWeaponOP = stats.getDynamic().getValue(Stats.MEDIUM_BEAM_MOD,
                                                                   trueWeaponOP);
                        break;
                    case LARGE:
                        trueWeaponOP = stats.getDynamic().getValue(Stats.LARGE_BEAM_MOD,
                                                                   trueWeaponOP);
                        break;
                }
            }
        }
        return trueWeaponOP;
    }
}
